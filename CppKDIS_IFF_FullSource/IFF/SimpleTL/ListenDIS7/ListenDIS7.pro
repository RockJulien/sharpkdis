TEMPLATE = app
CONFIG *= shared console

QT += network

DEFINES += DIS_VERSION=7

HEADERS       = inc\ListenDIS.h
SOURCES       = src/ListenDIS.cpp \
                src/main.cpp

INCLUDEPATH += ./inc ../../Delivery/win-msvc2015-x86/include

CONFIG(debug, debug|release) {
    LIBS += ../../Delivery/win-msvc2015-x86/lib/kdis7d.lib
} else {
    LIBS += ../../Delivery/win-msvc2015-x86/lib/kdis7.lib
}
