#include "callbacks.h"

// PDU
#include "KDIS/PDU/Distributed_Emission_Regeneration/Electromagnetic_Emission_PDU.h"
#include "KDIS/PDU/Distributed_Emission_Regeneration/Designator_PDU.h"
#include "KDIS/PDU/Distributed_Emission_Regeneration/IFF_PDU.h"
#include "KDIS/PDU/Distributed_Emission_Regeneration/Underwater_Acoustic_PDU.h"
#include "KDIS/PDU/Entity_Info_Interaction/Entity_State_PDU.h"
#include <KDIS/PDU/Entity_Management/Transfer_Control_Request_PDU.h>
#include "KDIS/PDU/Radio_Communications/Transmitter_PDU.h"
#include "KDIS/PDU/Radio_Communications/Signal_PDU.h"
#include <KDIS/PDU/Simulation_Management/Acknowledge_PDU.h>
#include <KDIS/PDU/Simulation_Management/Comment_PDU.h>
#include <KDIS/PDU/Simulation_Management/Data_Query_PDU.h>
#include <KDIS/PDU/Simulation_Management/Event_Report_PDU.h>
#include <KDIS/PDU/Simulation_Management/Stop_Freeze_PDU.h>
#include <KDIS/PDU/Simulation_Management/Start_Resume_PDU.h>
#include <KDIS/PDU/Simulation_Management_With_Reliability/Acknowledge_R_PDU.h>
#include <KDIS/PDU/Simulation_Management_With_Reliability/Record_R_PDU.h>
#include <KDIS/PDU/Simulation_Management_With_Reliability/Set_Record_R_PDU.h>
#include <KDIS/PDU/Warfare/Detonation_PDU.h>
#include <KDIS/PDU/Warfare/Fire_PDU.h>

// Custom PDU
#include "StopApplicationReflecter.h"
#include "StartApplicationReflecter.h"
#include "UninstallReflecter.h"

#include "StopApplicationClass.h"
#include "StartApplicationClass.h"
#include "UninstallClass.h"

#include "PDUsSubscriber.h"

#include "ListenDIS.h"

ListenDIS * extListenDIS = nullptr;



void initCallbacks (sith::dis::PDUsSubscriber * subscriber, ListenDIS * listenDIS)
{
    if (subscriber != nullptr && listenDIS != nullptr)
    {
        // Custom
        sith::dis::StopApplicationReflecter::init ();
        sith::dis::StartApplicationReflecter::init ();
        sith::dis::UninstallReflecter::init ();

        subscriber->addCallbackCustom ((KDIS::DATA_TYPE::ENUMS::DatumID)sith::dis::STARTAPPLICATION_ID, &StopApplicationCb,  subscriber);
        subscriber->addCallbackCustom ((KDIS::DATA_TYPE::ENUMS::DatumID)sith::dis::STOPAPPLICATION_ID,  &StartApplicationCb, subscriber);
        subscriber->addCallbackCustom ((KDIS::DATA_TYPE::ENUMS::DatumID)sith::dis::UNINSTALL_ID,        &UninstallCb,        subscriber);
        
        // KDIS common 
        subscriber->addCallbackCommon (KDIS::DATA_TYPE::ENUMS::Fire_PDU_Type,                     &FireCb,                    subscriber);
        subscriber->addCallbackCommon (KDIS::DATA_TYPE::ENUMS::Detonation_PDU_Type,               &DetonationCb,              subscriber);
        subscriber->addCallbackCommon (KDIS::DATA_TYPE::ENUMS::Start_Resume_PDU_Type,             &StartResumeCb,             subscriber);
        subscriber->addCallbackCommon (KDIS::DATA_TYPE::ENUMS::Stop_Freeze_PDU_Type,              &StopFreezeCb,              subscriber);
        subscriber->addCallbackCommon (KDIS::DATA_TYPE::ENUMS::Entity_State_PDU_Type,             &EntitiesCb,                subscriber);
        subscriber->addCallbackCommon (KDIS::DATA_TYPE::ENUMS::Electromagnetic_Emission_PDU_Type, &ElectromagneticEmissionCb, subscriber);
        subscriber->addCallbackCommon (KDIS::DATA_TYPE::ENUMS::Designator_PDU_Type,               &DesignatorCb,              subscriber);
        subscriber->addCallbackCommon (KDIS::DATA_TYPE::ENUMS::IFF_ATC_NAVAIDS_PDU_Type,          &IFFCb,                     subscriber);
        subscriber->addCallbackCommon (KDIS::DATA_TYPE::ENUMS::UnderwaterAcoustic_PDU_Type,       &underWaterAcousticCb,      subscriber);
        subscriber->addCallbackCommon (KDIS::DATA_TYPE::ENUMS::Transmitter_PDU_Type,              &transmitterCb,             subscriber);
        subscriber->addCallbackCommon (KDIS::DATA_TYPE::ENUMS::Signal_PDU_Type,                   &signalCb,                  subscriber);
        subscriber->addCallbackCommon (KDIS::DATA_TYPE::ENUMS::Acknowledge_PDU_Type,              &acknowledgeCb,             subscriber);
        subscriber->addCallbackCommon (KDIS::DATA_TYPE::ENUMS::Data_Query_PDU_Type,               &dataQueryCb,               subscriber);
        subscriber->addCallbackCommon (KDIS::DATA_TYPE::ENUMS::Remove_Entity_PDU_Type,            &removeEntityCb,            subscriber);
        subscriber->addCallbackCommon (KDIS::DATA_TYPE::ENUMS::Acknowledge_R_PDU_Type,            &acknowledgeRCb,            subscriber);
        subscriber->addCallbackCommon (KDIS::DATA_TYPE::ENUMS::TransferControl_PDU_Type,          &transfertControlCb,        subscriber);
        subscriber->addCallbackCommon (KDIS::DATA_TYPE::ENUMS::Event_Report_PDU_Type,             &eventReportCb,             subscriber);
        subscriber->addCallbackCommon (KDIS::DATA_TYPE::ENUMS::Record_R_PDU_Type,                 &recordRCb,                 subscriber);
        subscriber->addCallbackCommon (KDIS::DATA_TYPE::ENUMS::SetRecord_R_PDU_Type,              &setRecordRCb,              subscriber);

        extListenDIS = listenDIS;
    }
}


void StopApplicationCb (KDIS::DATA_TYPE::VariableDatum * datum, void * data)
{
    sith::dis::StopApplicationClass * specific = dynamic_cast <sith::dis::StopApplicationClass *> (datum);
    QString senderIP = QString::fromStdString (((sith::dis::PDUsSubscriber *)data)->getSenderIP ());
    if (specific)
    {
        if (specific->getNameApplicationLength () > 0)
        {
            extListenDIS->addMessage (sith::dis::STOPAPPLICATION_ID, QString ().sprintf ("in StopApplication callback : name = <%s>", specific->getNameApplication ()), senderIP);
        }
        else
        {
            extListenDIS->addMessage (sith::dis::STOPAPPLICATION_ID, QString ().sprintf ("in StopApplication callback : name = <null>"), senderIP);
        }
    }
}


void StartApplicationCb (KDIS::DATA_TYPE::VariableDatum * datum, void * data)
{
    sith::dis::StartApplicationClass * specific = dynamic_cast <sith::dis::StartApplicationClass *> (datum);
    QString senderIP = QString::fromStdString (((sith::dis::PDUsSubscriber *)data)->getSenderIP ());
    if (specific)
    {
        if (specific->getNameApplicationLength () > 0)
        {
            extListenDIS->addMessage (sith::dis::STARTAPPLICATION_ID, QString ().sprintf ("in StartApplication callback : name = <%s>", specific->getNameApplication ()), senderIP);
        }
        else
        {
            extListenDIS->addMessage (sith::dis::STARTAPPLICATION_ID, QString ().sprintf ("in StartApplication callback : name = <null>"), senderIP);
        }
    }
}


void UninstallCb (KDIS::DATA_TYPE::VariableDatum * datum, void * data)
{
    sith::dis::UninstallClass * specific = dynamic_cast <sith::dis::UninstallClass *> (datum);
    QString senderIP = QString::fromStdString (((sith::dis::PDUsSubscriber *)data)->getSenderIP ());
    if (specific)
    {
        extListenDIS->addMessage (sith::dis::UNINSTALL_ID, QString ().sprintf ("in Uninstall callback : name = <null>"), senderIP);
    }
}


void FireCb (KDIS::PDU::Header * pdu, void * data)
{
    KDIS::PDU::Fire_PDU * specific = dynamic_cast <KDIS::PDU::Fire_PDU *> (pdu);
    if (specific == nullptr)
    {
        return;
    }

    if (extListenDIS != nullptr && extListenDIS->isFiltered (specific->GetFiringEntityID ()))
    {
        return;
    }

    QString senderIP = QString::fromStdString (((sith::dis::PDUsSubscriber *)data)->getSenderIP ());
    QString message = QString ("Fire updated from ") + senderIP + QString (" : \n");
    message += specific->GetAsString ().c_str ();
    message += "\n\n";
    extListenDIS->addMessage (KDIS::DATA_TYPE::ENUMS::Fire_PDU_Type, message, pdu, senderIP);
}


void DetonationCb (KDIS::PDU::Header * pdu, void * data)
{
    KDIS::PDU::Detonation_PDU * specific = dynamic_cast <KDIS::PDU::Detonation_PDU *> (pdu);
    if (specific == nullptr)
    {
        return;
    }

    if (extListenDIS != nullptr && extListenDIS->isFiltered (specific->GetFiringEntityID ()))
    {
        return;
    }

    QString senderIP = QString::fromStdString (((sith::dis::PDUsSubscriber *)data)->getSenderIP ());
    QString message = QString ("Detonation updated from ") + senderIP + QString (" : \n");
    message += specific->GetAsString ().c_str ();
    message += "\n\n";
    extListenDIS->addMessage (KDIS::DATA_TYPE::ENUMS::Detonation_PDU_Type, message, pdu, senderIP);
}


void StartResumeCb (KDIS::PDU::Header * pdu, void * data)
{
    KDIS::PDU::Start_Resume_PDU * specific = dynamic_cast <KDIS::PDU::Start_Resume_PDU *> (pdu);
    if (specific == nullptr)
    {
        return;
    }

    QString senderIP = QString::fromStdString (((sith::dis::PDUsSubscriber *)data)->getSenderIP ());
    QString message = QString ("Start_Resume updated from ") + senderIP + QString (" : \n");
    message += specific->GetAsString ().c_str ();
    message += "\n\n";
    extListenDIS->addMessage (KDIS::DATA_TYPE::ENUMS::Start_Resume_PDU_Type, message, pdu, senderIP);
}


void StopFreezeCb (KDIS::PDU::Header * pdu, void * data)
{
    KDIS::PDU::Stop_Freeze_PDU * specific = dynamic_cast <KDIS::PDU::Stop_Freeze_PDU *> (pdu);
    if (specific == nullptr)
    {
        return;
    }

    QString senderIP = QString::fromStdString (((sith::dis::PDUsSubscriber *)data)->getSenderIP ());
    QString message = QString ("Stop_Freeze updated from ") + senderIP + QString (" : \n");
    message += specific->GetAsString ().c_str ();
    message += "\n\n";
    extListenDIS->addMessage (KDIS::DATA_TYPE::ENUMS::Stop_Freeze_PDU_Type, message, pdu, senderIP);
    extListenDIS->testExiting (specific->GetReason ());
}


void EntitiesCb (KDIS::PDU::Header * pdu, void * data)
{
    KDIS::PDU::Entity_State_PDU * specific = dynamic_cast <KDIS::PDU::Entity_State_PDU *> (pdu);
    if (specific == nullptr)
    {
        return;
    }

    if (extListenDIS != nullptr && extListenDIS->isFiltered (specific->GetEntityIdentifier ()))
    {
        return;
    }

    QString senderIP = QString::fromStdString (((sith::dis::PDUsSubscriber *)data)->getSenderIP ());
    QString message = QString ("Entity updated from ") + senderIP + QString (" : \n");
    message += specific->GetAsString ().c_str ();
    message += "\n\n";
    extListenDIS->addMessage (KDIS::DATA_TYPE::ENUMS::Entity_State_PDU_Type, message, pdu, senderIP);
}


void ElectromagneticEmissionCb (KDIS::PDU::Header * pdu, void * data)
{
    KDIS::PDU::Electromagnetic_Emission_PDU * specific = dynamic_cast <KDIS::PDU::Electromagnetic_Emission_PDU *> (pdu);
    if (specific == nullptr)
    {
        return;
    }

    if (extListenDIS != nullptr && extListenDIS->isFiltered (specific->GetEmittingEntityID ()))
    {
        return;
    }

    QString senderIP = QString::fromStdString (((sith::dis::PDUsSubscriber *)data)->getSenderIP ());
    QString message = QString ("ElectromagneticEmission updated from ") + senderIP + QString (" : \n");
    message += specific->GetAsString ().c_str ();
    message += "\n\n";
    extListenDIS->addMessage (KDIS::DATA_TYPE::ENUMS::Electromagnetic_Emission_PDU_Type, message, pdu, senderIP);
}



void DesignatorCb (KDIS::PDU::Header * pdu, void * data)
{
    KDIS::PDU::Designator_PDU * specific = dynamic_cast <KDIS::PDU::Designator_PDU *> (pdu);
    if (specific == nullptr)
    {
        return;
    }

    if (extListenDIS != nullptr && extListenDIS->isFiltered (specific->GetDesignatingEntityID ()))
    {
        return;
    }

    QString senderIP = QString::fromStdString (((sith::dis::PDUsSubscriber *)data)->getSenderIP ());
    QString message = QString ("Designator updated from ") + senderIP + QString (" : \n");
    message += specific->GetAsString ().c_str ();
    message += "\n\n";
    extListenDIS->addMessage (KDIS::DATA_TYPE::ENUMS::Designator_PDU_Type, message, pdu, senderIP);
}



void IFFCb (KDIS::PDU::Header * pdu, void * data)
{
    KDIS::PDU::IFF_PDU * specific = dynamic_cast <KDIS::PDU::IFF_PDU *> (pdu);
    if (specific == nullptr)
    {
        return;
    }

    if (extListenDIS != nullptr && extListenDIS->isFiltered (specific->GetEmittingEntityID ()))
    {
        return;
    }

    QString senderIP = QString::fromStdString (((sith::dis::PDUsSubscriber *)data)->getSenderIP ());
    QString message = QString ("IFF updated from ") + senderIP + QString (" : \n");
    message += specific->GetAsString ().c_str ();
    message += "\n\n";
    extListenDIS->addMessage (KDIS::DATA_TYPE::ENUMS::IFF_ATC_NAVAIDS_PDU_Type, message, pdu, senderIP);
}


void underWaterAcousticCb (KDIS::PDU::Header * pdu, void * data)
{
    KDIS::PDU::Underwater_Acoustic_PDU * specific = dynamic_cast <KDIS::PDU::Underwater_Acoustic_PDU *> (pdu);
    if (specific == nullptr)
    {
        return;
    }

    if (extListenDIS != nullptr && extListenDIS->isFiltered (specific->GetEmittingEntityID ()))
    {
        return;
    }

    QString senderIP = QString::fromStdString (((sith::dis::PDUsSubscriber *)data)->getSenderIP ());
    QString message = QString ("Underwater Acoustic updated from ") + senderIP + QString (" : \n");
    message += specific->GetAsString ().c_str ();
    message += "\n\n";
    extListenDIS->addMessage (KDIS::DATA_TYPE::ENUMS::UnderwaterAcoustic_PDU_Type, message, pdu, senderIP);
}


void transmitterCb (KDIS::PDU::Header * pdu, void * data)
{
    KDIS::PDU::Transmitter_PDU * specific = dynamic_cast <KDIS::PDU::Transmitter_PDU *> (pdu);
    if (specific == nullptr)
    {
        return;
    }

    if (extListenDIS != nullptr && extListenDIS->isFiltered (specific->GetEntityID ()))
    {
        return;
    }

    QString senderIP = QString::fromStdString (((sith::dis::PDUsSubscriber *)data)->getSenderIP ());
    QString message = QString ("Transmitter updated from ") + senderIP + QString (" : \n");
    message += specific->GetAsString ().c_str ();
    message += "\n\n";
    extListenDIS->addMessage (KDIS::DATA_TYPE::ENUMS::Transmitter_PDU_Type, message, pdu, senderIP);
}


void signalCb (KDIS::PDU::Header * pdu, void * data)
{
    KDIS::PDU::Signal_PDU * specific = dynamic_cast <KDIS::PDU::Signal_PDU *> (pdu);
    if (specific == nullptr)
    {
        return;
    }

    if (extListenDIS != nullptr && extListenDIS->isFiltered (specific->GetEntityID ()))
    {
        return;
    }

    QString senderIP = QString::fromStdString (((sith::dis::PDUsSubscriber *)data)->getSenderIP ());
    QString message = QString ("Signal updated from ") + senderIP + QString (" : \n");
    message += specific->GetAsString ().c_str ();
    message += "\n\n";
    extListenDIS->addMessage (KDIS::DATA_TYPE::ENUMS::Signal_PDU_Type, message, pdu, senderIP);
}


void acknowledgeCb (KDIS::PDU::Header * pdu, void * data)
{
    KDIS::PDU::Acknowledge_PDU * specific = dynamic_cast <KDIS::PDU::Acknowledge_PDU *> (pdu);
    if (specific == nullptr)
    {
        return;
    }

    if (extListenDIS != nullptr && extListenDIS->isFiltered (specific->GetOriginatingEntityID ()))
    {
        return;
    }

    QString senderIP = QString::fromStdString (((sith::dis::PDUsSubscriber *)data)->getSenderIP ());
    QString message = QString ("Acknowledge updated from ") + senderIP + QString (" : \n");
    message += specific->GetAsString ().c_str ();
    message += "\n\n";
    extListenDIS->addMessage (KDIS::DATA_TYPE::ENUMS::Acknowledge_PDU_Type, message, pdu, senderIP);
}


void dataQueryCb (KDIS::PDU::Header * pdu, void * data)
{
    KDIS::PDU::Data_Query_PDU * specific = dynamic_cast <KDIS::PDU::Data_Query_PDU *> (pdu);
    if (specific == nullptr)
    {
        return;
    }

    if (extListenDIS != nullptr && extListenDIS->isFiltered (specific->GetOriginatingEntityID ()))
    {
        return;
    }

    QString senderIP = QString::fromStdString (((sith::dis::PDUsSubscriber *)data)->getSenderIP ());
    QString message = QString ("Data Query updated from ") + senderIP + QString (" : \n");
    message += specific->GetAsString ().c_str ();
    message += "\n\n";
    extListenDIS->addMessage (KDIS::DATA_TYPE::ENUMS::Data_Query_PDU_Type, message, pdu, senderIP);
}


void removeEntityCb (KDIS::PDU::Header * pdu, void * data)
{
    KDIS::PDU::Remove_Entity_PDU * specific = dynamic_cast <KDIS::PDU::Remove_Entity_PDU *> (pdu);
    if (specific == nullptr)
    {
        return;
    }

    if (extListenDIS != nullptr && extListenDIS->isFiltered (specific->GetOriginatingEntityID ()))
    {
        return;
    }

    QString senderIP = QString::fromStdString (((sith::dis::PDUsSubscriber *)data)->getSenderIP ());
    QString message = QString ("Remove Entity updated from ") + senderIP + QString (" : \n");
    message += specific->GetAsString ().c_str ();
    message += "\n\n";
    extListenDIS->addMessage (KDIS::DATA_TYPE::ENUMS::Remove_Entity_PDU_Type, message, pdu, senderIP);
}


void acknowledgeRCb (KDIS::PDU::Header * pdu, void * data)
{
    KDIS::PDU::Acknowledge_R_PDU * specific = dynamic_cast <KDIS::PDU::Acknowledge_R_PDU *> (pdu);
    if (specific == nullptr)
    {
        return;
    }

    if (extListenDIS != nullptr && extListenDIS->isFiltered (specific->GetOriginatingEntityID ()))
    {
        return;
    }

    QString senderIP = QString::fromStdString (((sith::dis::PDUsSubscriber *)data)->getSenderIP ());
    QString message = QString ("Acknowledge R updated from ") + senderIP + QString (" : \n");
    message += specific->GetAsString ().c_str ();
    message += "\n\n";
    extListenDIS->addMessage (KDIS::DATA_TYPE::ENUMS::Acknowledge_R_PDU_Type, message, pdu, senderIP);
}


void transfertControlCb (KDIS::PDU::Header * pdu, void * data)
{
    KDIS::PDU::Transfer_Control_Request_PDU * specific = dynamic_cast <KDIS::PDU::Transfer_Control_Request_PDU *> (pdu);
    if (specific == nullptr)
    {
        return;
    }

    if (extListenDIS != nullptr && extListenDIS->isFiltered (specific->GetOriginatingEntityID ()))
    {
        return;
    }

    QString senderIP = QString::fromStdString (((sith::dis::PDUsSubscriber *)data)->getSenderIP ());
    QString message = QString ("Transfer Control Request updated from ") + senderIP + QString (" : \n");
    message += specific->GetAsString ().c_str ();
    message += "\n\n";
    extListenDIS->addMessage (KDIS::DATA_TYPE::ENUMS::TransferControl_PDU_Type, message, pdu, senderIP);
}


void eventReportCb (KDIS::PDU::Header * pdu, void * data)
{
    KDIS::PDU::Event_Report_PDU * specific = dynamic_cast <KDIS::PDU::Event_Report_PDU *> (pdu);
    if (specific == nullptr)
    {
        return;
    }

    if (extListenDIS != nullptr && extListenDIS->isFiltered (specific->GetOriginatingEntityID ()))
    {
        return;
    }

    QString senderIP = QString::fromStdString (((sith::dis::PDUsSubscriber *)data)->getSenderIP ());
    QString message = QString ("Event Report updated from ") + senderIP + QString (" : \n");
    message += specific->GetAsString ().c_str ();
    message += "\n\n";
    extListenDIS->addMessage (KDIS::DATA_TYPE::ENUMS::Event_Report_PDU_Type, message, pdu, senderIP);
}


void recordRCb (KDIS::PDU::Header * pdu, void * data)
{
    KDIS::PDU::Record_R_PDU * specific = dynamic_cast <KDIS::PDU::Record_R_PDU *> (pdu);
    if (specific == nullptr)
    {
        return;
    }

    if (extListenDIS != nullptr && extListenDIS->isFiltered (specific->GetOriginatingEntityID ()))
    {
        return;
    }

    QString senderIP = QString::fromStdString (((sith::dis::PDUsSubscriber *)data)->getSenderIP ());
    QString message = QString ("Record R updated from ") + senderIP + QString (" : \n");
    message += specific->GetAsString ().c_str ();
    message += "\n\n";
    extListenDIS->addMessage (KDIS::DATA_TYPE::ENUMS::Record_R_PDU_Type, message, pdu, senderIP);
}


void setRecordRCb (KDIS::PDU::Header * pdu, void * data)
{
    KDIS::PDU::Set_Record_R_PDU * specific = dynamic_cast <KDIS::PDU::Set_Record_R_PDU *> (pdu);
    if (specific == nullptr)
    {
        return;
    }

    if (extListenDIS != nullptr && extListenDIS->isFiltered (specific->GetOriginatingEntityID ()))
    {
        return;
    }

    QString senderIP = QString::fromStdString (((sith::dis::PDUsSubscriber *)data)->getSenderIP ());
    QString message = QString ("Set Record R updated from ") + senderIP + QString (" : \n");
    message += specific->GetAsString ().c_str ();
    message += "\n\n";
    extListenDIS->addMessage (KDIS::DATA_TYPE::ENUMS::SetRecord_R_PDU_Type, message, pdu, senderIP);
}