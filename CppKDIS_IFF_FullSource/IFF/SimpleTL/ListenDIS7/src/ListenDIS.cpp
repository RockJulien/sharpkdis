// Class specific
#include "ListenDIS.h"

// To avoid KDIS bad header warnings
#pragma warning ( disable : 4189 )
// KDIS : Classes
#include <KDIS/PDU/Distributed_Emission_Regeneration/IFF_PDU.h>
#include <KDIS/Extras/PDU_Factory.h>
#include <KDIS/KDefines.h>
#pragma warning ( default : 4189 )

// Qt
#include <QDebug>
#include <QUdpSocket.h>
#include <QHostAddress.h>

using namespace std;


const int exerciseID = 14;
QHostAddress address = QHostAddress ("227.0.0.7");
QHostAddress interfaceNet = QHostAddress ("192.168.1.45");
int port = 30999;


// Constructor
ListenDIS::ListenDIS ()
{
    // Init vars
    _socket = nullptr;

    intializeDIS ();

    _timer = startTimer (1000);
}


// Destructor
ListenDIS::~ListenDIS ()
{
    killTimer (_timer);

    delete _pduFact;
}


bool ListenDIS::intializeDIS ()
{
    _pduFact = new KDIS::UTILS::PDU_Factory;

    _socket = new QUdpSocket;
    bool result = _socket->bind (QHostAddress::AnyIPv4, port, QUdpSocket::ShareAddress|QUdpSocket::ReuseAddressHint);
    qDebug () << "Bind result : " << result;
    result = _socket->joinMulticastGroup (address);
    qDebug () << "Join result : " << result;

    connect (_socket, &QIODevice::readyRead, this, &ListenDIS::receiveIFF);

    return true;
}


void ListenDIS::timerEvent (QTimerEvent *)
{
    qDebug () << "Receive IFF";
    while (_socket != nullptr && _socket->hasPendingDatagrams ())
    {
        receiveIFF ();
    }
}


void ListenDIS::receiveIFF ()
{
    if (_socket == nullptr) return;

    while (_socket->hasPendingDatagrams ()) 
    {
        QByteArray datagram;
        datagram.resize (_socket->pendingDatagramSize ());
        QHostAddress sender;
        quint16 senderPort;

        _socket->readDatagram (datagram.data (), datagram.size (), &sender, &senderPort);

        KDIS::KDataStream stream ((KDIS::KOCTET*)datagram.constData (), datagram.size ());
        
        // Now process the stream
        if( stream.GetBufferSize() > 0 )
        {
            // Get the current write position
            KDIS::KUINT16 currentPos = stream.GetCurrentWritePosition();

            try
            {
                // Get the next/only PDU from the stream
                unique_ptr<KDIS::PDU::Header> pdu = _pduFact->Decode( stream );

                // If the PDU was decoded successfully then fire the next event
                if( pdu.get() )
                {
                    // Set the write pos for the next pdu. We do this here as its possible that when the PDU was decoded that some data may
				    // have been left un-decoded so to be extra safe we use the reported pdu size and not the current stream.
                    stream.SetCurrentWritePosition( currentPos + pdu->GetPDULength() );

                    // Now return the decoded pdu
                    KDIS::PDU::IFF_PDU * iffPDU = dynamic_cast <KDIS::PDU::IFF_PDU *> (pdu.get ());
                    if (iffPDU != nullptr)
                    {
                        qDebug () << iffPDU->GetAsString ().c_str ();
                    }
                }
                else
                {
                    // If a PDU could not be decoded in the PDU bundle, then we need to throw
                    // out the whole stream. There is no way to know where the next PDU might start
                    // in the data stream.
                    stream.Clear();
                }
            }
            catch( const std::exception & e )
            {
                qDebug () << e.what ();
                // Something went wrong, the stream is likely corrupted now so wipe it or we will have issues in the next GetNextPDU call.
                stream.Clear();
            }
        }
    }
}
    
