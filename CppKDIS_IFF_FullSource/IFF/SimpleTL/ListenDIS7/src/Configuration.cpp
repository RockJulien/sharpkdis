#include "Configuration.h"

// Qt
#include <QNetworkInterface>
#include <QHostInfo>


Configuration::Configuration (QWidget * parent) : QDialog (parent)
{
    _ui.setupUi (this);

    QList<QNetworkInterface> interfaces = QNetworkInterface::allInterfaces ();
    QListIterator<QNetworkInterface> itInterface (interfaces);
    while (itInterface.hasNext ())
    {
        QNetworkInterface interface = itInterface.next ();
        QNetworkInterface::InterfaceFlags flags = interface.flags ();

        if ((flags & QNetworkInterface::IsUp) != 0 && (flags & QNetworkInterface::IsRunning) != 0)
        {
            QList<QNetworkAddressEntry> addressEntries = interface.addressEntries ();
            QListIterator<QNetworkAddressEntry> itAddressEntry (addressEntries);
            while (itAddressEntry.hasNext ())
            {
                QNetworkAddressEntry addressEntry = itAddressEntry.next ();

                if (addressEntry.ip ().protocol () == QAbstractSocket::IPv4Protocol)
                {
                    _ui.adapterCB->addItem (addressEntry.ip ().toString () + QString (" (") + interface.humanReadableName () + QString (")"));
                }
            }
        }
    }
    // Add "ANY_ADDR", adress
    _ui.adapterCB->addItem ("ANY_ADDR");

    _ui.ttlCB->clear ();
    _ui.ttlCB->addItem ("Restricted to the same host (0)"       , 0);
    _ui.ttlCB->addItem ("Restricted to the same subnet (1)"     , 1);
    _ui.ttlCB->addItem ("Restricted to the same site (32)"      , 32);
    _ui.ttlCB->addItem ("Restricted to the same region (64)"    , 64);
    _ui.ttlCB->addItem ("Restricted to the same continent (128)", 128);
    _ui.ttlCB->addItem ("Unrestricted (255)"                    , 255);

    // Connections
    connect (_ui.okPB,     &QPushButton::clicked, this, &Configuration::onOKClick);
    connect (_ui.cancelPB, &QPushButton::clicked, this, &Configuration::onCancelClick);

    startTimer (100);
}


Configuration::~Configuration()
{  
}


void Configuration::timerEvent (QTimerEvent * event)
{
    Q_UNUSED (event);

    QHostAddress address (_ui.addressLE->text ());

    bool okEnabled = true;
    if (address.isNull () || _ui.addressLE->text ().count (".") != 3)
    {
        _ui.addressValidityL->setText ("Invalid");
        okEnabled = false;
    }
    else if (address.isMulticast ())
    {
        _ui.addressValidityL->setText ("Multicast");
    }
    else if (_ui.addressLE->text ().endsWith ("255"))
    {
        _ui.addressValidityL->setText ("Broadcast");
    }
    else
    {
        _ui.addressValidityL->setText ("Valid");
    }
    _ui.okPB->setEnabled (okEnabled);
}
        

void Configuration::onOKClick ()
{
    QString adapter = _ui.adapterCB->currentText ();
    if (adapter == QString ("ANY_ADDR"))
    {
        adapter = QString ("");
    }
    emit dataEdited (
        _ui.addressLE->text (),
        _ui.portSB->value (),
        adapter,
        _ui.ttlCB->currentData ().toInt (),
        _ui.exerciseIdSB->value (),
        _ui.siteIdSB->value (),
        _ui.applicationIdSB->value ());

    QDialog::accept ();
}


void Configuration::onCancelClick ()
{
    QDialog::reject ();
}


void Configuration::setData (QString address, int port, QString adapter, int ttl, unsigned char exerciseId, unsigned short siteId, unsigned short applicationId)
{
    if (adapter.isEmpty ())
    {
        adapter = QString ("ANY_ADDR");
    }
    _ui.addressLE->setText         (address);
    _ui.portSB->setValue           (port);
    _ui.adapterCB->setCurrentIndex (_ui.adapterCB->findText (adapter));
    _ui.exerciseIdSB->setValue     (exerciseId);
    _ui.siteIdSB->setValue         (siteId);
    _ui.applicationIdSB->setValue  (applicationId);
    _ui.ttlCB->setCurrentIndex     (_ui.ttlCB->findData (ttl));
}
