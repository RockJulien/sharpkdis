#include "NetConfigReader.h"

// Qt
#include <QFile>
#include <QTextStream>
#include <iostream>

NetConfigReader* NetConfigReader::_NetConfigReader = 0;
    
//--------------------------------------------------------
NetConfigReader* NetConfigReader::instance()
{
    if (!_NetConfigReader)
    {
        _NetConfigReader = new NetConfigReader ();
    }

    return _NetConfigReader;
}
//--------------------------------------------------------
NetConfigReader::NetConfigReader(void) : QDomDocument ("NetConfigReader")
{
}
//--------------------------------------------------------
NetConfigReader::~NetConfigReader(void)
{
}
//--------------------------------------------------------
bool NetConfigReader::parseXMLFile (QString strXMLFileName)
{
    QFile file (strXMLFileName);
    if (!file.open (QIODevice::ReadOnly))
    {
        return false;
    }
    if (!setContent (&file))
    {
        file.close ();
        return false;
    }
    file.close();

    return true;
}

bool NetConfigReader::getNetworkParameters (NetworkParameters & networkParameters)
{
    QDomElement root;
    QDomNodeList nodes = elementsByTagName ("Network");
    if (nodes.size () == 1 && nodes.at (0).isElement ())
    {
        QDomElement element = (nodes.at (0)).toElement ();

        // Address
        QString tmp = element.attribute ("address", "N.A.");
        if (tmp != "N.A.")
        {
            networkParameters.address = tmp;
        }

        // Send port
        tmp = element.attribute ("sendPort", "N.A.");
        if (tmp != "N.A.")
        {
            networkParameters.sendPort = tmp.toInt ();
        }

        // Recv port
        tmp = element.attribute ("recvPort", "N.A.");
        if (tmp != "N.A.")
        {
            networkParameters.recvPort = tmp.toInt ();
        }
    }
    else
    {
        return false;
    }

    return true;
}

bool NetConfigReader::getConnectionParameters (ConnectionParameters & connectionParameters)
{
    QDomElement root;
    QDomNodeList nodes = elementsByTagName ("Connection");
    if (nodes.size () == 1 && nodes.at (0).isElement ())
    {
        QDomElement element = (nodes.at (0)).toElement ();

        // Exercise
        QString tmp = element.attribute ("exercise", "N.A.");
        if (tmp == "N.A.")
        {
            return false;
        }
        connectionParameters.exercise = tmp.toInt ();

        // Site
        tmp = element.attribute ("site", "N.A.");
        if (tmp == "N.A.")
        {
            return false;
        }
        connectionParameters.site = tmp.toInt ();

        // Application
        tmp = element.attribute ("application", "N.A.");
        if (tmp == "N.A.")
        {
            return false;
        }
        connectionParameters.application = tmp.toInt ();
    }
    else
    {
        return false;
    }

    return true;
}


bool NetConfigReader::getDumpOptions (bool & enable, bool & dumpText, bool & dumpBinary, QString & dumpDirectory)
{
    QDomElement root;
    QDomNodeList nodes = elementsByTagName ("Dump");
    if (nodes.size () == 1 && nodes.at (0).isElement ())
    {
        QDomElement element = (nodes.at (0)).toElement ();

        // Enable
        QString tmp = element.attribute ("enable", "N.A.");
        if (tmp == "N.A.")
        {
            return false;
        }
        enable = (tmp.compare ("true", Qt::CaseInsensitive) == 0);

        // dumpText
        tmp = element.attribute ("dumpText", "N.A.");
        if (tmp == "N.A.")
        {
            return false;
        }
        dumpText = (tmp.compare ("true", Qt::CaseInsensitive) == 0);

        // dumpBinary
        tmp = element.attribute ("dumpBinary", "N.A.");
        if (tmp == "N.A.")
        {
            return false;
        }
        dumpBinary = (tmp.compare ("true", Qt::CaseInsensitive) == 0);
        
        // dumpDirectory
        tmp = element.attribute ("dumpDirectory", "N.A.");
        if (tmp == "N.A.")
        {
            return false;
        }
        dumpDirectory = tmp;
    }
    else
    {
        return false;
    }

    return true;
}


bool NetConfigReader::getReplayOptions (bool & enable, QString & dumpBinary, bool & startPlaying)
{
    QDomElement root;
    QDomNodeList nodes = elementsByTagName ("ReplayOnLoad");
    if (nodes.size () == 1 && nodes.at (0).isElement ())
    {
        QDomElement element = (nodes.at (0)).toElement ();

        // Enable
        QString tmp = element.attribute ("enable", "N.A.");
        if (tmp == "N.A.")
        {
            return false;
        }
        enable = (tmp.compare ("true", Qt::CaseInsensitive) == 0);

        // Start startPlaying
        tmp = element.attribute ("startPlaying", "N.A.");
        if (tmp == "N.A.")
        {
            return false;
        }
        startPlaying = (tmp.compare ("true", Qt::CaseInsensitive) == 0);

        // dumpBinary
        tmp = element.attribute ("dumpBinary", "N.A.");
        if (tmp == "N.A.")
        {
            return false;
        }
        dumpBinary = tmp;
    }
    else
    {
        return false;
    }

    return true;
}


bool NetConfigReader::getExitOptions (bool & enable, int & reason)
{
    QDomElement root;
    QDomNodeList nodes = elementsByTagName ("ExitOnStop");
    if (nodes.size () == 1 && nodes.at (0).isElement ())
    {
        QDomElement element = (nodes.at (0)).toElement ();

        // Enable
        QString tmp = element.attribute ("enable", "N.A.");
        if (tmp == "N.A.")
        {
            return false;
        }
        enable = (tmp.compare ("true", Qt::CaseInsensitive) == 0);

        // Reason
        tmp = element.attribute ("reason", "N.A.");
        if (tmp == "N.A.")
        {
            return false;
        }
        reason = tmp.toInt ();
    }
    else
    {
        return false;
    }

    return true;
}
