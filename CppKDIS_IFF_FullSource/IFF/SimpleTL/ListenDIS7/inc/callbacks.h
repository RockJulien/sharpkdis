#ifndef __CALLBACKS_H__
#define __CALLBACKS_H__

#include <KDIS/PDU/Header.h>

namespace KDIS
{
    namespace DATA_TYPE
    {
        class VariableDatum;
    }
}
namespace sith
{
    namespace dis
    {
        class PDUsSubscriber;
    }
}
class ListenDIS;


// Custom
void StopApplicationCb  (KDIS::DATA_TYPE::VariableDatum * datum, void * data);
void StartApplicationCb (KDIS::DATA_TYPE::VariableDatum * datum, void * data);
void UninstallCb        (KDIS::DATA_TYPE::VariableDatum * datum, void * data);

// Common
void FireCb                    (KDIS::PDU::Header * pdu, void * data);
void DetonationCb              (KDIS::PDU::Header * pdu, void * data);
void StartResumeCb             (KDIS::PDU::Header * pdu, void * data);
void StopFreezeCb              (KDIS::PDU::Header * pdu, void * data);
void EntitiesCb                (KDIS::PDU::Header * pdu, void * data);
void ElectromagneticEmissionCb (KDIS::PDU::Header * pdu, void * data);
void DesignatorCb              (KDIS::PDU::Header * pdu, void * data);
void IFFCb                     (KDIS::PDU::Header * pdu, void * data);
void underWaterAcousticCb      (KDIS::PDU::Header * pdu, void * data);
void transmitterCb             (KDIS::PDU::Header * pdu, void * data);
void signalCb                  (KDIS::PDU::Header * pdu, void * data);
void acknowledgeCb             (KDIS::PDU::Header * pdu, void * data);
void dataQueryCb               (KDIS::PDU::Header * pdu, void * data);
void removeEntityCb            (KDIS::PDU::Header * pdu, void * data);
void acknowledgeRCb            (KDIS::PDU::Header * pdu, void * data);
void transfertControlCb        (KDIS::PDU::Header * pdu, void * data);
void eventReportCb             (KDIS::PDU::Header * pdu, void * data);
void recordRCb                 (KDIS::PDU::Header * pdu, void * data);
void setRecordRCb              (KDIS::PDU::Header * pdu, void * data);

void initCallbacks (sith::dis::PDUsSubscriber * subscriber, ListenDIS * listenDIS);

#endif // __CALLBACKS_H__