//#define WITHOUT_LICENCE

#ifndef __LISTEN_DIS_H__
#define __LISTEN_DIS_H__

#pragma warning ( disable : 4189 )
// KDIS
#include <KDIS/PDU/Header.h>
#pragma warning ( default : 4189 )

// Qt
#include <QObject>

class QUdpSocket;

namespace KDIS
{
    namespace UTILS
    {
        class PDU_Factory;
    }
}

class ListenDIS : public QObject
{
    Q_OBJECT

public:
    ListenDIS ();
    virtual ~ListenDIS ();

    
protected:
    bool intializeDIS       ();
    void timerEvent         (QTimerEvent * event);

    void receiveIFF ();
    
private:
    // Settings
    QUdpSocket * _socket;

    KDIS::UTILS::PDU_Factory * _pduFact;

    int _timer;
};

#endif