// Qt
#include <QCoreApplication>

// Talk DIS
#include "TalkDIS.h"


int main (int argc, char* argv[])
{
    QCoreApplication application (argc, argv);

    TalkDIS talkDIS;

    return application.exec ();
}
