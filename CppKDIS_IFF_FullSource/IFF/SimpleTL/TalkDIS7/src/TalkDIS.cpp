// Class specific
#include "TalkDIS.h"

// To avoid KDIS bad header warnings
#pragma warning ( disable : 4189 )
// KDIS : Classes
#include <KDIS/PDU/Distributed_Emission_Regeneration/IFF_PDU.h>
#pragma warning ( default : 4189 )

// Qt
#include <QDebug>
#include <QUdpSocket.h>
#include <QHostAddress.h>

const int exerciseID = 14;
QHostAddress address = QHostAddress ("227.0.0.7");
int port = 30999;


// Constructor
TalkDIS::TalkDIS ()
{
    // Init vars
    _socket = nullptr;

    intializeDIS ();

    _timer = startTimer (1000);
}


// Destructor
TalkDIS::~TalkDIS ()
{
    killTimer (_timer);
}


bool TalkDIS::intializeDIS ()
{
    _socket = new QUdpSocket;

    return true;
}


void TalkDIS::timerEvent (QTimerEvent *)
{
    qDebug () << "Send IFF";
    sendIFF ();
}


void TalkDIS::sendIFF ()
{
    if (_socket == nullptr)
    {
        return;
    }
    
    KDIS::DATA_TYPE::TimeStamp timeStamp (KDIS::DATA_TYPE::ENUMS::RelativeTime, 0, true);
    timeStamp.CalculateTimeStamp ();

    KDIS::PDU::IFF_PDU pdu;

    pdu.SetEmittingEntityID (KDIS::DATA_TYPE::EntityIdentifier (1, 77, 99));

    pdu.SetEventID (KDIS::DATA_TYPE::EntityIdentifier (1, 77, 1));

    pdu.SetLocation (KDIS::DATA_TYPE::Vector (1., 2., 3.));

    KDIS::DATA_TYPE::SystemIdentifier systemId;
    systemId.SetSystemType (KDIS::DATA_TYPE::ENUMS::Mark_X_XII_ATCRBS_ModeS_Transponder);
    systemId.SetSystemName (KDIS::DATA_TYPE::ENUMS::Mark_XII);
    systemId.SetSystemMode ((KDIS::DATA_TYPE::ENUMS::SystemMode)3); // DCGF
    bool changeIndicator = true;
    bool altMode4 = true;
    bool altModeC = true;
    systemId.SetChangeOptions (changeIndicator, altMode4, altModeC);
    pdu.SetSystemIdentifier (systemId);

    // KDIS::DATA_TYPE::ENUMS::Mark_X_XII_ATCRBS_ModeS_Transponder:
    KDIS::DATA_TYPE::FundamentalOperationalData_MarkXTransponder transponder;
    bool markTransponderSystemOn = true;
    bool markTransponderMode1Status = true;
    bool markTransponderMode2Status = true;
    bool markTransponderMode3Status = true;
    bool markTransponderMode4Status = true;
    bool markTransponderModeCStatus = true;
    bool markTransponderModeSStatus = true;
    bool markTransponderOperationalStatus = true;
    transponder.SetSystemStatus (
        markTransponderSystemOn,
        markTransponderMode1Status,
        markTransponderMode2Status,
        markTransponderMode3Status,
        markTransponderMode4Status,
        markTransponderModeCStatus,
        markTransponderModeSStatus,
        markTransponderOperationalStatus);
    transponder.SetAlternateParameter4 (KDIS::DATA_TYPE::ENUMS::Valid);
    transponder.SetInfomationLayersPresence (true, true);
    bool markTransponderModifierEmergency = true;
    bool markTransponderModifierIdentSquawkFlash = true;
    bool markTransponderModifierSTI = true;
    transponder.SetModifier (markTransponderModifierEmergency, markTransponderModifierIdentSquawkFlash, markTransponderModifierSTI);
    transponder.SetMode1CodeStatus (1, 2, true, false, false);
    transponder.SetMode2CodeStatus (1, 2, 3, 4, true, false, false);
    transponder.SetMode3CodeStatus (1, 2, 3, 4, true, false, false);
    transponder.SetMode4CodeStatus (23, true, false, false);
    bool markTransponderModeCAltNegativeMeanSeaLevel = false;
    uint markTransponderModeCCodeAltitude = 105;
    transponder.SetModeCCodeStatus (
        markTransponderModeCAltNegativeMeanSeaLevel,
        markTransponderModeCCodeAltitude,
        true, false, false);
    transponder.SetModeSCodeStatus (
        (KDIS::DATA_TYPE::ENUMS::TCAS_I),
        true, false, false);

    pdu.SetFundamentalOperationalData (transponder);

    // Layer 2
    KDIS::DATA_TYPE::IFF_Layer2 * layer2 = new KDIS::DATA_TYPE::IFF_Layer2;
    layer2->SetBeamData (KDIS::DATA_TYPE::BeamData (0.1, 1.2, 2.4, 0.7, 0.8));
    layer2->SetSecondaryOperationalData (KDIS::DATA_TYPE::SecondaryOperationalData (0, 0, 0));

    pdu.AddLayer (layer2);

    // Layer 3
    KDIS::DATA_TYPE::IFF_Layer3Transponder * layer3 = new KDIS::DATA_TYPE::IFF_Layer3Transponder;

    KDIS::DATA_TYPE::EnhancedMode1Code mode1;
    mode1.SetCodeElement1 (1);
    mode1.SetCodeElement2 (2);
    mode1.SetCodeElement3 (3);
    mode1.SetCodeElement4 (4);
    layer3->SetReportingSimulation (KDIS::DATA_TYPE::SimulationIdentifier (1, 45));
    layer3->SetBasicData (
        KDIS::DATA_TYPE::Mode5TransponderBasicData (
            KDIS::DATA_TYPE::Mode5TransponderStatus (
                KDIS::DATA_TYPE::ENUMS::ValidReply, 
                true,
                KDIS::DATA_TYPE::ENUMS::Top,
                true,
                true,
                true,
                KDIS::DATA_TYPE::ENUMS::AirPlatformType,
                false,
                false,
                false,
                false),
            53,
            0,
            mode1,
            71,
            KDIS::DATA_TYPE::Mode5TransponderSupplementalData (
                false,
                false,
                4),
            KDIS::DATA_TYPE::ENUMS::GPS,
            1));

    pdu.AddLayer (layer3);

    // Layer 4
    KDIS::DATA_TYPE::IFF_Layer4Transponder * layer4 = new KDIS::DATA_TYPE::IFF_Layer4Transponder;
    layer4->SetReportingSimulation (KDIS::DATA_TYPE::SimulationIdentifier (1, 45));
    KDIS::KUINT8 plop[9] = "12345678";
    layer4->SetBasicData (
        KDIS::DATA_TYPE::ModeSTransponderBasicData (
            KDIS::DATA_TYPE::ModeSTransponderStatus (
                false,
                KDIS::DATA_TYPE::ENUMS::Acquisition,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false,
                false),
            KDIS::DATA_TYPE::ModeSLevelsPresent (false, false, false, false, false),
            KDIS::DATA_TYPE::ENUMS::Airbone,
            plop,
            8, 
            77,
            KDIS::DATA_TYPE::ENUMS::FlightNumber,
            KDIS::DATA_TYPE::DAPSource (false, false, false, false, false, false, false, false),
            KDIS::DATA_TYPE::ModeSAltitude (115, 1),
            KDIS::DATA_TYPE::ENUMS::NoCommunicationsCapability));


    pdu.AddLayer (layer4);

    // Layer 5
    KDIS::DATA_TYPE::IFF_Layer5 * layer5 = new KDIS::DATA_TYPE::IFF_Layer5;
    layer5->SetReportingSimulation (KDIS::DATA_TYPE::SimulationIdentifier (1, 45));
    layer5->SetApplicableLayers (KDIS::DATA_TYPE::InformationLayers (true, true, true, true, true, false, false));
    layer5->SetDataCategory (KDIS::DATA_TYPE::ENUMS::NoStatementDataCategory);
    pdu.AddLayer (layer5);

    pdu.SetExerciseID (exerciseID);

    KDIS::KDataStream steam = pdu.Encode ();

   const char * data = (const char *)steam.GetBuffer ().data ();
    _socket->writeDatagram (data, steam.GetBufferSize (), address, port);

    // Layers pointers are deleted by PDU destructor
}
