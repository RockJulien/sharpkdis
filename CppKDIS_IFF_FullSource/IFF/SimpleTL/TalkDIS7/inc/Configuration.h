#ifndef __CONFIGURATION_H__
#define __CONFIGURATION_H__

#include <QDialog>

#include "ui_Configuration.h"


class Configuration : public QDialog
{
    Q_OBJECT

public:
    Configuration (QWidget * parent = nullptr);
    virtual ~Configuration ();

    public slots:
    void onCancelClick    ();
    void onOKClick        ();

    void setData (QString address, int sendPport, int recvPort, QString adapter, int ttl, unsigned char exerciseId, unsigned short siteId, unsigned short applicationId);

signals:
    void dataEdited (QString address, int sendPport, int recvPort, QString adapter, int ttl, unsigned char exerciseId, unsigned short siteId, unsigned short applicationId);

protected:
    void timerEvent (QTimerEvent * event);

    Ui::Configuration _ui;
};

#endif // __CONFIGURATION_H__
