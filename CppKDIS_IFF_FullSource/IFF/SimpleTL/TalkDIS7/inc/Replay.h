#ifndef __REPLAY_H__
#define __REPLAY_H__

// Class specific
#include <QDialog>

// ui
#include "ui_Replay.h"

// Forward declarations
namespace sith
{
    namespace dis
    {
        class PDUsPublisher;
    }
}

typedef struct
{
    long   version;
    long   subversion;
    qint64 duration;
} DumpInformations;

typedef struct
{
    qint64        time;
    unsigned long size;
} RecordHeader;


class Replay : public QDialog
{
    Q_OBJECT;

    enum class State
    {
        STOP  = 0,
        PLAY  = 1,
        PAUSE = 2
    };

public:
    Replay (sith::dis::PDUsPublisher * pdusPublisher, QString dumpFile, bool startPlaying = false, QWidget * parent = 0, Qt::WindowFlags f = 0);
    virtual ~Replay ();

    public slots:
        void onPlayClick  (bool checked);
        void onPauseClick (bool checked);
        void onStopClick  (bool checked);

private:
    // DIS
    sith::dis::PDUsPublisher * _publisher;

    // Dump
    QString _dumpFile;
    State   _state;
    // ui
    Ui::Replay _ui;
};

#endif // __REPLAY_H__
