//#define WITHOUT_LICENCE

#ifndef __TALK_DIS_H__
#define __TALK_DIS_H__

#pragma warning ( disable : 4189 )
// KDIS
#include <KDIS/PDU/Header.h>
#pragma warning ( default : 4189 )

// Qt
#include <QObject>

class QUdpSocket;


class TalkDIS : public QObject
{
    Q_OBJECT

public:
    TalkDIS ();
    virtual ~TalkDIS ();

protected:
    bool intializeDIS       ();
    void timerEvent         (QTimerEvent * event);

    void sendIFF ();

private:
    // Settings
    QUdpSocket * _socket;

    int _timer;
};

#endif // __TALK_DIS_H__
