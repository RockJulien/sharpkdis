#ifndef NetConfigReader_H
#define NetConfigReader_H

// Qt
#include <QDomDocument>
#include <QDomElement>
#include <QVector>

typedef struct
{
    QString address;
    int     sendPort;
    int     recvPort;
} NetworkParameters;

typedef struct
{
    int exercise;
    int site;
    int application;
} ConnectionParameters;

class NetConfigReader : public QDomDocument
{
public:    
    static NetConfigReader * instance ();
    virtual ~NetConfigReader ();

    /*!
    *  \fn parseXMLFile (QString strXMLFileName)
    *  \brief Open and read a XML file
    *  \param QString strXMLFileName : The path to the XML file
    *  \return true if success
    */
    bool parseXMLFile       (QString strXMLFileName);

    /*!
    *  \fn getNetworkParameters (NetworkParameters & networkParameters)
    *  \brief Get the network information
    *  \return true if success
    */
    bool getNetworkParameters (NetworkParameters & networkParameters);

    /*!
    *  \fn getConnectionParameters (ConnectionParameters & connectionParameters)
    *  \brief Get the connection information
    *  \return true if success
    */
    bool getConnectionParameters (ConnectionParameters & connectionParameters);

    /*!
    *  \fn getDumpOptions (bool & enable, bool & dumpText, bool & dumpBinary, QString & dumpDirectory)
    *  \brief Get the dump options
    *  \param enable        : Enable the dump (text and/or binary)
    *  \param dumpText      : Text dump flag (on/off)
    *  \param dumpBinary    : Binary dump flag (on/off)
    *  \param dumpDirectory : Directoy where dump file will be written
    *  \return true if success
    */
    bool getDumpOptions (bool & enable, bool & dumpText, bool & dumpBinary, QString & dumpDirectory);

    /*!
    *  \fn getReplayOptions (bool & enable, QString & dumpBinary)
    *  \brief Get the replay options
    *  \param enable       : Enable the dump (text and/or binary)
    *  \param dumpBinary   : Binary dump file
    *  \param startPlaying : Start play on launch
    *  \return true if success
    */
    bool getReplayOptions (bool & enable, QString & dumpBinary, bool & startPlaying);

    /*!
    *  \fn getExitOptions (bool & enable, int & reason)
    *  \brief Get the exit options
    *  \param enable : Enable the exit application on PDU Stop reception
    *  \param reason : reason value for exiting
    *  \return true if success
    */
    bool getExitOptions (bool & enable, int & reason);

private :    
    NetConfigReader ();

    static NetConfigReader* _NetConfigReader;
};
#endif //ConfigReader_H
