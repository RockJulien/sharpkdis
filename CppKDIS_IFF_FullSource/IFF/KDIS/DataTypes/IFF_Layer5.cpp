#include "./IFF_Layer5.h"

//////////////////////////////////////////////////////////////////////////

using namespace std;
using namespace KDIS;
using namespace DATA_TYPE;
using namespace UTILS;
using namespace ENUMS;

//////////////////////////////////////////////////////////////////////////
// public:
//////////////////////////////////////////////////////////////////////////

IFF_Layer5::IFF_Layer5() :
	m_ui16Padding_1( 0 ),
    m_ui8DataCategory(0),
    m_ui16Padding_2( 0),
	m_ui16NumIffRecs( 0 )
{
	m_ui8LayerNumber = 5;
	m_ui16LayerLength = IFF_LAYER5_SIZE;
}

//////////////////////////////////////////////////////////////////////////

IFF_Layer5::IFF_Layer5( KDataStream & stream ) 
{
    Decode( stream, false );
}

//////////////////////////////////////////////////////////////////////////

IFF_Layer5::IFF_Layer5( const LayerHeader & H, KDataStream & stream )  :
	LayerHeader( H )
{
    Decode( stream, true );
}
//////////////////////////////////////////////////////////////////////////

IFF_Layer5::~IFF_Layer5()
{
}

//////////////////////////////////////////////////////////////////////////

void IFF_Layer5::SetReportingSimulation( const SimulationIdentifier & RS )
{
	m_RptSim = RS;
}

//////////////////////////////////////////////////////////////////////////

const SimulationIdentifier & IFF_Layer5::GetReportingSimulation() const
{
	return m_RptSim;
}

//////////////////////////////////////////////////////////////////////////

SimulationIdentifier & IFF_Layer5::GetReportingSimulation()
{
	return m_RptSim;
}

//////////////////////////////////////////////////////////////////////////

void IFF_Layer5::SetApplicableLayers( const InformationLayers & IL )
{
    m_ui8ApplicableLayers = IL;
}

//////////////////////////////////////////////////////////////////////////
   
const InformationLayers & IFF_Layer5::GetApplicableLayers() const
{
    return m_ui8ApplicableLayers;
}

//////////////////////////////////////////////////////////////////////////
    
InformationLayers & IFF_Layer5::GetApplicableLayers()
{
    return m_ui8ApplicableLayers;
}

//////////////////////////////////////////////////////////////////////////

void IFF_Layer5::SetDataCategory( KDIS::DATA_TYPE::ENUMS::DataCategory DC )
{
    m_ui8DataCategory = DC;
}

//////////////////////////////////////////////////////////////////////////

KDIS::DATA_TYPE::ENUMS::DataCategory IFF_Layer5::GetDataCategory() const
{
    return (KDIS::DATA_TYPE::ENUMS::DataCategory) m_ui8DataCategory;
}

//////////////////////////////////////////////////////////////////////////

void IFF_Layer5::AddDataRecord( StdVarPtr DR )
{
	m_vStdVarRecs.push_back( DR );
	++m_ui16NumIffRecs;
    m_ui16LayerLength += DR->GetRecordLength();
}

//////////////////////////////////////////////////////////////////////////

void IFF_Layer5::SetDataRecords( const std::vector<StdVarPtr> & DRS )
{
	m_vStdVarRecs = DRS;

    // Reset the PDU length.
	m_ui16LayerLength = IFF_LAYER5_SIZE;

    // Calculate the new length.
    vector<StdVarPtr>::const_iterator citr = m_vStdVarRecs.begin();
    vector<StdVarPtr>::const_iterator citrEnd = m_vStdVarRecs.end();
    for( ; citr != citrEnd; ++citr )
    {
        m_ui16LayerLength += ( *citr )->GetRecordLength();
    }

    m_ui16NumIffRecs = m_vStdVarRecs.size();
}

//////////////////////////////////////////////////////////////////////////

const std::vector<StdVarPtr> & IFF_Layer5::GetDataRecords() const
{
	return m_vStdVarRecs;
}

//////////////////////////////////////////////////////////////////////////

void IFF_Layer5::ClearDataRecords()
{
	// Reset the length.
	m_ui16LayerLength = IFF_LAYER5_SIZE;

    m_vStdVarRecs.clear();
    m_ui16NumIffRecs = 0;
}

//////////////////////////////////////////////////////////////////////////

KUINT16 IFF_Layer5::GetNumberDataRecords() const
{
	return m_ui16NumIffRecs;
}

//////////////////////////////////////////////////////////////////////////

const KOCTET* IFF_Layer5::GetAsString() const
{
    KStringStream ss;

    ss << "IFF Layer 5\n"
	   << LayerHeader::GetAsString()
       << "Reporting Simulation: " << m_RptSim.GetAsString()
       << IndentString( m_ui8ApplicableLayers.GetAsString(), 1 )
       << "Data category:        " << GetEnumAsStringDataCategory( m_ui8DataCategory ) << "\n"
       << "Num IFF Records: "      << m_ui16NumIffRecs 
       << "\nIFF Records:\n";

    vector<KDIS::DATA_TYPE::StdVarPtr>::const_iterator citr = m_vStdVarRecs.begin();
    vector<KDIS::DATA_TYPE::StdVarPtr>::const_iterator citrEnd = m_vStdVarRecs.end();
    for( ; citr != citrEnd; ++citr )
    {
		ss << ( *citr )->GetAsString();
    }

    return ss.str().c_str();
}

//////////////////////////////////////////////////////////////////////////

void IFF_Layer5::Decode(KDataStream & stream)
{
	LayerHeader::Decode( stream );
}

//////////////////////////////////////////////////////////////////////////

void IFF_Layer5::Decode( KDataStream & stream, bool ignoreHeader /*= true*/ ) 
{
    if( stream.GetBufferSize()  +  ( ignoreHeader ? LayerHeader::LAYER_HEADER_SIZE : 0 ) < IFF_LAYER5_SIZE )throw KException( __FUNCTION__, NOT_ENOUGH_DATA_IN_BUFFER );

    m_vStdVarRecs.clear();

	if( !ignoreHeader )
	{
		LayerHeader::Decode( stream );
	}

    stream >> KDIS_STREAM m_RptSim
           >> m_ui16Padding_1
           >> KDIS_STREAM m_ui8ApplicableLayers
           >> m_ui8DataCategory
           >> m_ui16Padding_2
           >> m_ui16NumIffRecs;

    // Use the factory decode function for each standard variable
    for( KUINT16 i = 0; i < m_ui16NumIffRecs; ++i )
    {
        // NIKOKO Pour gerer les enum inconnu dans les standard vars, ca peut retourner null
        // Dans ce cas on le push pas
        //m_vStdVarRecs.push_back( StandardVariable::FactoryDecodeStandardVariable( stream ) );
        StdVarPtr decodedStdVar = StandardVariable::FactoryDecodeStandardVariable( stream );
        if (decodedStdVar != nullptr)
        {
            m_vStdVarRecs.push_back( decodedStdVar );
        }
    }
}

//////////////////////////////////////////////////////////////////////////

void IFF_Layer5::EncodeToStream( KDataStream & stream ) const
{
	LayerHeader::EncodeToStream( stream );

    stream << KDIS_STREAM m_RptSim
           << m_ui16Padding_1
           << KDIS_STREAM m_ui8ApplicableLayers
           << m_ui8DataCategory
           << m_ui16Padding_2
           << m_ui16NumIffRecs;

    vector<KDIS::DATA_TYPE::StdVarPtr>::const_iterator citr = m_vStdVarRecs.begin();
    vector<KDIS::DATA_TYPE::StdVarPtr>::const_iterator citrEnd = m_vStdVarRecs.end();
    for( ; citr != citrEnd; ++citr )
    {
        ( *citr )->EncodeToStream( stream );
    }
}

//////////////////////////////////////////////////////////////////////////

KBOOL IFF_Layer5::operator == ( const IFF_Layer5 & Value ) const
{
    if( LayerHeader::operator !=( Value ) )                         return false;    
    if( m_RptSim              != Value.m_RptSim )                   return false;
    if( m_ui8ApplicableLayers != Value.m_ui8ApplicableLayers )      return false;
    if( m_ui8DataCategory     != Value.m_ui8DataCategory )          return false;
    if( m_ui16NumIffRecs      != Value.m_ui16NumIffRecs )           return false;
    if( m_vStdVarRecs         != Value.m_vStdVarRecs )              return false;
    return true;
}

//////////////////////////////////////////////////////////////////////////

KBOOL IFF_Layer5::operator != ( const IFF_Layer5 & Value ) const
{
    return !( *this == Value );
}

//////////////////////////////////////////////////////////////////////////
