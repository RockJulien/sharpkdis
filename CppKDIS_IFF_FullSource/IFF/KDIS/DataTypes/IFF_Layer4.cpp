#include "./IFF_Layer4.h"

//////////////////////////////////////////////////////////////////////////

using namespace std;
using namespace KDIS;
using namespace DATA_TYPE;
using namespace UTILS;

//////////////////////////////////////////////////////////////////////////
// public:
//////////////////////////////////////////////////////////////////////////

IFF_Layer4::IFF_Layer4() :
	m_ui16Padding( 0 ),
	m_ui16NumIffRecs( 0 )	
{
	m_ui8LayerNumber = 4;
	m_ui16LayerLength = IFF_LAYER4_SIZE;
}

//////////////////////////////////////////////////////////////////////////

IFF_Layer4::IFF_Layer4( const LayerHeader & H ) :
	LayerHeader( H )
{
}

//////////////////////////////////////////////////////////////////////////

IFF_Layer4::~IFF_Layer4()
{
}

//////////////////////////////////////////////////////////////////////////

void IFF_Layer4::SetReportingSimulation( const SimulationIdentifier & RS )
{
	m_RptSim = RS;
}

//////////////////////////////////////////////////////////////////////////

const SimulationIdentifier & IFF_Layer4::GetReportingSimulation() const
{
	return m_RptSim;
}

//////////////////////////////////////////////////////////////////////////

SimulationIdentifier & IFF_Layer4::GetReportingSimulation()
{
	return m_RptSim;
}

//////////////////////////////////////////////////////////////////////////

void IFF_Layer4::AddDataRecord( StdVarPtr DR )
{
	m_vStdVarRecs.push_back( DR );
	++m_ui16NumIffRecs;
    m_ui16LayerLength += DR->GetRecordLength();
}

//////////////////////////////////////////////////////////////////////////

void IFF_Layer4::SetDataRecords( const std::vector<StdVarPtr> & DRS )
{
	m_vStdVarRecs = DRS;

    // Reset the PDU length.
	m_ui16LayerLength = IFF_LAYER4_SIZE;

    // Calculate the new length.
    vector<StdVarPtr>::const_iterator citr = m_vStdVarRecs.begin();
    vector<StdVarPtr>::const_iterator citrEnd = m_vStdVarRecs.end();
    for( ; citr != citrEnd; ++citr )
    {
        m_ui16LayerLength += ( *citr )->GetRecordLength();
    }

    m_ui16NumIffRecs = m_vStdVarRecs.size();
}

//////////////////////////////////////////////////////////////////////////

const std::vector<StdVarPtr> & IFF_Layer4::GetDataRecords() const
{
	return m_vStdVarRecs;
}

//////////////////////////////////////////////////////////////////////////

void IFF_Layer4::ClearDataRecords()
{
	// Reset the length.
	m_ui16LayerLength = IFF_LAYER4_SIZE;

    m_vStdVarRecs.clear();
    m_ui16NumIffRecs = 0;
}

//////////////////////////////////////////////////////////////////////////

KUINT16 IFF_Layer4::GetNumberDataRecords() const
{
	return m_ui16NumIffRecs;
}

//////////////////////////////////////////////////////////////////////////

KBOOL IFF_Layer4::operator == ( const IFF_Layer4 & Value ) const
{
    if( LayerHeader::operator !=( Value ) )            return false;    
    if( m_RptSim              != Value.m_RptSim )      return false;
    if( m_vStdVarRecs         != Value.m_vStdVarRecs ) return false;
    return true;
}

//////////////////////////////////////////////////////////////////////////

KBOOL IFF_Layer4::operator != ( const IFF_Layer4 & Value ) const
{
    return !( *this == Value );
}

//////////////////////////////////////////////////////////////////////////
