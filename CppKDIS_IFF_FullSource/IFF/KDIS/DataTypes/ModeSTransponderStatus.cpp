#include "./ModeSTransponderStatus.h"

//////////////////////////////////////////////////////////////////////////

using namespace std;
using namespace KDIS;
using namespace DATA_TYPE;
using namespace ENUMS;

//////////////////////////////////////////////////////////////////////////
// public:
//////////////////////////////////////////////////////////////////////////

ModeSTransponderStatus::ModeSTransponderStatus() 
{
	m_StatusUnion.m_ui16Status = 0;	
}

//////////////////////////////////////////////////////////////////////////

ModeSTransponderStatus::ModeSTransponderStatus( KDataStream & stream ) 
{
    Decode( stream );
}

//////////////////////////////////////////////////////////////////////////

ModeSTransponderStatus::ModeSTransponderStatus( KBOOL SquitterStatus, KDIS::DATA_TYPE::ENUMS::SquitterType ST, KBOOL RecordSource,
	                                            KBOOL AirbonePositionReportIndicator, KBOOL AirborneVelocityReportIndicator, KBOOL SurfacePositionReportIndicator, KBOOL IdentificationReportIndicator,
                                                KBOOL EventDrivenReportIndicator, KBOOL Status, KBOOL Dmg, KBOOL Malfnc )
{	
	m_StatusUnion.m_ui16SquitterStatus = SquitterStatus;
	m_StatusUnion.m_ui16SquitterType = ST;      
	m_StatusUnion.m_ui16SquitterRecordSource = RecordSource;
	m_StatusUnion.m_ui16AirbornePositionReportIndicator = AirbonePositionReportIndicator; 
	m_StatusUnion.m_ui16AirborneVelocityReportIndicator = AirborneVelocityReportIndicator;
	m_StatusUnion.m_ui16SurfacePositionReportIndicator = SurfacePositionReportIndicator;
	m_StatusUnion.m_ui16IdentificationReportIndicator = IdentificationReportIndicator;
	m_StatusUnion.m_ui16EventDrivenReportIndicator = EventDrivenReportIndicator;
	m_StatusUnion.m_ui16Padding = 0;
	m_StatusUnion.m_ui16OnOff = Status;
	m_StatusUnion.m_ui16Dmg = Dmg;          
	m_StatusUnion.m_ui16MalFnc = Malfnc;   	
}

//////////////////////////////////////////////////////////////////////////

ModeSTransponderStatus::~ModeSTransponderStatus()
{
}

//////////////////////////////////////////////////////////////////////////

void ModeSTransponderStatus::SetSquitterStatus( KBOOL SquitterStatus ) 
{	
	m_StatusUnion.m_ui16SquitterStatus = SquitterStatus;
}

//////////////////////////////////////////////////////////////////////////

KBOOL ModeSTransponderStatus::GetSquitterStatus() const
{
	return ( KBOOL )m_StatusUnion.m_ui16SquitterStatus;
}

//////////////////////////////////////////////////////////////////////////

void ModeSTransponderStatus::SetSquitterType( KDIS::DATA_TYPE::ENUMS::SquitterType ST ) 
{
	m_StatusUnion.m_ui16SquitterType = ST;
}

//////////////////////////////////////////////////////////////////////////

SquitterType ModeSTransponderStatus::GetSquitterType() const
{
	return ( SquitterType )m_StatusUnion.m_ui16SquitterType;
}

//////////////////////////////////////////////////////////////////////////

void ModeSTransponderStatus::SetRecordSource( KBOOL RecordSource )
{
	m_StatusUnion.m_ui16SquitterRecordSource = RecordSource;
}

//////////////////////////////////////////////////////////////////////////

KBOOL ModeSTransponderStatus::GetRecordSource() const
{
	return ( KBOOL )m_StatusUnion.m_ui16SquitterRecordSource;
}

//////////////////////////////////////////////////////////////////////////

void ModeSTransponderStatus::SetAirbonePositionReportIndicator( KBOOL AirbonePositionReportIndicator )
{
	m_StatusUnion.m_ui16AirbornePositionReportIndicator = AirbonePositionReportIndicator;
}

//////////////////////////////////////////////////////////////////////////

KBOOL ModeSTransponderStatus::GetAirbonePositionReportIndicator() const
{
	return ( KBOOL )m_StatusUnion.m_ui16AirbornePositionReportIndicator;
}

//////////////////////////////////////////////////////////////////////////

void ModeSTransponderStatus::SetAirborneVelocityReportIndicator( KBOOL AirborneVelocityReportIndicator )
{
	m_StatusUnion.m_ui16AirborneVelocityReportIndicator = AirborneVelocityReportIndicator;
}

//////////////////////////////////////////////////////////////////////////

KBOOL ModeSTransponderStatus::GetAirborneVelocityReportIndicator() const
{
	return ( KBOOL )m_StatusUnion.m_ui16AirborneVelocityReportIndicator;
}

//////////////////////////////////////////////////////////////////////////

void ModeSTransponderStatus::SetSurfacePositionReportIndicator( KBOOL SurfacePositionReportIndicator )
{
	m_StatusUnion.m_ui16SurfacePositionReportIndicator = SurfacePositionReportIndicator;
}

//////////////////////////////////////////////////////////////////////////

KBOOL ModeSTransponderStatus::GetSurfacePositionReportIndicator() const
{
	return ( KBOOL )m_StatusUnion.m_ui16SurfacePositionReportIndicator;
}

//////////////////////////////////////////////////////////////////////////

void ModeSTransponderStatus::SetIdentificationReportIndicator( KBOOL IdentificationReportIndicator )
{
	m_StatusUnion.m_ui16IdentificationReportIndicator = IdentificationReportIndicator;
}

//////////////////////////////////////////////////////////////////////////

KBOOL ModeSTransponderStatus::GetIdentificationReportIndicator() const
{
	return ( KBOOL )m_StatusUnion.m_ui16IdentificationReportIndicator;
}

//////////////////////////////////////////////////////////////////////////

void ModeSTransponderStatus::SetEventDrivenReportIndicator( KBOOL EventDrivenReportIndicator )
{
	m_StatusUnion.m_ui16EventDrivenReportIndicator = EventDrivenReportIndicator;
}

//////////////////////////////////////////////////////////////////////////

KBOOL ModeSTransponderStatus::GetEventDrivenReportIndicator() const
{
	return ( KBOOL )m_StatusUnion.m_ui16EventDrivenReportIndicator;
}

//////////////////////////////////////////////////////////////////////////

void ModeSTransponderStatus::SetStatus( KBOOL S )
{
	m_StatusUnion.m_ui16OnOff = S;
}

//////////////////////////////////////////////////////////////////////////

KBOOL ModeSTransponderStatus::GetStatus() const
{
	return ( KBOOL )m_StatusUnion.m_ui16OnOff;
}

//////////////////////////////////////////////////////////////////////////

void ModeSTransponderStatus::SetDamaged( KBOOL D )
{
	m_StatusUnion.m_ui16Dmg = D;
}

//////////////////////////////////////////////////////////////////////////

KBOOL ModeSTransponderStatus::IsDamaged() const
{
	return ( KBOOL )m_StatusUnion.m_ui16Dmg;
}

//////////////////////////////////////////////////////////////////////////

void ModeSTransponderStatus::SetMalfunctioning( KBOOL M )
{
	m_StatusUnion.m_ui16MalFnc = M;
}

//////////////////////////////////////////////////////////////////////////

KBOOL ModeSTransponderStatus::IsMalfunctioning() const
{
	return ( KBOOL )m_StatusUnion.m_ui16MalFnc;
}

//////////////////////////////////////////////////////////////////////////

const KOCTET* ModeSTransponderStatus::GetAsString() const
{
    KStringStream ss;	

	ss << "Mode S Transponder Status:"
	   << "\n\tSquitter Status:                   " << ( KBOOL )m_StatusUnion.m_ui16SquitterStatus
	   << "\n\tSquitter Type:                     " << GetEnumAsStringSquitterType( m_StatusUnion.m_ui16SquitterType )
	   << "\n\tSquitter Record Source:            " << ( KBOOL )m_StatusUnion.m_ui16SquitterRecordSource
	   << "\n\tAirborne Position Report Indicator " << ( KBOOL )m_StatusUnion.m_ui16AirbornePositionReportIndicator
	   << "\n\tAirborne Velocity Report Indicator " << ( KBOOL )m_StatusUnion.m_ui16AirborneVelocityReportIndicator
	   << "\n\tSurface Position Report Indicator  " << ( KBOOL )m_StatusUnion.m_ui16SurfacePositionReportIndicator
	   << "\n\tIdentification Report Indicator    " << ( KBOOL )m_StatusUnion.m_ui16IdentificationReportIndicator
	   << "\n\tEvent Driven Report Indicator      " << ( KBOOL )m_StatusUnion.m_ui16EventDrivenReportIndicator
	   << "\n\tOn/Off Status:                     " << ( KBOOL )m_StatusUnion.m_ui16OnOff
	   << "\n\tDamaged:                           " << ( KBOOL )m_StatusUnion.m_ui16Dmg
	   << "\n\tMalfunction:                       " << ( KBOOL )m_StatusUnion.m_ui16MalFnc 
	   << "\n";

    return ss.str().c_str();
}

//////////////////////////////////////////////////////////////////////////

void ModeSTransponderStatus::Decode( KDataStream & stream ) 
{
    if( stream.GetBufferSize() < MODE_S_TRANSPONDER_STATUS_SIZE )throw KException( __FUNCTION__, NOT_ENOUGH_DATA_IN_BUFFER );
	
	stream >> m_StatusUnion.m_ui16Status;
}

//////////////////////////////////////////////////////////////////////////

const KDataStream* ModeSTransponderStatus::GetEncodedStream() const
{
	m_stream.Clear();

	this->EncodeToStream(m_stream);

	return &m_stream;
}

//////////////////////////////////////////////////////////////////////////

void ModeSTransponderStatus::EncodeToStream( KDataStream & stream ) const
{
	stream << m_StatusUnion.m_ui16Status;
}

//////////////////////////////////////////////////////////////////////////

KBOOL ModeSTransponderStatus::operator == ( const ModeSTransponderStatus & Value ) const
{
	if( m_StatusUnion.m_ui16Status != Value.m_StatusUnion.m_ui16Status ) return false;
    return true;
}

//////////////////////////////////////////////////////////////////////////

KBOOL ModeSTransponderStatus::operator != ( const ModeSTransponderStatus & Value ) const
{
    return !( *this == Value );
}

//////////////////////////////////////////////////////////////////////////
