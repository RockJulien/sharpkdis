
#include "./Mode5STransponderLocation.h"

//////////////////////////////////////////////////////////////////////////

using namespace KDIS;
using namespace DATA_TYPE;
using namespace ENUMS;

//////////////////////////////////////////////////////////////////////////
// Public:
//////////////////////////////////////////////////////////////////////////

Mode5STransponderLocation::Mode5STransponderLocation() :
    m_i32Latitude( 0 ),
    m_i32Longitude( 0 ),
    m_ui16Mode5Altitude( 0 ),
    m_ui16BarometricAltitude( 0 ),
    m_ui16Padding( 0 )
{
    m_ui32Type = Mode5STransponderLocationRecord;
    m_ui16Length = MODE_5S_TRANSPONDER_LOCATION_TYPE_SIZE;
}

//////////////////////////////////////////////////////////////////////////

Mode5STransponderLocation::Mode5STransponderLocation( KINT32 Latitude, KINT32 Longitude, KUINT16 Mode5Altitude, KUINT16 BarometricAltitude )  :
    m_i32Latitude( Latitude ),
    m_i32Longitude( Longitude ),
    m_ui16Mode5Altitude( Mode5Altitude ),
    m_ui16BarometricAltitude( BarometricAltitude ),
    m_ui16Padding( 0 )
{
    m_ui32Type = Mode5STransponderLocationRecord;
    m_ui16Length = MODE_5S_TRANSPONDER_LOCATION_TYPE_SIZE;
}

//////////////////////////////////////////////////////////////////////////

Mode5STransponderLocation::Mode5STransponderLocation( KDataStream & stream ) 
{
    Decode( stream );
}

//////////////////////////////////////////////////////////////////////////

Mode5STransponderLocation::~Mode5STransponderLocation()
{
}

//////////////////////////////////////////////////////////////////////////

void Mode5STransponderLocation::SetLatitude( KINT32 Latitude )
{
    m_i32Latitude = Latitude;
}

//////////////////////////////////////////////////////////////////////////

KINT32 Mode5STransponderLocation::GetLatitude() const
{
    return m_i32Latitude;
}

//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////

void Mode5STransponderLocation::SetLongitude( KINT32 Longitude )
{
    m_i32Longitude = Longitude;
}

//////////////////////////////////////////////////////////////////////////

KINT32 Mode5STransponderLocation::GetLongitude() const
{
    return m_i32Longitude;
}

//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////

void Mode5STransponderLocation::SetMode5Altitude( KUINT16 Mode5Altitude )
{
    m_ui16Mode5Altitude = Mode5Altitude;
}

//////////////////////////////////////////////////////////////////////////

KUINT16 Mode5STransponderLocation::GetMode5Altitude() const
{
    return m_ui16Mode5Altitude;
}

//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////

void Mode5STransponderLocation::SetBarometricAltitude( KUINT16 BarometricAltitude )
{
    m_ui16BarometricAltitude = BarometricAltitude;
}

//////////////////////////////////////////////////////////////////////////

KUINT16 Mode5STransponderLocation::GetBarometricAltitude() const
{
    return m_ui16BarometricAltitude;
}

//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////

const KOCTET* Mode5STransponderLocation::GetAsString() const
{
    KStringStream ss;

    ss << StandardVariable::GetAsString()
       << "Latitude:             " << m_i32Latitude    << "\n"
       << "Longitude:            " << m_i32Longitude   << "\n"
       << "Mode5Altitude:        " << m_ui16Mode5Altitude   << "\n"
       << "BarometricAltitude:   " << m_ui16BarometricAltitude   << "\n";

    return ss.str().c_str();
}

//////////////////////////////////////////////////////////////////////////

void Mode5STransponderLocation::Decode( KDataStream & stream ) 
{
    if( stream.GetBufferSize() < MODE_5S_TRANSPONDER_LOCATION_TYPE_SIZE )throw KException( __FUNCTION__, NOT_ENOUGH_DATA_IN_BUFFER );

    StandardVariable::Decode( stream );

    stream >> m_ui16Padding
           >> m_i32Latitude
           >> m_i32Longitude
           >> m_ui16Mode5Altitude
           >> m_ui16BarometricAltitude;
}

//////////////////////////////////////////////////////////////////////////

void Mode5STransponderLocation::EncodeToStream( KDataStream & stream ) const
{
    StandardVariable::EncodeToStream( stream );

    stream << m_ui16Padding
           << m_i32Latitude
           << m_i32Longitude
           << m_ui16Mode5Altitude
           << m_ui16BarometricAltitude;
}

//////////////////////////////////////////////////////////////////////////

KBOOL Mode5STransponderLocation::operator == ( const Mode5STransponderLocation & Value ) const
{
    if( StandardVariable::operator   !=( Value ) )                           return false;
    if( m_i32Latitude                != Value.m_i32Latitude )                return false;
    if( m_i32Longitude               != Value.m_i32Longitude )               return false;
    if( m_ui16Mode5Altitude          != Value.m_ui16Mode5Altitude )          return false;
    if( m_ui16BarometricAltitude     != Value.m_ui16BarometricAltitude )     return false;
    return true;
}

//////////////////////////////////////////////////////////////////////////

KBOOL Mode5STransponderLocation::operator != ( const Mode5STransponderLocation & Value ) const
{
    return !( *this == Value );
}

//////////////////////////////////////////////////////////////////////////
