#include "./ModeSLevelsPresent.h"

//////////////////////////////////////////////////////////////////////////

using namespace std;
using namespace KDIS;
using namespace DATA_TYPE;
using namespace ENUMS;

//////////////////////////////////////////////////////////////////////////
// public:
//////////////////////////////////////////////////////////////////////////

ModeSLevelsPresent::ModeSLevelsPresent() 
{
	m_ModeSLevelsPresentUnion.m_ui8ModeSLevelsPresent = 0;	
}

//////////////////////////////////////////////////////////////////////////

ModeSLevelsPresent::ModeSLevelsPresent( KDataStream & stream ) 
{
    Decode( stream );
}

//////////////////////////////////////////////////////////////////////////

ModeSLevelsPresent::ModeSLevelsPresent( KBOOL Level1, KBOOL Level2_ElementarySurveillanceSublevel, KBOOL Level2_EnhancedSurveillanceSublevel, KBOOL Level3, KBOOL Level4 )
{	
	m_ModeSLevelsPresentUnion.m_uiLevel1                                = Level1;
	m_ModeSLevelsPresentUnion.m_uiLevel2_ElementarySurveillanceSublevel = Level2_ElementarySurveillanceSublevel;
	m_ModeSLevelsPresentUnion.m_uiLevel2_EnhancedSurveillanceSublevel   = Level2_EnhancedSurveillanceSublevel;
	m_ModeSLevelsPresentUnion.m_uiLevel3                                = Level3;
	m_ModeSLevelsPresentUnion.m_uiLevel4                                = Level4;
}

//////////////////////////////////////////////////////////////////////////

ModeSLevelsPresent::~ModeSLevelsPresent()
{
}

//////////////////////////////////////////////////////////////////////////

void ModeSLevelsPresent::SetLevel1( KBOOL Level1 ) 
{	
	m_ModeSLevelsPresentUnion.m_uiLevel1 = Level1;
}

//////////////////////////////////////////////////////////////////////////

KBOOL ModeSLevelsPresent::GetLevel1() const
{
	return ( KBOOL )m_ModeSLevelsPresentUnion.m_uiLevel1;
}

//////////////////////////////////////////////////////////////////////////

void ModeSLevelsPresent::SetLevel2_ElementarySurveillanceSublevel( KBOOL Level2_ElementarySurveillanceSublevel ) 
{
	m_ModeSLevelsPresentUnion.m_uiLevel2_ElementarySurveillanceSublevel = Level2_ElementarySurveillanceSublevel;
}

//////////////////////////////////////////////////////////////////////////

KBOOL ModeSLevelsPresent::GetLevel2_ElementarySurveillanceSublevel() const
{
	return ( KBOOL )m_ModeSLevelsPresentUnion.m_uiLevel2_ElementarySurveillanceSublevel;
}

//////////////////////////////////////////////////////////////////////////

void ModeSLevelsPresent::SetLevel2_EnhancedSurveillanceSublevel( KBOOL Level2_EnhancedSurveillanceSublevel )
{
	m_ModeSLevelsPresentUnion.m_uiLevel2_EnhancedSurveillanceSublevel = Level2_EnhancedSurveillanceSublevel;
}

//////////////////////////////////////////////////////////////////////////

KBOOL ModeSLevelsPresent::GetLevel2_EnhancedSurveillanceSublevel() const
{
	return ( KBOOL )m_ModeSLevelsPresentUnion.m_uiLevel2_EnhancedSurveillanceSublevel;
}

//////////////////////////////////////////////////////////////////////////

void ModeSLevelsPresent::SetLevel3( KBOOL Level3 )
{
	m_ModeSLevelsPresentUnion.m_uiLevel3 = Level3;
}

//////////////////////////////////////////////////////////////////////////

KBOOL ModeSLevelsPresent::GetLevel3() const
{
	return ( KBOOL )m_ModeSLevelsPresentUnion.m_uiLevel3;
}

//////////////////////////////////////////////////////////////////////////

void ModeSLevelsPresent::SetLevel4( KBOOL Level4 )
{
	m_ModeSLevelsPresentUnion.m_uiLevel4 = Level4;
}

//////////////////////////////////////////////////////////////////////////

KBOOL ModeSLevelsPresent::GetLevel4() const
{
	return ( KBOOL )m_ModeSLevelsPresentUnion.m_uiLevel4;
}

//////////////////////////////////////////////////////////////////////////

const KOCTET* ModeSLevelsPresent::GetAsString() const
{
    KStringStream ss;	

	ss << "Mode S Levels Present:"
	   << "\n\tLevel1:                                " << ( KBOOL )m_ModeSLevelsPresentUnion.m_uiLevel1
	   << "\n\tLevel2_ElementarySurveillanceSublevel: " << ( KBOOL )m_ModeSLevelsPresentUnion.m_uiLevel2_ElementarySurveillanceSublevel
	   << "\n\tLevel2_EnhancedSurveillanceSublevel:   " << ( KBOOL )m_ModeSLevelsPresentUnion.m_uiLevel2_EnhancedSurveillanceSublevel
	   << "\n\tLevel3:                                " << ( KBOOL )m_ModeSLevelsPresentUnion.m_uiLevel3
	   << "\n\tLevel4:                                " << ( KBOOL )m_ModeSLevelsPresentUnion.m_uiLevel4
	   << "\n";

    return ss.str().c_str();
}

//////////////////////////////////////////////////////////////////////////

void ModeSLevelsPresent::Decode( KDataStream & stream ) 
{
    if( stream.GetBufferSize() < MODE_S_LEVELS_PRESENT_SIZE )throw KException( __FUNCTION__, NOT_ENOUGH_DATA_IN_BUFFER );
	
	stream >> m_ModeSLevelsPresentUnion.m_ui8ModeSLevelsPresent;
}

//////////////////////////////////////////////////////////////////////////

const KDataStream* ModeSLevelsPresent::GetEncodedStream() const
{
	m_stream.Clear();

	this->EncodeToStream(m_stream);

	return &m_stream;
}

//////////////////////////////////////////////////////////////////////////

void ModeSLevelsPresent::EncodeToStream( KDataStream & stream ) const
{
	stream << m_ModeSLevelsPresentUnion.m_ui8ModeSLevelsPresent;
}

//////////////////////////////////////////////////////////////////////////

KBOOL ModeSLevelsPresent::operator == ( const ModeSLevelsPresent & Value ) const
{
	if( m_ModeSLevelsPresentUnion.m_ui8ModeSLevelsPresent != Value.m_ModeSLevelsPresentUnion.m_ui8ModeSLevelsPresent ) return false;
    return true;
}

//////////////////////////////////////////////////////////////////////////

KBOOL ModeSLevelsPresent::operator != ( const ModeSLevelsPresent & Value ) const
{
    return !( *this == Value );
}

//////////////////////////////////////////////////////////////////////////
