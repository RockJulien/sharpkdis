#include "./InformationLayers.h"

//////////////////////////////////////////////////////////////////////////

using namespace std;
using namespace KDIS;
using namespace DATA_TYPE;
using namespace ENUMS;

//////////////////////////////////////////////////////////////////////////
// public:
//////////////////////////////////////////////////////////////////////////

InformationLayers::InformationLayers() 
{
	m_InformationLayersUnion.m_ui8InformationLayers = 0;	
}

//////////////////////////////////////////////////////////////////////////

InformationLayers::InformationLayers( KDataStream & stream ) 
{
    Decode( stream );
}

//////////////////////////////////////////////////////////////////////////

InformationLayers::InformationLayers( KBOOL Layer1, KBOOL Layer2, KBOOL Layer3, KBOOL Layer4, KBOOL Layer5, KBOOL Layer6, KBOOL Layer7 )
{	
	m_InformationLayersUnion.m_uiLayer1 = Layer1;
	m_InformationLayersUnion.m_uiLayer2 = Layer2;
	m_InformationLayersUnion.m_uiLayer3 = Layer3;
	m_InformationLayersUnion.m_uiLayer4 = Layer4;
	m_InformationLayersUnion.m_uiLayer5 = Layer5;
	m_InformationLayersUnion.m_uiLayer6 = Layer6;
	m_InformationLayersUnion.m_uiLayer7 = Layer7;
}

//////////////////////////////////////////////////////////////////////////

InformationLayers::~InformationLayers()
{
}

//////////////////////////////////////////////////////////////////////////

void InformationLayers::SetLayer1( KBOOL Layer1 ) 
{	
	m_InformationLayersUnion.m_uiLayer1 = Layer1;
}

//////////////////////////////////////////////////////////////////////////

KBOOL InformationLayers::GetLayer1() const
{
	return ( KBOOL )m_InformationLayersUnion.m_uiLayer1;
}

//////////////////////////////////////////////////////////////////////////

void InformationLayers::SetLayer2( KBOOL Layer2 ) 
{	
	m_InformationLayersUnion.m_uiLayer2 = Layer2;
}

//////////////////////////////////////////////////////////////////////////

KBOOL InformationLayers::GetLayer2() const
{
	return ( KBOOL )m_InformationLayersUnion.m_uiLayer2;
}

//////////////////////////////////////////////////////////////////////////

void InformationLayers::SetLayer3( KBOOL Layer3 ) 
{	
	m_InformationLayersUnion.m_uiLayer3 = Layer3;
}

//////////////////////////////////////////////////////////////////////////

KBOOL InformationLayers::GetLayer3() const
{
	return ( KBOOL )m_InformationLayersUnion.m_uiLayer3;
}

//////////////////////////////////////////////////////////////////////////

void InformationLayers::SetLayer4( KBOOL Layer4 ) 
{	
	m_InformationLayersUnion.m_uiLayer4 = Layer4;
}

//////////////////////////////////////////////////////////////////////////

KBOOL InformationLayers::GetLayer4() const
{
	return ( KBOOL )m_InformationLayersUnion.m_uiLayer4;
}

//////////////////////////////////////////////////////////////////////////

void InformationLayers::SetLayer5( KBOOL Layer5 ) 
{	
	m_InformationLayersUnion.m_uiLayer5 = Layer5;
}

//////////////////////////////////////////////////////////////////////////

KBOOL InformationLayers::GetLayer5() const
{
	return ( KBOOL )m_InformationLayersUnion.m_uiLayer5;
}

//////////////////////////////////////////////////////////////////////////

void InformationLayers::SetLayer6( KBOOL Layer6 ) 
{	
	m_InformationLayersUnion.m_uiLayer6 = Layer6;
}

//////////////////////////////////////////////////////////////////////////

KBOOL InformationLayers::GetLayer6() const
{
	return ( KBOOL )m_InformationLayersUnion.m_uiLayer6;
}

//////////////////////////////////////////////////////////////////////////

void InformationLayers::SetLayer7( KBOOL Layer7 ) 
{	
	m_InformationLayersUnion.m_uiLayer7 = Layer7;
}

//////////////////////////////////////////////////////////////////////////

KBOOL InformationLayers::GetLayer7() const
{
	return ( KBOOL )m_InformationLayersUnion.m_uiLayer7;
}

//////////////////////////////////////////////////////////////////////////

const KOCTET* InformationLayers::GetAsString() const
{
    KStringStream ss;	

	ss << "Information Layers:"
	   << "\n\tLLayer1: " << ( KBOOL )m_InformationLayersUnion.m_uiLayer1
	   << "\n\tLLayer2: " << ( KBOOL )m_InformationLayersUnion.m_uiLayer2
	   << "\n\tLLayer3: " << ( KBOOL )m_InformationLayersUnion.m_uiLayer3
	   << "\n\tLLayer4: " << ( KBOOL )m_InformationLayersUnion.m_uiLayer4
	   << "\n\tLLayer5: " << ( KBOOL )m_InformationLayersUnion.m_uiLayer5
	   << "\n\tLLayer6: " << ( KBOOL )m_InformationLayersUnion.m_uiLayer6
	   << "\n\tLLayer7: " << ( KBOOL )m_InformationLayersUnion.m_uiLayer7
	   << "\n";

    return ss.str().c_str();
}

//////////////////////////////////////////////////////////////////////////

void InformationLayers::Decode( KDataStream & stream ) 
{
    if( stream.GetBufferSize() < INFORMATION_LAYERS_SIZE )throw KException( __FUNCTION__, NOT_ENOUGH_DATA_IN_BUFFER );
	
	stream >> m_InformationLayersUnion.m_ui8InformationLayers;
}

//////////////////////////////////////////////////////////////////////////

const KDataStream* InformationLayers::GetEncodedStream() const
{
	m_stream.Clear();

	this->EncodeToStream(m_stream);

	return &m_stream;
}

//////////////////////////////////////////////////////////////////////////

void InformationLayers::EncodeToStream( KDataStream & stream ) const
{
	stream << m_InformationLayersUnion.m_ui8InformationLayers;
}

//////////////////////////////////////////////////////////////////////////

KBOOL InformationLayers::operator == ( const InformationLayers & Value ) const
{
	if( m_InformationLayersUnion.m_ui8InformationLayers != Value.m_InformationLayersUnion.m_ui8InformationLayers ) return false;
    return true;
}

//////////////////////////////////////////////////////////////////////////

KBOOL InformationLayers::operator != ( const InformationLayers & Value ) const
{
    return !( *this == Value );
}

//////////////////////////////////////////////////////////////////////////
