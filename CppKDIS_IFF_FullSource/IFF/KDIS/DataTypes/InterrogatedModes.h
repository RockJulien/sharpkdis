/********************************************************************
    class:      InterrogatedModes
    DIS:        (7) 1278.1 - 2012
    created:    02/04/2019
    author:     Nicolas Duboys

    purpose:    The specific modes and other basic data types related 
                to an interactive interrogation, reply, or squitter
                transmission shall be communicated using the Interrogated 
                Modes record. This is a non-self-identifying record that 
                is contained in the Basic Interactive IFF Data record.

    Size:       16 bits / 2 octet
*********************************************************************/

#pragma once

#include "./DataTypeBase.h"

namespace KDIS {
namespace DATA_TYPE {

class KDIS_EXPORT InterrogatedModes : public DataTypeBase
{
protected:

	union
	{
		struct
		{
			KUINT16 m_ui16NotUsed        : 1; 
			KUINT16 m_ui16Mode1          : 1;
			KUINT16 m_ui16Mode2          : 1;
			KUINT16 m_ui16Mode3A         : 1;
			KUINT16 m_ui16Mode4          : 1;
			KUINT16 m_ui16Mode5          : 1;
			KUINT16 m_ui16ModeS          : 1;
			KUINT16 m_ui16ModeCAlt       : 1;
			KUINT16 m_ui16TCAS_ACAS      : 1;
			KUINT16 m_ui16SpecialIP      : 1;
			KUINT16 m_ui16Squitter       : 1;
			KUINT16 m_ui16Padding        : 5;
		};
		KUINT16 m_ui16InterrogatedModes;

	} m_InterrogatedModesUnion;

	mutable KDataStream m_stream;
		
public:

    static const KUINT16 INTERROGATED_MODES_SIZE = 2; 

    InterrogatedModes();

    InterrogatedModes( KDataStream & stream ) ;

	InterrogatedModes( KBOOL Mode1, KBOOL Mode2, KBOOL Mode3A, KBOOL Mode4, KBOOL Mode5, KBOOL ModeS, KBOOL ModeCAlt,
                            KBOOL TCAS_ACAS, KBOOL SpecialIP, KBOOL Squitter );
	
    virtual ~InterrogatedModes();

    //************************************
    // FullName:    KDIS::DATA_TYPE::InterrogatedModes::SetMode1
    //              KDIS::DATA_TYPE::InterrogatedModes::GetMode1
    // Description: Specify if Mode 1 is being interrogated or contained 
    //              in a transponder reply. It shall be represented by a 
    //              1-bit enumeration value set to Not Included (false) or Included (true).
    // Parameter:   KBOOL Mode1
    //************************************
	void SetMode1( KBOOL Mode1 );
	KBOOL GetMode1() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::InterrogatedModes::SetMode2
    //              KDIS::DATA_TYPE::InterrogatedModes::GetMode2
    // Description: Specify if Mode 2 is being interrogated or contained 
    //              in a transponder reply. It shall be represented by a 
    //              1-bit enumeration value set to Not Included (false) or Included (true).
    // Parameter:   KBOOL Mode2
    //************************************
	void SetMode2( KBOOL Mode2 );
	KBOOL GetMode2() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::InterrogatedModes::SetMode3A
    //              KDIS::DATA_TYPE::InterrogatedModes::GetMode3A
    // Description: Specify if Mode 3A is being interrogated or contained 
    //              in a transponder reply. It shall be represented by a 
    //              1-bit enumeration value set to Not Included (false) or Included (true).
    // Parameter:   KBOOL Mode3A
    //************************************
	void SetMode3A( KBOOL Mode3A );
	KBOOL GetMode3A() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::InterrogatedModes::SetMode4
    //              KDIS::DATA_TYPE::InterrogatedModes::GetMode4
    // Description: Specify if Mode 4 is being interrogated or contained 
    //              in a transponder reply. It shall be represented by a 
    //              1-bit enumeration value set to Not Included (false) or Included (true).
    // Parameter:   KBOOL Mode4
    //************************************
	void SetMode4( KBOOL Mode4 );
	KBOOL GetMode4() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::InterrogatedModes::SetMode5
    //              KDIS::DATA_TYPE::InterrogatedModes::GetMode5
    // Description: Specify if Mode 5 is being interrogated or contained 
    //              in a transponder reply. It shall be represented by a 
    //              1-bit enumeration value set to Not Included (false) or Included (true).
    // Parameter:   KBOOL Mode5
    //************************************
	void SetMode5( KBOOL Mode5 );
	KBOOL GetMode5() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::InterrogatedModes::SetModeS
    //              KDIS::DATA_TYPE::InterrogatedModes::GetModeS
    // Description: Specify if Mode S is being interrogated or contained 
    //              in a transponder reply. It shall be represented by a 
    //              1-bit enumeration value set to Not Included (false) or Included (true).
    // Parameter:   KBOOL ModeS
    //************************************
	void SetModeS( KBOOL ModeS );
	KBOOL GetModeS() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::InterrogatedModes::SetModeCAlt
    //              KDIS::DATA_TYPE::InterrogatedModes::GetModeCAlt
    // Description: Specify if Mode CAlt is being interrogated or contained 
    //              in a transponder reply. It shall be represented by a 
    //              1-bit enumeration value set to Not Included (false) or Included (true).
    // Parameter:   KBOOL ModeCAlt
    //************************************
	void SetModeCAlt( KBOOL ModeCAlt );
	KBOOL GetModeCAlt() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::InterrogatedModes::SetTCAS_ACAS
    //              KDIS::DATA_TYPE::InterrogatedModes::GetTCAS_ACAS
    // Description: Specify if TCAS/ACAS is being interrogated or contained in a
    //              transponder reply. It shall be represented by a 
    //              1-bit enumeration value set to Not Included (false) or Included (true).
    // Parameter:   KBOOL TCAS_ACAS
    //************************************
	void SetTCAS_ACAS( KBOOL TCAS_ACAS );
	KBOOL GetTCAS_ACAS() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::InterrogatedModes::SetSpecialIP
    //              KDIS::DATA_TYPE::InterrogatedModes::GetSpecialIP
    // Description: Specify if this is a Special Identification of Position (IP) transmission. 
    //              It shall be represented by a 1-bit enumeration value set to Not Included (false) or Included (true).
    // Parameter:   KBOOL SpecialIP
    //************************************
	void SetSpecialIP( KBOOL SpecialIP );
	KBOOL GetSpecialIP() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::InterrogatedModes::SetSquitter
    //              KDIS::DATA_TYPE::InterrogatedModes::GetSquitter
    // Description: Specify if this is an interactive squitter transmission.. 
    //              It shall be represented by a 1-bit enumeration value set to Not Included (false) or Included (true).
    // Parameter:   KBOOL Squitter
    //************************************
	void SetSquitter( KBOOL Squitter );
	KBOOL GetSquitter() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::InterrogatedModes::GetAsString
    // Description: Returns a string representation 
    //************************************
    virtual const KOCTET* GetAsString() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::InterrogatedModes::Decode
    // Description: Convert From Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual void Decode( KDataStream & stream ) ;

    //************************************
    // FullName:    KDIS::DATA_TYPE::InterrogatedModes::Encode
    // Description: Convert To Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual const KDataStream* GetEncodedStream() const;
    virtual void EncodeToStream( KDataStream & stream ) const;

    KBOOL operator == ( const InterrogatedModes & Value ) const;
    KBOOL operator != ( const InterrogatedModes & Value ) const;
};

} // END namespace DATA_TYPE
} // END namespace KDIS
