#include "./ModeSAltitude.h"

//////////////////////////////////////////////////////////////////////////

using namespace std;
using namespace KDIS;
using namespace DATA_TYPE;
using namespace ENUMS;

//////////////////////////////////////////////////////////////////////////
// public:
//////////////////////////////////////////////////////////////////////////

ModeSAltitude::ModeSAltitude() 
{
	m_ModeSAltitudeUnion.m_ui16ModeSAltitude = 0;	
}

//////////////////////////////////////////////////////////////////////////

ModeSAltitude::ModeSAltitude( KDataStream & stream ) 
{
    Decode( stream );
}

//////////////////////////////////////////////////////////////////////////

ModeSAltitude::ModeSAltitude( KUINT16 Altitude, KUINT16 AltitudeResolution )
{	
	m_ModeSAltitudeUnion.m_ui16Altitude           = Altitude;
	m_ModeSAltitudeUnion.m_ui16AltitudeResolution = (KDIS::DATA_TYPE::ENUMS::AltitudeResolution)AltitudeResolution;
}

//////////////////////////////////////////////////////////////////////////

ModeSAltitude::~ModeSAltitude()
{
}

//////////////////////////////////////////////////////////////////////////

void ModeSAltitude::SetAltitude( KUINT16 Altitude ) 
{	
	m_ModeSAltitudeUnion.m_ui16Altitude = Altitude;
}

//////////////////////////////////////////////////////////////////////////

KUINT16 ModeSAltitude::GetAltitude() const
{
	return ( KUINT16 )m_ModeSAltitudeUnion.m_ui16Altitude;
}
//////////////////////////////////////////////////////////////////////////

void ModeSAltitude::SetAltitudeResolution( KDIS::DATA_TYPE::ENUMS::AltitudeResolution AltitudeResolution ) 
{	
	m_ModeSAltitudeUnion.m_ui16AltitudeResolution = AltitudeResolution;
}

//////////////////////////////////////////////////////////////////////////

KDIS::DATA_TYPE::ENUMS::AltitudeResolution ModeSAltitude::GetAltitudeResolution() const
{
	return ( KDIS::DATA_TYPE::ENUMS::AltitudeResolution )m_ModeSAltitudeUnion.m_ui16AltitudeResolution;
}

//////////////////////////////////////////////////////////////////////////

const KOCTET* ModeSAltitude::GetAsString() const
{
    KStringStream ss;	

	ss << "Mode S Levels Present:"
	   << "\n\tMode S Altitude:       " << m_ModeSAltitudeUnion.m_ui16Altitude
	   << "\n\tZltitude Resolution:   " << GetEnumAsStringAltitudeResolution(m_ModeSAltitudeUnion.m_ui16AltitudeResolution)
	   << "\n";

    return ss.str().c_str();
}

//////////////////////////////////////////////////////////////////////////

void ModeSAltitude::Decode( KDataStream & stream ) 
{
    if( stream.GetBufferSize() < MODE_S_ALTITUDE_SIZE )throw KException( __FUNCTION__, NOT_ENOUGH_DATA_IN_BUFFER );
	
	stream >> m_ModeSAltitudeUnion.m_ui16ModeSAltitude;
}

//////////////////////////////////////////////////////////////////////////

const KDataStream* ModeSAltitude::GetEncodedStream() const
{
	m_stream.Clear();

	this->EncodeToStream(m_stream);

	return &m_stream;
}

//////////////////////////////////////////////////////////////////////////

void ModeSAltitude::EncodeToStream( KDataStream & stream ) const
{
	stream << m_ModeSAltitudeUnion.m_ui16ModeSAltitude;
}

//////////////////////////////////////////////////////////////////////////

KBOOL ModeSAltitude::operator == ( const ModeSAltitude & Value ) const
{
	if( m_ModeSAltitudeUnion.m_ui16ModeSAltitude != Value.m_ModeSAltitudeUnion.m_ui16ModeSAltitude ) return false;
    return true;
}

//////////////////////////////////////////////////////////////////////////

KBOOL ModeSAltitude::operator != ( const ModeSAltitude & Value ) const
{
    return !( *this == Value );
}

//////////////////////////////////////////////////////////////////////////
