#include "./ModeSTransponderBasicData.h"

//////////////////////////////////////////////////////////////////////////

using namespace std;
using namespace KDIS;
using namespace UTILS;
using namespace DATA_TYPE;
using namespace ENUMS;

//////////////////////////////////////////////////////////////////////////
// public:
//////////////////////////////////////////////////////////////////////////

ModeSTransponderBasicData::ModeSTransponderBasicData() :
    m_ui8AircraftPresentDomain( 0 ),
    m_ui32AircraftAddress( 0 ),
    m_ui8AircraftIDType( 0 ),
    m_ui8CapabilityReport( 0 ),
    m_ui8Padding( 0 ),
    m_ui16Padding( 0 )
{
    memset (m_ui64AircraftIdentification, 0, sizeof (m_ui64AircraftIdentification));
}

//////////////////////////////////////////////////////////////////////////

ModeSTransponderBasicData::ModeSTransponderBasicData( const ModeSTransponderStatus & S, const ModeSLevelsPresent & MSLS, KDIS::DATA_TYPE::ENUMS::AircraftPresentDomain APD,
								const KOCTET* AircraftIdentification, KUINT8 AircraftIdentificationSize, KUINT32 AircraftAddress, KDIS::DATA_TYPE::ENUMS::AircraftIDType AIDT,
                               const DAPSource & DAPS, const ModeSAltitude & MSA, KDIS::DATA_TYPE::ENUMS::CapabilityReport CR ) :
    m_Status( S ),
    m_ModeSLevelsPresent( MSLS ),
    m_ui8AircraftPresentDomain( APD ),
    m_ui32AircraftAddress( AircraftAddress ),
    m_ui8AircraftIDType( AIDT ),
    m_ui8DAPSource( DAPS ),
    m_ui16ModeSAltitude( MSA ),
    m_ui8CapabilityReport( CR ),
    m_ui8Padding( 0 ),
    m_ui16Padding( 0 )
{
    if( AircraftIdentificationSize > AIRCRAFT_IDENTIFICATION_SIZE )throw KException( __FUNCTION__, DATA_TYPE_TOO_LARGE );

	// Set
	memcpy(m_ui64AircraftIdentification, AircraftIdentification, AircraftIdentificationSize);

	// Clear extra space
	if (AircraftIdentificationSize < AIRCRAFT_IDENTIFICATION_SIZE)
	{
		memset(m_ui64AircraftIdentification + AircraftIdentificationSize, 0, AIRCRAFT_IDENTIFICATION_SIZE - AircraftIdentificationSize);
	}
}

//////////////////////////////////////////////////////////////////////////

ModeSTransponderBasicData::ModeSTransponderBasicData( KDataStream & stream ) 
{
    Decode( stream );
}

//////////////////////////////////////////////////////////////////////////

ModeSTransponderBasicData::~ModeSTransponderBasicData()
{
}

//////////////////////////////////////////////////////////////////////////

void ModeSTransponderBasicData::SetStatus( const ModeSTransponderStatus & S )
{
    m_Status = S;
}

//////////////////////////////////////////////////////////////////////////

const ModeSTransponderStatus & ModeSTransponderBasicData::GetStatusConst() const
{
    return m_Status;
}

//////////////////////////////////////////////////////////////////////////

ModeSTransponderStatus & ModeSTransponderBasicData::GetStatus()
{
    return m_Status;
}

//////////////////////////////////////////////////////////////////////////

void ModeSTransponderBasicData::SetModeSLevelsPresent( const ModeSLevelsPresent & MSLS )
{
    m_ModeSLevelsPresent = MSLS;
}

//////////////////////////////////////////////////////////////////////////

const ModeSLevelsPresent & ModeSTransponderBasicData::GetModeSLevelsPresentConst() const
{
    return m_ModeSLevelsPresent;
}

//////////////////////////////////////////////////////////////////////////

ModeSLevelsPresent & ModeSTransponderBasicData::GetModeSLevelsPresent()
{
    return m_ModeSLevelsPresent;
}

//////////////////////////////////////////////////////////////////////////

void ModeSTransponderBasicData::SetAircraftPresentDomain( KDIS::DATA_TYPE::ENUMS::AircraftPresentDomain APD )
{
    m_ui8AircraftPresentDomain = APD;
}

//////////////////////////////////////////////////////////////////////////

KDIS::DATA_TYPE::ENUMS::AircraftPresentDomain ModeSTransponderBasicData::GetAircraftPresentDomain() const
{
    return (KDIS::DATA_TYPE::ENUMS::AircraftPresentDomain)m_ui8AircraftPresentDomain;
}

//////////////////////////////////////////////////////////////////////////

void ModeSTransponderBasicData::SetAircraftIdentification(const KOCTET* AircraftIdentification, KUINT8 AircraftIdentificationSize)
{
	if (AircraftIdentificationSize > AIRCRAFT_IDENTIFICATION_SIZE)throw KException(__FUNCTION__, DATA_TYPE_TOO_LARGE);

	// Set
	memcpy(m_ui64AircraftIdentification, AircraftIdentification, AircraftIdentificationSize);

	// Clear extra space
	if (AircraftIdentificationSize < AIRCRAFT_IDENTIFICATION_SIZE)
	{
		memset(m_ui64AircraftIdentification + AircraftIdentificationSize, 0, AIRCRAFT_IDENTIFICATION_SIZE - AircraftIdentificationSize);
	}
}

//////////////////////////////////////////////////////////////////////////

const KOCTET* ModeSTransponderBasicData::GetAircraftIdentificationConst() const
{
	return (KOCTET*)&m_ui64AircraftIdentification[0];
}

//////////////////////////////////////////////////////////////////////////

const KOCTET* ModeSTransponderBasicData::GetAircraftIdentification()
{
	return (KOCTET*)&m_ui64AircraftIdentification[0];
}
//////////////////////////////////////////////////////////////////////////

void ModeSTransponderBasicData::SetAircraftAddress( KUINT32 AircraftAddress )
{
    m_ui32AircraftAddress = AircraftAddress;
}

//////////////////////////////////////////////////////////////////////////

KUINT32 ModeSTransponderBasicData::GetAircraftAddress() const
{
    return m_ui32AircraftAddress;
}

//////////////////////////////////////////////////////////////////////////

void  ModeSTransponderBasicData::SetAircraftIDType( KDIS::DATA_TYPE::ENUMS::AircraftIDType AIDT )
{
    m_ui8AircraftIDType = AIDT;
}

//////////////////////////////////////////////////////////////////////////

KDIS::DATA_TYPE::ENUMS::AircraftIDType  ModeSTransponderBasicData::GetAircraftIDType() const
{
    return (KDIS::DATA_TYPE::ENUMS::AircraftIDType)m_ui8AircraftIDType;
}

//////////////////////////////////////////////////////////////////////////

void ModeSTransponderBasicData::SetDAPSource( const DAPSource & DAPS )
{
    m_ui8DAPSource = DAPS;
}

//////////////////////////////////////////////////////////////////////////

const DAPSource & ModeSTransponderBasicData::GetDAPSourceConst() const
{
    return m_ui8DAPSource;
}

//////////////////////////////////////////////////////////////////////////

DAPSource & ModeSTransponderBasicData::GetDAPSource()
{
    return m_ui8DAPSource;
}

//////////////////////////////////////////////////////////////////////////

void ModeSTransponderBasicData::SetModeSAltitude( const ModeSAltitude & MSA )
{
    m_ui16ModeSAltitude = MSA;
}

//////////////////////////////////////////////////////////////////////////

const ModeSAltitude & ModeSTransponderBasicData::GetModeSAltitudeConst() const
{
    return m_ui16ModeSAltitude;
}

//////////////////////////////////////////////////////////////////////////

ModeSAltitude & ModeSTransponderBasicData::GetModeSAltitude()
{
    return m_ui16ModeSAltitude;
}

//////////////////////////////////////////////////////////////////////////

void ModeSTransponderBasicData::SetCapabilityReport( KDIS::DATA_TYPE::ENUMS::CapabilityReport CR )
{
    m_ui8CapabilityReport = CR;
}

//////////////////////////////////////////////////////////////////////////

KDIS::DATA_TYPE::ENUMS::CapabilityReport ModeSTransponderBasicData::GetCapabilityReport() const
{
    return ( KDIS::DATA_TYPE::ENUMS::CapabilityReport )m_ui8CapabilityReport;
}

//////////////////////////////////////////////////////////////////////////

const KOCTET* ModeSTransponderBasicData::GetAsString() const
{
	KStringStream ss;

	char AircraftIdentification[AIRCRAFT_IDENTIFICATION_SIZE + 1] = "";
	memcpy(AircraftIdentification, m_ui64AircraftIdentification, AIRCRAFT_IDENTIFICATION_SIZE);
	AircraftIdentification[AIRCRAFT_IDENTIFICATION_SIZE] = '\0';
	ss << "Mode S Transponder Basic Data:\n"
		<< IndentString(m_Status.GetAsString())
		<< IndentString(m_ModeSLevelsPresent.GetAsString())
		<< "AircraftPresentDomain:  " << GetEnumAsStringAircraftPresentDomain(m_ui8AircraftPresentDomain) << "\n"
		<< "AircraftIdentification: " << AircraftIdentification << "\n"
		<< "AircraftAddress:        " << m_ui32AircraftAddress << "\n"
		<< "AircraftIDType:         " << GetEnumAsStringAircraftIDType(m_ui8AircraftIDType) << "\n"
		<< IndentString(m_ui8DAPSource.GetAsString())
		<< IndentString(m_ui16ModeSAltitude.GetAsString())
		<< "CapabilityReport:       " << GetEnumAsStringCapabilityReport(m_ui8CapabilityReport) << "\n";

	return ss.str().c_str();
}

//////////////////////////////////////////////////////////////////////////

void ModeSTransponderBasicData::Decode( KDataStream & stream ) 
{
    if( stream.GetBufferSize() < MODE_S_TRANSPONDER_BASIC_DATA_SIZE )throw KException( __FUNCTION__, NOT_ENOUGH_DATA_IN_BUFFER );

    stream >> KDIS_STREAM m_Status
           >> KDIS_STREAM m_ModeSLevelsPresent
           >> m_ui8AircraftPresentDomain;
    for (KUINT8 i = 0; i < AIRCRAFT_IDENTIFICATION_SIZE; ++i )
    {
        stream >> m_ui64AircraftIdentification[i];
    }
    stream >> m_ui32AircraftAddress
           >> m_ui8AircraftIDType
           >> KDIS_STREAM m_ui8DAPSource
           >> KDIS_STREAM m_ui16ModeSAltitude
           >> m_ui8CapabilityReport
           >> m_ui8Padding
           >> m_ui16Padding;
}

//////////////////////////////////////////////////////////////////////////

const KDataStream* ModeSTransponderBasicData::GetEncodedStream() const
{
	m_stream.Clear();

	this->EncodeToStream(m_stream);

	return &m_stream;
}

//////////////////////////////////////////////////////////////////////////

void ModeSTransponderBasicData::EncodeToStream( KDataStream & stream ) const
{
    stream << KDIS_STREAM m_Status
           << KDIS_STREAM m_ModeSLevelsPresent
           << m_ui8AircraftPresentDomain;
    for( KUINT8 i = 0; i < AIRCRAFT_IDENTIFICATION_SIZE; ++i )
	{
		stream << m_ui64AircraftIdentification[i];
	}
    stream << m_ui32AircraftAddress
           << m_ui8AircraftIDType
           << KDIS_STREAM m_ui8DAPSource
           << KDIS_STREAM m_ui16ModeSAltitude
           << m_ui8CapabilityReport
           << m_ui8Padding
           << m_ui16Padding;
}

//////////////////////////////////////////////////////////////////////////

KBOOL ModeSTransponderBasicData::operator == ( const ModeSTransponderBasicData & Value ) const
{
    if( m_Status                     != Value.m_Status )                        return false;
    if( m_ModeSLevelsPresent         != Value.m_ModeSLevelsPresent )            return false;
    if( m_ui8AircraftPresentDomain   != Value.m_ui8AircraftPresentDomain )      return false;
    if( m_ui64AircraftIdentification != Value.m_ui64AircraftIdentification )    return false;
    if( m_ui32AircraftAddress        != Value.m_ui32AircraftAddress )           return false;
    if( m_ui8AircraftIDType          != Value.m_ui8AircraftIDType )             return false;
    if( m_ui8DAPSource               != Value.m_ui8DAPSource )                  return false;
    if( m_ui16ModeSAltitude          != Value.m_ui16ModeSAltitude )             return false;
    if( m_ui8CapabilityReport        != Value.m_ui8CapabilityReport )           return false;
    return true;
}

//////////////////////////////////////////////////////////////////////////

KBOOL ModeSTransponderBasicData::operator != ( const ModeSTransponderBasicData & Value ) const
{
    return !( *this == Value );
}

//////////////////////////////////////////////////////////////////////////
