
#include "./SquitterAirbornePositionReport.h"

//////////////////////////////////////////////////////////////////////////

using namespace KDIS;
using namespace DATA_TYPE;
using namespace ENUMS;

//////////////////////////////////////////////////////////////////////////
// Public:
//////////////////////////////////////////////////////////////////////////

SquitterAirbornePositionReport::SquitterAirbornePositionReport() :
    m_ui16SquitterReport( 0 )
{
    m_ui32Type = SquitterAirbornePositionReportRecord;
    m_ui16Length = SQUITTER_AIRBORNE_POSITION_REPORT_TYPE_SIZE;
}

//////////////////////////////////////////////////////////////////////////

SquitterAirbornePositionReport::SquitterAirbornePositionReport( KUINT16 SquitterReport )  :
    m_ui16SquitterReport( SquitterReport )
{
    m_ui32Type = SquitterAirbornePositionReportRecord;
    m_ui16Length = SQUITTER_AIRBORNE_POSITION_REPORT_TYPE_SIZE;
}

//////////////////////////////////////////////////////////////////////////

SquitterAirbornePositionReport::SquitterAirbornePositionReport( KDataStream & stream ) 
{
    Decode( stream );
}

//////////////////////////////////////////////////////////////////////////

SquitterAirbornePositionReport::~SquitterAirbornePositionReport()
{
}

//////////////////////////////////////////////////////////////////////////

void SquitterAirbornePositionReport::SetSquitterReport( KUINT16 SquitterReport )
{
    m_ui16SquitterReport = SquitterReport;
}

//////////////////////////////////////////////////////////////////////////

KUINT16 SquitterAirbornePositionReport::GetSquitterReport() const
{
    return m_ui16SquitterReport;
}

//////////////////////////////////////////////////////////////////////////

const KOCTET* SquitterAirbornePositionReport::GetAsString() const
{
    KStringStream ss;

    ss << StandardVariable::GetAsString()
       << "Squitter Report:    " << m_ui16SquitterReport   << "\n";

    return ss.str().c_str();
}

//////////////////////////////////////////////////////////////////////////

void SquitterAirbornePositionReport::Decode( KDataStream & stream ) 
{
    if( stream.GetBufferSize() < SQUITTER_AIRBORNE_POSITION_REPORT_TYPE_SIZE )throw KException( __FUNCTION__, NOT_ENOUGH_DATA_IN_BUFFER );

    StandardVariable::Decode( stream );

    stream >> m_ui16SquitterReport;
}

//////////////////////////////////////////////////////////////////////////

void SquitterAirbornePositionReport::EncodeToStream( KDataStream & stream ) const
{
    StandardVariable::EncodeToStream( stream );

    stream << m_ui16SquitterReport;
}

//////////////////////////////////////////////////////////////////////////

KBOOL SquitterAirbornePositionReport::operator == ( const SquitterAirbornePositionReport & Value ) const
{
    if( StandardVariable::operator  !=( Value ) )                       return false;
    if( m_ui16SquitterReport        != Value.m_ui16SquitterReport )     return false;
    return true;
}

//////////////////////////////////////////////////////////////////////////

KBOOL SquitterAirbornePositionReport::operator != ( const SquitterAirbornePositionReport & Value ) const
{
    return !( *this == Value );
}

//////////////////////////////////////////////////////////////////////////
