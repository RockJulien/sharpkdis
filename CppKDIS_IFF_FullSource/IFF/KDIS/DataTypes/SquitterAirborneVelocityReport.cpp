
#include "./SquitterAirborneVelocityReport.h"

//////////////////////////////////////////////////////////////////////////

using namespace KDIS;
using namespace DATA_TYPE;
using namespace ENUMS;

//////////////////////////////////////////////////////////////////////////
// Public:
//////////////////////////////////////////////////////////////////////////

SquitterAirborneVelocityReport::SquitterAirborneVelocityReport() :
    m_ui16SquitterReport( 0 )
{
    m_ui32Type = SquitterAirborneVelocityReportRecord;
    m_ui16Length = SQUITTER_AIRBORNE_VELOCITY_REPORT_TYPE_SIZE;
}

//////////////////////////////////////////////////////////////////////////

SquitterAirborneVelocityReport::SquitterAirborneVelocityReport( KUINT16 SquitterReport )  :
    m_ui16SquitterReport( SquitterReport )
{
    m_ui32Type = SquitterAirborneVelocityReportRecord;
    m_ui16Length = SQUITTER_AIRBORNE_VELOCITY_REPORT_TYPE_SIZE;
}

//////////////////////////////////////////////////////////////////////////

SquitterAirborneVelocityReport::SquitterAirborneVelocityReport( KDataStream & stream ) 
{
    Decode( stream );
}

//////////////////////////////////////////////////////////////////////////

SquitterAirborneVelocityReport::~SquitterAirborneVelocityReport()
{
}

//////////////////////////////////////////////////////////////////////////

void SquitterAirborneVelocityReport::SetSquitterReport( KUINT16 SquitterReport )
{
    m_ui16SquitterReport = SquitterReport;
}

//////////////////////////////////////////////////////////////////////////

KUINT16 SquitterAirborneVelocityReport::GetSquitterReport() const
{
    return m_ui16SquitterReport;
}
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////

const KOCTET* SquitterAirborneVelocityReport::GetAsString() const
{
    KStringStream ss;

    ss << StandardVariable::GetAsString()
       << "Squitter Report:    " << m_ui16SquitterReport   << "\n";

    return ss.str().c_str();
}

//////////////////////////////////////////////////////////////////////////

void SquitterAirborneVelocityReport::Decode( KDataStream & stream ) 
{
    if( stream.GetBufferSize() < SQUITTER_AIRBORNE_VELOCITY_REPORT_TYPE_SIZE )throw KException( __FUNCTION__, NOT_ENOUGH_DATA_IN_BUFFER );

    StandardVariable::Decode( stream );

    stream >> m_ui16SquitterReport;
}

//////////////////////////////////////////////////////////////////////////

void SquitterAirborneVelocityReport::EncodeToStream( KDataStream & stream ) const
{
    StandardVariable::EncodeToStream( stream );

    stream << m_ui16SquitterReport;
}

//////////////////////////////////////////////////////////////////////////

KBOOL SquitterAirborneVelocityReport::operator == ( const SquitterAirborneVelocityReport & Value ) const
{
    if( StandardVariable::operator !=( Value ) )                        return false;
    if( m_ui16SquitterReport       != Value.m_ui16SquitterReport )      return false;
    return true;
}

//////////////////////////////////////////////////////////////////////////

KBOOL SquitterAirborneVelocityReport::operator != ( const SquitterAirborneVelocityReport & Value ) const
{
    return !( *this == Value );
}

//////////////////////////////////////////////////////////////////////////
