/********************************************************************
    class:      ModeSLevelsPresent
    DIS:        (7) 1278.1 - 2012
    created:    21/03/2019
    author:     Nicolas Duboys

    purpose:    

    Size:       8 bits / 1 octet
*********************************************************************/

#pragma once

#include "./DataTypeBase.h"

namespace KDIS {
namespace DATA_TYPE {

class KDIS_EXPORT ModeSLevelsPresent : public DataTypeBase
{
protected:

	union
	{
		struct
		{
			KUINT8 m_ui8NotUsed_1                            : 1;
			KUINT8 m_uiLevel1                                : 1;
			KUINT8 m_uiLevel2_ElementarySurveillanceSublevel : 1;
			KUINT8 m_uiLevel2_EnhancedSurveillanceSublevel   : 1;
			KUINT8 m_uiLevel3                                : 1;
			KUINT8 m_uiLevel4                                : 1;
			KUINT8 m_ui8NotUsed_2                            : 2;
		};
		KUINT8 m_ui8ModeSLevelsPresent;

	} m_ModeSLevelsPresentUnion;

	mutable KDataStream m_stream;
		
public:

    static const KUINT16 MODE_S_LEVELS_PRESENT_SIZE = 1; 

    ModeSLevelsPresent();

    ModeSLevelsPresent( KDataStream & stream ) ;

	ModeSLevelsPresent( KBOOL Level1, KBOOL Level2_ElementarySurveillanceSublevel, KBOOL Level2_EnhancedSurveillanceSublevel, KBOOL Level3, KBOOL Level4 );
	
    virtual ~ModeSLevelsPresent();

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSLevelsPresent::SetLevel1
    //              KDIS::DATA_TYPE::ModeSLevelsPresent::GetLevel1
    // Description: Indicates whether the Mode S level 1 is Not Present (false) or Present (true).
    // Parameter:   KBOOL Level1 
    //************************************
	void SetLevel1( KBOOL Level1 );
	KBOOL GetLevel1() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSLevelsPresent::SetLevel2_ElementarySurveillanceSublevel
    //              KDIS::DATA_TYPE::ModeSLevelsPresent::GetLevel2_ElementarySurveillanceSublevel
    // Description: Indicates whether the Mode S level 2 (Elementary Surveillance sublevel) is Not Present (false) or Present (true).
    // Parameter:   KBOOL Level2_ElementarySurveillanceSublevel 
    //************************************
	void SetLevel2_ElementarySurveillanceSublevel( KBOOL Level2_ElementarySurveillanceSublevel );
	KBOOL GetLevel2_ElementarySurveillanceSublevel() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSLevelsPresent::SetLevel2_EnhancedSurveillanceSublevel
    //              KDIS::DATA_TYPE::ModeSLevelsPresent::GetLevel2_EnhancedSurveillanceSublevel
    // Description: Indicates whether the Mode S level 2 (Enhanced Surveillance sublevel) is Not Present (false) or Present (true).
    // Parameter:   KBOOL Level2_EnhancedSurveillanceSublevel 
    //************************************
	void SetLevel2_EnhancedSurveillanceSublevel( KBOOL Level2_EnhancedSurveillanceSublevel );
	KBOOL GetLevel2_EnhancedSurveillanceSublevel() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSLevelsPresent::SetLevel3
    //              KDIS::DATA_TYPE::ModeSLevelsPresent::GetLevel3
    // Description: Indicates whether the Mode S level 3 is Not Present (false) or Present (true).
    // Parameter:   KBOOL Level3 
    //************************************
	void SetLevel3( KBOOL Level3 );
	KBOOL GetLevel3() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSLevelsPresent::SetLevel4
    //              KDIS::DATA_TYPE::ModeSLevelsPresent::GetLevel4
    // Description: Indicates whether the Mode S level 4 is Not Present (false) or Present (true).
    // Parameter:   KBOOL Level4 
    //************************************
	void SetLevel4( KBOOL Level4 );
	KBOOL GetLevel4() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSLevelsPresent::GetAsString
    // Description: Returns a string representation 
    //************************************
    virtual const KOCTET* GetAsString() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSLevelsPresent::Decode
    // Description: Convert From Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual void Decode( KDataStream & stream ) ;

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSLevelsPresent::Encode
    // Description: Convert To Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual const KDataStream* GetEncodedStream() const;
    virtual void EncodeToStream( KDataStream & stream ) const;

    KBOOL operator == ( const ModeSLevelsPresent & Value ) const;
    KBOOL operator != ( const ModeSLevelsPresent & Value ) const;
};

} // END namespace DATA_TYPE
} // END namespace KDIS
