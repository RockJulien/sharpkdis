/********************************************************************
    class:      SquitterAirborneVelocityReport
    created:    21/03/2019
    author:     Nicolas Duboys

    purpose:    Squitter Airborne Velocity Report entry for IFF data record (Layer 4 Mode S).
    size:       16 bits / 2 octets
*********************************************************************/

#pragma once

#include "./StandardVariable.h"

namespace KDIS {
namespace DATA_TYPE {

class KDIS_EXPORT SquitterAirborneVelocityReport : public StandardVariable
{
protected:

    KUINT16 m_ui16SquitterReport;

public:

    static const KUINT16 SQUITTER_AIRBORNE_VELOCITY_REPORT_TYPE_SIZE = 8;

    SquitterAirborneVelocityReport( KUINT16 SquitterReport ) ;

    SquitterAirborneVelocityReport();

    SquitterAirborneVelocityReport( KDataStream & stream ) ;

    virtual ~SquitterAirborneVelocityReport();

    //************************************
    // FullName:    KDIS::DATA_TYPE::SquitterAirborneVelocityReport::SetSquitterReport
    //              KDIS::DATA_TYPE::SquitterAirborneVelocityReport::GetSquitterReport
    // Description: Set Squitter report.
    // Parameter:   Squitter report
    //************************************
    void SetSquitterReport( KUINT16 SquitterReport );
    KUINT16 GetSquitterReport() const;


    //************************************
    // FullName:    KDIS::DATA_TYPE::SquitterAirborneVelocityReport::GetAsString
    // Description: Returns a string representation.
    //************************************
    virtual const KOCTET* GetAsString() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::SquitterAirborneVelocityReport::Decode
    // Description: Convert From Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual void Decode( KDataStream & stream ) ;

    //************************************
    // FullName:    KDIS::DATA_TYPE::SquitterAirborneVelocityReport::Encode
    // Description: Convert To Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual void EncodeToStream( KDataStream & stream ) const;

    KBOOL operator == ( const SquitterAirborneVelocityReport & Value ) const;
    KBOOL operator != ( const SquitterAirborneVelocityReport & Value ) const;
};

} // END namespace DATA_TYPES
} // END namespace KDIS
