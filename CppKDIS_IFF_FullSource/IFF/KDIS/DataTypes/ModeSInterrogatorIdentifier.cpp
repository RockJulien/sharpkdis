
#include "./ModeSInterrogatorIdentifier.h"

//////////////////////////////////////////////////////////////////////////

using namespace KDIS;
using namespace DATA_TYPE;
using namespace ENUMS;

//////////////////////////////////////////////////////////////////////////
// Public:
//////////////////////////////////////////////////////////////////////////

ModeSInterrogatorIdentifier::ModeSInterrogatorIdentifier()
{
    m_ModeSInterrogatorIdentifierUnion.m_ui16ModeSInterrogatorIdentifier = 0;
}

//////////////////////////////////////////////////////////////////////////

ModeSInterrogatorIdentifier::ModeSInterrogatorIdentifier( ::DATA_TYPE::ENUMS::ModeSInterrogatorIdentifierICType MSIIICT_P, KUINT8 Code_P,
        KDIS::DATA_TYPE::ENUMS::ModeSInterrogatorIdentifierICType MSIIICT_S, KUINT8 Code_S )
{
    m_ModeSInterrogatorIdentifierUnion.m_ui8PrimaryICCodeUnion.m_ui8ICType   = MSIIICT_P;
    m_ModeSInterrogatorIdentifierUnion.m_ui8PrimaryICCodeUnion.m_ui8ICCode   = Code_P;
    m_ModeSInterrogatorIdentifierUnion.m_ui8SecondaryICCodeUnion.m_ui8ICType = MSIIICT_S;
    m_ModeSInterrogatorIdentifierUnion.m_ui8SecondaryICCodeUnion.m_ui8ICCode = Code_S;
}

//////////////////////////////////////////////////////////////////////////

ModeSInterrogatorIdentifier::ModeSInterrogatorIdentifier( KDataStream & stream ) 
{
    Decode( stream );
}

//////////////////////////////////////////////////////////////////////////

ModeSInterrogatorIdentifier::~ModeSInterrogatorIdentifier()
{
}

//////////////////////////////////////////////////////////////////////////

void ModeSInterrogatorIdentifier::SetPrimaryICType( KDIS::DATA_TYPE::ENUMS::ModeSInterrogatorIdentifierICType MSIIICT_P )
{
    m_ModeSInterrogatorIdentifierUnion.m_ui8PrimaryICCodeUnion.m_ui8ICType = MSIIICT_P;
}

//////////////////////////////////////////////////////////////////////////

KDIS::DATA_TYPE::ENUMS::ModeSInterrogatorIdentifierICType ModeSInterrogatorIdentifier::GetPrimaryICType() const
{
    return (KDIS::DATA_TYPE::ENUMS::ModeSInterrogatorIdentifierICType)m_ModeSInterrogatorIdentifierUnion.m_ui8PrimaryICCodeUnion.m_ui8ICType;
}

//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////

void ModeSInterrogatorIdentifier::SetPrimaryICCode( KUINT8 code )
{
    m_ModeSInterrogatorIdentifierUnion.m_ui8PrimaryICCodeUnion.m_ui8ICCode = code;
}

//////////////////////////////////////////////////////////////////////////

KUINT8 ModeSInterrogatorIdentifier::GetPrimaryICCode() const
{
    return m_ModeSInterrogatorIdentifierUnion.m_ui8PrimaryICCodeUnion.m_ui8ICCode;
}

//////////////////////////////////////////////////////////////////////////

void ModeSInterrogatorIdentifier::SetSecondaryICType( KDIS::DATA_TYPE::ENUMS::ModeSInterrogatorIdentifierICType MSIIICT_P )
{
    m_ModeSInterrogatorIdentifierUnion.m_ui8SecondaryICCodeUnion.m_ui8ICType = MSIIICT_P;
}

//////////////////////////////////////////////////////////////////////////

KDIS::DATA_TYPE::ENUMS::ModeSInterrogatorIdentifierICType ModeSInterrogatorIdentifier::GetSecondaryICType() const
{
    return (KDIS::DATA_TYPE::ENUMS::ModeSInterrogatorIdentifierICType)m_ModeSInterrogatorIdentifierUnion.m_ui8SecondaryICCodeUnion.m_ui8ICType;
}

//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////

void ModeSInterrogatorIdentifier::SetSecondaryICCode( KUINT8 code )
{
    m_ModeSInterrogatorIdentifierUnion.m_ui8SecondaryICCodeUnion.m_ui8ICCode = code;
}

//////////////////////////////////////////////////////////////////////////

KUINT8 ModeSInterrogatorIdentifier::GetSecondaryICCode() const
{
    return m_ModeSInterrogatorIdentifierUnion.m_ui8SecondaryICCodeUnion.m_ui8ICCode;
}

//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////

const KOCTET* ModeSInterrogatorIdentifier::GetAsString() const
{
    KStringStream ss;

    ss << "Mode S Interrogator Identifier:\n"
       << GetEnumAsStringModeSInterrogatorIdentifierICType( m_ModeSInterrogatorIdentifierUnion.m_ui8PrimaryICCodeUnion.m_ui8ICType ) << "\n"
       << "Primary IC Code:   " << m_ModeSInterrogatorIdentifierUnion.m_ui8PrimaryICCodeUnion.m_ui8ICCode   << "\n"
       << GetEnumAsStringModeSInterrogatorIdentifierICType( m_ModeSInterrogatorIdentifierUnion.m_ui8SecondaryICCodeUnion.m_ui8ICType ) << "\n"
       << "Secondary IC Code:  " << m_ModeSInterrogatorIdentifierUnion.m_ui8SecondaryICCodeUnion.m_ui8ICCode   << "\n";

    return ss.str().c_str();
}

//////////////////////////////////////////////////////////////////////////

void ModeSInterrogatorIdentifier::Decode( KDataStream & stream ) 
{
    if( stream.GetBufferSize() < MODE_S_INTERROGATOR_IDENTIFIER_TYPE_SIZE )throw KException( __FUNCTION__, NOT_ENOUGH_DATA_IN_BUFFER );

    stream >> m_ModeSInterrogatorIdentifierUnion.m_ui16ModeSInterrogatorIdentifier;
}

//////////////////////////////////////////////////////////////////////////

const KDataStream* ModeSInterrogatorIdentifier::GetEncodedStream() const
{
	m_stream.Clear();

	this->EncodeToStream(m_stream);

	return &m_stream;
}

//////////////////////////////////////////////////////////////////////////

void ModeSInterrogatorIdentifier::EncodeToStream( KDataStream & stream ) const
{
    stream << m_ModeSInterrogatorIdentifierUnion.m_ui16ModeSInterrogatorIdentifier;
}

//////////////////////////////////////////////////////////////////////////

KBOOL ModeSInterrogatorIdentifier::operator == ( const ModeSInterrogatorIdentifier & Value ) const
{
    if( m_ModeSInterrogatorIdentifierUnion.m_ui16ModeSInterrogatorIdentifier   != Value.m_ModeSInterrogatorIdentifierUnion.m_ui16ModeSInterrogatorIdentifier )   return false;
    return true;
}

//////////////////////////////////////////////////////////////////////////

KBOOL ModeSInterrogatorIdentifier::operator != ( const ModeSInterrogatorIdentifier & Value ) const
{
    return !( *this == Value );
}

//////////////////////////////////////////////////////////////////////////
