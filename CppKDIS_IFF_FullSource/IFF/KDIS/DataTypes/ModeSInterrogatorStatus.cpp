#include "./ModeSInterrogatorStatus.h"

//////////////////////////////////////////////////////////////////////////

using namespace std;
using namespace KDIS;
using namespace DATA_TYPE;
using namespace ENUMS;

//////////////////////////////////////////////////////////////////////////
// public:
//////////////////////////////////////////////////////////////////////////

ModeSInterrogatorStatus::ModeSInterrogatorStatus() 
{
	m_StatusUnion.m_ui8Status = 0;	
}

//////////////////////////////////////////////////////////////////////////

ModeSInterrogatorStatus::ModeSInterrogatorStatus( KDataStream & stream ) 
{
    Decode( stream );
}

//////////////////////////////////////////////////////////////////////////

ModeSInterrogatorStatus::ModeSInterrogatorStatus( KBOOL Status, KDIS::DATA_TYPE::ENUMS::TransmitStateModeS TS, KBOOL Dmg, KBOOL Malfnc )
{	
	m_StatusUnion.m_ui8OnOff         = Status;
	m_StatusUnion.m_ui8TransmitState = TS;      
	m_StatusUnion.m_ui8Dmg           = Dmg;          
	m_StatusUnion.m_ui8MalFnc        = Malfnc;   	
}

//////////////////////////////////////////////////////////////////////////

ModeSInterrogatorStatus::~ModeSInterrogatorStatus()
{
}

//////////////////////////////////////////////////////////////////////////

void ModeSInterrogatorStatus::SetStatus( KBOOL S )
{
	m_StatusUnion.m_ui8OnOff = S;
}

//////////////////////////////////////////////////////////////////////////

KBOOL ModeSInterrogatorStatus::GetStatus() const
{
	return ( KBOOL )m_StatusUnion.m_ui8OnOff;
}

//////////////////////////////////////////////////////////////////////////

void ModeSInterrogatorStatus::SetTransmitState( KDIS::DATA_TYPE::ENUMS::TransmitStateModeS S )
{
	m_StatusUnion.m_ui8TransmitState = S;
}

//////////////////////////////////////////////////////////////////////////

KDIS::DATA_TYPE::ENUMS::TransmitStateModeS ModeSInterrogatorStatus::GetTransmitState() const
{
	return ( KDIS::DATA_TYPE::ENUMS::TransmitStateModeS )m_StatusUnion.m_ui8TransmitState;
}

//////////////////////////////////////////////////////////////////////////

void ModeSInterrogatorStatus::SetDamaged( KBOOL D )
{
	m_StatusUnion.m_ui8Dmg = D;
}

//////////////////////////////////////////////////////////////////////////

KBOOL ModeSInterrogatorStatus::IsDamaged() const
{
	return ( KBOOL )m_StatusUnion.m_ui8Dmg;
}

//////////////////////////////////////////////////////////////////////////

void ModeSInterrogatorStatus::SetMalfunctioning( KBOOL M )
{
	m_StatusUnion.m_ui8MalFnc = M;
}

//////////////////////////////////////////////////////////////////////////

KBOOL ModeSInterrogatorStatus::IsMalfunctioning() const
{
	return ( KBOOL )m_StatusUnion.m_ui8MalFnc;
}

//////////////////////////////////////////////////////////////////////////

const KOCTET* ModeSInterrogatorStatus::GetAsString() const
{
    KStringStream ss;	

	ss << "Mode S Transponder Status:"
	   << "\n\tOn/Off Status:                     " << ( KBOOL )m_StatusUnion.m_ui8OnOff
	   << "\n\tTransmit state:                    " << GetEnumAsStringSquitterType( m_StatusUnion.m_ui8TransmitState )
	   << "\n\tDamaged:                           " << ( KBOOL )m_StatusUnion.m_ui8Dmg
	   << "\n\tMalfunction:                       " << ( KBOOL )m_StatusUnion.m_ui8MalFnc 
	   << "\n";

    return ss.str().c_str();
}

//////////////////////////////////////////////////////////////////////////

void ModeSInterrogatorStatus::Decode( KDataStream & stream ) 
{
    if( stream.GetBufferSize() < MODE_S_INTERROGATOR_STATUS_SIZE )throw KException( __FUNCTION__, NOT_ENOUGH_DATA_IN_BUFFER );
	
	stream >> m_StatusUnion.m_ui8Status;
}

//////////////////////////////////////////////////////////////////////////

const KDataStream* ModeSInterrogatorStatus::GetEncodedStream() const
{
	m_stream.Clear();

	this->EncodeToStream(m_stream);

	return &m_stream;
}

//////////////////////////////////////////////////////////////////////////

void ModeSInterrogatorStatus::EncodeToStream( KDataStream & stream ) const
{
	stream << m_StatusUnion.m_ui8Status;
}

//////////////////////////////////////////////////////////////////////////

KBOOL ModeSInterrogatorStatus::operator == ( const ModeSInterrogatorStatus & Value ) const
{
	if( m_StatusUnion.m_ui8Status != Value.m_StatusUnion.m_ui8Status ) return false;
    return true;
}

//////////////////////////////////////////////////////////////////////////

KBOOL ModeSInterrogatorStatus::operator != ( const ModeSInterrogatorStatus & Value ) const
{
    return !( *this == Value );
}

//////////////////////////////////////////////////////////////////////////
