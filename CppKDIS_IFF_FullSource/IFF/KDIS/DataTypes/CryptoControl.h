/********************************************************************
    class:      CryptoControl
    created:    21/03/2019
    author:     Nicolas Duboys

    purpose:    Crypto control entry for IFF data record (Layer 3 Mode 5).
    size:       48 bits / 6 octets
*********************************************************************/

#pragma once

#include "./StandardVariable.h"

namespace KDIS {
namespace DATA_TYPE {

class KDIS_EXPORT CryptoControl : public StandardVariable
{
protected:

    KUINT16 m_ui16PseudoCryptoCode;

    KUINT16 m_ui16PseudoCryptoCodeA;

    KUINT16 m_ui16PseudoCryptoCodeB;

public:

    static const KUINT16 CRYPTO_CONTROL_TYPE_SIZE = 12;

    CryptoControl( KUINT16 PseudoCryptoCode, KUINT16 PseudoCryptoCode1A, KUINT16 PseudoCryptoCodeB ) ;

    CryptoControl();

    CryptoControl( KDataStream & stream ) ;

    virtual ~CryptoControl();

    //************************************
    // FullName:    KDIS::DATA_TYPE::CryptoControl::SetPseudoCryptoCode
    //              KDIS::DATA_TYPE::CryptoControl::GetPseudoCryptoCode
    // Description: Set the Crypto code.
    // Parameter:   Pseudo crypto code
    //************************************
    void SetPseudoCryptoCode( KUINT16 PseudoCryptoCode );
    KUINT16 GetPseudoCryptoCode() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::CryptoControl::SetPseudoCryptoCodeA
    //              KDIS::DATA_TYPE::CryptoControl::GetPseudoCryptoCodeA
    // Description: Set the Crypto code A.
    // Parameter:   Pseudo crypto code A
    //************************************
    void SetPseudoCryptoCodeA( KUINT16 PseudoCryptoCodeA );
    KUINT16 GetPseudoCryptoCodeA() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::CryptoControl::SetPseudoCryptoCodeB
    //              KDIS::DATA_TYPE::CryptoControl::GetPseudoCryptoCodeB
    // Description: Set the Crypto code B.
    // Parameter:   Pseudo crypto code B
    //************************************
    void SetPseudoCryptoCodeB( KUINT16 PseudoCryptoCodeB );
    KUINT16 GetPseudoCryptoCodeB() const;


    //************************************
    // FullName:    KDIS::DATA_TYPE::CryptoControl::GetAsString
    // Description: Returns a string representation.
    //************************************
    virtual const KOCTET* GetAsString() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::CryptoControl::Decode
    // Description: Convert From Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual void Decode( KDataStream & stream ) ;

    //************************************
    // FullName:    KDIS::DATA_TYPE::CryptoControl::Encode
    // Description: Convert To Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual void EncodeToStream( KDataStream & stream ) const;

    KBOOL operator == ( const CryptoControl & Value ) const;
    KBOOL operator != ( const CryptoControl & Value ) const;
};

} // END namespace DATA_TYPES
} // END namespace KDIS
