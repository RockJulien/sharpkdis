/********************************************************************
    class:      ModeSTransponderStatus
    DIS:        (7) 1278.1 - 2012
    created:    21/03/2019
    author:     Nicolas Duboys

    purpose:    

    Size:       16 bits / 2 octet
*********************************************************************/

#pragma once

#include "./DataTypeBase.h"

namespace KDIS {
namespace DATA_TYPE {

class KDIS_EXPORT ModeSTransponderStatus : public DataTypeBase
{
protected:

	union
	{
		struct
		{
			KUINT16 m_ui16SquitterStatus                  : 1;
			KUINT16 m_ui16SquitterType                    : 3;
			KUINT16 m_ui16SquitterRecordSource            : 1;
			KUINT16 m_ui16AirbornePositionReportIndicator : 1;
			KUINT16 m_ui16AirborneVelocityReportIndicator : 1;
			KUINT16 m_ui16SurfacePositionReportIndicator  : 1;
			KUINT16 m_ui16IdentificationReportIndicator   : 1;
			KUINT16 m_ui16EventDrivenReportIndicator      : 1;
			KUINT16 m_ui16Padding                         : 3;
			KUINT16 m_ui16OnOff                           : 1;
			KUINT16 m_ui16Dmg                             : 1;
			KUINT16 m_ui16MalFnc                          : 1;
		};
		KUINT16 m_ui16Status;

	} m_StatusUnion;

	mutable KDataStream m_stream;
		
public:

    static const KUINT16 MODE_S_TRANSPONDER_STATUS_SIZE = 2; 

    ModeSTransponderStatus();

    ModeSTransponderStatus( KDataStream & stream ) ;

	ModeSTransponderStatus( KBOOL SquitterStatus, KDIS::DATA_TYPE::ENUMS::SquitterType ST, KBOOL RecordSource,
	                        KBOOL AirbonePositionReportIndicator, KBOOL AirborneVelocityReportIndicator, KBOOL SurfacePositionReportIndicator, KBOOL IdentificationReportIndicator,
                            KBOOL EventDrivenReportIndicator, KBOOL Status, KBOOL Dmg, KBOOL Malfnc );
	
    virtual ~ModeSTransponderStatus();

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSTransponderStatus::SetSquitterStatus
    //              KDIS::DATA_TYPE::ModeSTransponderStatus::GetSquitterStatus
    // Description: Indicates whether the squitter is On (true) or Off (false).
    // Parameter:   KBOOL SquitterStatus 
    //************************************
	void SetSquitterStatus( KBOOL SquitterStatus );
	KBOOL GetSquitterStatus() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSTransponderStatus::SetSquitterType
    //              KDIS::DATA_TYPE::ModeSTransponderStatus::GetSquitterType
    // Description: Indicates the type of Mode S squitter and shall be set to 
    //              one of the following values: 
    //              Not Capable (0), Acquisition (Short) (1), or Extended (2).
    // Parameter:   KDIS::DATA_TYPE::ENUMS::SquitterType ST
    //************************************
	void SetSquitterType( KDIS::DATA_TYPE::ENUMS::SquitterType ST );
	KDIS::DATA_TYPE::ENUMS::SquitterType GetSquitterType() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSTransponderStatus::SetRecordSource
    //              KDIS::DATA_TYPE::ModeSTransponderStatus::IsRecordSource
    // Description: Indicates whether the squitter records are only present 
    //              as Layer 4 IFF Data Records (false) 
    //              or are also included as Layer 5 GICB IFF Data Records (true).
    // Parameter:   KBOOL RecordSource
    //************************************
	void SetRecordSource( KBOOL RecordSource );
	KBOOL GetRecordSource() const;

	//************************************
    // FullName:    KDIS::DATA_TYPE::ModeSTransponderStatus::SetAirbonePositionReportIndicator
    //              KDIS::DATA_TYPE::ModeSTransponderStatus::IsAirbonePositionReportIndicator
    // Description: Indicates whether there is a Squitter Airborne Position Report IFF 
    //              Data Record either Present (true) or Not Present (false).
    // Parameter:   KBOOL AirbonePositionReportIndicator
    //************************************
	void SetAirbonePositionReportIndicator( KBOOL AirbonePositionReportIndicator );
	KBOOL GetAirbonePositionReportIndicator() const;

	//************************************
    // FullName:    KDIS::DATA_TYPE::ModeSTransponderStatus::SetAirborneVelocityReportIndicator
    //              KDIS::DATA_TYPE::ModeSTransponderStatus::IsAirborneVelocityReportIndicator
    // Description: Indicates whether there is a Squitter Airborne Velocity Report IFF Data 
    //              Record either Present (true) or Not Present (false).
    // Parameter:   KBOOL AirborneVelocityReportIndicator  
    //************************************
	void SetAirborneVelocityReportIndicator( KBOOL AirborneVelocityReportIndicator );
	KBOOL GetAirborneVelocityReportIndicator() const;

	//************************************
    // FullName:    KDIS::DATA_TYPE::ModeSTransponderStatus::SetSurfacePositionReportIndicator
    //              KDIS::DATA_TYPE::ModeSTransponderStatus::IsSurfacePositionReportIndicator
    // Description: Indicates whether there is a Squitter Surface Position Report IFF 
    //              Data Record either Present (true) or Not Present (false).
    // Parameter:   KBOOL SurfacePositionReportIndicator
    //************************************
	void SetSurfacePositionReportIndicator( KBOOL SurfacePositionReportIndicator );
	KBOOL GetSurfacePositionReportIndicator() const;

	//************************************
    // FullName:    KDIS::DATA_TYPE::ModeSTransponderStatus::SetIdentificationReportIndicator
    //              KDIS::DATA_TYPE::ModeSTransponderStatus::GetIdentificationReportIndicator
    // Description: Indicates whether there is a Squitter Identification Report IFF 
    //              Data Record either Present (true) or Not Present (false).
    // Parameter:   KBOOL IdentificationReportIndicator
    //************************************
	void SetIdentificationReportIndicator( KBOOL IdentificationReportIndicator );
	KBOOL GetIdentificationReportIndicator() const;

	//************************************
    // FullName:    KDIS::DATA_TYPE::ModeSTransponderStatus::SetEventDrivenReportIndicator
    //              KDIS::DATA_TYPE::ModeSTransponderStatus::GetEventDrivenReportIndicator
    // Description: Indicates whether there is a Squitter Event-Driven Report IFF Data 
    //              Record either Present (true) or Not Present (false).
    // Parameter:   KBOOL EventDrivenReportIndicator
    //************************************
	void SetEventDrivenReportIndicator( KBOOL EventDrivenReportIndicator );
	KBOOL GetEventDrivenReportIndicator() const;

	//************************************
    // FullName:    KDIS::DATA_TYPE::ModeSTransponderStatus::SetStatus
    //              KDIS::DATA_TYPE::ModeSTransponderStatus::GetStatus
    // Description: Indicates whether the Mode 5 transponder is On (true or Off (false).
    // Parameter:   KBOOL S
    //************************************
	void SetStatus( KBOOL S );
	KBOOL GetStatus() const;

	//************************************
    // FullName:    KDIS::DATA_TYPE::ModeSTransponderStatus::SetDamaged
    //              KDIS::DATA_TYPE::ModeSTransponderStatus::IsDamaged
    // Description: Indicates whether there is damage to the Mode 5 transponder.
    // Parameter:   KBOOL S
    //************************************
	void SetDamaged( KBOOL D );
	KBOOL IsDamaged() const;
		
	//************************************
    // FullName:    KDIS::DATA_TYPE::ModeSTransponderStatus::SetMalfunctioning
    //              KDIS::DATA_TYPE::ModeSTransponderStatus::IsMalfunctioning
    // Description: Indicates whether there is damage to the Mode 5 transponder.
    // Parameter:   KBOOL M
    //************************************
	void SetMalfunctioning( KBOOL M );
	KBOOL IsMalfunctioning() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSTransponderStatus::GetAsString
    // Description: Returns a string representation 
    //************************************
    virtual const KOCTET* GetAsString() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSTransponderStatus::Decode
    // Description: Convert From Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual void Decode( KDataStream & stream ) ;

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSTransponderStatus::Encode
    // Description: Convert To Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual const KDataStream* GetEncodedStream() const;
    virtual void EncodeToStream( KDataStream & stream ) const;

    KBOOL operator == ( const ModeSTransponderStatus & Value ) const;
    KBOOL operator != ( const ModeSTransponderStatus & Value ) const;
};

} // END namespace DATA_TYPE
} // END namespace KDIS
