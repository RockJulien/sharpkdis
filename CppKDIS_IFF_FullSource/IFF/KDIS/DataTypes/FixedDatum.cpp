/*********************************************************************
Copyright 2013 Karl Jones
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

For Further Information Please Contact me at
Karljj1@yahoo.com
http://p.sf.net/kdis/UserGuide
*********************************************************************/

#include "./FixedDatum.h"

using namespace KDIS;
using namespace DATA_TYPE;
using namespace ENUMS;

//////////////////////////////////////////////////////////////////////////
// Public:
//////////////////////////////////////////////////////////////////////////

FixedDatum::FixedDatum() :
    m_ui32DatumID( 0 )
{
	std::fill(m_DatumValue.begin(), m_DatumValue.end(), 0);
}

//////////////////////////////////////////////////////////////////////////

FixedDatum::FixedDatum( KDataStream & stream ) 
{
    Decode( stream );
}

//////////////////////////////////////////////////////////////////////////

FixedDatum::~FixedDatum()
{
}

//////////////////////////////////////////////////////////////////////////

void FixedDatum::SetDatumID( DatumID ID )
{
    m_ui32DatumID = ID;
}

//////////////////////////////////////////////////////////////////////////

DatumID FixedDatum::GetDatumID() const
{
    return ( DatumID )m_ui32DatumID;
}

//////////////////////////////////////////////////////////////////////////

const std::vector<KUOCTET> & FixedDatum::GetDatumValue() const
{
	return this->m_DatumValue;
}

//////////////////////////////////////////////////////////////////////////

void FixedDatum::SetDatumValue(const std::vector<KUOCTET> & pValues)
{
	this->m_DatumValue = pValues;
}

//////////////////////////////////////////////////////////////////////////

const KOCTET* FixedDatum::GetAsString() const
{
    KStringStream ss;

	unsigned int lPacked = 0;
	if (m_DatumValue.size() == 4)
	{
		lPacked = pack4chars(m_DatumValue[0], m_DatumValue[1], m_DatumValue[2], m_DatumValue[3]);
	}
	
    // TODO: maybe determine what the data type should be when printing out, this
    // could be a lot of work.
    ss << "Fixed Datum:"
       << "\n\tID:          " << GetEnumAsStringDatumID( m_ui32DatumID )
       << "\n\tValue(UI32): " << lPacked
       << "\n";

    return ss.str().c_str();
}

//////////////////////////////////////////////////////////////////////////

KUINT32 FixedDatum::pack4chars(KUOCTET c1, KUOCTET c2, KUOCTET c3, KUOCTET c4) const
{
	return (KUINT32)((int)(c1 << 24)
					| (int)(c2 << 16)
					| (int)(c3 << 8)
					| (int)c4);
}

//////////////////////////////////////////////////////////////////////////

void FixedDatum::Decode( KDataStream & stream ) 
{
    if( stream.GetBufferSize() < FixedDatum::FIXED_DATUM_SIZE )throw KException( __FUNCTION__, NOT_ENOUGH_DATA_IN_BUFFER );

    stream >> m_ui32DatumID;

    for( KUINT16 i = 0; i < 4; ++i )
    {
        stream >> m_DatumValue[i];
    }
}

//////////////////////////////////////////////////////////////////////////

const KDataStream* FixedDatum::GetEncodedStream() const
{
    m_stream.Clear();

	this->EncodeToStream( m_stream );

	return &m_stream;
}

//////////////////////////////////////////////////////////////////////////

void FixedDatum::EncodeToStream( KDataStream & stream ) const
{
    stream << m_ui32DatumID;

    for( KUINT16 i = 0; i < 4; ++i )
    {
        stream << m_DatumValue[i];
    }
}

//////////////////////////////////////////////////////////////////////////

KBOOL FixedDatum::operator == ( const FixedDatum & Value ) const
{
    if( m_ui32DatumID != Value.m_ui32DatumID )                 return false;
    return std::equal(m_DatumValue.begin(), m_DatumValue.end(), Value.m_DatumValue.begin());
}

//////////////////////////////////////////////////////////////////////////

KBOOL FixedDatum::operator != ( const FixedDatum & Value ) const
{
    return !( *this == Value );
}

//////////////////////////////////////////////////////////////////////////


