/*********************************************************************
Copyright 2013 Karl Jones
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

For Further Information Please Contact me at
Karljj1@yahoo.com
http://p.sf.net/kdis/UserGuide
*********************************************************************/

#include "./EnumIFF.h"

using namespace KDIS;
using namespace DATA_TYPE;
using namespace ENUMS;

// Implementation of string values for AlternateParameter4

#ifdef KDIS_USE_ENUM_DESCRIPTORS

const EnumDescriptor AlternateParameter4Descriptor[] =
{
    { 0 , "OtherAlternateParameter4" },
    { 1 , "Valid" },
    { 2 , "Invalid" },
    { 3 , "NoResponse" }
};

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizeAlternateParameter4()
{
    return sizeof( AlternateParameter4Descriptor ) / sizeof( EnumDescriptor );
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorAlternateParameter4( KUINT32 Index )
{
    return &AlternateParameter4Descriptor[Index];
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringAlternateParameter4( KINT32 Value )
{
    return EnumHelper::GetEnumAsString( AlternateParameter4Descriptor, sizeof( AlternateParameter4Descriptor ) / sizeof( EnumDescriptor ), Value );
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringAlternateParameter4( const KOCTET* Value, KINT32 & ValueOut )
{
    return EnumHelper::GetEnumFromString( AlternateParameter4Descriptor, sizeof( AlternateParameter4Descriptor ) / sizeof( EnumDescriptor ), Value, ValueOut );
}

#else

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizeAlternateParameter4()
{
    return 0;
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorAlternateParameter4( KUINT32 Index )
{
    return NULL;
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringAlternateParameter4( KINT32 Value )
{
    KStringStream ss;
    ss << Value;
    return ss.str().c_str();
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringAlternateParameter4( const KOCTET* Value, KINT32 & ValueOut )
{
    return false; // Maybe throw an exception?
}

#endif

//////////////////////////////////////////////////////////////////////////

// Implementation of string values for TCAS

#ifdef KDIS_USE_ENUM_DESCRIPTORS

const EnumDescriptor TCASDescriptor[] =
{
    { 0 , "TCAS_I" },
    { 1 , "TCAS_II" }
};

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizeTCAS()
{
    return sizeof( TCASDescriptor ) / sizeof( EnumDescriptor );
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorTCAS( KUINT32 Index )
{
    return &TCASDescriptor[Index];
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringTCAS( KINT32 Value )
{
    return EnumHelper::GetEnumAsString( TCASDescriptor, sizeof( TCASDescriptor ) / sizeof( EnumDescriptor ), Value );
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringTCAS( const KOCTET* Value, KINT32 & ValueOut )
{
    return EnumHelper::GetEnumFromString( TCASDescriptor, sizeof( TCASDescriptor ) / sizeof( EnumDescriptor ), Value, ValueOut );
}

#else

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizeTCAS()
{
    return 0;
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorTCAS( KUINT32 Index )
{
    return NULL;
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringTCAS( KINT32 Value )
{
    KStringStream ss;
    ss << Value;
    return ss.str().c_str();
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringTCAS( const KOCTET* Value, KINT32 & ValueOut )
{
    return false; // Maybe throw an exception?
}

#endif

//////////////////////////////////////////////////////////////////////////
#if DIS_VERSION > 6

// Implementation of string values for Mode5MessageFormat

#ifdef KDIS_USE_ENUM_DESCRIPTORS

const EnumDescriptor Mode5MessageFormatDescriptor[] =
{
    { 0 , "Capability" },
    { 1 , "ActiveInterrogation" }
};

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizeMode5MessageFormat()
{
    return sizeof( Mode5MessageFormatDescriptor ) / sizeof( EnumDescriptor );
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorMode5MessageFormat( KUINT32 Index )
{
    return &Mode5MessageFormatDescriptor[Index];
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringMode5MessageFormat( KINT32 Value )
{
    return EnumHelper::GetEnumAsString( Mode5MessageFormatDescriptor, sizeof( Mode5MessageFormatDescriptor ) / sizeof( EnumDescriptor ), Value );
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringMode5MessageFormat( const KOCTET* Value, KINT32 & ValueOut )
{
    return EnumHelper::GetEnumFromString( Mode5MessageFormatDescriptor, sizeof( Mode5MessageFormatDescriptor ) / sizeof( EnumDescriptor ), Value, ValueOut );
}

#else

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizeMode5MessageFormat()
{
    return 0;
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorMode5MessageFormat( KUINT32 Index )
{
    return NULL;
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringMode5MessageFormat( KINT32 Value )
{
    KStringStream ss;
    ss << Value;
    return ss.str().c_str();
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringMode5MessageFormat( const KOCTET* Value, KINT32 & ValueOut )
{
    return false; // Maybe throw an exception?
}

#endif

//////////////////////////////////////////////////////////////////////////

// Implementation of string values for Mode5Reply

#ifdef KDIS_USE_ENUM_DESCRIPTORS

const EnumDescriptor Mode5ReplyDescriptor[] =
{
    { 0 , "No Response" },
    { 1 , "Valid Reply" },
    { 2 , "Invalid Reply" },
    { 3 , "Unable To Verify" }
};

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizeMode5Reply()
{
    return sizeof( Mode5ReplyDescriptor ) / sizeof( EnumDescriptor );
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorMode5Reply( KUINT32 Index )
{
    return &Mode5ReplyDescriptor[Index];
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringMode5Reply( KINT32 Value )
{
    return EnumHelper::GetEnumAsString( Mode5MessageFormatDescriptor, sizeof( Mode5ReplyDescriptor ) / sizeof( EnumDescriptor ), Value );
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringMode5Reply( const KOCTET* Value, KINT32 & ValueOut )
{
    return EnumHelper::GetEnumFromString( Mode5MessageFormatDescriptor, sizeof( Mode5ReplyDescriptor ) / sizeof( EnumDescriptor ), Value, ValueOut );
}

#else

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizeMode5Reply()
{
    return 0;
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorMode5Reply( KUINT32 Index )
{
    return NULL;
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringMode5Reply( KINT32 Value )
{
    KStringStream ss;
    ss << Value;
    return ss.str().c_str();
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringMode5Reply( const KOCTET* Value, KINT32 & ValueOut )
{
    return false; // Maybe throw an exception?
}

#endif

//////////////////////////////////////////////////////////////////////////

// Implementation of string values for AntennaSelection

#ifdef KDIS_USE_ENUM_DESCRIPTORS

const EnumDescriptor AntennaSelectionDescriptor[] =
{
    { 0 , "No Statement" },
    { 1 , "Top" },
    { 2 , "Bottom" },
    { 3 , "Diversity" }
};

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizeAntennaSelection()
{
    return sizeof( AntennaSelectionDescriptor ) / sizeof( EnumDescriptor );
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorAntennaSelection( KUINT32 Index )
{
    return &AntennaSelectionDescriptor[Index];
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringAntennaSelection( KINT32 Value )
{
    return EnumHelper::GetEnumAsString( AntennaSelectionDescriptor, sizeof( Mode5ReplyDescriptor ) / sizeof( EnumDescriptor ), Value );
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringAntennaSelection( const KOCTET* Value, KINT32 & ValueOut )
{
    return EnumHelper::GetEnumFromString( AntennaSelectionDescriptor, sizeof( Mode5ReplyDescriptor ) / sizeof( EnumDescriptor ), Value, ValueOut );
}

#else

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizeAntennaSelection()
{
    return 0;
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorAntennaSelection( KUINT32 Index )
{
    return NULL;
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringAntennaSelection( KINT32 Value )
{
    KStringStream ss;
    ss << Value;
    return ss.str().c_str();
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringAntennaSelection( const KOCTET* Value, KINT32 & ValueOut )
{
    return false; // Maybe throw an exception?
}

#endif

//////////////////////////////////////////////////////////////////////////

// Implementation of string values for PlatformType

#ifdef KDIS_USE_ENUM_DESCRIPTORS

const EnumDescriptor PlatformTypeDescriptor[] =
{
    { 0 , "Ground" },
    { 1 , "Air" },
};

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizePlatformType()
{
    return sizeof( PlatformTypeDescriptor ) / sizeof( EnumDescriptor );
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorPlatformType( KUINT32 Index )
{
    return &PlatformTypeDescriptor[Index];
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringPlatformType( KINT32 Value )
{
    return EnumHelper::GetEnumAsString( PlatformTypeDescriptor, sizeof( Mode5ReplyDescriptor ) / sizeof( EnumDescriptor ), Value );
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringPlatformType( const KOCTET* Value, KINT32 & ValueOut )
{
    return EnumHelper::GetEnumFromString( PlatformTypeDescriptor, sizeof( Mode5ReplyDescriptor ) / sizeof( EnumDescriptor ), Value, ValueOut );
}

#else

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizePlatformType()
{
    return 0;
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorPlatformType( KUINT32 Index )
{
    return NULL;
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringPlatformType( KINT32 Value )
{
    KStringStream ss;
    ss << Value;
    return ss.str().c_str();
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringPlatformType( const KOCTET* Value, KINT32 & ValueOut )
{
    return false; // Maybe throw an exception?
}

#endif

//////////////////////////////////////////////////////////////////////////

// Implementation of string values for NavigationSource

#ifdef KDIS_USE_ENUM_DESCRIPTORS

const EnumDescriptor NavigationSourceDescriptor[] =
{
    { 0 , "No Statement" },
    { 1 , "GPS" },
    { 2 , "INS" },
    { 3 , "INS/GPS" },
};

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizeNavigationSource()
{
    return sizeof( NavigationSourceDescriptor ) / sizeof( EnumDescriptor );
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorNavigationSource( KUINT32 Index )
{
    return &NavigationSourceDescriptor[Index];
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringNavigationSource( KINT32 Value )
{
    return EnumHelper::GetEnumAsString( NavigationSourceDescriptor, sizeof( NavigationSourceDescriptor ) / sizeof( EnumDescriptor ), Value );
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringNavigationSource( const KOCTET* Value, KINT32 & ValueOut )
{
    return EnumHelper::GetEnumFromString( NavigationSourceDescriptor, sizeof( NavigationSourceDescriptor ) / sizeof( EnumDescriptor ), Value, ValueOut );
}

#else

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizeNavigationSource()
{
    return 0;
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorNavigationSource( KUINT32 Index )
{
    return NULL;
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringNavigationSource( KINT32 Value )
{
    KStringStream ss;
    ss << Value;
    return ss.str().c_str();
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringNavigationSource( const KOCTET* Value, KINT32 & ValueOut )
{
    return false; // Maybe throw an exception?
}

#endif

//////////////////////////////////////////////////////////////////////////

// Implementation of string values for SquitterType

#ifdef KDIS_USE_ENUM_DESCRIPTORS

const EnumDescriptor SquitterTypeDescriptor[] =
{
    { 0 , "Not Capable" },
    { 1 , "Acquisition" },
    { 2 , "Extended" },
    { 3 , "Short" },
};

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizeSquitterType()
{
    return sizeof( SquitterTypeDescriptor ) / sizeof( EnumDescriptor );
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorSquitterType( KUINT32 Index )
{
    return &SquitterTypeDescriptor[Index];
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringSquitterType( KINT32 Value )
{
    return EnumHelper::GetEnumAsString( SquitterTypeDescriptor, sizeof( SquitterTypeDescriptor ) / sizeof( EnumDescriptor ), Value );
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringSquitterType( const KOCTET* Value, KINT32 & ValueOut )
{
    return EnumHelper::GetEnumFromString( SquitterTypeDescriptor, sizeof( SquitterTypeDescriptor ) / sizeof( EnumDescriptor ), Value, ValueOut );
}

#else

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizeSquitterType()
{
    return 0;
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorSquitterType( KUINT32 Index )
{
    return NULL;
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringSquitterType( KINT32 Value )
{
    KStringStream ss;
    ss << Value;
    return ss.str().c_str();
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringSquitterType( const KOCTET* Value, KINT32 & ValueOut )
{
    return false; // Maybe throw an exception?
}

#endif

//////////////////////////////////////////////////////////////////////////

// Implementation of string values for AircraftPresentDomain

#ifdef KDIS_USE_ENUM_DESCRIPTORS

const EnumDescriptor AircraftPresentDomainDescriptor[] =
{
    { 0 , "No Statement" },
    { 1 , "Airbone" },
    { 2 , "On Ground/Surface" },
};

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizeAircraftPresentDomain()
{
    return sizeof( AircraftPresentDomainDescriptor ) / sizeof( EnumDescriptor );
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorAircraftPresentDomain( KUINT32 Index )
{
    return &AircraftPresentDomainDescriptor[Index];
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringAircraftPresentDomain( KINT32 Value )
{
    return EnumHelper::GetEnumAsString( AircraftPresentDomainDescriptor, sizeof( AircraftPresentDomainDescriptor ) / sizeof( EnumDescriptor ), Value );
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringAircraftPresentDomain( const KOCTET* Value, KINT32 & ValueOut )
{
    return EnumHelper::GetEnumFromString( AircraftPresentDomainDescriptor, sizeof( AircraftPresentDomainDescriptor ) / sizeof( EnumDescriptor ), Value, ValueOut );
}

#else

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizeAircraftPresentDomain()
{
    return 0;
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorAircraftPresentDomain( KUINT32 Index )
{
    return NULL;
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringAircraftPresentDomain( KINT32 Value )
{
    KStringStream ss;
    ss << Value;
    return ss.str().c_str();
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringAircraftPresentDomain( const KOCTET* Value, KINT32 & ValueOut )
{
    return false; // Maybe throw an exception?
}

#endif


//////////////////////////////////////////////////////////////////////////

// Implementation of string values for AircraftIDType

#ifdef KDIS_USE_ENUM_DESCRIPTORS

const EnumDescriptor AircraftIDTypeDescriptor[] =
{
    { 0 , "No Statement" },
    { 1 , "Flight Number" },
    { 2 , "Tail Number" },
};

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizeAircraftIDType()
{
    return sizeof( AircraftIDTypeDescriptor ) / sizeof( EnumDescriptor );
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorAircraftIDType( KUINT32 Index )
{
    return &AircraftIDTypeDescriptor[Index];
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringAircraftIDType( KINT32 Value )
{
    return EnumHelper::GetEnumAsString( AircraftIDTypeDescriptor, sizeof( AircraftIDTypeDescriptor ) / sizeof( EnumDescriptor ), Value );
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringAircraftIDType( const KOCTET* Value, KINT32 & ValueOut )
{
    return EnumHelper::GetEnumFromString( AircraftIDTypeDescriptor, sizeof( AircraftIDTypeDescriptor ) / sizeof( EnumDescriptor ), Value, ValueOut );
}

#else

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizeAircraftIDType()
{
    return 0;
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorAircraftIDType( KUINT32 Index )
{
    return NULL;
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringAircraftIDType( KINT32 Value )
{
    KStringStream ss;
    ss << Value;
    return ss.str().c_str();
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringAircraftIDType( const KOCTET* Value, KINT32 & ValueOut )
{
    return false; // Maybe throw an exception?
}

#endif

//////////////////////////////////////////////////////////////////////////

// Implementation of string values for AltitudeResolution

#ifdef KDIS_USE_ENUM_DESCRIPTORS

const EnumDescriptor AltitudeResolutionDescriptor[] =
{
    { 0 , "100-foot" },
    { 1 , "25-foot" },
};

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizeAltitudeResolution()
{
    return sizeof( AltitudeResolutionDescriptor ) / sizeof( EnumDescriptor );
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorAltitudeResolution( KUINT32 Index )
{
    return &AltitudeResolutionDescriptor[Index];
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringAltitudeResolution( KINT32 Value )
{
    return EnumHelper::GetEnumAsString( AltitudeResolutionDescriptor, sizeof( AltitudeResolutionDescriptor ) / sizeof( EnumDescriptor ), Value );
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringAltitudeResolution( const KOCTET* Value, KINT32 & ValueOut )
{
    return EnumHelper::GetEnumFromString( AltitudeResolutionDescriptor, sizeof( AltitudeResolutionDescriptor ) / sizeof( EnumDescriptor ), Value, ValueOut );
}

#else

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizeAltitudeResolution()
{
    return 0;
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorAltitudeResolution( KUINT32 Index )
{
    return NULL;
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringAltitudeResolution( KINT32 Value )
{
    KStringStream ss;
    ss << Value;
    return ss.str().c_str();
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringAltitudeResolution( const KOCTET* Value, KINT32 & ValueOut )
{
    return false; // Maybe throw an exception?
}

#endif

//////////////////////////////////////////////////////////////////////////

// Implementation of string values for CapabilityReport

#ifdef KDIS_USE_ENUM_DESCRIPTORS

const EnumDescriptor CapabilityReportDescriptor[] =
{
    { 0 , "No Communications Capability (CA)" },
    { 1 , "Reserved" },
    { 2 , "Reserved" },
    { 3 , "Reserved" },
    { 4 , "Signifies at Least Comm-A and Comm-B Capability and Ability to Set CA Code 7 and on the Ground" },
    { 5 , "Signifies at Least Comm-A and Comm-B capability and Ability to Set CA Code 7 and Airborne" },
    { 6 , "Signifies at Least Comm-A and Comm-B capability and Ability to Set CA Code 7 and Either Airborne or on the Ground" },
    { 7 , "Signifies the Downlink Request (DR) Field Is Not Equal To 0 and The Flight Status (FS) Field Equals 2, 3, 4 or 5, and Either Airborne or on the Ground" },
    { 255 , "No Statement" },
};

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizeCapabilityReport()
{
    return sizeof( CapabilityReportDescriptor ) / sizeof( EnumDescriptor );
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorCapabilityReport( KUINT32 Index )
{
    return &CapabilityReportDescriptor[Index];
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringCapabilityReport( KINT32 Value )
{
    return EnumHelper::GetEnumAsString( CapabilityReportDescriptor, sizeof( CapabilityReportDescriptor ) / sizeof( EnumDescriptor ), Value );
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringCapabilityReport( const KOCTET* Value, KINT32 & ValueOut )
{
    return EnumHelper::GetEnumFromString( CapabilityReportDescriptor, sizeof( CapabilityReportDescriptor ) / sizeof( EnumDescriptor ), Value, ValueOut );
}

#else

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizeCapabilityReport()
{
    return 0;
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorCapabilityReport( KUINT32 Index )
{
    return NULL;
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringCapabilityReport( KINT32 Value )
{
    KStringStream ss;
    ss << Value;
    return ss.str().c_str();
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringCapabilityReport( const KOCTET* Value, KINT32 & ValueOut )
{
    return false; // Maybe throw an exception?
}

#endif

//////////////////////////////////////////////////////////////////////////

// Implementation of string values for DataCategory

#ifdef KDIS_USE_ENUM_DESCRIPTORS

const EnumDescriptor DataCategoryDescriptor[] =
{
    { 0 , "No Statement" },
    { 1 , "Functional Data" },
    { 2 , "Transponder/Interrogator Data Link Messages" },
};

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizeDataCategory()
{
    return sizeof( DataCategoryDescriptor ) / sizeof( EnumDescriptor );
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorDataCategory( KUINT32 Index )
{
    return &DataCategoryDescriptor[Index];
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringDataCategory( KINT32 Value )
{
    return EnumHelper::GetEnumAsString( DataCategoryDescriptor, sizeof( DataCategoryDescriptor ) / sizeof( EnumDescriptor ), Value );
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringDataCategory( const KOCTET* Value, KINT32 & ValueOut )
{
    return EnumHelper::GetEnumFromString( DataCategoryDescriptor, sizeof( DataCategoryDescriptor ) / sizeof( EnumDescriptor ), Value, ValueOut );
}

#else

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizeDataCategory()
{
    return 0;
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorDataCategory( KUINT32 Index )
{
    return NULL;
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringDataCategory( KINT32 Value )
{
    KStringStream ss;
    ss << Value;
    return ss.str().c_str();
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringDataCategory( const KOCTET* Value, KINT32 & ValueOut )
{
    return false; // Maybe throw an exception?
}

#endif

//////////////////////////////////////////////////////////////////////////

// Implementation of string values for TransmitStateModeS

#ifdef KDIS_USE_ENUM_DESCRIPTORS

const EnumDescriptor TransmitStateModeSDescriptor[] =
{
    { 0 , "No Statement" },
    { 1 , "Roll-Call" },
    { 2 , "All Call" },
    { 3 , "Lockout Override" },
    { 4 , "Temporary Lockout" },
    { 5 , "Intermittent Lockout" },
};

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizeTransmitStateModeS()
{
    return sizeof( TransmitStateModeSDescriptor ) / sizeof( EnumDescriptor );
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorTransmitStateModeS( KUINT32 Index )
{
    return &TransmitStateModeSDescriptor[Index];
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringTransmitStateModeS( KINT32 Value )
{
    return EnumHelper::GetEnumAsString( TransmitStateModeSDescriptor, sizeof( TransmitStateModeSDescriptor ) / sizeof( EnumDescriptor ), Value );
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringTransmitStateModeS( const KOCTET* Value, KINT32 & ValueOut )
{
    return EnumHelper::GetEnumFromString( TransmitStateModeSDescriptor, sizeof( TransmitStateModeSDescriptor ) / sizeof( EnumDescriptor ), Value, ValueOut );
}

#else

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizeTransmitStateModeS()
{
    return 0;
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorTransmitStateModeS( KUINT32 Index )
{
    return NULL;
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringTransmitStateModeS( KINT32 Value )
{
    KStringStream ss;
    ss << Value;
    return ss.str().c_str();
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringTransmitStateModeS( const KOCTET* Value, KINT32 & ValueOut )
{
    return false; // Maybe throw an exception?
}

#endif

//////////////////////////////////////////////////////////////////////////

// Implementation of string values for TransmissionIndicator

#ifdef KDIS_USE_ENUM_DESCRIPTORS

const EnumDescriptor TransmissionIndicatorDescriptor[] =
{
    { 0 , "No Statement" },
    { 1 , "Original Interrogation" },
    { 2 , "Interrogation Reply" },
    { 3 , "Squitter Transmission" },
};

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizeTransmissionIndicator()
{
    return sizeof( TransmissionIndicatorDescriptor ) / sizeof( EnumDescriptor );
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorTransmissionIndicator( KUINT32 Index )
{
    return &TransmissionIndicatorDescriptor[Index];
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringTransmissionIndicator( KINT32 Value )
{
    return EnumHelper::GetEnumAsString( TransmissionIndicatorDescriptor, sizeof( TransmissionIndicatorDescriptor ) / sizeof( EnumDescriptor ), Value );
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringTransmissionIndicator( const KOCTET* Value, KINT32 & ValueOut )
{
    return EnumHelper::GetEnumFromString( TransmissionIndicatorDescriptor, sizeof( TransmissionIndicatorDescriptor ) / sizeof( EnumDescriptor ), Value, ValueOut );
}

#else

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizeTransmissionIndicator()
{
    return 0;
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorTransmissionIndicator( KUINT32 Index )
{
    return NULL;
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringTransmissionIndicator( KINT32 Value )
{
    KStringStream ss;
    ss << Value;
    return ss.str().c_str();
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringTransmissionIndicator( const KOCTET* Value, KINT32 & ValueOut )
{
    return false; // Maybe throw an exception?
}

#endif

//////////////////////////////////////////////////////////////////////////

// Implementation of string values for ReplyAmplification

#ifdef KDIS_USE_ENUM_DESCRIPTORS

const EnumDescriptor ReplyAmplificationDescriptor[] =
{
    { 0 , "No Statement" },
    { 1 , "Complete" },
    { 2 , "Limted" },
    { 3 , "Unable to Respond" },
};

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizeReplyAmplification()
{
    return sizeof( ReplyAmplificationDescriptor ) / sizeof( EnumDescriptor );
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorReplyAmplification( KUINT32 Index )
{
    return &ReplyAmplificationDescriptor[Index];
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringReplyAmplification( KINT32 Value )
{
    return EnumHelper::GetEnumAsString( ReplyAmplificationDescriptor, sizeof( ReplyAmplificationDescriptor ) / sizeof( EnumDescriptor ), Value );
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringReplyAmplification( const KOCTET* Value, KINT32 & ValueOut )
{
    return EnumHelper::GetEnumFromString( ReplyAmplificationDescriptor, sizeof( ReplyAmplificationDescriptor ) / sizeof( EnumDescriptor ), Value, ValueOut );
}

#else

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizeReplyAmplification()
{
    return 0;
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorReplyAmplification( KUINT32 Index )
{
    return NULL;
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringReplyAmplification( KINT32 Value )
{
    KStringStream ss;
    ss << Value;
    return ss.str().c_str();
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringReplyAmplification( const KOCTET* Value, KINT32 & ValueOut )
{
    return false; // Maybe throw an exception?
}

#endif

//////////////////////////////////////////////////////////////////////////

// Implementation of string values for AlternateMode4ChallengeReply

#ifdef KDIS_USE_ENUM_DESCRIPTORS

const EnumDescriptor AlternateMode4ChallengeReplyDescriptor[] =
{
    { 0 , "No Statement" },
    { 1 , "Valid" },
    { 2 , "Invalid" },
    { 3 , "No Response" },
    { 4 , "Unable to Verify" },
};

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizeAlternateMode4ChallengeReply()
{
    return sizeof( AlternateMode4ChallengeReplyDescriptor ) / sizeof( EnumDescriptor );
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorAlternateMode4ChallengeReply( KUINT32 Index )
{
    return &AlternateMode4ChallengeReplyDescriptor[Index];
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringAlternateMode4ChallengeReply( KINT32 Value )
{
    return EnumHelper::GetEnumAsString( AlternateMode4ChallengeReplyDescriptor, sizeof( AlternateMode4ChallengeReplyDescriptor ) / sizeof( EnumDescriptor ), Value );
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringAlternateMode4ChallengeReply( const KOCTET* Value, KINT32 & ValueOut )
{
    return EnumHelper::GetEnumFromString( AlternateMode4ChallengeReplyDescriptor, sizeof( AlternateMode4ChallengeReplyDescriptor ) / sizeof( EnumDescriptor ), Value, ValueOut );
}

#else

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizeAlternateMode4ChallengeReply()
{
    return 0;
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorAlternateMode4ChallengeReply( KUINT32 Index )
{
    return NULL;
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringAlternateMode4ChallengeReply( KINT32 Value )
{
    KStringStream ss;
    ss << Value;
    return ss.str().c_str();
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringAlternateMode4ChallengeReply( const KOCTET* Value, KINT32 & ValueOut )
{
    return false; // Maybe throw an exception?
}

#endif

//////////////////////////////////////////////////////////////////////////

// Implementation of string values for ModeSInterrogatorIdentifierICType

#ifdef KDIS_USE_ENUM_DESCRIPTORS

const EnumDescriptor ModeSInterrogatorIdentifierICTypeDescriptor[] =
{
    { 0 , "II" },
    { 1 , "SI" },
};

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizeModeSInterrogatorIdentifierICType()
{
    return sizeof( ModeSInterrogatorIdentifierICTypeDescriptor ) / sizeof( EnumDescriptor );
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorModeSInterrogatorIdentifierICType( KUINT32 Index )
{
    return &ModeSInterrogatorIdentifierICTypeDescriptor[Index];
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringModeSInterrogatorIdentifierICType( KINT32 Value )
{
    return EnumHelper::GetEnumAsString( ModeSInterrogatorIdentifierICTypeDescriptor, sizeof( ModeSInterrogatorIdentifierICTypeDescriptor ) / sizeof( EnumDescriptor ), Value );
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringModeSInterrogatorIdentifierICType( const KOCTET* Value, KINT32 & ValueOut )
{
    return EnumHelper::GetEnumFromString( ModeSInterrogatorIdentifierICTypeDescriptor, sizeof( ModeSInterrogatorIdentifierICTypeDescriptor ) / sizeof( EnumDescriptor ), Value, ValueOut );
}

#else

KUINT32 KDIS::DATA_TYPE::ENUMS::GetEnumSizeModeSInterrogatorIdentifierICType()
{
    return 0;
}

const EnumDescriptor * KDIS::DATA_TYPE::ENUMS::GetEnumDescriptorModeSInterrogatorIdentifierICType( KUINT32 Index )
{
    return NULL;
}

const KOCTET* KDIS::DATA_TYPE::ENUMS::GetEnumAsStringModeSInterrogatorIdentifierICType( KINT32 Value )
{
    KStringStream ss;
    ss << Value;
    return ss.str().c_str();
}

KBOOL KDIS::DATA_TYPE::ENUMS::GetEnumFromStringModeSInterrogatorIdentifierICType( const KOCTET* Value, KINT32 & ValueOut )
{
    return false; // Maybe throw an exception?
}

#endif

#endif // Endif DIS Version > 6
