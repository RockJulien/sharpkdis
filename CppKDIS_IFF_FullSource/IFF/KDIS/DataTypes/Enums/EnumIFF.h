/*********************************************************************
Copyright 2013 Karl Jones
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

For Further Information Please Contact me at
Karljj1@yahoo.com
http://p.sf.net/kdis/UserGuide
*********************************************************************/

/********************************************************************
    EnumFundamentalOperationalData
    created:    5/12/2008
    author:     Karl Jones

    purpose:    Enums for Fundamental Operational Data
*********************************************************************/

#pragma once

#include "./EnumDescriptor.h"

namespace KDIS {
namespace DATA_TYPE {
namespace ENUMS {

/************************************************************************/
/* Alternate Parameter 4                                                */
/* Used for an alternative representation of parameter 4                */
/* Used In:                                                             */
/*  Fundamental Operational Data                                        */
/************************************************************************/

enum AlternateParameter4
{
    OtherAlternateParameter4                                          = 0,
    Valid                                                             = 1,
    Invalid                                                           = 2,
    NoResponse                                                        = 3,
    // 4+ Not used
};

// Returns number of values in the EnumDescriptor for this enum.
// This can be used to iterate through all possible enum values by using GetEnumDescriptor<enum>.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then 0 will be returned.
KDIS_EXPORT KUINT32 GetEnumSizeAlternateParameter4();

// Returns the EnumDescriptor value for the specified index.
// Use GetEnumSize<enum> to get the array size.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then NULL will be returned.
KDIS_EXPORT const EnumDescriptor * GetEnumDescriptorAlternateParameter4( KUINT32 Index );

KDIS_EXPORT const KOCTET* GetEnumAsStringAlternateParameter4( KINT32 Value );

// Returns true if a value was found.
KDIS_EXPORT KBOOL GetEnumFromStringAlternateParameter4( const KOCTET* Value, KINT32 & ValueOut );

/************************************************************************/
/* TCAS                                                                 */
/* TCAS Version                                                         */
/* Used In:                                                             */
/*  Fundamental Operational Data System 1                               */
/************************************************************************/

enum TCAS
{
    TCAS_I                                                            = 0,
    TCAS_II                                                           = 1
};

// Returns number of values in the EnumDescriptor for this enum.
// This can be used to iterate through all possible enum values by using GetEnumDescriptor<enum>.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then 0 will be returned.
KDIS_EXPORT KUINT32 GetEnumSizeTCAS();

// Returns the EnumDescriptor value for the specified index.
// Use GetEnumSize<enum> to get the array size.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then NULL will be returned.
KDIS_EXPORT const EnumDescriptor * GetEnumDescriptorTCAS( KUINT32 Index );

KDIS_EXPORT const KOCTET* GetEnumAsStringTCAS( KINT32 Value );

// Returns true if a value was found.
KDIS_EXPORT KBOOL GetEnumFromStringTCAS( const KOCTET* Value, KINT32 & ValueOut );

#if DIS_VERSION > 6

/************************************************************************/
/* Mode 5 Message Format                                                */
/* Indicate the Mode 5 Message Format.                                  */
/* Used In:                                                             */
/*  Mode 5 Interrogator Status                                          */
/************************************************************************/

enum Mode5MessageFormat
{
    Capability                                                        = 0,
    ActiveInterrogation                                               = 1
};

// Returns number of values in the EnumDescriptor for this enum.
// This can be used to iterate through all possible enum values by using GetEnumDescriptor<enum>.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then 0 will be returned.
KDIS_EXPORT KUINT32 GetEnumSizeMode5MessageFormat();

// Returns the EnumDescriptor value for the specified index.
// Use GetEnumSize<enum> to get the array size.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then NULL will be returned.
KDIS_EXPORT const EnumDescriptor * GetEnumDescriptorMode5MessageFormat( KUINT32 Index );

KDIS_EXPORT const KOCTET* GetEnumAsStringMode5MessageFormat( KINT32 Value );

// Returns true if a value was found.
KDIS_EXPORT KBOOL GetEnumFromStringMode5MessageFormat( const KOCTET* Value, KINT32 & ValueOut );

/************************************************************************/
/* Mode 5 Reply                                                         */
/* Specifies the validity of a reply that would be transmitted          */
/* by a Mode 5 transponder if interrogated.                             */
/* Used In:                                                             */
/*  Mode 5 Transponder Status                                           */
/************************************************************************/

enum Mode5Reply
{
    NoResponseReply                                                   = 0,
    ValidReply                                                        = 1,
    InvalidReply                                                      = 2,
    UnableToVerify                                                    = 3
};

// Returns number of values in the EnumDescriptor for this enum.
// This can be used to iterate through all possible enum values by using GetEnumDescriptor<enum>.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then 0 will be returned.
KDIS_EXPORT KUINT32 GetEnumSizeMode5Reply();

// Returns the EnumDescriptor value for the specified index.
// Use GetEnumSize<enum> to get the array size.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then NULL will be returned.
KDIS_EXPORT const EnumDescriptor * GetEnumDescriptorMode5Reply( KUINT32 Index );

KDIS_EXPORT const KOCTET* GetEnumAsStringMode5Reply( KINT32 Value );

// Returns true if a value was found.
KDIS_EXPORT KBOOL GetEnumFromStringMode5Reply( const KOCTET* Value, KINT32 & ValueOut );

/************************************************************************/
/* Antenna Selection                                                    */
/* The present Mode 5 transponder antenna selection                     */
/* Used In:                                                             */
/*  Mode 5 Transponder Status                                           */
/************************************************************************/

enum AntennaSelection
{
    AntennaSelectionNoStatement                                       = 0,
    Top                                                               = 1,
    Bottom                                                            = 2,
    Diversity                                                         = 3
};

// Returns number of values in the EnumDescriptor for this enum.
// This can be used to iterate through all possible enum values by using GetEnumDescriptor<enum>.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then 0 will be returned.
KDIS_EXPORT KUINT32 GetEnumSizeAntennaSelection();

// Returns the EnumDescriptor value for the specified index.
// Use GetEnumSize<enum> to get the array size.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then NULL will be returned.
KDIS_EXPORT const EnumDescriptor * GetEnumDescriptorAntennaSelection( KUINT32 Index );

KDIS_EXPORT const KOCTET* GetEnumAsStringAntennaSelection( KINT32 Value );

// Returns true if a value was found.
KDIS_EXPORT KBOOL GetEnumFromStringAntennaSelection( const KOCTET* Value, KINT32 & ValueOut );

/************************************************************************/
/* Platform Type	                                                    */
/* The type of platform (air or ground) that is associated with a Mode  */
/* 5 transponder.                                                       */
/* Used In:                                                             */
/*  Mode 5 Transponder Status                                           */
/************************************************************************/

enum PlatformType
{
    GroundPlatformType                                                = 0,
    AirPlatformType                                                   = 1
};

// Returns number of values in the EnumDescriptor for this enum.
// This can be used to iterate through all possible enum values by using GetEnumDescriptor<enum>.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then 0 will be returned.
KDIS_EXPORT KUINT32 GetEnumSizePlatformType();

// Returns the EnumDescriptor value for the specified index.
// Use GetEnumSize<enum> to get the array size.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then NULL will be returned.
KDIS_EXPORT const EnumDescriptor * GetEnumDescriptorPlatformType( KUINT32 Index );

KDIS_EXPORT const KOCTET* GetEnumAsStringPlatformType( KINT32 Value );

// Returns true if a value was found.
KDIS_EXPORT KBOOL GetEnumFromStringPlatformType( const KOCTET* Value, KINT32 & ValueOut );

/************************************************************************/
/* Navigation Source                                                    */
/* The navigation source.                                               */
/* Used In:                                                             */
/*  Mode 5 Transponder Basic Data                                       */
/************************************************************************/

enum NavigationSource
{
    NoStatementNavigationSource                                       = 0,
    GPS                                                               = 1,
    INS                                                               = 2,
    INS_GPS                                                           = 3
};

// Returns number of values in the EnumDescriptor for this enum.
// This can be used to iterate through all possible enum values by using GetEnumDescriptor<enum>.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then 0 will be returned.
KDIS_EXPORT KUINT32 GetEnumSizeNavigationSource();

// Returns the EnumDescriptor value for the specified index.
// Use GetEnumSize<enum> to get the array size.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then NULL will be returned.
KDIS_EXPORT const EnumDescriptor * GetEnumDescriptorNavigationSource( KUINT32 Index );

KDIS_EXPORT const KOCTET* GetEnumAsStringNavigationSource( KINT32 Value );

// Returns true if a value was found.
KDIS_EXPORT KBOOL GetEnumFromStringNavigationSource( const KOCTET* Value, KINT32 & ValueOut );

/************************************************************************/
/* Squitter type                                                        */
/* The type of Mode S squitter.                                         */
/* Used In:                                                             */
/*  Mode S Transponder Basic Data                                       */
/************************************************************************/

enum SquitterType
{
    NotCapable                                                        = 0,
    Acquisition                                                       = 1,
    Extended                                                          = 2,
    Short                                                             = 3
};

// Returns number of values in the EnumDescriptor for this enum.
// This can be used to iterate through all possible enum values by using GetEnumDescriptor<enum>.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then 0 will be returned.
KDIS_EXPORT KUINT32 GetEnumSizeSquitterType();

// Returns the EnumDescriptor value for the specified index.
// Use GetEnumSize<enum> to get the array size.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then NULL will be returned.
KDIS_EXPORT const EnumDescriptor * GetEnumDescriptorSquitterType( KUINT32 Index );

KDIS_EXPORT const KOCTET* GetEnumAsStringSquitterType( KINT32 Value );

// Returns true if a value was found.
KDIS_EXPORT KBOOL GetEnumFromStringSquitterType( const KOCTET* Value, KINT32 & ValueOut );

/************************************************************************/
/* Aircraft Present Domain                                              */
/* The domain of the aircraft.                                          */
/* Used In:                                                             */
/*  Mode S Transponder Basic Data                                       */
/************************************************************************/

enum AircraftPresentDomain
{
    NoStatementAircraftPresentDomain                              = 0,
    Airbone                                                       = 1,
    OnGroundSurface                                               = 2
};

// Returns number of values in the EnumDescriptor for this enum.
// This can be used to iterate through all possible enum values by using GetEnumDescriptor<enum>.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then 0 will be returned.
KDIS_EXPORT KUINT32 GetEnumSizeAircraftPresentDomain();

// Returns the EnumDescriptor value for the specified index.
// Use GetEnumSize<enum> to get the array size.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then NULL will be returned.
KDIS_EXPORT const EnumDescriptor * GetEnumDescriptorAircraftPresentDomain( KUINT32 Index );

KDIS_EXPORT const KOCTET* GetEnumAsStringAircraftPresentDomain( KINT32 Value );

// Returns true if a value was found.
KDIS_EXPORT KBOOL GetEnumFromStringAircraftPresentDomain( const KOCTET* Value, KINT32 & ValueOut );

/************************************************************************/
/* Aircraft Identification Type                                         */
/* The identification type of the aircraft.                             */
/* Used In:                                                             */
/*  Mode S Transponder Basic Data                                       */
/************************************************************************/

enum AircraftIDType
{
    NoStatementAircraftIDType                              = 0,
    FlightNumber                                           = 1,
    TailNumber                                             = 2
};

// Returns number of values in the EnumDescriptor for this enum.
// This can be used to iterate through all possible enum values by using GetEnumDescriptor<enum>.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then 0 will be returned.
KDIS_EXPORT KUINT32 GetEnumSizeAircraftIDType();

// Returns the EnumDescriptor value for the specified index.
// Use GetEnumSize<enum> to get the array size.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then NULL will be returned.
KDIS_EXPORT const EnumDescriptor * GetEnumDescriptorAircraftIDType( KUINT32 Index );

KDIS_EXPORT const KOCTET* GetEnumAsStringAircraftIDType( KINT32 Value );

// Returns true if a value was found.
KDIS_EXPORT KBOOL GetEnumFromStringAircraftIDType( const KOCTET* Value, KINT32 & ValueOut );

/************************************************************************/
/* Mode 5/S Altitude Resolution                                         */
/* Mode S altitude resolution as being reported in either               */
/* 100-foot or 25-foot increments.                                      */
/* Used In:                                                             */
/*  Mode S Transponder Basic Data                                       */
/************************************************************************/

enum AltitudeResolution
{
    FOOT_100                                               = 0,
    FOOT_25                                                = 1
};

// Returns number of values in the EnumDescriptor for this enum.
// This can be used to iterate through all possible enum values by using GetEnumDescriptor<enum>.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then 0 will be returned.
KDIS_EXPORT KUINT32 GetEnumSizeAltitudeResolution();

// Returns the EnumDescriptor value for the specified index.
// Use GetEnumSize<enum> to get the array size.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then NULL will be returned.
KDIS_EXPORT const EnumDescriptor * GetEnumDescriptorAltitudeResolution( KUINT32 Index );

KDIS_EXPORT const KOCTET* GetEnumAsStringAltitudeResolution( KINT32 Value );

// Returns true if a value was found.
KDIS_EXPORT KBOOL GetEnumFromStringAltitudeResolution( const KOCTET* Value, KINT32 & ValueOut );

/************************************************************************/
/* Capability Report                                                    */
/* Shall contain the Capability Report. The numeric values are from     */
/* Annex 10 to ICAO Vol. IV, para 3.1.2.5.2.2.1,                        */
/* except for No Statement (see Clause 2)                               */
/* Used In:                                                             */
/*  Mode S Transponder Basic Data                                       */
/************************************************************************/

enum CapabilityReport
{
    NoCommunicationsCapability                                = 0,
    Reserved_1                                                = 1,
    Reserved_2                                                = 2,
    Reserved_3                                                = 3,
    CA_CB_Ground                                              = 4,
    CA_CB_Airbone                                             = 5,
    CA_CB_GroundAirbone                                       = 6,
    DR                                                        = 7,
    NoStatementCapabilityReport                               = 255
};

// Returns number of values in the EnumDescriptor for this enum.
// This can be used to iterate through all possible enum values by using GetEnumDescriptor<enum>.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then 0 will be returned.
KDIS_EXPORT KUINT32 GetEnumSizeCapabilityReport();

// Returns the EnumDescriptor value for the specified index.
// Use GetEnumSize<enum> to get the array size.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then NULL will be returned.
KDIS_EXPORT const EnumDescriptor * GetEnumDescriptorCapabilityReport( KUINT32 Index );

KDIS_EXPORT const KOCTET* GetEnumAsStringCapabilityReport( KINT32 Value );

// Returns true if a value was found.
KDIS_EXPORT KBOOL GetEnumFromStringCapabilityReport( const KOCTET* Value, KINT32 & ValueOut );


/************************************************************************/
/* Data category                                                        */
/* Shall contain the Capability Report. The numeric values are from     */
/* Annex 10 to ICAO Vol. IV, para 3.1.2.5.2.2.1,                        */
/* except for No Statement (see Clause 2)                               */
/* Used In:                                                             */
/*  Mode S Transponder Basic Data                                       */
/************************************************************************/

enum DataCategory
{
    NoStatementDataCategory                                   = 0,
    FunctionalData                                            = 1,
    Transponder_InterrogatorDataLinkMessages                  = 2
};

// Returns number of values in the EnumDescriptor for this enum.
// This can be used to iterate through all possible enum values by using GetEnumDescriptor<enum>.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then 0 will be returned.
KDIS_EXPORT KUINT32 GetEnumSizeDataCategory();

// Returns the EnumDescriptor value for the specified index.
// Use GetEnumSize<enum> to get the array size.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then NULL will be returned.
KDIS_EXPORT const EnumDescriptor * GetEnumDescriptorDataCategory( KUINT32 Index );

KDIS_EXPORT const KOCTET* GetEnumAsStringDataCategory( KINT32 Value );

// Returns true if a value was found.
KDIS_EXPORT KBOOL GetEnumFromStringDataCategory( const KOCTET* Value, KINT32 & ValueOut );


/************************************************************************/
/* Transmit State                                                       */
/* Indicates the state of the Mode S interrogator (see [UID 347]).      */
/* This field shall be set to No Statement (0) when the Mode S          */
/* interrogator simulation is operating in the Regeneration Mode.       */
/* It may be set to other values if operating in the Interactive Mode   */
/* Used In:                                                             */
/*  Mode S Transponder Basic Data                                       */
/************************************************************************/

enum TransmitStateModeS
{
    NoStatementTransmitState = 0,
    RollCall                 = 1,
    AllCall                  = 2,
    LockoutOverride          = 3,
    TemporaryLockout         = 4,
    IntermittentLockout      = 5
};

// Returns number of values in the EnumDescriptor for this enum.
// This can be used to iterate through all possible enum values by using GetEnumDescriptor<enum>.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then 0 will be returned.
KDIS_EXPORT KUINT32 GetEnumSizeTransmitStateModeS();

// Returns the EnumDescriptor value for the specified index.
// Use GetEnumSize<enum> to get the array size.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then NULL will be returned.
KDIS_EXPORT const EnumDescriptor * GetEnumDescriptorTransmitStateModeS( KUINT32 Index );

KDIS_EXPORT const KOCTET* GetEnumAsStringTransmitStateModeS( KINT32 Value );

// Returns true if a value was found.
KDIS_EXPORT KBOOL GetEnumFromStringTransmitStateModeS( const KOCTET* Value, KINT32 & ValueOut );


/************************************************************************/
/* Transmission Indicator                                               */
/* Specify whether this is an interrogation, a reply to an              */
/* interrogation, or a squitter transmission. It shall be represented   */
/* by an 8-bit enumeration (see [UID 372]                               */
/* Used In:                                                             */
/*  Layer 5 Basic Interactive record                                    */
/************************************************************************/

enum TransmissionIndicator
{
    NoStatementTransmissionIndicator = 0,
    OriginalInterrogation            = 1,
    InterrogationReply               = 2,
    SquitterTransmission             = 3
};

// Returns number of values in the EnumDescriptor for this enum.
// This can be used to iterate through all possible enum values by using GetEnumDescriptor<enum>.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then 0 will be returned.
KDIS_EXPORT KUINT32 GetEnumSizeTransmissionIndicator();

// Returns the EnumDescriptor value for the specified index.
// Use GetEnumSize<enum> to get the array size.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then NULL will be returned.
KDIS_EXPORT const EnumDescriptor * GetEnumDescriptorTransmissionIndicator( KUINT32 Index );

KDIS_EXPORT const KOCTET* GetEnumAsStringTransmissionIndicator( KINT32 Value );

// Returns true if a value was found.
KDIS_EXPORT KBOOL GetEnumFromStringTransmissionIndicator( const KOCTET* Value, KINT32 & ValueOut );


/************************************************************************/
/* Reply Amplification                                                  */
/* Provide amplifying information about the reply                       */
/* It shall be represented by an 8-bit enumeration (see [UID 373]       */
/* Used In:                                                             */
/*  Layer 5 Basic Interactive record                                    */
/************************************************************************/

enum ReplyAmplification
{
    NoStatementReplyAmplification = 0,
    CompleteReplyAmplification    = 1,
    Limted                        = 2,
    UnableToRespond               = 3
};

// Returns number of values in the EnumDescriptor for this enum.
// This can be used to iterate through all possible enum values by using GetEnumDescriptor<enum>.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then 0 will be returned.
KDIS_EXPORT KUINT32 GetEnumSizeReplyAmplification();

// Returns the EnumDescriptor value for the specified index.
// Use GetEnumSize<enum> to get the array size.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then NULL will be returned.
KDIS_EXPORT const EnumDescriptor * GetEnumDescriptorReplyAmplification( KUINT32 Index );

KDIS_EXPORT const KOCTET* GetEnumAsStringReplyAmplification( KINT32 Value );

// Returns true if a value was found.
KDIS_EXPORT KBOOL GetEnumFromStringReplyAmplification( const KOCTET* Value, KINT32 & ValueOut );


/************************************************************************/
/* Alternate Mode 4 Challenge/Reply                                     */
/* Specify the Mode 4 reply that would be sent by this transponder to   */
/* this Interactive Mode 4 interrogation. It shall use the same         */
/* enumerations as specified for the Alternate Mode 4 Challenge/Reply   */
/* field                                                                */
/* It shall be represented by an 8-bit enumeration (see [UID 96]        */
/* Used In:                                                             */
/*  Layer 5 Basic Interactive record                                    */
/************************************************************************/

enum AlternateMode4ChallengeReply
{
    NoStatementAlternateMode4ChallengeReply     = 0,
    ValidAlternateMode4ChallengeReply           = 1,
    InvalidAlternateMode4ChallengeReply         = 2,
    NoResponseAlternateMode4ChallengeReply      = 3,
    UnableToVerifyAlternateMode4ChallengeReply  = 4
};

// Returns number of values in the EnumDescriptor for this enum.
// This can be used to iterate through all possible enum values by using GetEnumDescriptor<enum>.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then 0 will be returned.
KDIS_EXPORT KUINT32 GetEnumSizeAlternateMode4ChallengeReply();

// Returns the EnumDescriptor value for the specified index.
// Use GetEnumSize<enum> to get the array size.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then NULL will be returned.
KDIS_EXPORT const EnumDescriptor * GetEnumDescriptorAlternateMode4ChallengeReply( KUINT32 Index );

KDIS_EXPORT const KOCTET* GetEnumAsStringAlternateMode4ChallengeReply( KINT32 Value );

// Returns true if a value was found.
KDIS_EXPORT KBOOL GetEnumFromStringAlternateMode4ChallengeReply( const KOCTET* Value, KINT32 & ValueOut );


/************************************************************************/
/* Mode S Interrogator Identifier IC Type                               */
/* Interrogator Identifier (II) and Surveillance Identifier (SI)        */
/* It shall be represented by an 8-bit enumeration (see [UID 348]       */
/* Used In:                                                             */
/*  Layer 5 Basic Interactive record                                    */
/************************************************************************/

enum ModeSInterrogatorIdentifierICType
{
    II     = 0,
    SI     = 1
};

// Returns number of values in the EnumDescriptor for this enum.
// This can be used to iterate through all possible enum values by using GetEnumDescriptor<enum>.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then 0 will be returned.
KDIS_EXPORT KUINT32 GetEnumSizeModeSInterrogatorIdentifierICType();

// Returns the EnumDescriptor value for the specified index.
// Use GetEnumSize<enum> to get the array size.
// If KDIS_USE_ENUM_DESCRIPTORS is not set then NULL will be returned.
KDIS_EXPORT const EnumDescriptor * GetEnumDescriptorModeSInterrogatorIdentifierICType( KUINT32 Index );

KDIS_EXPORT const KOCTET* GetEnumAsStringModeSInterrogatorIdentifierICType( KINT32 Value );

// Returns true if a value was found.
KDIS_EXPORT KBOOL GetEnumFromStringModeSInterrogatorIdentifierICType( const KOCTET* Value, KINT32 & ValueOut );
#endif // DIS 6

} // END namespace ENUMS
} // END namespace DATA_TYPES
} // END namespace KDIS
