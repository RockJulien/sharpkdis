#include "./DAPSource.h"

//////////////////////////////////////////////////////////////////////////

using namespace std;
using namespace KDIS;
using namespace DATA_TYPE;
using namespace ENUMS;

//////////////////////////////////////////////////////////////////////////
// public:
//////////////////////////////////////////////////////////////////////////

DAPSource::DAPSource() 
{
	m_DAPSourceUnion.m_ui8DAPSource = 0;	
}

//////////////////////////////////////////////////////////////////////////

DAPSource::DAPSource( KDataStream & stream ) 
{
    Decode( stream );
}

//////////////////////////////////////////////////////////////////////////

DAPSource::DAPSource( KBOOL IAS, KBOOL MachNumber, KBOOL GroundSpeed, KBOOL MagneticHeading, KBOOL TrackAngleRate, KBOOL TrueTrackAngle, KBOOL TrueAirSpeed, KBOOL VerticalRate )
{	
	m_DAPSourceUnion.m_ui8IAS             = IAS;
	m_DAPSourceUnion.m_ui8MachNumber      = MachNumber;
	m_DAPSourceUnion.m_ui8GroundSpeed     = GroundSpeed;
	m_DAPSourceUnion.m_ui8MagneticHeading = MagneticHeading;
	m_DAPSourceUnion.m_ui8TrackAngleRate  = TrackAngleRate;
	m_DAPSourceUnion.m_ui8TrueTrackAngle  = TrueTrackAngle;
	m_DAPSourceUnion.m_ui8TrueAirSpeed    = TrueAirSpeed;
	m_DAPSourceUnion.m_ui8VerticalRate    = VerticalRate;
}

//////////////////////////////////////////////////////////////////////////

DAPSource::~DAPSource()
{
}

//////////////////////////////////////////////////////////////////////////

void DAPSource::SetIAS( KBOOL IAS ) 
{	
	m_DAPSourceUnion.m_ui8IAS = IAS;
}

//////////////////////////////////////////////////////////////////////////

KBOOL DAPSource::GetIAS() const
{
	return ( KBOOL )m_DAPSourceUnion.m_ui8IAS;
}
//////////////////////////////////////////////////////////////////////////

void DAPSource::SetMachNumber( KBOOL MachNumber ) 
{	
	m_DAPSourceUnion.m_ui8MachNumber = MachNumber;
}

//////////////////////////////////////////////////////////////////////////

KBOOL DAPSource::GetMachNumber() const
{
	return ( KBOOL )m_DAPSourceUnion.m_ui8MachNumber;
}

//////////////////////////////////////////////////////////////////////////

void DAPSource::SetGroundSpeed( KBOOL GroundSpeed ) 
{	
	m_DAPSourceUnion.m_ui8GroundSpeed = GroundSpeed;
}

//////////////////////////////////////////////////////////////////////////

KBOOL DAPSource::GetGroundSpeed() const
{
	return ( KBOOL )m_DAPSourceUnion.m_ui8GroundSpeed;
}

//////////////////////////////////////////////////////////////////////////

void DAPSource::SetMagneticHeading( KBOOL MagneticHeading ) 
{	
	m_DAPSourceUnion.m_ui8MagneticHeading = MagneticHeading;
}

//////////////////////////////////////////////////////////////////////////

KBOOL DAPSource::GetMagneticHeading() const
{
	return ( KBOOL )m_DAPSourceUnion.m_ui8MagneticHeading;
}

//////////////////////////////////////////////////////////////////////////

void DAPSource::SetTrackAngleRate( KBOOL TrackAngleRate ) 
{	
	m_DAPSourceUnion.m_ui8TrackAngleRate = TrackAngleRate;
}

//////////////////////////////////////////////////////////////////////////

KBOOL DAPSource::GetTrackAngleRate() const
{
	return ( KBOOL )m_DAPSourceUnion.m_ui8TrackAngleRate;
}

//////////////////////////////////////////////////////////////////////////

void DAPSource::SetTrueTrackAngle( KBOOL TrueTrackAngle ) 
{	
	m_DAPSourceUnion.m_ui8TrueTrackAngle = TrueTrackAngle;
}

//////////////////////////////////////////////////////////////////////////

KBOOL DAPSource::GetTrueTrackAngle() const
{
	return ( KBOOL )m_DAPSourceUnion.m_ui8TrueTrackAngle;
}

//////////////////////////////////////////////////////////////////////////

void DAPSource::SetTrueAirSpeed( KBOOL TrueAirSpeed ) 
{	
	m_DAPSourceUnion.m_ui8TrueAirSpeed = TrueAirSpeed;
}

//////////////////////////////////////////////////////////////////////////

KBOOL DAPSource::GetTrueAirSpeed() const
{
	return ( KBOOL )m_DAPSourceUnion.m_ui8TrueAirSpeed;
}

//////////////////////////////////////////////////////////////////////////

void DAPSource::SetVerticalRate( KBOOL VerticalRate ) 
{	
	m_DAPSourceUnion.m_ui8VerticalRate = VerticalRate;
}

//////////////////////////////////////////////////////////////////////////

KBOOL DAPSource::GetVerticalRate() const
{
	return ( KBOOL )m_DAPSourceUnion.m_ui8VerticalRate;
}

//////////////////////////////////////////////////////////////////////////

const KOCTET* DAPSource::GetAsString() const
{
    KStringStream ss;	

	ss << "Mode S Levels Present:"
	   << "\n\tIAS:             " << ( KBOOL )m_DAPSourceUnion.m_ui8IAS
	   << "\n\tMachNumber:      " << ( KBOOL )m_DAPSourceUnion.m_ui8MachNumber
	   << "\n\tGroundSpeed:     " << ( KBOOL )m_DAPSourceUnion.m_ui8GroundSpeed
	   << "\n\tMagneticHeading: " << ( KBOOL )m_DAPSourceUnion.m_ui8MagneticHeading
	   << "\n\tTrackAngleRate:  " << ( KBOOL )m_DAPSourceUnion.m_ui8TrackAngleRate
	   << "\n\tTrueTrackAngle:  " << ( KBOOL )m_DAPSourceUnion.m_ui8TrueTrackAngle
	   << "\n\tTrueAirSpeed:    " << ( KBOOL )m_DAPSourceUnion.m_ui8TrueAirSpeed
	   << "\n\tVerticalRate:    " << ( KBOOL )m_DAPSourceUnion.m_ui8VerticalRate
	   << "\n";

    return ss.str().c_str();
}

//////////////////////////////////////////////////////////////////////////

void DAPSource::Decode( KDataStream & stream ) 
{
    if( stream.GetBufferSize() < DAP_SOURCE_SIZE )throw KException( __FUNCTION__, NOT_ENOUGH_DATA_IN_BUFFER );
	
	stream >> m_DAPSourceUnion.m_ui8DAPSource;
}

//////////////////////////////////////////////////////////////////////////

const KDataStream* DAPSource::GetEncodedStream() const
{
	m_stream.Clear();

	this->EncodeToStream(m_stream);

	return &m_stream;
}

//////////////////////////////////////////////////////////////////////////

void DAPSource::EncodeToStream( KDataStream & stream ) const
{
	stream << m_DAPSourceUnion.m_ui8DAPSource;
}

//////////////////////////////////////////////////////////////////////////

KBOOL DAPSource::operator == ( const DAPSource & Value ) const
{
	if( m_DAPSourceUnion.m_ui8DAPSource != Value.m_DAPSourceUnion.m_ui8DAPSource ) return false;
    return true;
}

//////////////////////////////////////////////////////////////////////////

KBOOL DAPSource::operator != ( const DAPSource & Value ) const
{
    return !( *this == Value );
}

//////////////////////////////////////////////////////////////////////////
