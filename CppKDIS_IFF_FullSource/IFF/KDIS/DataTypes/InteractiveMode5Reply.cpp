
#include "./InteractiveMode5Reply.h"

//////////////////////////////////////////////////////////////////////////

using namespace KDIS;
using namespace DATA_TYPE;
using namespace ENUMS;

//////////////////////////////////////////////////////////////////////////
// Public:
//////////////////////////////////////////////////////////////////////////

InteractiveMode5Reply::InteractiveMode5Reply() :
    m_ui8InteractiveMode5Reply( 0 ),
    m_ui8Padding( 0 ),
    m_ui32Padding( 0 )
{
    m_ui32Type = InteractiveMode5ReplyRecord;
    m_ui16Length = INTERACTIVE_MODE5_REPLY_TYPE_SIZE;
}

//////////////////////////////////////////////////////////////////////////

InteractiveMode5Reply::InteractiveMode5Reply( KDIS::DATA_TYPE::ENUMS::Mode5Reply M5R )  :
    m_ui8InteractiveMode5Reply( M5R ),
    m_ui8Padding( 0 ),
    m_ui32Padding( 0 )
{
    m_ui32Type = InteractiveMode5ReplyRecord;
    m_ui16Length = INTERACTIVE_MODE5_REPLY_TYPE_SIZE;
}

//////////////////////////////////////////////////////////////////////////

InteractiveMode5Reply::InteractiveMode5Reply( KDataStream & stream ) 
{
    Decode( stream );
}

//////////////////////////////////////////////////////////////////////////

InteractiveMode5Reply::~InteractiveMode5Reply()
{
}

//////////////////////////////////////////////////////////////////////////

void InteractiveMode5Reply::SetMode5Replyv( KDIS::DATA_TYPE::ENUMS::Mode5Reply M5R )
{
    m_ui8InteractiveMode5Reply = M5R;
}

//////////////////////////////////////////////////////////////////////////

KDIS::DATA_TYPE::ENUMS::Mode5Reply InteractiveMode5Reply::GetMode5Reply() const
{
    return (KDIS::DATA_TYPE::ENUMS::Mode5Reply) m_ui8InteractiveMode5Reply;
}

//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////

const KOCTET* InteractiveMode5Reply::GetAsString() const
{
    KStringStream ss;

    ss << StandardVariable::GetAsString()
       << GetEnumAsStringMode5Reply( m_ui8InteractiveMode5Reply ) << "\n";

    return ss.str().c_str();
}

//////////////////////////////////////////////////////////////////////////

void InteractiveMode5Reply::Decode( KDataStream & stream ) 
{
    if( stream.GetBufferSize() < INTERACTIVE_MODE5_REPLY_TYPE_SIZE )throw KException( __FUNCTION__, NOT_ENOUGH_DATA_IN_BUFFER );

    StandardVariable::Decode( stream );

    stream >> m_ui8InteractiveMode5Reply;
    stream >> m_ui8Padding;
    stream >> m_ui32Padding;
}

//////////////////////////////////////////////////////////////////////////

void InteractiveMode5Reply::EncodeToStream( KDataStream & stream ) const
{
    StandardVariable::EncodeToStream( stream );

    stream << m_ui8InteractiveMode5Reply;
    stream << m_ui8Padding;
    stream << m_ui32Padding;
}

//////////////////////////////////////////////////////////////////////////

KBOOL InteractiveMode5Reply::operator == ( const InteractiveMode5Reply & Value ) const
{
    if( StandardVariable::operator  !=( Value ) )                          return false;
    if( m_ui8InteractiveMode5Reply  != Value.m_ui8InteractiveMode5Reply )      return false;
    return true;
}

//////////////////////////////////////////////////////////////////////////

KBOOL InteractiveMode5Reply::operator != ( const InteractiveMode5Reply & Value ) const
{
    return !( *this == Value );
}

//////////////////////////////////////////////////////////////////////////
