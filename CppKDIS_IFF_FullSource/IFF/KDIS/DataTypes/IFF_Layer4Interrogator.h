/********************************************************************
    class:      IFF_Layer4Interrogator
    DIS:        (7) 1278.1 - 2012
    created:    21/03/2019
    author:     Nicolas Duboys

    purpose:    Layer 4 Mode S interrogator functional data.

    Size:       288 bits / 36 octets - min size
*********************************************************************/


#pragma once

#include "./IFF_Layer4.h"
#include "./SimulationIdentifier.h"
#include "./ModeSInterrogatorBasicData.h"

namespace KDIS {
namespace DATA_TYPE {

class KDIS_EXPORT IFF_Layer4Interrogator : public IFF_Layer4
{
protected:

	ModeSInterrogatorBasicData m_BasicData;
		
public:
	
    IFF_Layer4Interrogator();

    IFF_Layer4Interrogator( KDataStream & stream ) ;

	IFF_Layer4Interrogator( const SimulationIdentifier & ReportingSimulation, const ModeSInterrogatorBasicData & Data,
		                   std::vector<StdVarPtr> & Records );

    IFF_Layer4Interrogator( const LayerHeader & H, KDataStream & stream ) ;

    virtual ~IFF_Layer4Interrogator();
	
    //************************************
    // FullName:    KDIS::DATA_TYPE::IFF_Layer4Interrogator::SetBasicData
    //              KDIS::DATA_TYPE::IFF_Layer4Interrogator::GetBasicData
    // Description: Basic Mode S interrogator data that is always required to be transmitted.
    // Parameter:   const ModeSInterrogatorBasicData & BD
    //************************************
    void SetBasicData( const ModeSInterrogatorBasicData & BD );
    const ModeSInterrogatorBasicData & GetBasicData() const;
    ModeSInterrogatorBasicData & GetBasicDatan();
	
    //************************************
    // FullName:    KDIS::DATA_TYPE::IFF_Layer4Interrogator::GetAsString
    // Description: Returns a string representation 
    //************************************
    virtual const KOCTET* GetAsString() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::IFF_Layer4Interrogator::Decode
    // Description: Convert From Network Data.
    // Parameter:   KDataStream & stream
    // Parameter:   bool ignoreHeader = false - Decode the layer header from the stream? 
    //************************************
	virtual void Decode( KDataStream & stream ) ;
    virtual void Decode( KDataStream & stream, bool ignoreHeader = false ) ;

    //************************************
    // FullName:    KDIS::DATA_TYPE::IFF_Layer4Interrogator::Encode
    // Description: Convert To Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual void EncodeToStream( KDataStream & stream ) const;

    KBOOL operator == ( const IFF_Layer4Interrogator & Value ) const;
    KBOOL operator != ( const IFF_Layer4Interrogator & Value ) const;
};

} // END namespace DATA_TYPE
} // END namespace KDIS
