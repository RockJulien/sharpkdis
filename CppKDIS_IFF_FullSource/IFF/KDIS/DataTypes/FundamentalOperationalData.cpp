/*********************************************************************
Copyright 2013 Karl Jones
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

For Further Information Please Contact me at
Karljj1@yahoo.com
http://p.sf.net/kdis/UserGuide
*********************************************************************/

#include "./FundamentalOperationalData.h"

using namespace KDIS;
using namespace DATA_TYPE;

//////////////////////////////////////////////////////////////////////////
// Public:
//////////////////////////////////////////////////////////////////////////

FundamentalOperationalData::FundamentalOperationalData() :
    m_ui8SystemStatus( 0 ),
    m_ui8AltParam4( 0 ),
    m_ui8InfoLayers( 0 ),
    m_ui8Modifier( 0 ),
    m_ui16Param1( 0 ),
    m_ui16Param2( 0 ),
    m_ui16Param3( 0 ),
    m_ui16Param4( 0 ),
    m_ui16Param5( 0 ),
    m_ui16Param6( 0 )
{
}

//////////////////////////////////////////////////////////////////////////

FundamentalOperationalData::FundamentalOperationalData( KDataStream & stream ) 
{
    Decode( stream );
}

//////////////////////////////////////////////////////////////////////////

FundamentalOperationalData::FundamentalOperationalData( const FundamentalOperationalData_MarkXTransponder & FOD )
{
    m_FODSystemMarkTransponder = FOD;
}

//////////////////////////////////////////////////////////////////////////

FundamentalOperationalData::FundamentalOperationalData( const FundamentalOperationalData_MarkXInterrogator & FOD )
{
    m_FODSystemMarkInterrogator = FOD;
}

//////////////////////////////////////////////////////////////////////////

FundamentalOperationalData::FundamentalOperationalData( const FundamentalOperationalData_Soviet & FOD )
{
    m_FODSystemSoviet = FOD;
}

//////////////////////////////////////////////////////////////////////////

FundamentalOperationalData::FundamentalOperationalData( const FundamentalOperationalData_RRB & FOD )
{
    m_FODSystemRRB = FOD;
}

//////////////////////////////////////////////////////////////////////////

FundamentalOperationalData::~FundamentalOperationalData()
{
}

//////////////////////////////////////////////////////////////////////////

void FundamentalOperationalData::SetFundamentalOperationalData_MarkXTransponder( const FundamentalOperationalData_MarkXTransponder & FOD )
{
    m_FODSystemMarkTransponder = FOD;
}

//////////////////////////////////////////////////////////////////////////

void FundamentalOperationalData::SetFundamentalOperationalData_MarkXInterrogator( const FundamentalOperationalData_MarkXInterrogator & FOD )
{
    m_FODSystemMarkInterrogator = FOD;
}

//////////////////////////////////////////////////////////////////////////

void FundamentalOperationalData::SetFundamentalOperationalData_Soviet( const FundamentalOperationalData_Soviet & FOD )
{
    m_FODSystemSoviet = FOD;
}

//////////////////////////////////////////////////////////////////////////

void FundamentalOperationalData::SetFundamentalOperationalData_RRB( const FundamentalOperationalData_RRB & FOD )
{
    m_FODSystemRRB = FOD;
}

//////////////////////////////////////////////////////////////////////////

const FundamentalOperationalData_MarkXTransponder & FundamentalOperationalData::GetFundamentalOperationalData_MarkXTransponder_Const() const
{
    return m_FODSystemMarkTransponder;
}

//////////////////////////////////////////////////////////////////////////

FundamentalOperationalData_MarkXTransponder & FundamentalOperationalData::GetFundamentalOperationalData_MarkXTransponder()
{
    return m_FODSystemMarkTransponder;
}

//////////////////////////////////////////////////////////////////////////

const FundamentalOperationalData_MarkXInterrogator & FundamentalOperationalData::GetFundamentalOperationalData_MarkXInterrogator_Const() const
{
    return m_FODSystemMarkInterrogator;
}

//////////////////////////////////////////////////////////////////////////

FundamentalOperationalData_MarkXInterrogator & FundamentalOperationalData::GetFundamentalOperationalData_MarkXInterrogator()
{
    return m_FODSystemMarkInterrogator;
}

//////////////////////////////////////////////////////////////////////////

const FundamentalOperationalData_Soviet & FundamentalOperationalData::GetFundamentalOperationalData_Soviet_Const() const
{
    return m_FODSystemSoviet;
}

//////////////////////////////////////////////////////////////////////////

FundamentalOperationalData_Soviet & FundamentalOperationalData::GetFundamentalOperationalData_Soviet()
{
    return m_FODSystemSoviet;
}

//////////////////////////////////////////////////////////////////////////

const FundamentalOperationalData_RRB & FundamentalOperationalData::GetFundamentalOperationalData_RRB_Const() const
{
    return m_FODSystemRRB;
}

//////////////////////////////////////////////////////////////////////////

FundamentalOperationalData_RRB & FundamentalOperationalData::GetFundamentalOperationalData_RRB()
{
    return m_FODSystemRRB;
}

//////////////////////////////////////////////////////////////////////////

const KOCTET* FundamentalOperationalData::GetAsString(KDIS::DATA_TYPE::ENUMS::SystemType type) const
{
    KStringStream ss;

    ss << "FundamentalOperationalData:";
    switch (type)
    {
    case KDIS::DATA_TYPE::ENUMS::Mark_X_XII_ATCRBS_ModeS_Transponder:
        ss << "Type : " << GetEnumAsStringSystemType (type) << "\n";
        ss << " Status system on : " <<  m_FODSystemMarkTransponder.GetSystemStatusSystemOn () << std::endl;
        ss << " Status param 1 " <<  m_FODSystemMarkTransponder.GetSystemStatusParam1Capable () << std::endl;
        ss << " Status param 2 " <<  m_FODSystemMarkTransponder.GetSystemStatusParam2Capable () << std::endl;
        ss << " Status param 3 " <<  m_FODSystemMarkTransponder.GetSystemStatusParam3Capable () << std::endl;
        ss << " Status param 4 " <<  m_FODSystemMarkTransponder.GetSystemStatusParam4Capable () << std::endl;
        ss << " Status param 5 " <<  m_FODSystemMarkTransponder.GetSystemStatusParam5Capable () << std::endl;
        ss << " Status param 6 " <<  m_FODSystemMarkTransponder.GetSystemStatusParam6Capable () << std::endl;
        ss << " Status system operationnal : " <<  m_FODSystemMarkTransponder.GetSystemStatusIsOperational () << std::endl;
        ss << " Alternate param 4 : " <<  GetEnumAsStringAlternateParameter4 (m_FODSystemMarkTransponder.GetAlternateParameter4 ()) << std::endl;
        ss << " Info Layer 1 is present : " << m_FODSystemMarkTransponder.IsInfomationLayer1Present () << std::endl;
        ss << " Info Layer 2 is present : " << m_FODSystemMarkTransponder.IsInfomationLayer2Present () << std::endl;
#if DIS_VERSION > 6
        ss << " Info Layer 3 is present : " << m_FODSystemMarkTransponder.IsInfomationLayer3Present () << std::endl;
        ss << " Info Layer 4 is present : " << m_FODSystemMarkTransponder.IsInfomationLayer4Present () << std::endl;
        ss << " Info Layer 5 is present : " << m_FODSystemMarkTransponder.IsInfomationLayer5Present () << std::endl;
        ss << " Info Layer 6 is present : " << m_FODSystemMarkTransponder.IsInfomationLayer6Present () << std::endl;
        ss << " Info Layer 7 is present : " << m_FODSystemMarkTransponder.IsInfomationLayer7Present () << std::endl;
#endif  //DIS_VERSION > 6
        ss << " Modifier :\n";
        ss << "   Emergency on         : " << m_FODSystemMarkTransponder.IsModifierEmergencyOn () << std::endl;
        ss << "   IdentSquawk Flash on : " << m_FODSystemMarkTransponder.IsModifierIdentSquawkFlashOn () << std::endl;
        ss << "   STI on               : " << m_FODSystemMarkTransponder.IsModifierSTIOn () << std::endl;
        ss << " Mode 1 : \n";
        ss << "   Code        :" << ( KUINT16 )m_FODSystemMarkTransponder.GetMode1CodeElement2 () << ( KUINT16 )m_FODSystemMarkTransponder.GetMode1CodeElement1 () << std::endl;
        ss << "   Activated   :" << m_FODSystemMarkTransponder.IsMode1StatusOn () << std::endl;
        ss << "   Damaged     :" << m_FODSystemMarkTransponder.IsMode1Damaged  () << std::endl;
        ss << "   Malfunction :" << m_FODSystemMarkTransponder.IsMode1Malfunctioning () << std::endl;
        ss << " Mode 2 : \n";
        ss << "   Code        :" << ( KUINT16 )m_FODSystemMarkTransponder.GetMode2CodeElement4 () << ( KUINT16 )m_FODSystemMarkTransponder.GetMode2CodeElement3 () << ( KUINT16 )m_FODSystemMarkTransponder.GetMode2CodeElement2 () << ( KUINT16 )m_FODSystemMarkTransponder.GetMode2CodeElement1 () << std::endl;
        ss << "   Activated   :" << m_FODSystemMarkTransponder.IsMode2StatusOn () << std::endl;
        ss << "   Damaged     :" << m_FODSystemMarkTransponder.IsMode2Damaged  () << std::endl;
        ss << "   Malfunction :" << m_FODSystemMarkTransponder.IsMode2Malfunctioning () << std::endl;
        ss << " Mode 3 : \n";
        ss << "   Code        :" << ( KUINT16 )m_FODSystemMarkTransponder.GetMode3CodeElement4 () << ( KUINT16 )m_FODSystemMarkTransponder.GetMode3CodeElement3 () << ( KUINT16 )m_FODSystemMarkTransponder.GetMode3CodeElement2 () << ( KUINT16 )m_FODSystemMarkTransponder.GetMode3CodeElement1 () << std::endl;
        ss << "   Activated   :" << m_FODSystemMarkTransponder.IsMode3StatusOn () << std::endl;
        ss << "   Damaged     :" << m_FODSystemMarkTransponder.IsMode3Damaged  () << std::endl;
        ss << "   Malfunction :" << m_FODSystemMarkTransponder.IsMode3Malfunctioning () << std::endl;
        ss << " Mode 4 : \n";
        ss << "   Code        :" << ( KUINT16 )m_FODSystemMarkTransponder.GetMode4CodeElement1 () << std::endl;
        ss << "   Activated   :" << m_FODSystemMarkTransponder.IsMode4StatusOn () << std::endl;
        ss << "   Damaged     :" << m_FODSystemMarkTransponder.IsMode4Damaged  () << std::endl;
        ss << "   Malfunction :" << m_FODSystemMarkTransponder.IsMode4Malfunctioning () << std::endl;
        ss << " Mode C : \n";
        ss << "   IsModeCAltitudeNegativeMeanSeaLevel :" << m_FODSystemMarkTransponder.IsModeCAltitudeNegativeMeanSeaLevel () << std::endl;
        ss << "   Mode Alti   :" << m_FODSystemMarkTransponder.GetModeCAltitude () << std::endl;
        ss << "   Alt mode 5  :" << m_FODSystemMarkTransponder.IsModeCAlternativeMode5 () << std::endl;
        ss << "   Activated   :" << m_FODSystemMarkTransponder.IsModeCStatusOn () << std::endl;
        ss << "   Damaged     :" << m_FODSystemMarkTransponder.IsModeCDamaged  () << std::endl;
        ss << "   Malfunction :" << m_FODSystemMarkTransponder.IsModeCMalfunctioning () << std::endl;
        ss << " Mode S : \n";
        ss << "   TACS vers   :" << GetEnumAsStringTCAS (m_FODSystemMarkTransponder.GetTCASVersion ()) << std::endl;
        ss << "   Activated   :" << m_FODSystemMarkTransponder.IsModeSStatusOn () << std::endl;
        ss << "   Damaged     :" << m_FODSystemMarkTransponder.IsModeSDamaged  () << std::endl;
        ss << "   Malfunction :" << m_FODSystemMarkTransponder.IsModeSMalfunctioning () << std::endl;
        break;

    case KDIS::DATA_TYPE::ENUMS::Mark_X_XII_ATCRBS_ModeS_Interrogator:
        ss << "Type : " << GetEnumAsStringSystemType (type) << "\n";
        ss << " Status system on : " <<  m_FODSystemMarkInterrogator.GetSystemStatusSystemOn () << std::endl;
        ss << " Status param 1 " <<  m_FODSystemMarkInterrogator.GetSystemStatusParam1Capable () << std::endl;
        ss << " Status param 2 " <<  m_FODSystemMarkInterrogator.GetSystemStatusParam2Capable () << std::endl;
        ss << " Status param 3 " <<  m_FODSystemMarkInterrogator.GetSystemStatusParam3Capable () << std::endl;
        ss << " Status param 4 " <<  m_FODSystemMarkInterrogator.GetSystemStatusParam4Capable () << std::endl;
        ss << " Status param 5 " <<  m_FODSystemMarkInterrogator.GetSystemStatusParam5Capable () << std::endl;
        ss << " Status param 6 " <<  m_FODSystemMarkInterrogator.GetSystemStatusParam6Capable () << std::endl;
        ss << " Status system operationnal : " <<  m_FODSystemMarkInterrogator.GetSystemStatusIsOperational () << std::endl;
        ss << " Alternate param 4 : " <<  GetEnumAsStringAlternateParameter4 (m_FODSystemMarkInterrogator.GetAlternateParameter4 ()) << std::endl;
        ss << " Info Layer 1 is present : " << m_FODSystemMarkInterrogator.IsInfomationLayer1Present () << std::endl;
        ss << " Info Layer 2 is present : " << m_FODSystemMarkInterrogator.IsInfomationLayer2Present () << std::endl;
#if DIS_VERSION > 6
        ss << " Info Layer 3 is present : " << m_FODSystemMarkInterrogator.IsInfomationLayer3Present () << std::endl;
        ss << " Info Layer 4 is present : " << m_FODSystemMarkInterrogator.IsInfomationLayer4Present () << std::endl;
        ss << " Info Layer 5 is present : " << m_FODSystemMarkInterrogator.IsInfomationLayer5Present () << std::endl;
        ss << " Info Layer 6 is present : " << m_FODSystemMarkInterrogator.IsInfomationLayer6Present () << std::endl;
        ss << " Info Layer 7 is present : " << m_FODSystemMarkInterrogator.IsInfomationLayer7Present () << std::endl;
#endif  //DIS_VERSION > 6
        ss << " Mode 1 : \n";
        ss << "   Activated   :" << m_FODSystemMarkInterrogator.IsMode1StatusOn () << std::endl;
        ss << "   Damaged     :" << m_FODSystemMarkInterrogator.IsMode1Damaged  () << std::endl;
        ss << "   Malfunction :" << m_FODSystemMarkInterrogator.IsMode1Malfunctioning () << std::endl;
        ss << " Mode 2 : \n";
        ss << "   Activated   :" << m_FODSystemMarkInterrogator.IsMode2StatusOn () << std::endl;
        ss << "   Damaged     :" << m_FODSystemMarkInterrogator.IsMode2Damaged  () << std::endl;
        ss << "   Malfunction :" << m_FODSystemMarkInterrogator.IsMode2Malfunctioning () << std::endl;
        ss << " Mode 3 : \n";
        ss << "   Activated   :" << m_FODSystemMarkInterrogator.IsMode3StatusOn () << std::endl;
        ss << "   Damaged     :" << m_FODSystemMarkInterrogator.IsMode3Damaged  () << std::endl;
        ss << "   Malfunction :" << m_FODSystemMarkInterrogator.IsMode3Malfunctioning () << std::endl;
        ss << " Mode 4 : \n";
        ss << "   Code        :" << ( KUINT16 )m_FODSystemMarkInterrogator.GetMode4CodeElement1 () << std::endl;
        ss << "   Activated   :" << m_FODSystemMarkInterrogator.IsMode4StatusOn () << std::endl;
        ss << "   Damaged     :" << m_FODSystemMarkInterrogator.IsMode4Damaged  () << std::endl;
        ss << "   Malfunction :" << m_FODSystemMarkInterrogator.IsMode4Malfunctioning () << std::endl;
        ss << " Mode C : \n";
        ss << "   Activated   :" << m_FODSystemMarkInterrogator.IsModeCStatusOn () << std::endl;
        ss << "   Damaged     :" << m_FODSystemMarkInterrogator.IsModeCDamaged  () << std::endl;
        ss << "   Malfunction :" << m_FODSystemMarkInterrogator.IsModeCMalfunctioning () << std::endl;
        ss << " Mode S : \n";
        ss << "   TACS vers   :" << GetEnumAsStringTCAS (m_FODSystemMarkInterrogator.GetTCASVersion ()) << std::endl;
        ss << "   Activated   :" << m_FODSystemMarkInterrogator.IsModeSStatusOn () << std::endl;
        ss << "   Damaged     :" << m_FODSystemMarkInterrogator.IsModeSDamaged  () << std::endl;
        ss << "   Malfunction :" << m_FODSystemMarkInterrogator.IsModeSMalfunctioning () << std::endl;
        break;

    default:
        ss << "\tNOT IMPLEMENTED YET for type : " << GetEnumAsStringSystemType( type ) << "\n";
        break;
    }

    // TODO: Get string for a specific system type.

    return ss.str().c_str();
}

const KOCTET* FundamentalOperationalData::GetAsString() const
{
    KStringStream ss;

    ss << "FundamentalOperationalData:";
    ss << "\tNOT IMPLEMENTED YET\n";
    return ss.str().c_str();
}
//////////////////////////////////////////////////////////////////////////

void FundamentalOperationalData::Decode( KDataStream & stream ) 
{
    if( stream.GetBufferSize() < FUNDAMENTAL_OPERATIONAL_DATA_SIZE )throw KException( __FUNCTION__, NOT_ENOUGH_DATA_IN_BUFFER );

    stream >> m_ui8SystemStatus
           >> m_ui8AltParam4
           >> m_ui8InfoLayers
           >> m_ui8Modifier
           >> m_ui16Param1
           >> m_ui16Param2
           >> m_ui16Param3
           >> m_ui16Param4
           >> m_ui16Param5
           >> m_ui16Param6;
}

//////////////////////////////////////////////////////////////////////////

const KDataStream* FundamentalOperationalData::GetEncodedStream() const
{
	m_stream.Clear();

	this->EncodeToStream(m_stream);

	return &m_stream;
}

//////////////////////////////////////////////////////////////////////////

void FundamentalOperationalData::EncodeToStream( KDataStream & stream ) const
{
    stream << m_ui8SystemStatus
           << m_ui8AltParam4
           << m_ui8InfoLayers
           << m_ui8Modifier
           << m_ui16Param1
           << m_ui16Param2
           << m_ui16Param3
           << m_ui16Param4
           << m_ui16Param5
           << m_ui16Param6;
}

//////////////////////////////////////////////////////////////////////////

KBOOL FundamentalOperationalData::operator == ( const FundamentalOperationalData & Value ) const
{
    if( m_ui8SystemStatus   != Value.m_ui8SystemStatus )    return false;
    if( m_ui8AltParam4      != Value.m_ui8AltParam4 )       return false;
    if( m_ui8InfoLayers     != Value.m_ui8InfoLayers )      return false;
    if( m_ui8Modifier       != Value.m_ui8Modifier )        return false;
    if( m_ui16Param1        != Value.m_ui16Param1 )         return false;
    if( m_ui16Param2        != Value.m_ui16Param2 )         return false;
    if( m_ui16Param3        != Value.m_ui16Param3 )         return false;
    if( m_ui16Param4        != Value.m_ui16Param4 )         return false;
    if( m_ui16Param5        != Value.m_ui16Param5 )         return false;
    if( m_ui16Param6        != Value.m_ui16Param6 )         return false;
    return true;
}

//////////////////////////////////////////////////////////////////////////

KBOOL FundamentalOperationalData::operator != ( const FundamentalOperationalData & Value ) const
{
    return !( *this == Value );
}

//////////////////////////////////////////////////////////////////////////


