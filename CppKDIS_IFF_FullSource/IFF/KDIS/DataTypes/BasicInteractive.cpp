
#include "./BasicInteractive.h"

//////////////////////////////////////////////////////////////////////////

using namespace KDIS;
using namespace DATA_TYPE;
using namespace ENUMS;
using namespace UTILS;

//////////////////////////////////////////////////////////////////////////
// Public:
//////////////////////////////////////////////////////////////////////////

BasicInteractive::BasicInteractive() :
    m_ui32Padding (0),
    m_TransmissionIndicator (0),
    m_ReplyAmplification (0)
{
    m_ui32Type = BasicInteractiveRecord;
    m_ui16Length = BASIC_INTERACTIVE_TYPE_SIZE;
}

//////////////////////////////////////////////////////////////////////////

BasicInteractive::BasicInteractive( const EntityIdentifier & InterrogatingEntityID, const EntityIdentifier & InterrogatedEntityID,
        const EntityIdentifier & EventID, const InterrogatedModes & InterrogatedModes,
        KDIS::DATA_TYPE::ENUMS::TransmissionIndicator TI, KDIS::DATA_TYPE::ENUMS::ReplyAmplification RA) :
    m_ui32Padding (0)
{
    m_ui32Type = BasicInteractiveRecord;
    m_ui16Length = BASIC_INTERACTIVE_TYPE_SIZE;

    m_InterrogatingEntityID = InterrogatingEntityID;
    m_InterrogatedEntityID  = InterrogatedEntityID;
    m_EventID               = EventID;
    m_InterrogatedModes     = InterrogatedModes;
    m_TransmissionIndicator = TI;
    m_ReplyAmplification    = RA;
}

//////////////////////////////////////////////////////////////////////////

BasicInteractive::BasicInteractive( KDataStream & stream ) 
{
    Decode( stream );
}

//////////////////////////////////////////////////////////////////////////

BasicInteractive::~BasicInteractive()
{
}

//////////////////////////////////////////////////////////////////////////

void BasicInteractive::SetInterrogatingEntityID( const EntityIdentifier & InterrogatingEntityID )
{
    m_InterrogatingEntityID = InterrogatingEntityID;
}

//////////////////////////////////////////////////////////////////////////

const EntityIdentifier & BasicInteractive::GetInterrogatingEntityID() const
{
    return m_InterrogatingEntityID;
}

//////////////////////////////////////////////////////////////////////////

EntityIdentifier & BasicInteractive::GetInterrogatingEntityID()
{
    return m_InterrogatingEntityID;
}

//////////////////////////////////////////////////////////////////////////

void BasicInteractive::SetInterrogatedEntityID( const EntityIdentifier & InterrogatedEntityID )
{
    m_InterrogatedEntityID = InterrogatedEntityID;
}

//////////////////////////////////////////////////////////////////////////

const EntityIdentifier & BasicInteractive::GetInterrogatedEntityID() const
{
    return m_InterrogatedEntityID;
}

//////////////////////////////////////////////////////////////////////////

EntityIdentifier & BasicInteractive::GetInterrogatedEntityID()
{
    return m_InterrogatedEntityID;
}

//////////////////////////////////////////////////////////////////////////

void BasicInteractive::SetEventID( const EntityIdentifier & EventID )
{
    m_EventID = EventID;
}

//////////////////////////////////////////////////////////////////////////

const EntityIdentifier & BasicInteractive::GetEventID() const
{
    return m_EventID;
}

//////////////////////////////////////////////////////////////////////////

EntityIdentifier & BasicInteractive::GetEventID()
{
    return m_EventID;
}

//////////////////////////////////////////////////////////////////////////

void BasicInteractive::SetInterrogatedModes( const InterrogatedModes & InterrogatedModes )
{
    m_InterrogatedModes = InterrogatedModes;
}

//////////////////////////////////////////////////////////////////////////

const InterrogatedModes & BasicInteractive::GetInterrogatedModes() const
{
    return m_InterrogatedModes;
}

//////////////////////////////////////////////////////////////////////////

InterrogatedModes & BasicInteractive::GetInterrogatedModes()
{
    return m_InterrogatedModes;
}

//////////////////////////////////////////////////////////////////////////

void BasicInteractive::SetTransmissionIndicator( TransmissionIndicator TI )
{
    m_TransmissionIndicator = TI;
}

//////////////////////////////////////////////////////////////////////////

TransmissionIndicator BasicInteractive::GetTransmissionIndicator() const
{
    return ( TransmissionIndicator )m_TransmissionIndicator;
}

//////////////////////////////////////////////////////////////////////////

void BasicInteractive::SetReplyAmplification( ReplyAmplification RA )
{
    m_ReplyAmplification = RA;
}

//////////////////////////////////////////////////////////////////////////

ReplyAmplification BasicInteractive::GetReplyAmplification() const
{
    return ( ReplyAmplification )m_ReplyAmplification;
}

//////////////////////////////////////////////////////////////////////////

const KOCTET* BasicInteractive::GetAsString() const
{
    KStringStream ss;

    ss << StandardVariable::GetAsString()
       << IndentString( m_InterrogatingEntityID.GetAsString(), 1 )
       << IndentString( m_InterrogatedEntityID.GetAsString(), 1 )
       << IndentString( m_EventID.GetAsString(), 1 )
       << IndentString( m_InterrogatedModes.GetAsString(), 1 )
       << GetEnumAsStringTransmissionIndicator( m_TransmissionIndicator ) << "\n"
       << GetEnumAsStringReplyAmplification( m_ReplyAmplification ) << "\n";

    return ss.str().c_str();
}

//////////////////////////////////////////////////////////////////////////

void BasicInteractive::Decode( KDataStream & stream ) 
{
    if( stream.GetBufferSize() < BASIC_INTERACTIVE_TYPE_SIZE )throw KException( __FUNCTION__, NOT_ENOUGH_DATA_IN_BUFFER );

    StandardVariable::Decode( stream );

    stream >> KDIS_STREAM m_InterrogatingEntityID;
    stream >> KDIS_STREAM m_InterrogatedEntityID;
    stream >> KDIS_STREAM m_EventID;
    stream >> KDIS_STREAM m_InterrogatedModes;
    stream >> m_TransmissionIndicator;
    stream >> m_ReplyAmplification;
    stream >> m_ui32Padding;
}

//////////////////////////////////////////////////////////////////////////

void BasicInteractive::EncodeToStream( KDataStream & stream ) const
{
    StandardVariable::EncodeToStream( stream );

    stream << KDIS_STREAM m_InterrogatingEntityID;
    stream << KDIS_STREAM m_InterrogatedEntityID;
    stream << KDIS_STREAM m_EventID;
    stream << KDIS_STREAM m_InterrogatedModes;
    stream << m_TransmissionIndicator;
    stream << m_ReplyAmplification;
    stream << m_ui32Padding;
}

//////////////////////////////////////////////////////////////////////////

KBOOL BasicInteractive::operator == ( const BasicInteractive & Value ) const
{
    if( StandardVariable::operator  !=( Value ) )                         return false;
    if( m_InterrogatingEntityID     != Value.m_InterrogatingEntityID )    return false;
    if( m_InterrogatedEntityID      != Value.m_InterrogatedEntityID )     return false;
    if( m_EventID                   != Value.m_EventID )                  return false;
    if( m_InterrogatedModes         != Value.m_InterrogatedModes )        return false;
    if( m_TransmissionIndicator     != Value.m_TransmissionIndicator )        return false;
    if( m_ReplyAmplification        != Value.m_ReplyAmplification )        return false;
    return true;
}

//////////////////////////////////////////////////////////////////////////

KBOOL BasicInteractive::operator != ( const BasicInteractive & Value ) const
{
    return !( *this == Value );
}

//////////////////////////////////////////////////////////////////////////
