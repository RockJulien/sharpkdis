/********************************************************************
    class:      InformationLayers
    DIS:        (7) 1278.1 - 2012
    created:    21/03/2019
    author:     Nicolas Duboys

    purpose:    

    Size:       8 bits / 1 octet
*********************************************************************/

#pragma once

#include "./DataTypeBase.h"

namespace KDIS {
namespace DATA_TYPE {

class KDIS_EXPORT InformationLayers : public DataTypeBase
{
protected:

	union
	{
		struct
		{
			KUINT8 m_ui8NotUsed                              : 1;
			KUINT8 m_uiLayer1                                : 1;
			KUINT8 m_uiLayer2                                : 1;
			KUINT8 m_uiLayer3                                : 1;
			KUINT8 m_uiLayer4                                : 1;
			KUINT8 m_uiLayer5                                : 1;
			KUINT8 m_uiLayer6                                : 1;
			KUINT8 m_uiLayer7                                : 1;
		};
		KUINT8 m_ui8InformationLayers;

	} m_InformationLayersUnion;

	mutable KDataStream m_stream;
		
public:

    static const KUINT16 INFORMATION_LAYERS_SIZE = 1; 

    InformationLayers();

    InformationLayers( KDataStream & stream ) ;

	InformationLayers( KBOOL Layer1, KBOOL Layer2, KBOOL Layer3, KBOOL Layer4, KBOOL Layer5, KBOOL Layer6, KBOOL Layer7 );
	
    virtual ~InformationLayers();

    //************************************
    // FullName:    KDIS::DATA_TYPE::InformationLayers::SetLayer1
    //              KDIS::DATA_TYPE::InformationLayers::GetLayer1
    // Description: Indicates to which layer(s) the IFF data records 
    //              present in Layer 5 are applicable. 
    //              In this case, each Layer 1 through 7 field shall 
    //              be set to either Not Applicable (false) or Applicable (true)
    // Parameter:   KBOOL Layer1 
    //************************************
	void SetLayer1( KBOOL Layer1 );
	KBOOL GetLayer1() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::InformationLayers::SetLayer2
    //              KDIS::DATA_TYPE::InformationLayers::GetLayer2
    // Description: Indicates to which layer(s) the IFF data records 
    //              present in Layer 5 are applicable. 
    //              In this case, each Layer 1 through 7 field shall 
    //              be set to either Not Applicable (false) or Applicable (true)
    // Parameter:   KBOOL Layer2 
    //************************************
	void SetLayer2( KBOOL Layer2 );
	KBOOL GetLayer2() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::InformationLayers::SetLayer3
    //              KDIS::DATA_TYPE::InformationLayers::GetLayer3
    // Description: Indicates to which layer(s) the IFF data records 
    //              present in Layer 5 are applicable. 
    //              In this case, each Layer 1 through 7 field shall 
    //              be set to either Not Applicable (false) or Applicable (true)
    // Parameter:   KBOOL Layer3 
    //************************************
	void SetLayer3( KBOOL Layer3 );
	KBOOL GetLayer3() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::InformationLayers::SetLayer4
    //              KDIS::DATA_TYPE::InformationLayers::GetLayer4
    // Description: Indicates to which layer(s) the IFF data records 
    //              present in Layer 5 are applicable. 
    //              In this case, each Layer 1 through 7 field shall 
    //              be set to either Not Applicable (false) or Applicable (true)
    // Parameter:   KBOOL Layer4 
    //************************************
	void SetLayer4( KBOOL Layer4 );
	KBOOL GetLayer4() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::InformationLayers::SetLayer5
    //              KDIS::DATA_TYPE::InformationLayers::GetLayer5
    // Description: Indicates to which layer(s) the IFF data records 
    //              present in Layer 5 are applicable. 
    //              In this case, each Layer 1 through 7 field shall 
    //              be set to either Not Applicable (false) or Applicable (true)
    // Parameter:   KBOOL Layer5 
    //************************************
	void SetLayer5( KBOOL Layer5 );
	KBOOL GetLayer5() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::InformationLayers::SetLayer6
    //              KDIS::DATA_TYPE::InformationLayers::GetLayer6
    // Description: Indicates to which layer(s) the IFF data records 
    //              present in Layer 5 are applicable. 
    //              In this case, each Layer 1 through 7 field shall 
    //              be set to either Not Applicable (false) or Applicable (true)
    // Parameter:   KBOOL Layer6 
    //************************************
	void SetLayer6( KBOOL Layer6 );
	KBOOL GetLayer6() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::InformationLayers::SetLayer7
    //              KDIS::DATA_TYPE::InformationLayers::GetLayer7
    // Description: Indicates to which layer(s) the IFF data records 
    //              present in Layer 5 are applicable. 
    //              In this case, each Layer 1 through 7 field shall 
    //              be set to either Not Applicable (false) or Applicable (true)
    // Parameter:   KBOOL Layer7 
    //************************************
	void SetLayer7( KBOOL Layer7 );
	KBOOL GetLayer7() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::InformationLayers::GetAsString
    // Description: Returns a string representation 
    //************************************
    virtual const KOCTET* GetAsString() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::InformationLayers::Decode
    // Description: Convert From Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual void Decode( KDataStream & stream ) ;

    //************************************
    // FullName:    KDIS::DATA_TYPE::InformationLayers::Encode
    // Description: Convert To Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual const KDataStream* GetEncodedStream() const;
    virtual void EncodeToStream( KDataStream & stream ) const;

    KBOOL operator == ( const InformationLayers & Value ) const;
    KBOOL operator != ( const InformationLayers & Value ) const;
};

} // END namespace DATA_TYPE
} // END namespace KDIS
