
#include "./CryptoControl.h"

//////////////////////////////////////////////////////////////////////////

using namespace KDIS;
using namespace DATA_TYPE;
using namespace ENUMS;

//////////////////////////////////////////////////////////////////////////
// Public:
//////////////////////////////////////////////////////////////////////////

CryptoControl::CryptoControl() :
    m_ui16PseudoCryptoCode( 0 ),
    m_ui16PseudoCryptoCodeA( 0 ),
    m_ui16PseudoCryptoCodeB( 0 )
{
    m_ui32Type = CryptoControlRecord;
    m_ui16Length = CRYPTO_CONTROL_TYPE_SIZE;
}

//////////////////////////////////////////////////////////////////////////

CryptoControl::CryptoControl( KUINT16 PseudoCryptoCode, KUINT16 PseudoCryptoCodeA, KUINT16 PseudoCryptoCodeB )  :
    m_ui16PseudoCryptoCode( PseudoCryptoCode ),
    m_ui16PseudoCryptoCodeA( PseudoCryptoCodeA ),
    m_ui16PseudoCryptoCodeB( PseudoCryptoCodeB )
{
    m_ui32Type = CryptoControlRecord;
    m_ui16Length = CRYPTO_CONTROL_TYPE_SIZE;
}

//////////////////////////////////////////////////////////////////////////

CryptoControl::CryptoControl( KDataStream & stream ) 
{
    Decode( stream );
}

//////////////////////////////////////////////////////////////////////////

CryptoControl::~CryptoControl()
{
}

//////////////////////////////////////////////////////////////////////////

void CryptoControl::SetPseudoCryptoCode( KUINT16 PseudoCryptoCode )
{
    m_ui16PseudoCryptoCode = PseudoCryptoCode;
}

//////////////////////////////////////////////////////////////////////////

KUINT16 CryptoControl::GetPseudoCryptoCode() const
{
    return m_ui16PseudoCryptoCode;
}

//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////

void CryptoControl::SetPseudoCryptoCodeA( KUINT16 PseudoCryptoCodeA )
{
    m_ui16PseudoCryptoCodeA = PseudoCryptoCodeA;
}

//////////////////////////////////////////////////////////////////////////

KUINT16 CryptoControl::GetPseudoCryptoCodeA() const
{
    return m_ui16PseudoCryptoCodeA;
}

//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////

void CryptoControl::SetPseudoCryptoCodeB( KUINT16 PseudoCryptoCodeB )
{
    m_ui16PseudoCryptoCodeB = PseudoCryptoCodeB;
}

//////////////////////////////////////////////////////////////////////////

KUINT16 CryptoControl::GetPseudoCryptoCodeB() const
{
    return m_ui16PseudoCryptoCodeB;
}

//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////

const KOCTET* CryptoControl::GetAsString() const
{
    KStringStream ss;

    ss << StandardVariable::GetAsString()
       << "PseudoCyptoCode:    " << m_ui16PseudoCryptoCode    << "\n"
       << "PseudoCyptoCodeA:   " << m_ui16PseudoCryptoCodeA   << "\n"
       << "PseudoCyptoCodeB:   " << m_ui16PseudoCryptoCodeB   << "\n";

    return ss.str().c_str();
}

//////////////////////////////////////////////////////////////////////////

void CryptoControl::Decode( KDataStream & stream ) 
{
    if( stream.GetBufferSize() < CRYPTO_CONTROL_TYPE_SIZE )throw KException( __FUNCTION__, NOT_ENOUGH_DATA_IN_BUFFER );

    StandardVariable::Decode( stream );

    stream >> m_ui16PseudoCryptoCode
           >> m_ui16PseudoCryptoCodeA
           >> m_ui16PseudoCryptoCodeB;
}

//////////////////////////////////////////////////////////////////////////

void CryptoControl::EncodeToStream( KDataStream & stream ) const
{
    StandardVariable::EncodeToStream( stream );

    stream << m_ui16PseudoCryptoCode
           << m_ui16PseudoCryptoCodeA
           << m_ui16PseudoCryptoCodeB;
}

//////////////////////////////////////////////////////////////////////////

KBOOL CryptoControl::operator == ( const CryptoControl & Value ) const
{
    if( StandardVariable::operator  !=( Value ) )                          return false;
    if( m_ui16PseudoCryptoCode      != Value.m_ui16PseudoCryptoCode )      return false;
    if( m_ui16PseudoCryptoCodeA     != Value.m_ui16PseudoCryptoCodeA )     return false;
    if( m_ui16PseudoCryptoCodeB     != Value.m_ui16PseudoCryptoCodeB )     return false;
    return true;
}

//////////////////////////////////////////////////////////////////////////

KBOOL CryptoControl::operator != ( const CryptoControl & Value ) const
{
    return !( *this == Value );
}

//////////////////////////////////////////////////////////////////////////
