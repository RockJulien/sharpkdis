#include "./ModeSInterrogatorBasicData.h"

//////////////////////////////////////////////////////////////////////////

using namespace std;
using namespace KDIS;
using namespace UTILS;
using namespace DATA_TYPE;
using namespace ENUMS;

//////////////////////////////////////////////////////////////////////////
// public:
//////////////////////////////////////////////////////////////////////////

ModeSInterrogatorBasicData::ModeSInterrogatorBasicData() :
    m_ui8Padding_1( 0 ),
    m_ui8Padding_2( 0 ),
    m_ui32Padding_1( 0 ),
    m_ui32Padding_2( 0 ),
    m_ui32Padding_3( 0 ),
    m_ui32Padding_4( 0 ),
    m_ui32Padding_5( 0 )
{
}

//////////////////////////////////////////////////////////////////////////

ModeSInterrogatorBasicData::ModeSInterrogatorBasicData( const ModeSInterrogatorStatus & S, const ModeSLevelsPresent & MSLS ) :
    m_Status( S ),
    m_ModeSLevelsPresent( MSLS ),
    m_ui8Padding_1( 0 ),
    m_ui8Padding_2( 0 ),
    m_ui32Padding_1( 0 ),
    m_ui32Padding_2( 0 ),
    m_ui32Padding_3( 0 ),
    m_ui32Padding_4( 0 ),
    m_ui32Padding_5( 0 )
{
}

//////////////////////////////////////////////////////////////////////////

ModeSInterrogatorBasicData::ModeSInterrogatorBasicData( KDataStream & stream ) 
{
    Decode( stream );
}

//////////////////////////////////////////////////////////////////////////

ModeSInterrogatorBasicData::~ModeSInterrogatorBasicData()
{
}

//////////////////////////////////////////////////////////////////////////

void ModeSInterrogatorBasicData::SetStatus( const ModeSInterrogatorStatus & S )
{
    m_Status = S;
}

//////////////////////////////////////////////////////////////////////////

const ModeSInterrogatorStatus & ModeSInterrogatorBasicData::GetStatus() const
{
    return m_Status;
}

//////////////////////////////////////////////////////////////////////////

ModeSInterrogatorStatus & ModeSInterrogatorBasicData::GetStatus()
{
    return m_Status;
}

//////////////////////////////////////////////////////////////////////////

void ModeSInterrogatorBasicData::SetModeSLevelsPresent( const ModeSLevelsPresent & MSLS )
{
    m_ModeSLevelsPresent = MSLS;
}

//////////////////////////////////////////////////////////////////////////

const ModeSLevelsPresent & ModeSInterrogatorBasicData::GetModeSLevelsPresent() const
{
    return m_ModeSLevelsPresent;
}

//////////////////////////////////////////////////////////////////////////

ModeSLevelsPresent & ModeSInterrogatorBasicData::GetModeSLevelsPresent()
{
    return m_ModeSLevelsPresent;
}

//////////////////////////////////////////////////////////////////////////

const KOCTET* ModeSInterrogatorBasicData::GetAsString() const
{
    KStringStream ss;

    ss << "Mode S Interrogator Basic Data:\n"
        << IndentString( m_Status.GetAsString() )
        << IndentString( m_ModeSLevelsPresent.GetAsString() );

    return ss.str().c_str();
}

//////////////////////////////////////////////////////////////////////////

void ModeSInterrogatorBasicData::Decode( KDataStream & stream ) 
{
    if( stream.GetBufferSize() < MODE_S_INTERROGATOR_BASIC_DATA_SIZE )throw KException( __FUNCTION__, NOT_ENOUGH_DATA_IN_BUFFER );

    stream >> KDIS_STREAM m_Status
           >> m_ui8Padding_1
           >> KDIS_STREAM m_ModeSLevelsPresent
           >> m_ui8Padding_2
           >> m_ui32Padding_1
           >> m_ui32Padding_2
           >> m_ui32Padding_3
           >> m_ui32Padding_4
           >> m_ui32Padding_5;
}

//////////////////////////////////////////////////////////////////////////

const KDataStream* ModeSInterrogatorBasicData::GetEncodedStream() const
{
	m_stream.Clear();

	this->EncodeToStream(m_stream);

	return &m_stream;
}

//////////////////////////////////////////////////////////////////////////

void ModeSInterrogatorBasicData::EncodeToStream( KDataStream & stream ) const
{
    stream << KDIS_STREAM m_Status
           << m_ui8Padding_1
           << KDIS_STREAM m_ModeSLevelsPresent
           << m_ui8Padding_2
           << m_ui32Padding_1
           << m_ui32Padding_2
           << m_ui32Padding_3
           << m_ui32Padding_4
           << m_ui32Padding_5;
}

//////////////////////////////////////////////////////////////////////////

KBOOL ModeSInterrogatorBasicData::operator == ( const ModeSInterrogatorBasicData & Value ) const
{
    if( m_Status                     != Value.m_Status )                        return false;
    if( m_ModeSLevelsPresent         != Value.m_ModeSLevelsPresent )            return false;
    return true;
}

//////////////////////////////////////////////////////////////////////////

KBOOL ModeSInterrogatorBasicData::operator != ( const ModeSInterrogatorBasicData & Value ) const
{
    return !( *this == Value );
}

//////////////////////////////////////////////////////////////////////////
