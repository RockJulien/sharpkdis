/********************************************************************
    class:      InteractiveMode4Reply
    created:    21/03/2019
    author:     Nicolas Duboys

    purpose:    Interactive mode4 reply entry for IFF data record (Layer 5 transponder).
    size:       48 bits / 6 octets
*********************************************************************/

#pragma once

#include "./StandardVariable.h"

namespace KDIS {
namespace DATA_TYPE {

class KDIS_EXPORT InteractiveMode4Reply : public StandardVariable
{
protected:

    KUINT8  m_ui8InteractiveMode4Reply;

    KUINT8  m_ui8Padding;

    KUINT32 m_ui32Padding;

public:

    static const KUINT16 INTERACTIVE_MODE4_REPLY_TYPE_SIZE = 12;

    InteractiveMode4Reply( KDIS::DATA_TYPE::ENUMS::AlternateMode4ChallengeReply AM4CR ) ;

    InteractiveMode4Reply();

    InteractiveMode4Reply( KDataStream & stream ) ;

    virtual ~InteractiveMode4Reply();

    //************************************
    // FullName:    KDIS::DATA_TYPE::InteractiveMode4Reply::SetAlternateMode4ChallengeReply
    //              KDIS::DATA_TYPE::InteractiveMode4Reply::GetAlternateMode4ChallengeReply
    // Description: Set the reply.
    // Parameter:   Alternate Mode4 Challenge Reply
    //************************************
    void SetAlternateMode4ChallengeReply( KDIS::DATA_TYPE::ENUMS::AlternateMode4ChallengeReply AM4CR );
    KDIS::DATA_TYPE::ENUMS::AlternateMode4ChallengeReply GetAlternateMode4ChallengeReply() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::InteractiveMode4Reply::GetAsString
    // Description: Returns a string representation.
    //************************************
    virtual const KOCTET* GetAsString() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::InteractiveMode4Reply::Decode
    // Description: Convert From Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual void Decode( KDataStream & stream ) ;

    //************************************
    // FullName:    KDIS::DATA_TYPE::InteractiveMode4Reply::Encode
    // Description: Convert To Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual void EncodeToStream( KDataStream & stream ) const;

    KBOOL operator == ( const InteractiveMode4Reply & Value ) const;
    KBOOL operator != ( const InteractiveMode4Reply & Value ) const;
};

} // END namespace DATA_TYPES
} // END namespace KDIS
