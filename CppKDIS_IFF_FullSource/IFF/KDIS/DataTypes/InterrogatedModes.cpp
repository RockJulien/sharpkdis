#include "./InterrogatedModes.h"

//////////////////////////////////////////////////////////////////////////

using namespace std;
using namespace KDIS;
using namespace DATA_TYPE;
using namespace ENUMS;

//////////////////////////////////////////////////////////////////////////
// public:
//////////////////////////////////////////////////////////////////////////

InterrogatedModes::InterrogatedModes() 
{
	m_InterrogatedModesUnion.m_ui16InterrogatedModes = 0;	
}

//////////////////////////////////////////////////////////////////////////

InterrogatedModes::InterrogatedModes( KDataStream & stream ) 
{
    Decode( stream );
}

//////////////////////////////////////////////////////////////////////////

InterrogatedModes::InterrogatedModes( KBOOL Mode1, KBOOL Mode2, KBOOL Mode3A, KBOOL Mode4, KBOOL Mode5, KBOOL ModeS, KBOOL ModeCAlt,
                            KBOOL TCAS_ACAS, KBOOL SpecialIP, KBOOL Squitter )
{	
	m_InterrogatedModesUnion.m_ui16NotUsed   = 0;
	m_InterrogatedModesUnion.m_ui16Mode1     = Mode1;
	m_InterrogatedModesUnion.m_ui16Mode2     = Mode2;      
	m_InterrogatedModesUnion.m_ui16Mode3A    = Mode3A;
	m_InterrogatedModesUnion.m_ui16Mode4     = Mode4; 
	m_InterrogatedModesUnion.m_ui16Mode5     = Mode5;
	m_InterrogatedModesUnion.m_ui16ModeS     = ModeS;
	m_InterrogatedModesUnion.m_ui16ModeCAlt  = ModeCAlt;
	m_InterrogatedModesUnion.m_ui16TCAS_ACAS = TCAS_ACAS;
	m_InterrogatedModesUnion.m_ui16SpecialIP = SpecialIP;
	m_InterrogatedModesUnion.m_ui16Squitter  = Squitter;
	m_InterrogatedModesUnion.m_ui16Padding   = 0;          
}

//////////////////////////////////////////////////////////////////////////

InterrogatedModes::~InterrogatedModes()
{
}

//////////////////////////////////////////////////////////////////////////

void InterrogatedModes::SetMode1( KBOOL Mode1 ) 
{	
	m_InterrogatedModesUnion.m_ui16Mode1 = Mode1;
}

//////////////////////////////////////////////////////////////////////////

KBOOL InterrogatedModes::GetMode1() const
{
	return ( KBOOL )m_InterrogatedModesUnion.m_ui16Mode1;
}

//////////////////////////////////////////////////////////////////////////

void InterrogatedModes::SetMode2( KBOOL Mode2 ) 
{	
	m_InterrogatedModesUnion.m_ui16Mode2 = Mode2;
}

//////////////////////////////////////////////////////////////////////////

KBOOL InterrogatedModes::GetMode2() const
{
	return ( KBOOL )m_InterrogatedModesUnion.m_ui16Mode2;
}

//////////////////////////////////////////////////////////////////////////

void InterrogatedModes::SetMode3A( KBOOL Mode3A ) 
{	
	m_InterrogatedModesUnion.m_ui16Mode3A = Mode3A;
}

//////////////////////////////////////////////////////////////////////////

KBOOL InterrogatedModes::GetMode3A() const
{
	return ( KBOOL )m_InterrogatedModesUnion.m_ui16Mode3A;
}

//////////////////////////////////////////////////////////////////////////

void InterrogatedModes::SetMode4( KBOOL Mode4 ) 
{	
	m_InterrogatedModesUnion.m_ui16Mode4 = Mode4;
}

//////////////////////////////////////////////////////////////////////////

KBOOL InterrogatedModes::GetMode4() const
{
	return ( KBOOL )m_InterrogatedModesUnion.m_ui16Mode4;
}

//////////////////////////////////////////////////////////////////////////

void InterrogatedModes::SetMode5( KBOOL Mode5 ) 
{	
	m_InterrogatedModesUnion.m_ui16Mode5 = Mode5;
}

//////////////////////////////////////////////////////////////////////////

KBOOL InterrogatedModes::GetMode5() const
{
	return ( KBOOL )m_InterrogatedModesUnion.m_ui16Mode5;
}

//////////////////////////////////////////////////////////////////////////

void InterrogatedModes::SetModeS( KBOOL ModeS ) 
{	
	m_InterrogatedModesUnion.m_ui16ModeS = ModeS;
}

//////////////////////////////////////////////////////////////////////////

KBOOL InterrogatedModes::GetModeS() const
{
	return ( KBOOL )m_InterrogatedModesUnion.m_ui16ModeS;
}

//////////////////////////////////////////////////////////////////////////

void InterrogatedModes::SetModeCAlt( KBOOL ModeCAlt ) 
{	
	m_InterrogatedModesUnion.m_ui16ModeCAlt = ModeCAlt;
}

//////////////////////////////////////////////////////////////////////////

KBOOL InterrogatedModes::GetModeCAlt() const
{
	return ( KBOOL )m_InterrogatedModesUnion.m_ui16ModeCAlt;
}

//////////////////////////////////////////////////////////////////////////

void InterrogatedModes::SetTCAS_ACAS( KBOOL TCAS_ACAS ) 
{	
	m_InterrogatedModesUnion.m_ui16TCAS_ACAS = TCAS_ACAS;
}

//////////////////////////////////////////////////////////////////////////

KBOOL InterrogatedModes::GetTCAS_ACAS() const
{
	return ( KBOOL )m_InterrogatedModesUnion.m_ui16TCAS_ACAS;
}

//////////////////////////////////////////////////////////////////////////

void InterrogatedModes::SetSpecialIP( KBOOL SpecialIP ) 
{	
	m_InterrogatedModesUnion.m_ui16SpecialIP = SpecialIP;
}

//////////////////////////////////////////////////////////////////////////

KBOOL InterrogatedModes::GetSpecialIP() const
{
	return ( KBOOL )m_InterrogatedModesUnion.m_ui16SpecialIP;
}

//////////////////////////////////////////////////////////////////////////

void InterrogatedModes::SetSquitter( KBOOL Squitter ) 
{	
	m_InterrogatedModesUnion.m_ui16Squitter = Squitter;
}

//////////////////////////////////////////////////////////////////////////

KBOOL InterrogatedModes::GetSquitter() const
{
	return ( KBOOL )m_InterrogatedModesUnion.m_ui16Squitter;
}

//////////////////////////////////////////////////////////////////////////

const KOCTET* InterrogatedModes::GetAsString() const
{
    KStringStream ss;	

	ss << "Interrogated Modes:"
	   << "\n\tMode1:                   " << ( KBOOL )m_InterrogatedModesUnion.m_ui16Mode1
	   << "\n\tMode2:                   " << ( KBOOL )m_InterrogatedModesUnion.m_ui16Mode2
	   << "\n\tMode3A:                  " << ( KBOOL )m_InterrogatedModesUnion.m_ui16Mode3A
	   << "\n\tMode4:                   " << ( KBOOL )m_InterrogatedModesUnion.m_ui16Mode4
	   << "\n\tMode5:                   " << ( KBOOL )m_InterrogatedModesUnion.m_ui16Mode5
	   << "\n\tModeS:                   " << ( KBOOL )m_InterrogatedModesUnion.m_ui16ModeS
	   << "\n\tModeCAlt:                " << ( KBOOL )m_InterrogatedModesUnion.m_ui16ModeCAlt
	   << "\n\tTCAS ACAS:               " << ( KBOOL )m_InterrogatedModesUnion.m_ui16TCAS_ACAS
	   << "\n\tSpecial IP:              " << ( KBOOL )m_InterrogatedModesUnion.m_ui16SpecialIP
	   << "\n\tSquitter:                " << ( KBOOL )m_InterrogatedModesUnion.m_ui16Squitter
	   << "\n";

    return ss.str().c_str();
}

//////////////////////////////////////////////////////////////////////////

void InterrogatedModes::Decode( KDataStream & stream ) 
{
    if( stream.GetBufferSize() < INTERROGATED_MODES_SIZE )throw KException( __FUNCTION__, NOT_ENOUGH_DATA_IN_BUFFER );
	
	stream >> m_InterrogatedModesUnion.m_ui16InterrogatedModes;
}

//////////////////////////////////////////////////////////////////////////

const KDataStream* InterrogatedModes::GetEncodedStream() const
{
	m_stream.Clear();

	this->EncodeToStream(m_stream);

	return &m_stream;
}

//////////////////////////////////////////////////////////////////////////

void InterrogatedModes::EncodeToStream( KDataStream & stream ) const
{
	stream << m_InterrogatedModesUnion.m_ui16InterrogatedModes;
}

//////////////////////////////////////////////////////////////////////////

KBOOL InterrogatedModes::operator == ( const InterrogatedModes & Value ) const
{
	if( m_InterrogatedModesUnion.m_ui16InterrogatedModes != Value.m_InterrogatedModesUnion.m_ui16InterrogatedModes ) return false;
    return true;
}

//////////////////////////////////////////////////////////////////////////

KBOOL InterrogatedModes::operator != ( const InterrogatedModes & Value ) const
{
    return !( *this == Value );
}

//////////////////////////////////////////////////////////////////////////
