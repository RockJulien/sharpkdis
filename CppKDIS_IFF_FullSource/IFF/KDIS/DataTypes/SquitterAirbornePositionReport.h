/********************************************************************
    class:      SquitterAirbornePositionReport
    created:    21/03/2019
    author:     Nicolas Duboys

    purpose:    Squitter Airborne Position Report entry for IFF data record (Layer 4 Mode S).
    size:       16 bits / 2 octets
*********************************************************************/

#pragma once

#include "./StandardVariable.h"

namespace KDIS {
namespace DATA_TYPE {

class KDIS_EXPORT SquitterAirbornePositionReport : public StandardVariable
{
protected:

    KUINT16 m_ui16SquitterReport;

public:

    static const KUINT16 SQUITTER_AIRBORNE_POSITION_REPORT_TYPE_SIZE = 8;

    SquitterAirbornePositionReport( KUINT16 SquitterReport ) ;

    SquitterAirbornePositionReport();

    SquitterAirbornePositionReport( KDataStream & stream ) ;

    virtual ~SquitterAirbornePositionReport();

    //************************************
    // FullName:    KDIS::DATA_TYPE::SquitterAirbornePositionReport::SetSquitterReport
    //              KDIS::DATA_TYPE::SquitterAirbornePositionReport::GetSquitterReport
    // Description: Set Squitter report.
    // Parameter:   Squitter report
    //************************************
    void SetSquitterReport( KUINT16 SquitterReport );
    KUINT16 GetSquitterReport() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::SquitterAirbornePositionReport::GetAsString
    // Description: Returns a string representation.
    //************************************
    virtual const KOCTET* GetAsString() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::SquitterAirbornePositionReport::Decode
    // Description: Convert From Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual void Decode( KDataStream & stream ) ;

    //************************************
    // FullName:    KDIS::DATA_TYPE::SquitterAirbornePositionReport::Encode
    // Description: Convert To Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual void EncodeToStream( KDataStream & stream ) const;

    KBOOL operator == ( const SquitterAirbornePositionReport & Value ) const;
    KBOOL operator != ( const SquitterAirbornePositionReport & Value ) const;
};

} // END namespace DATA_TYPES
} // END namespace KDIS
