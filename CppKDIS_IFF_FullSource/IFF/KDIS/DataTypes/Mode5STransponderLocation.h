/********************************************************************
    class:      CryptoControl
    created:    21/03/2019
    author:     Nicolas Duboys

    purpose:    Mode 5/S Transponder Location entry for IFF data record (Layer 3 Mode 5).
    size:       112 bits / 14 octets
*********************************************************************/

#pragma once

#include "./StandardVariable.h"

namespace KDIS {
namespace DATA_TYPE {

class KDIS_EXPORT Mode5STransponderLocation : public StandardVariable
{
protected:

    KINT32 m_i32Latitude;

    KINT32 m_i32Longitude;

    KUINT16 m_ui16Mode5Altitude;

    KUINT16 m_ui16BarometricAltitude;

    KUINT16 m_ui16Padding;

public:

    static const KUINT16 MODE_5S_TRANSPONDER_LOCATION_TYPE_SIZE = 20;

    Mode5STransponderLocation( KINT32 Latitude, KINT32 Longitude, KUINT16 Mode5Altitude, KUINT16 BarometricAltitude ) ;

    Mode5STransponderLocation();

    Mode5STransponderLocation( KDataStream & stream ) ;

    virtual ~Mode5STransponderLocation();

    //************************************
    // FullName:    KDIS::DATA_TYPE::Mode5STransponderLocation::SetLatitude
    //              KDIS::DATA_TYPE::Mode5STransponderLocation::GetLatitude
    // Description: Set the latitude.
    // Parameter:   Latitude in degrees
    //************************************
    void SetLatitude( KINT32 Latitude );
    KINT32 GetLatitude() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::Mode5STransponderLocation::SetLongitude
    //              KDIS::DATA_TYPE::Mode5STransponderLocation::GetLongitude
    // Description: Set the longitude.
    // Parameter:   Longitude in degrees
    //************************************
    void SetLongitude( KINT32 Longitude );
    KINT32 GetLongitude() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::Mode5STransponderLocation::SetMode5Altitude
    //              KDIS::DATA_TYPE::Mode5STransponderLocation::GetMode5Altitude
    // Description: Set the mode 5 altitude.
    // Parameter:   Mode5Altitude
    //************************************
    void SetMode5Altitude( KUINT16 Mode5Altitude );
    KUINT16 GetMode5Altitude() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::Mode5STransponderLocation::SetBarometricAltitude
    //              KDIS::DATA_TYPE::Mode5STransponderLocation::GetBarometricAltitude
    // Description: Set the barometric altitude.
    // Parameter:   BarometricAltitude
    //************************************
    void SetBarometricAltitude( KUINT16 BarometricAltitude );
    KUINT16 GetBarometricAltitude() const;


    //************************************
    // FullName:    KDIS::DATA_TYPE::Mode5STransponderLocation::GetAsString
    // Description: Returns a string representation.
    //************************************
    virtual const KOCTET* GetAsString() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::Mode5STransponderLocation::Decode
    // Description: Convert From Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual void Decode( KDataStream & stream ) ;

    //************************************
    // FullName:    KDIS::DATA_TYPE::Mode5STransponderLocation::Encode
    // Description: Convert To Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual void EncodeToStream( KDataStream & stream ) const;

    KBOOL operator == ( const Mode5STransponderLocation & Value ) const;
    KBOOL operator != ( const Mode5STransponderLocation & Value ) const;
};

} // END namespace DATA_TYPES
} // END namespace KDIS
