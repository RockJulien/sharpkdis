#include "./IFF_Layer4Interrogator.h"

//////////////////////////////////////////////////////////////////////////

using namespace std;
using namespace KDIS;
using namespace DATA_TYPE;
using namespace UTILS;

//////////////////////////////////////////////////////////////////////////
// public:
//////////////////////////////////////////////////////////////////////////

IFF_Layer4Interrogator::IFF_Layer4Interrogator() 
{
}

//////////////////////////////////////////////////////////////////////////

IFF_Layer4Interrogator::IFF_Layer4Interrogator( KDataStream & stream ) 
{
    Decode( stream, false );
}

//////////////////////////////////////////////////////////////////////////

IFF_Layer4Interrogator::IFF_Layer4Interrogator( const SimulationIdentifier & ReportingSimulation, const ModeSInterrogatorBasicData & Data,
                                              std::vector<StdVarPtr> & Records ) 
{
	m_RptSim = ReportingSimulation;
	m_BasicData = Data;
	SetDataRecords( Records );
}

//////////////////////////////////////////////////////////////////////////

IFF_Layer4Interrogator::IFF_Layer4Interrogator( const LayerHeader & H, KDataStream & stream )  :
	IFF_Layer4( H )
{
    Decode( stream, true );
}

//////////////////////////////////////////////////////////////////////////

IFF_Layer4Interrogator::~IFF_Layer4Interrogator()
{
}

//////////////////////////////////////////////////////////////////////////

void IFF_Layer4Interrogator::SetBasicData( const ModeSInterrogatorBasicData & BD )
{
	m_BasicData = BD;
}

//////////////////////////////////////////////////////////////////////////
const ModeSInterrogatorBasicData & IFF_Layer4Interrogator::GetBasicData() const
{
	return m_BasicData;
}

//////////////////////////////////////////////////////////////////////////

ModeSInterrogatorBasicData & IFF_Layer4Interrogator::GetBasicDatan()
{
	return m_BasicData;
}

//////////////////////////////////////////////////////////////////////////

const KOCTET* IFF_Layer4Interrogator::GetAsString() const
{
    KStringStream ss;

    ss << "IFF Layer 4 Interrogator\n"
		<< LayerHeader::GetAsString()
		<< "Reporting Simulation: " << m_RptSim.GetAsString()
		<< "Basic Data: "           << m_BasicData.GetAsString() 
		<< "Num IFF Records: "      << m_ui16NumIffRecs 
		<< "\nIFF Records:\n";

    vector<KDIS::DATA_TYPE::StdVarPtr>::const_iterator citr = m_vStdVarRecs.begin();
    vector<KDIS::DATA_TYPE::StdVarPtr>::const_iterator citrEnd = m_vStdVarRecs.end();
    for( ; citr != citrEnd; ++citr )
    {
		ss << ( *citr )->GetAsString();
    }	

    return ss.str().c_str();
}

//////////////////////////////////////////////////////////////////////////

void IFF_Layer4Interrogator::Decode(KDataStream & stream)
{
	LayerHeader::Decode( stream );
}

//////////////////////////////////////////////////////////////////////////

void IFF_Layer4Interrogator::Decode( KDataStream & stream, bool ignoreHeader /*= true*/ ) 
{
    if( stream.GetBufferSize()  +  ( ignoreHeader ? LayerHeader::LAYER_HEADER_SIZE : 0 ) < IFF_LAYER4_SIZE )throw KException( __FUNCTION__, NOT_ENOUGH_DATA_IN_BUFFER );

    m_vStdVarRecs.clear();

	if( !ignoreHeader )
	{
		LayerHeader::Decode( stream );
	}

	stream >> KDIS_STREAM m_RptSim
		   >> KDIS_STREAM m_BasicData
		   >> m_ui16Padding
		   >> m_ui16NumIffRecs;

	// Use the factory decode function for each standard variable
    for( KUINT16 i = 0; i < m_ui16NumIffRecs; ++i )
    {
        // NIKOKO Pour gerer les enum inconnu dans les standard vars, ca peut retourner null
        // Dans ce cas on le push pas
        // m_vStdVarRecs.push_back( StandardVariable::FactoryDecodeStandardVariable( stream ) );
        StdVarPtr decodedStdVar = StandardVariable::FactoryDecodeStandardVariable( stream );
        if (decodedStdVar != nullptr)
        {
            m_vStdVarRecs.push_back( decodedStdVar );
        }
    }
}

//////////////////////////////////////////////////////////////////////////

void IFF_Layer4Interrogator::EncodeToStream( KDataStream & stream ) const
{
	LayerHeader::EncodeToStream( stream );

    stream << KDIS_STREAM m_RptSim
	       << KDIS_STREAM m_BasicData
		   << m_ui16Padding
		   << m_ui16NumIffRecs;

    vector<KDIS::DATA_TYPE::StdVarPtr>::const_iterator citr = m_vStdVarRecs.begin();
    vector<KDIS::DATA_TYPE::StdVarPtr>::const_iterator citrEnd = m_vStdVarRecs.end();
    for( ; citr != citrEnd; ++citr )
    {
        ( *citr )->EncodeToStream( stream );
    }	
}

//////////////////////////////////////////////////////////////////////////

KBOOL IFF_Layer4Interrogator::operator == ( const IFF_Layer4Interrogator & Value ) const
{
    if( IFF_Layer4::operator !=( Value ) )          return false;    
    if( m_BasicData          != Value.m_BasicData ) return false; 
    return true;
}

//////////////////////////////////////////////////////////////////////////

KBOOL IFF_Layer4Interrogator::operator != ( const IFF_Layer4Interrogator & Value ) const
{
    return !( *this == Value );
}

//////////////////////////////////////////////////////////////////////////
