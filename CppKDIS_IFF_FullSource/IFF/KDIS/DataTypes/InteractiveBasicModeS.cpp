
#include "./InteractiveBasicModeS.h"

//////////////////////////////////////////////////////////////////////////

using namespace KDIS;
using namespace DATA_TYPE;
using namespace ENUMS;
using namespace UTILS;

//////////////////////////////////////////////////////////////////////////
// Public:
//////////////////////////////////////////////////////////////////////////

InteractiveBasicModeS::InteractiveBasicModeS() :
    m_ui16TransmitState( 0 ),
    m_ui16Padding( 0 )
{
    m_ui32Type = InteractiveBasicModeSRecord;
    m_ui16Length = INTERACTIVE_BASIC_MODES_REPLY_TYPE_SIZE;
}

//////////////////////////////////////////////////////////////////////////

InteractiveBasicModeS::InteractiveBasicModeS( const ModeSLevelsPresent & MSLP, KDIS::DATA_TYPE::ENUMS::TransmitStateModeS TS, const ModeSInterrogatorIdentifier & MSII )  :
    m_ModeSLevelsPresent( MSLP ),
    m_ui16TransmitState( TS ),
    m_ModeSInterrogatorIdentifier( MSII ),
    m_ui16Padding( 0 )
{
    m_ui32Type = InteractiveBasicModeSRecord;
    m_ui16Length = INTERACTIVE_BASIC_MODES_REPLY_TYPE_SIZE;
}

//////////////////////////////////////////////////////////////////////////

InteractiveBasicModeS::InteractiveBasicModeS( KDataStream & stream ) 
{
    Decode( stream );
}

//////////////////////////////////////////////////////////////////////////

InteractiveBasicModeS::~InteractiveBasicModeS()
{
}

//////////////////////////////////////////////////////////////////////////

void InteractiveBasicModeS::SetModeSLevelsPresent( const ModeSLevelsPresent & MSLS )
{
    m_ModeSLevelsPresent = MSLS;
}

//////////////////////////////////////////////////////////////////////////

const KDIS::DATA_TYPE::ModeSLevelsPresent & InteractiveBasicModeS::GetModeSLevelsPresent() const
{
    return m_ModeSLevelsPresent;
}

//////////////////////////////////////////////////////////////////////////

KDIS::DATA_TYPE::ModeSLevelsPresent & InteractiveBasicModeS::GetModeSLevelsPresent()
{
    return m_ModeSLevelsPresent;
}

//////////////////////////////////////////////////////////////////////////

void InteractiveBasicModeS::SetTransmitState( KDIS::DATA_TYPE::ENUMS::TransmitStateModeS TS )
{
    m_ui16TransmitState = TS;
}

//////////////////////////////////////////////////////////////////////////

KDIS::DATA_TYPE::ENUMS::TransmitStateModeS InteractiveBasicModeS::GetTransmitState() const
{
    return ( KDIS::DATA_TYPE::ENUMS::TransmitStateModeS )m_ui16TransmitState;
}

//////////////////////////////////////////////////////////////////////////

void InteractiveBasicModeS::SetModeSInterrogatorIdentifier( const ModeSInterrogatorIdentifier & MSII )
{
    m_ModeSInterrogatorIdentifier = MSII;
}

//////////////////////////////////////////////////////////////////////////

const KDIS::DATA_TYPE::ModeSInterrogatorIdentifier & InteractiveBasicModeS::GetModeSInterrogatorIdentifier() const
{
    return m_ModeSInterrogatorIdentifier;
}

//////////////////////////////////////////////////////////////////////////

KDIS::DATA_TYPE::ModeSInterrogatorIdentifier & InteractiveBasicModeS::GetModeSInterrogatorIdentifier()
{
    return m_ModeSInterrogatorIdentifier;
}

//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////

const KOCTET* InteractiveBasicModeS::GetAsString() const
{
    KStringStream ss;

    ss << StandardVariable::GetAsString()
       << IndentString( m_ModeSLevelsPresent.GetAsString() )
       << GetEnumAsStringTransmitState( m_ui16TransmitState ) << "\n"
       << IndentString( m_ModeSInterrogatorIdentifier.GetAsString() );

    return ss.str().c_str();
}

//////////////////////////////////////////////////////////////////////////

void InteractiveBasicModeS::Decode( KDataStream & stream ) 
{
    if( stream.GetBufferSize() < INTERACTIVE_BASIC_MODES_REPLY_TYPE_SIZE )throw KException( __FUNCTION__, NOT_ENOUGH_DATA_IN_BUFFER );

    StandardVariable::Decode( stream );

    stream >> KDIS_STREAM m_ModeSLevelsPresent;
    stream >> m_ui16TransmitState;
    stream >> KDIS_STREAM m_ModeSInterrogatorIdentifier;
    stream >> m_ui16Padding;
}

//////////////////////////////////////////////////////////////////////////

void InteractiveBasicModeS::EncodeToStream( KDataStream & stream ) const
{
    StandardVariable::EncodeToStream( stream );

    stream << KDIS_STREAM m_ModeSLevelsPresent;
    stream << m_ui16TransmitState;
    stream << KDIS_STREAM m_ModeSInterrogatorIdentifier;
    stream << m_ui16Padding;
}

//////////////////////////////////////////////////////////////////////////

KBOOL InteractiveBasicModeS::operator == ( const InteractiveBasicModeS & Value ) const
{
    if( StandardVariable::operator     !=( Value ) )                                 return false;
    if( m_ModeSLevelsPresent           != Value.m_ModeSLevelsPresent )               return false;
    if( m_ui16TransmitState            != Value.m_ui16TransmitState )                return false;
    if( m_ModeSInterrogatorIdentifier  != Value.m_ModeSInterrogatorIdentifier )      return false;
    return true;
}

//////////////////////////////////////////////////////////////////////////

KBOOL InteractiveBasicModeS::operator != ( const InteractiveBasicModeS & Value ) const
{
    return !( *this == Value );
}

//////////////////////////////////////////////////////////////////////////
