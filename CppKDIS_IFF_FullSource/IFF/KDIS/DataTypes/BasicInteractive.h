/********************************************************************
    class:      BasicInteractive
    created:    21/03/2019
    author:     Nicolas Duboys

    purpose:    The specification of basic information for an interactive interrogation, 
                reply, or squitter transmission between two specific entities having 
                an IFF system contained in an interactive IFF PDU shall be communicated 
                by the Basic Interactive IFF Data record (Layer 5).
    size:       208 bits / 26 octets
*********************************************************************/

#pragma once

#include "./StandardVariable.h"
#include "./EntityIdentifier.h"
#include "./InterrogatedModes.h"

namespace KDIS {
namespace DATA_TYPE {

class KDIS_EXPORT BasicInteractive : public StandardVariable
{
protected:

    EntityIdentifier  m_InterrogatingEntityID;
    EntityIdentifier  m_InterrogatedEntityID;
    EntityIdentifier  m_EventID;
    InterrogatedModes m_InterrogatedModes;
    KUINT8            m_TransmissionIndicator;
    KUINT8            m_ReplyAmplification;
    KUINT32           m_ui32Padding;

public:

    static const KUINT16 BASIC_INTERACTIVE_TYPE_SIZE = 32;

    BasicInteractive( const EntityIdentifier & InterrogatingEntityID, const EntityIdentifier & InterrogatedEntityID,
        const EntityIdentifier & EventID, const InterrogatedModes & InterrogatedModes,
        KDIS::DATA_TYPE::ENUMS::TransmissionIndicator TI, KDIS::DATA_TYPE::ENUMS::ReplyAmplification RA) ;

    BasicInteractive();

    BasicInteractive( KDataStream & stream ) ;

    virtual ~BasicInteractive();

    //************************************
    // FullName:    KDIS::DATA_TYPE::BasicInteractive::SetInterrogatingEntityID
    //              KDIS::DATA_TYPE::BasicInteractive::GetInterrogatingEntityID
    // Description: Set interrogating entity ID.
    // Parameter:   InterrogatingEntityID
    //************************************
    void SetInterrogatingEntityID( const EntityIdentifier & InterrogatingEntityID );
    const EntityIdentifier & GetInterrogatingEntityID() const;
    EntityIdentifier & GetInterrogatingEntityID();

    //************************************
    // FullName:    KDIS::DATA_TYPE::BasicInteractive::SetInterrogatedEntityID
    //              KDIS::DATA_TYPE::BasicInteractive::GetInterrogatedEntityID
    // Description: Set interrogated entity ID.
    // Parameter:   InterrogatedEntityID
    //************************************
    void SetInterrogatedEntityID( const EntityIdentifier & InterrogatedEntityID );
    const EntityIdentifier & GetInterrogatedEntityID() const;
    EntityIdentifier & GetInterrogatedEntityID();

    //************************************
    // FullName:    KDIS::DATA_TYPE::BasicInteractive::SetEventID
    //              KDIS::DATA_TYPE::BasicInteractive::GetEventID
    // Description: Set event ID.
    // Parameter:   EventID
    //************************************
    void SetEventID( const EntityIdentifier & EventID );
    const EntityIdentifier & GetEventID() const;
    EntityIdentifier & GetEventID();

    //************************************
    // FullName:    KDIS::DATA_TYPE::BasicInteractive::SetInterrogatedModes
    //              KDIS::DATA_TYPE::BasicInteractive::GetInterrogatedModes
    // Description: Interrogated modes.
    // Parameter:   InterrogatedModes
    //************************************
    void SetInterrogatedModes( const InterrogatedModes & InterrogatedModes );
    const InterrogatedModes & GetInterrogatedModes() const;
    InterrogatedModes & GetInterrogatedModes();

    //************************************
    // FullName:    KDIS::DATA_TYPE::BasicInteractive::SetTransmissionIndicator
    //              KDIS::DATA_TYPE::BasicInteractive::GetTransmissionIndicator
    // Description: Specify whether this is an interrogation, a reply to an
    //              interrogation, or a squitter transmission.
    // Parameter:   KDIS::DATA_TYPE::ENUMS::TransmissionIndicator TI
    //************************************
    void SetTransmissionIndicator( KDIS::DATA_TYPE::ENUMS::TransmissionIndicator NS );
    KDIS::DATA_TYPE::ENUMS::TransmissionIndicator GetTransmissionIndicator() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::BasicInteractive::SetReplyAmplification
    //              KDIS::DATA_TYPE::BasicInteractive::GetReplyAmplification
    // Description: Provide amplifying information about the reply.
    // Parameter:   KDIS::DATA_TYPE::ENUMS::ReplyAmplification RA
    //************************************
    void SetReplyAmplification( KDIS::DATA_TYPE::ENUMS::ReplyAmplification NS );
    KDIS::DATA_TYPE::ENUMS::ReplyAmplification GetReplyAmplification() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::BasicInteractive::GetAsString
    // Description: Returns a string representation.
    //************************************
    virtual const KOCTET* GetAsString() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::BasicInteractive::Decode
    // Description: Convert From Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual void Decode( KDataStream & stream ) ;

    //************************************
    // FullName:    KDIS::DATA_TYPE::BasicInteractive::Encode
    // Description: Convert To Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual void EncodeToStream( KDataStream & stream ) const;

    KBOOL operator == ( const BasicInteractive & Value ) const;
    KBOOL operator != ( const BasicInteractive & Value ) const;
};

} // END namespace DATA_TYPES
} // END namespace KDIS
