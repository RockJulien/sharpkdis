
#include "./InteractiveMode4Reply.h"

//////////////////////////////////////////////////////////////////////////

using namespace KDIS;
using namespace DATA_TYPE;
using namespace ENUMS;

//////////////////////////////////////////////////////////////////////////
// Public:
//////////////////////////////////////////////////////////////////////////

InteractiveMode4Reply::InteractiveMode4Reply() :
    m_ui8InteractiveMode4Reply( 0 ),
    m_ui8Padding( 0 ),
    m_ui32Padding( 0 )
{
    m_ui32Type = InteractiveMode4ReplyRecord;
    m_ui16Length = INTERACTIVE_MODE4_REPLY_TYPE_SIZE;
}

//////////////////////////////////////////////////////////////////////////

InteractiveMode4Reply::InteractiveMode4Reply( KDIS::DATA_TYPE::ENUMS::AlternateMode4ChallengeReply AM4CR )  :
    m_ui8InteractiveMode4Reply( AM4CR ),
    m_ui8Padding( 0 ),
    m_ui32Padding( 0 )
{
    m_ui32Type = InteractiveMode4ReplyRecord;
    m_ui16Length = INTERACTIVE_MODE4_REPLY_TYPE_SIZE;
}

//////////////////////////////////////////////////////////////////////////

InteractiveMode4Reply::InteractiveMode4Reply( KDataStream & stream ) 
{
    Decode( stream );
}

//////////////////////////////////////////////////////////////////////////

InteractiveMode4Reply::~InteractiveMode4Reply()
{
}

//////////////////////////////////////////////////////////////////////////

void InteractiveMode4Reply::SetAlternateMode4ChallengeReply( KDIS::DATA_TYPE::ENUMS::AlternateMode4ChallengeReply AM4CR )
{
    m_ui8InteractiveMode4Reply = AM4CR;
}

//////////////////////////////////////////////////////////////////////////

KDIS::DATA_TYPE::ENUMS::AlternateMode4ChallengeReply InteractiveMode4Reply::GetAlternateMode4ChallengeReply() const
{
    return (KDIS::DATA_TYPE::ENUMS::AlternateMode4ChallengeReply) m_ui8InteractiveMode4Reply;
}

//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////

const KOCTET* InteractiveMode4Reply::GetAsString() const
{
    KStringStream ss;

    ss << StandardVariable::GetAsString()
       << GetEnumAsStringAlternateMode4ChallengeReply( m_ui8InteractiveMode4Reply ) << "\n";

    return ss.str().c_str();
}

//////////////////////////////////////////////////////////////////////////

void InteractiveMode4Reply::Decode( KDataStream & stream ) 
{
    if( stream.GetBufferSize() < INTERACTIVE_MODE4_REPLY_TYPE_SIZE )throw KException( __FUNCTION__, NOT_ENOUGH_DATA_IN_BUFFER );

    StandardVariable::Decode( stream );

    stream >> m_ui8InteractiveMode4Reply;
    stream >> m_ui8Padding;
    stream >> m_ui32Padding;
}

//////////////////////////////////////////////////////////////////////////

void InteractiveMode4Reply::EncodeToStream( KDataStream & stream ) const
{
    StandardVariable::EncodeToStream( stream );

    stream << m_ui8InteractiveMode4Reply;
    stream << m_ui8Padding;
    stream << m_ui32Padding;
}

//////////////////////////////////////////////////////////////////////////

KBOOL InteractiveMode4Reply::operator == ( const InteractiveMode4Reply & Value ) const
{
    if( StandardVariable::operator  !=( Value ) )                          return false;
    if( m_ui8InteractiveMode4Reply  != Value.m_ui8InteractiveMode4Reply )      return false;
    return true;
}

//////////////////////////////////////////////////////////////////////////

KBOOL InteractiveMode4Reply::operator != ( const InteractiveMode4Reply & Value ) const
{
    return !( *this == Value );
}

//////////////////////////////////////////////////////////////////////////
