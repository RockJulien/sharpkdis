/*********************************************************************
Copyright 2013 Karl Jones
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

For Further Information Please Contact me at
Karljj1@yahoo.com
http://p.sf.net/kdis/UserGuide
*********************************************************************/

#include "./PDU_Factory.h"

// The following are DIS version 6 PDUs.
#if DIS_VERSION >= 6
#include "./../PDU/Distributed_Emission_Regeneration/IFF_PDU.h"
#endif

using namespace std;
using namespace KDIS;
using namespace DATA_TYPE;
using namespace UTILS;
using namespace PDU;
using namespace ENUMS;

//////////////////////////////////////////////////////////////////////////
// protected:
//////////////////////////////////////////////////////////////////////////

KBOOL PDU_Factory::applyFiltersBeforeDecodingPDUBody( const Header * H )
{
    // Test all the filters
    vector<PDU_Factory_Filter*>::const_iterator citr = m_vFilters.begin();
    vector<PDU_Factory_Filter*>::const_iterator citrEnd = m_vFilters.end();

    for (; citr != citrEnd; ++citr)
    {
        if (!(*citr)->ApplyFilterBeforeDecodingPDUBody(H))
        {
            return false;
        }
    }

    return true;
}

//////////////////////////////////////////////////////////////////////////

unique_ptr<Header> PDU_Factory::applyFilters( Header * H )
{
    // Test all the filters
    vector<PDU_Factory_Filter*>::const_iterator citr = m_vFilters.begin();
    vector<PDU_Factory_Filter*>::const_iterator citrEnd = m_vFilters.end();

    for( ; citr != citrEnd; ++citr )
    {
        if( !( *citr )->ApplyFilter( H ) )
        {
            // The PDU failed a test so free the memory and return NULL.
            delete H;
            return unique_ptr<Header>();
        }
    }

    return unique_ptr<Header>( H );
}

//////////////////////////////////////////////////////////////////////////
// public:
//////////////////////////////////////////////////////////////////////////

PDU_Factory::PDU_Factory()
{
}

//////////////////////////////////////////////////////////////////////////

PDU_Factory::~PDU_Factory()
{
    // Delete the filters
    vector<PDU_Factory_Filter*>::iterator itr = m_vFilters.begin();
    vector<PDU_Factory_Filter*>::iterator itrEnd = m_vFilters.end();
    for( ; itr != itrEnd; ++itr )
    {
        delete *itr;
    }
}

//////////////////////////////////////////////////////////////////////////

void PDU_Factory::AddFilter( PDU_Factory_Filter * F )
{
    m_vFilters.push_back( F );
}

//////////////////////////////////////////////////////////////////////////

void PDU_Factory::RemoveFilter( PDU_Factory_Filter * F )
{
    // Check for filter and remove it if we find it.
    vector<PDU_Factory_Filter*>::iterator itr = m_vFilters.begin();
    vector<PDU_Factory_Filter*>::iterator itrEnd = m_vFilters.end();
    for( ; itr != itrEnd; ++itr )
    {
        if( (*itr) == F )
        {
            itr = m_vFilters.erase( itr );
        }
    }
}

//////////////////////////////////////////////////////////////////////////

unique_ptr<Header> PDU_Factory::Decode(const std::vector<KUOCTET> & Buffer, KUINT16 BufferSize )throw( KException )
{
    KDataStream kd( Buffer );
    return Decode( kd );
}

//////////////////////////////////////////////////////////////////////////

unique_ptr<Header> PDU_Factory::Decode( KDataStream & Stream )throw( KException )
{
    return Decode( Header( Stream ), Stream );
}

//////////////////////////////////////////////////////////////////////////

unique_ptr<Header> PDU_Factory::Decode( const Header & H, KDataStream & Stream )throw( KException )
{
    if (!applyFiltersBeforeDecodingPDUBody(&H))
    {
        return unique_ptr<Header>();
    }

    switch( H.GetPDUType() )
    {
// The following are DIS version 6 PDUs.
#if DIS_VERSION >= 6

    case IFF_ATC_NAVAIDS_PDU_Type:
            return applyFilters( new IFF_PDU( H, Stream ) );
	#endif
    }

    // We could not decode the PDU
    return unique_ptr<Header>();
}

//////////////////////////////////////////////////////////////////////////
