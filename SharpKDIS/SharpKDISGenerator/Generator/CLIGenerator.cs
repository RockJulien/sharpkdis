﻿using CppSharp;
using CppSharp.AST;
using CppSharp.Parser;
using CppSharp.Passes;
using System;
using System.Collections.Generic;
using System.IO;

namespace SharpKDISGenerator.Generator
{
    /// <summary>
    /// Definition of the <see cref="CLIGenerator"/> class.
    /// </summary>
    public class CLIGenerator : ILibrary
    {
        #region Fields

        /// <summary>
        /// Constant path to source files.
        /// </summary>
        private static readonly string cSourcePath = string.Format(@"{0}\{1}", Environment.CurrentDirectory, @"..\..\CppKDIS");

        /// <summary>
        /// Constant path to generated files.
        /// </summary>
        private static readonly string cGeneratedPath = string.Format(@"{0}\{1}", Environment.CurrentDirectory, @"..\..\SharpKDIS\SharpKDISGenerator\Generated");

        /// <summary>
        /// Stores the set of included directories.
        /// </summary>
        private HashSet<string> mIncludedDirs = new HashSet<string>();

        /// <summary>
        /// Stores the set of included headers to generate.
        /// </summary>
        private HashSet<string> mIncludedHeaders = new HashSet<string>();

        /// <summary>
        /// Stores the header file(s) to exclude.
        /// </summary>
        private HashSet<string> mHeaderFilesToExclude = new HashSet<string>();

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the generated path.
        /// </summary>
        public string GeneratedPath
        {
            get
            {
                return cGeneratedPath;
            }
        }

        /// <summary>
        /// Gets the set of included directories.
        /// </summary>
        public IEnumerable<string> IncludedDirs
        {
            get
            {
                return this.mIncludedDirs;
            }
        }

        /// <summary>
        /// Gets the set of included headers to generate.
        /// </summary>
        public IEnumerable<string> IncludedHeaders
        {
            get
            {
                return this.mIncludedHeaders;
            }
        }

        /// <summary>
        /// Gets the header file(s) to exclude.
        /// </summary>
        public IEnumerable<string> HeaderFilesToExclude
        {
            get
            {
                return this.mHeaderFilesToExclude;
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Cleans up the generated directory.
        /// </summary>
        private void CleanUp()
        {
            DirectoryInfo lGeneratedDir = new DirectoryInfo(cGeneratedPath);
            foreach (FileInfo lSubFile in lGeneratedDir.EnumerateFiles())
            {
                lSubFile.Delete();
            }

            foreach (DirectoryInfo lSubDirectory in lGeneratedDir.EnumerateDirectories())
            {
                lSubDirectory.Delete(true);
            }
        }

        /// <summary>
        /// Excludes the given header file.
        /// </summary>
        /// <param name="pHeaderFileName"></param>
        public void ExcludeHeader(string pHeaderFileName)
        {
            this.mHeaderFilesToExclude.Add(pHeaderFileName);
        }

        /// <summary>
        /// Excludes the given header file.
        /// </summary>
        /// <param name="pHeaderFileNames"></param>
        public void ExcludeHeader(IEnumerable<string> pHeaderFileNames)
        {
            this.mHeaderFilesToExclude.UnionWith(pHeaderFileNames);
        }

        /// <summary>
        /// Setup the driver options.
        /// </summary>
        /// <param name="pDriver"></param>
        public void Setup(Driver pDriver)
        {
            if (Directory.Exists(cGeneratedPath) == false)
            {
                Directory.CreateDirectory(cGeneratedPath);
            }
            else
            {
                this.CleanUp();
            }

            // Parser option(s)
            ParserOptions lParserOptions = pDriver.ParserOptions;
            lParserOptions.MicrosoftMode = true;
            lParserOptions.TargetTriple = "i686-pc-win32-msvc"; //"x86_64-pc-win32-msvc";
            lParserOptions.AddArguments("-fcxx-exceptions");
            lParserOptions.AddDefines("DIS_VERSION=7");
            lParserOptions.AddDefines("EXPORT_KDIS");
            lParserOptions.AddDefines("KDIS_USE_ENUM_DESCRIPTORS");

            // Driver option(s)
            DriverOptions lDriverOptions = pDriver.Options;
            lDriverOptions.GeneratorKind = CppSharp.Generators.GeneratorKind.CLI;
            lDriverOptions.OutputDir = cGeneratedPath;
            lDriverOptions.GenerateClassTemplates = true;
            lDriverOptions.GenerateSingleCSharpFile = false;
            lDriverOptions.Verbose = true;
            //lDriverOptions.CompileCode = true;

            Module lModule = lDriverOptions.AddModule("KDIS7_DLL");

            string lSourceFilesDirectory = string.Format(@"{0}\{1}", cSourcePath, @"include\KDIS");

            lModule.LibraryDirs.Add(string.Format(@"{0}\{1}", cSourcePath, @"bin"));
            lModule.LibraryDirs.Add(string.Format(@"{0}\{1}", cSourcePath, @"lib"));
            lModule.Libraries.Add("KDIS7_DLL.lib");
            if (Directory.Exists(lSourceFilesDirectory))
            {
                string[] lHeaderFiles = Directory.GetFiles(lSourceFilesDirectory, "*.h", SearchOption.AllDirectories);
                for (int lCurr = 0; lCurr < lHeaderFiles.Length; lCurr++)
                {
                    FileInfo lCurrentFile = new FileInfo(lHeaderFiles[lCurr]);

                    this.mIncludedDirs.Add(lCurrentFile.Directory.FullName);

                    if (this.mHeaderFilesToExclude.Contains(lCurrentFile.Name))
                    {
                        continue;
                    }

                    this.mIncludedHeaders.Add(lCurrentFile.Name);
                }
            }

            // Tests.
            //lModule.IncludeDirs.Add( @"D:\KDIS\RC\include\KDIS\DataTypes\" );
            //lModule.Headers.Add( "StandardVariable.h" );

            lModule.IncludeDirs.AddRange(this.mIncludedDirs);
            lModule.Headers.AddRange(this.mIncludedHeaders);
        }

        /// <summary>
        /// Setup the optional pass(es)
        /// </summary>
        /// <param name="pDriver"></param>
        public void SetupPasses(Driver pDriver)
        {
            //pDriver.Context.TranslationUnitPasses.RenameDeclsUpperCase(RenameTargets.Any);
            //pDriver.Context.TranslationUnitPasses.AddPass(new FunctionToInstanceMethodPass());

            //var generateSymbolsPass = pDriver.Context.TranslationUnitPasses.FindPass<GenerateSymbolsPass>();
            //generateSymbolsPass.SymbolsCodeGenerated += (sender, e) =>
            //{
            //    e.OutputDir = pDriver.Context.Options.OutputDir;
            //    //this.CompileMakefile(e);
            //};

            //pDriver.Context.TranslationUnitPasses.AddPass(new SkipGenerationPass());
        }

        class SkipGenerationPass : TranslationUnitPass
        {
            public override bool VisitClassDecl(Class @class)
            {
                if ( @class.Name == "KException" )
                {
                    return false;
                }

                return base.VisitClassDecl(@class);
            }
        }

        /// <summary>
        /// Pre pocess.
        /// </summary>
        /// <param name="pDriver"></param>
        /// <param name="pContext"></param>
        public void Preprocess(Driver pDriver, ASTContext pContext)
        {

        }

        /// <summary>
        /// Post process.
        /// </summary>
        /// <param name="pDriver"></param>
        /// <param name="pContext"></param>
        public void Postprocess(Driver pDriver, ASTContext pContext)
        {

        }

        #endregion Methods
    }
}
