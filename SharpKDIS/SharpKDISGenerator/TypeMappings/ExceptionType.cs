﻿using CppSharp.AST;
using CppSharp.Generators;
using CppSharp.Types;

namespace SharpKDISGenerator.TypeMappings
{
    /// <summary>
    /// Definition of the type mapping for std exception
    /// </summary>
    [TypeMap("std::exception", GeneratorKind = GeneratorKind.CLI)]
    public class ExceptionType : TypeMap
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctx"></param>
        /// <returns></returns>
        public override Type CLISignatureType(TypePrinterContext ctx)
        {
            return new CILType(typeof(System.Exception));
        }
    }
}
