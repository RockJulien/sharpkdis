﻿using CppSharp.Generators;
using CppSharp.Types;

namespace SharpKDISGenerator.TypeMappings
{
    /// <summary>
    /// Definition of the type mapping for std exception
    /// </summary>
    [TypeMap("KDIS::KException", GeneratorKind = GeneratorKind.CLI)]
    public class KExceptionType : TypeMap
    {
        /// <summary>
        /// 
        /// </summary>
        public override bool IsIgnored { get { return true; } }
    }
}
