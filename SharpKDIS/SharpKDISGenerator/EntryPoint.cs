﻿using CppSharp;
using Microsoft.CSharp;
using SharpKDISGenerator.Generator;
using System;
using System.CodeDom.Compiler;
using System.IO;

namespace SharpKDISGenerator
{
    /// <summary>
    /// Definition of the <see cref="EntryPoint"/> class.
    /// </summary>
    class EntryPoint
    {
        /// <summary>
        /// Entry point.
        /// </summary>
        static void Main()
        {
            CLIGenerator lGenerator = new CLIGenerator();
            lGenerator.ExcludeHeader( "RelativeWorldCoordinates.h" ); // Not needed and involving the KFIXED template class that is not supported by CppSharp.
            lGenerator.ExcludeHeader( "Header.h" ); // Not needed and involving the KFIXED template class that is not supported by CppSharp.

            Console.WriteLine("Launching KDIS C# code wrapper generation ...");
            Console.WriteLine();
            Console.WriteLine();

            ConsoleDriver.Run( lGenerator );

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("... Generation finished.");
            Console.WriteLine();
            Console.WriteLine();

            Console.WriteLine("Creating .Net dll using generated code ...");
            Console.WriteLine();
            Console.WriteLine();

            string lSharpKDISOutput = string.Format( @"{0}\..\", Environment.CurrentDirectory );
            string[] lGeneratedCSharpFiles = Directory.GetFiles( lGenerator.GeneratedPath, "*.cs", SearchOption.AllDirectories );

            //Microsoft.VisualC.CppCodeProvider lCppCodeProvider = new Microsoft.VisualC.CppCodeProvider();
            //CompilerParameters lCppParameters = new CompilerParameters();
            //lCppParameters.CompilerOptions = "/platform:x86 /target:library /unsafe /optimize+";
            //lCppParameters.GenerateExecutable = false;
            //lCppParameters.IncludeDebugInformation = true;
            //lCppParameters.OutputAssembly = string.Format( @"{0}{1}", lSharpKDISOutput, "Std-symbols.dll");
            //CompilerResults lCppDllResults = lCppCodeProvider.CompileAssemblyFromFile( lCppParameters, string.Format( @"{0}\{1}", lGenerator.GeneratedPath , "Std-symbols.cpp" ) );

            //if ( lCppDllResults.Errors.Count > 0 )
            //{
            //    foreach ( var lError in lCppDllResults.Errors )
            //    {
            //        Console.WriteLine( lError );
            //    }

            //    Console.WriteLine();
            //    Console.WriteLine();
            //    Console.WriteLine( "... Compilation error(s) occured while compiling c++ Std dll." );
            //}
            //else
            //{
            //    Console.WriteLine();
            //    Console.WriteLine();
            //    Console.WriteLine( "... Std c++ Dll created into {0}.", lCppParameters.OutputAssembly );
            //}

            CSharpCodeProvider lCodeProvider = new CSharpCodeProvider();
            CompilerParameters lParameters = new CompilerParameters();
            lParameters.CompilerOptions = "/platform:x86 /target:library /unsafe /optimize+";
            lParameters.GenerateExecutable = false;
            lParameters.IncludeDebugInformation = true;
            lParameters.ReferencedAssemblies.Add("System.dll");
            lParameters.ReferencedAssemblies.Add("System.Core.dll");
            lParameters.ReferencedAssemblies.Add("System.Data.dll");
            lParameters.ReferencedAssemblies.Add("System.Data.DataSetExtensions.dll");
            lParameters.ReferencedAssemblies.Add("System.Xml.dll");
            lParameters.ReferencedAssemblies.Add("System.Xml.Linq.dll");
            
            string lCppSharpRuntime = string.Format( @"{0}\{1}", Environment.CurrentDirectory, "CppSharp.Runtime.dll" );
            lParameters.ReferencedAssemblies.Add( lCppSharpRuntime );
            lParameters.OutputAssembly = string.Format( @"{0}{1}", lSharpKDISOutput, "SharpKDIS.dll" );

            try
            {
                File.Copy( lCppSharpRuntime, string.Format( "{0}{1}", lSharpKDISOutput, "CppSharp.Runtime.dll" ) );
            }
            catch
            {
                Console.WriteLine( "Unable to copy CppSharp.Runtime.dll." );
            }

            CompilerResults lResults = lCodeProvider.CompileAssemblyFromFile( lParameters, lGeneratedCSharpFiles );

            if ( lResults.Errors.Count > 0 )
            {
                foreach ( var lError in lResults.Errors )
                {
                    Console.WriteLine( lError );
                }

                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine( "... Compilation error(s) occured." );
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine( "... Dll created into {0}.", lSharpKDISOutput );
            }

            Console.WriteLine();
            Console.WriteLine();
        }
    }
}
