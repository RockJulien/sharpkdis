﻿using KDIS7_DLL.KDIS;
using KDIS7_DLL.KDIS.DATA_TYPE;
using KDIS7_DLL.KDIS.DATA_TYPE.ENUMS;
using KDIS7_DLL.KDIS.PDU;
using System;
using System.Collections.Generic;
using System.Text;

namespace KDISTests
{
    class Program
    {
        static void Main(string[] args)
        {
            IFF_PDU lPDU = new IFF_PDU();
            
            // Mode 1
            // Enable
            bool lMode1Enabled = true;

            // Mode 2
            // Enable
            bool lMode2Enabled = true;

            // Mode 3A
            // Enable
            bool lMode3AEnabled = true;

            // Mode 4
            // Enable
            bool lMode4Enabled = true;

            // Mode 5/C
            // Enable
            bool lMode5CEnabled = true;

            // Mode S
            // Enable
            bool lModeSEnabled = true;

            // Emitter settings
            lPDU.EmittingEntityID = new EntityIdentifier(
                    1,
                    42,
                    0);

            lPDU.EventID = new EntityIdentifier(
                1,
                42,
                1);
            // Avant index :                 int lCurrentTransponderIndex = lListTranponder.ToList().FindIndex(x => ((string)x["Name"].Value) == lIFFTransponderPart.Name);
            // 0 si un seul transponder sur l'entité
            // NIKOKO TODO lPDU.SystemDesignator = nikoko todo;
            lPDU.Location = new KDIS7_DLL.KDIS.DATA_TYPE.Vector(
                0.0f,
                0.0f,
                0.0f);
            lPDU.SystemIdentifier = new SystemIdentifier(
                SystemType.MarkX_XII_ATCRBS_ModeS_Transponder,
                SystemName.MarkXII,
                SystemMode.Normal,
                true, lMode4Enabled, true);

            FundamentalOperationalDataMarkXTransponder lFODTransponder = new FundamentalOperationalDataMarkXTransponder();
            lFODTransponder.SetSystemStatus(
                true,
                lMode1Enabled,
                lMode2Enabled,
                lMode3AEnabled,
                lMode4Enabled,
                lMode5CEnabled || lMode5CEnabled,
                lModeSEnabled,
                true);

            lFODTransponder.AlternateParameter4 = AlternateParameter4.Valid;
            lFODTransponder.SetInfomationLayer1(true);
            lFODTransponder.SetInfomationLayer2(true);
            lFODTransponder.SetModifier(false, false, true); // NIKOKO TODO check true STIOn avant !
            lFODTransponder.SetMode1CodeStatus(
                7,
                7,
                true, false, false);
            lFODTransponder.SetMode2CodeStatus(
                1,
                2,
                3,
                4,
                true, false, false);
            lFODTransponder.SetMode3CodeStatus(
                1,
                2,
                3,
                4,
                true, false, false);
            lFODTransponder.SetMode4CodeStatus(
                1254,
                true, false, false);
            lFODTransponder.SetModeCCodeStatus(
                false, 200,
                true, false, false);
            lFODTransponder.SetModeSCodeStatus(
                TCAS.TCAS_I,
                lModeSEnabled, false, false);
            lPDU.FundamentalOperationalData = lFODTransponder;

            // NIKOKO TODO : lPDU.SystemSpecificData = ;

            // Layer 2
            IFF_Layer2 layer2 = new IFF_Layer2();
            BeamData lBeamData = new BeamData();
            lBeamData.AzimuthCenter = 0.0f;
            lBeamData.AzimuthSweep = 180.0f;
            lBeamData.BeamSweepSync = 0.0f;
            lBeamData.ElevationCenter = 0.0f;
            lBeamData.ElevationSweep = 180.0f;
            layer2.BeamData = lBeamData;
            SecondaryOperationalData lSOD = new SecondaryOperationalData();
            lSOD.SetNumberOfFundamentalParamSets(1);
            lSOD.Parameter1 = 0;
            lSOD.Parameter2 = 0;
            layer2.SecondaryOperationalData = lSOD;
            IFF_ATC_NAVAIDS_FundamentalParameterData lFPD = new IFF_ATC_NAVAIDS_FundamentalParameterData();
            lFPD.ApplicableModes = SystemMode.OffSystemMode;
            lFPD.BurstLength = 1;
            lFPD.EffectiveRadiatedPower = 0.0f;
            lFPD.Frequency = 1.0f;
            lFPD.PgRF = 0.0f;
            lFPD.PulseWidth = 0.0f;
            layer2.AddFundamentalParameterData(lFPD);
            lPDU.AddLayer(layer2);

            IFF_Layer3Transponder layer3 = new IFF_Layer3Transponder();
            layer3.ReportingSimulation = new SimulationIdentifier(
                1,
                42);
            Mode5TransponderBasicData basicData = new Mode5TransponderBasicData();
            Mode5TransponderStatus lMode5TransponderStatus = new Mode5TransponderStatus();
            {
                lMode5TransponderStatus.Reply = Mode5Reply.NoResponseReply;
                
                lMode5TransponderStatus.SetLineTestInProgress(false);
                lMode5TransponderStatus.AntennaSelection = AntennaSelection.AntennaSelectionNoStatement;
                lMode5TransponderStatus.SetCryptoControlPresent(true);
                lMode5TransponderStatus.SetLocationRecordPresent(true);
                lMode5TransponderStatus.SetLocationErrorRecordPresent(false);
                lMode5TransponderStatus.PlatformType = PlatformType.GroundPlatformType;
                lMode5TransponderStatus.PlatformType = PlatformType.AirPlatformType;
                lMode5TransponderStatus.SetMode5Level2Included(false);
                lMode5TransponderStatus.Status = true;
                lMode5TransponderStatus.SetDamaged(false);
                lMode5TransponderStatus.SetMalfunctioning(false);
            }
            basicData.Status = lMode5TransponderStatus;
            basicData.PersonalIdentificationNumber = 45;
            //set the mode 5 message format present todo (see the AIMS 03-1000A standard)
            //todo in interactive mode, add an Interactive Basic Mode 5 IFF Data record (layer 3 or layer 5??)
            basicData.MessageFormatsPresent = 0;
            EnhancedMode1Code lEnhancedMode1Code = new EnhancedMode1Code();
            lEnhancedMode1Code.CodeElement1 = 1;
            lEnhancedMode1Code.CodeElement2 = 2;
            lEnhancedMode1Code.CodeElement3 = 3;
            lEnhancedMode1Code.CodeElement4 = 4;
            basicData.EnhancedMode1Code = lEnhancedMode1Code;
            basicData.NationalOrigin = 75;
            Mode5TransponderSupplementalData lMode5TransponderSupplementalData = new Mode5TransponderSupplementalData();
            lMode5TransponderSupplementalData.Squitter = false;
            lMode5TransponderSupplementalData.Level2Squitter = false;
            lMode5TransponderSupplementalData.IFFMission = 6;
            basicData.SupplementalData = lMode5TransponderSupplementalData;
            basicData.NavigationSource = NavigationSource.NoStatementNavigationSource;
            basicData.FigureOfMerit = 30;
            layer3.BasicData = basicData;
            CryptoControl lCryptoControl = new CryptoControl(255, 0, 0);
            layer3.AddDataRecord(lCryptoControl);
            int lLatitude = 45; //Use entity truth position
            int lLongitude = 0; //Use entity truth position
            ushort lMode5Altitude = 10000;
            ushort lBarometricAltitude = 2047; //use entity truth altitude to derive the Barometric altitude
                                               //todo see AIMS 03-1000A for implementations
            Mode5STransponderLocation lMode5STransponderLocation = new Mode5STransponderLocation(lLatitude, lLongitude, lMode5Altitude, lBarometricAltitude);
            layer3.AddDataRecord(lMode5STransponderLocation);
            lPDU.AddLayer(layer3);

            ModeSLevelsPresent lLayer4ModeSLevelsPresent = new ModeSLevelsPresent();
            IFF_Layer4Transponder lLayer4 = new IFF_Layer4Transponder();
            lLayer4.ReportingSimulation = new SimulationIdentifier(
                1,
                42);
            // Basic data
            ModeSTransponderBasicData lModeSTransponderBasicData = new ModeSTransponderBasicData();
            ModeSTransponderStatus lModeSTransponderStatus = new ModeSTransponderStatus();
            DAPSource lDAPSource = new DAPSource();
            ModeSAltitude lModeSAltitude = new ModeSAltitude();
            {
                // Status                    
                {
                    lModeSTransponderStatus.SquitterStatus = false; //Squitter is off
                    lModeSTransponderStatus.AirbonePositionReportIndicator = false;
                    lModeSTransponderStatus.AirborneVelocityReportIndicator = false;
                    lModeSTransponderStatus.SquitterType = SquitterType.NotCapable;
                    lModeSTransponderStatus.RecordSource = false;
                    lModeSTransponderStatus.SurfacePositionReportIndicator = false;
                    lModeSTransponderStatus.IdentificationReportIndicator = false;
                    lModeSTransponderStatus.EventDrivenReportIndicator = false;
                    lModeSTransponderStatus.Status = true; //always on
                    lModeSTransponderStatus.SetDamaged(false);
                    lModeSTransponderStatus.SetMalfunctioning(false);
                }
                lModeSTransponderBasicData.Status = lModeSTransponderStatus;
                // Levels present
                {
                    lLayer4ModeSLevelsPresent.Level1 = false;
                    lLayer4ModeSLevelsPresent.Level2ElementarySurveillanceSublevel = true;
                    lLayer4ModeSLevelsPresent.Level2EnhancedSurveillanceSublevel = false;
                    lLayer4ModeSLevelsPresent.Level3 = true;
                    lLayer4ModeSLevelsPresent.Level4 = true;
                }
                
                lModeSTransponderBasicData.ModeSLevelsPresent = lLayer4ModeSLevelsPresent;
                lModeSTransponderBasicData.AircraftPresentDomain = AircraftPresentDomain.NoStatementAircraftPresentDomain;
                lModeSTransponderBasicData.AircraftPresentDomain = AircraftPresentDomain.Airbone;

                string lId = "Airtoto";
                if (lId.Length <= 8)
                {
                    lModeSTransponderBasicData.SetAircraftIdentification(lId, (byte)lId.Length);
                }
                else
                {
                    lModeSTransponderBasicData.SetAircraftIdentification(lId.Substring(0, 8), 8);
                }
                lModeSTransponderBasicData.AircraftAddress = 45;
                lModeSTransponderBasicData.AircraftIDType = AircraftIDType.TailNumber; //it's a tail number 
                                                                                       // DAP source
                {
                    lDAPSource.IAS = false;
                    lDAPSource.MachNumber = false;
                    lDAPSource.GroundSpeed = false;
                    lDAPSource.MagneticHeading = false;
                    lDAPSource.TrackAngleRate = false;
                    lDAPSource.TrueTrackAngle = false;
                    lDAPSource.TrueAirSpeed = false;
                    lDAPSource.VerticalRate = false;
                    
                }
                lModeSTransponderBasicData.DAPSource = lDAPSource;
                // Mode S altitude
                {
                    lModeSAltitude.AltitudeResolution = AltitudeResolution.FOOT_100;
                    lModeSAltitude.Altitude = 0;
                    ushort lAltitude = 10000;
                    byte lResolution = 1;
                    lModeSAltitude.Altitude = lAltitude;
                    lModeSAltitude.AltitudeResolution = (AltitudeResolution)lResolution;
                }
                lModeSTransponderBasicData.ModeSAltitude = lModeSAltitude;
                lModeSTransponderBasicData.CapabilityReport = CapabilityReport.NoCommunicationsCapability;
                lModeSTransponderBasicData.CapabilityReport = CapabilityReport.NoStatementCapabilityReport; //No statment??
            }
            lLayer4.BasicData = lModeSTransponderBasicData;
            // Additional records
            SquitterAirbornePositionReport lSquitterAirbornePositionReport = new SquitterAirbornePositionReport();
            lSquitterAirbornePositionReport.SquitterReport = 0;
            //todo see the International Civil Aviation Organization (ICAO) for implementation
            lLayer4.AddDataRecord(lSquitterAirbornePositionReport);

            SquitterAirborneVelocityReport lSquitterAirborneVelocityReport = new SquitterAirborneVelocityReport();
            lSquitterAirborneVelocityReport.SquitterReport = 0;
            //todo see the International Civil Aviation Organization (ICAO) for implementation
            lLayer4.AddDataRecord(lSquitterAirborneVelocityReport);
            lPDU.AddLayer(lLayer4);

            IFF_Layer5 lLayer5 = new IFF_Layer5();
            lLayer5.ReportingSimulation = new SimulationIdentifier(
                1,
                42);
            InformationLayers lApplicableLayers = new InformationLayers(); //it's only applicable to the layer 5? 
            lApplicableLayers.Layer1 = false;
            lApplicableLayers.Layer2 = false;
            lApplicableLayers.Layer3 = false;
            lApplicableLayers.Layer4 = false;
            lApplicableLayers.Layer5 = true;
            lApplicableLayers.Layer6 = false;
            lApplicableLayers.Layer7 = false;
            lLayer5.ApplicableLayers = lApplicableLayers;
            //it's a Transponder/Interrogator Data Link Messages?
            lLayer5.DataCategory = DataCategory.TransponderInterrogatorDataLinkMessages;
            // Data records
            // Fill the Basic interactive IFF Data Record
            lPDU.AddLayer(lLayer5);

            KDataStream lStream = lPDU.EncodedStream;

            string lStream1 = Encoding.UTF8.GetString(lStream.Buffer.ToArray() );
            Console.WriteLine(lStream1);

            KDataStream lDataStream2 = new KDataStream( Endian.BigEndian );
            lPDU.EncodeToStream(lDataStream2);

            string lStream2 = Encoding.ASCII.GetString(lDataStream2.Buffer.ToArray());
            Console.WriteLine(lStream2);

            //List<byte> lMessageData = new List<byte>(Encoding.ASCII.GetBytes(lStream.BufferPtr));

            //Console.WriteLine( lStream.AsString );
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine( lPDU.AsString );
            Console.WriteLine();
            Console.WriteLine();
            Console.ReadKey();
        }
    }
}
