#pragma once

#include "..\Generated\CppSharp.h"

namespace KDIS7_DLL
{
	namespace KDIS
	{
		namespace DATA_TYPE
		{
			ref class LayerHeader;
		}

		namespace PDU
		{
			ref class IFF_PDU;

			[System::Runtime::CompilerServices::ExtensionAttribute]
			public ref class IFF_PDU_Extensions abstract sealed
			{

			public:

				generic <typename TLayerType> where TLayerType : ref class, gcnew(), DATA_TYPE::LayerHeader
				[System::Runtime::CompilerServices::ExtensionAttribute]
				static TLayerType GetCastedLayer(IFF_PDU ^ext, int pIndex);
			};
		}
	}
}

