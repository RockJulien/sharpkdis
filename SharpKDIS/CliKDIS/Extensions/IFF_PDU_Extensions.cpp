
#include "IFF_PDU_Extensions.h"
#include "..\Generated\IFF_PDU.h"
#include "..\Generated\LayerHeader.h"
#include "..\Generated\IFF_Layer2.h"
#include "..\Generated\IFF_Layer3Transponder.h"
#include "..\Generated\IFF_Layer3Interrogator.h"
#include "..\Generated\IFF_Layer4Transponder.h"
#include "..\Generated\IFF_Layer4Interrogator.h"
#include "..\Generated\IFF_Layer5.h"
#include "..\Generated\SystemIdentifier.h"
#include "..\Generated\EnumSystemIdentifier.h"

using namespace System;
using namespace System::Runtime::InteropServices;

generic <typename TLayerType> where TLayerType : ref class, gcnew(), KDIS7_DLL::KDIS::DATA_TYPE::LayerHeader
TLayerType KDIS7_DLL::KDIS::PDU::IFF_PDU_Extensions::GetCastedLayer(KDIS7_DLL::KDIS::PDU::IFF_PDU ^pPdu, int pIndex)
{
	auto __ret = ((::KDIS::PDU::IFF_PDU*)pPdu->NativePtr)->GetLayer(pIndex);
	if (__ret == nullptr)
	{
		return TLayerType();
	}
	
	KDIS7_DLL::KDIS::DATA_TYPE::LayerHeader^ lToReturn = nullptr;
	switch (__ret->GetLayerNumber())
	{
	case 2: lToReturn = gcnew KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer2((::KDIS::DATA_TYPE::IFF_Layer2*)__ret);
		break;
	case 3:
		switch (pPdu->SystemIdentifier->SystemType)
		{
		case KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::SystemType::MarkX_XII_ATCRBS_ModeS_Transponder:
		case KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::SystemType::RRB_Transponder:
		case KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::SystemType::SovietTransponder:
			lToReturn = gcnew KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer3Transponder((::KDIS::DATA_TYPE::IFF_Layer3Transponder*)__ret);
			break;
		case KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::SystemType::MarkX_XII_ATCRBS_ModeS_Interrogator:
		case KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::SystemType::SovietInterrogator:
			lToReturn = gcnew KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer3Interrogator((::KDIS::DATA_TYPE::IFF_Layer3Interrogator*)__ret);
			break;
		}
		break;

	case 4:
		switch (pPdu->SystemIdentifier->SystemType)
		{
		case KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::SystemType::MarkX_XII_ATCRBS_ModeS_Transponder:
		case KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::SystemType::RRB_Transponder:
		case KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::SystemType::SovietTransponder:
			lToReturn = gcnew KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4Transponder((::KDIS::DATA_TYPE::IFF_Layer4Transponder*)__ret);
			break;
		case KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::SystemType::MarkX_XII_ATCRBS_ModeS_Interrogator:
		case KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::SystemType::SovietInterrogator:
			lToReturn = gcnew KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4Interrogator((::KDIS::DATA_TYPE::IFF_Layer4Interrogator*)__ret);
			break;
		}
		break;

	case 5: lToReturn = gcnew KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer5((::KDIS::DATA_TYPE::IFF_Layer5*)__ret);
		break;
	}

	return static_cast<TLayerType>( lToReturn );
}