
#include <vector>
#include "IFF_Layer5_Extensions.h"
#include "..\Generated\StandardVariable.h"
#include "..\Generated\IFF_Layer5.h"
#include "..\Generated\BasicInteractive.h"
#include "..\Generated\CryptoControl.h"
#include "..\Generated\Mode5STransponderLocation.h"
#include "..\Generated\SquitterAirbornePositionReport.h"
#include "..\Generated\SquitterAirborneVelocityReport.h"
#include "..\Generated\InteractiveMode4Reply.h"
#include "..\Generated\InteractiveMode5Reply.h"
#include "..\Generated\InteractiveBasicModeS.h"

using namespace System;
using namespace System::Runtime::InteropServices;

generic <typename TVariableType> where TVariableType : ref class, gcnew(), KDIS7_DLL::KDIS::DATA_TYPE::StandardVariable
TVariableType KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer5_Extensions::GetCastedDataRecords(KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer5 ^pLayer, int pIndex)
{
	auto __ret = ((::KDIS::DATA_TYPE::IFF_Layer5*)pLayer->NativePtr)->GetDataRecords();
	if (__ret.size() == 0 )
	{
		return TVariableType();
	}

	::KDIS::DATA_TYPE::StdVarPtr lVariable = __ret[ pIndex ];
	if ( lVariable == nullptr )
	{
		return TVariableType();
	}

	KDIS7_DLL::KDIS::DATA_TYPE::StandardVariable^ lToReturn = nullptr;
	switch ( lVariable->GetStandardVariableType() )
	{
	case ::KDIS::DATA_TYPE::ENUMS::StandardVariableType::CryptoControlRecord:
		lToReturn = gcnew KDIS7_DLL::KDIS::DATA_TYPE::CryptoControl( (::KDIS::DATA_TYPE::CryptoControl*)lVariable );
		break;
	case ::KDIS::DATA_TYPE::ENUMS::StandardVariableType::Mode5STransponderLocationRecord:
		lToReturn = gcnew KDIS7_DLL::KDIS::DATA_TYPE::Mode5STransponderLocation( (::KDIS::DATA_TYPE::Mode5STransponderLocation*)lVariable );
		break;
	case ::KDIS::DATA_TYPE::ENUMS::StandardVariableType::SquitterAirbornePositionReportRecord:
		lToReturn = gcnew KDIS7_DLL::KDIS::DATA_TYPE::SquitterAirbornePositionReport( (::KDIS::DATA_TYPE::SquitterAirbornePositionReport*)lVariable );
		break;
	case ::KDIS::DATA_TYPE::ENUMS::StandardVariableType::SquitterAirborneVelocityReportRecord:
		lToReturn = gcnew KDIS7_DLL::KDIS::DATA_TYPE::SquitterAirborneVelocityReport( (::KDIS::DATA_TYPE::SquitterAirborneVelocityReport*)lVariable );
		break;
	case ::KDIS::DATA_TYPE::ENUMS::StandardVariableType::InteractiveMode4ReplyRecord:
		lToReturn = gcnew KDIS7_DLL::KDIS::DATA_TYPE::InteractiveMode4Reply( (::KDIS::DATA_TYPE::InteractiveMode4Reply*)lVariable );
		break;
	case ::KDIS::DATA_TYPE::ENUMS::StandardVariableType::InteractiveMode5ReplyRecord:
		lToReturn = gcnew KDIS7_DLL::KDIS::DATA_TYPE::InteractiveMode5Reply( (::KDIS::DATA_TYPE::InteractiveMode5Reply*)lVariable );
		break;
	case ::KDIS::DATA_TYPE::ENUMS::StandardVariableType::InteractiveBasicModeSRecord:
		lToReturn = gcnew KDIS7_DLL::KDIS::DATA_TYPE::InteractiveBasicModeS( (::KDIS::DATA_TYPE::InteractiveBasicModeS*)lVariable );
		break;
	case ::KDIS::DATA_TYPE::ENUMS::StandardVariableType::BasicInteractiveRecord:
	default:
		lToReturn = gcnew KDIS7_DLL::KDIS::DATA_TYPE::BasicInteractive( (::KDIS::DATA_TYPE::BasicInteractive*)lVariable );
		break;
	}

	return static_cast<TVariableType>( lToReturn );
}

generic <typename TVariableType> where TVariableType : ref class, gcnew(), KDIS7_DLL::KDIS::DATA_TYPE::StandardVariable
System::Collections::Generic::List<TVariableType>^ KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer5_Extensions::GetCastedDataRecords(KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer5 ^pLayer)
{
	auto __ret = ((::KDIS::DATA_TYPE::IFF_Layer5*)pLayer->NativePtr)->GetDataRecords();
	if ( __ret.size() == 0 )
	{
		return gcnew System::Collections::Generic::List<TVariableType>();
	}

	System::Collections::Generic::List<TVariableType>^ lToReturn = gcnew System::Collections::Generic::List<TVariableType>();
	std::vector<::KDIS::DATA_TYPE::StdVarPtr>::const_iterator citr = __ret.begin();
	std::vector<::KDIS::DATA_TYPE::StdVarPtr>::const_iterator citrEnd = __ret.end();
	for ( ; citr != citrEnd; ++citr )
	{
		switch ( (*citrEnd)->GetStandardVariableType() )
		{
		case ::KDIS::DATA_TYPE::ENUMS::StandardVariableType::CryptoControlRecord:
			lToReturn->Add( static_cast<TVariableType>( gcnew KDIS7_DLL::KDIS::DATA_TYPE::CryptoControl( (::KDIS::DATA_TYPE::CryptoControl*)(*citrEnd) ) ) );
			break;
		case ::KDIS::DATA_TYPE::ENUMS::StandardVariableType::Mode5STransponderLocationRecord:
			lToReturn->Add( static_cast<TVariableType>( gcnew KDIS7_DLL::KDIS::DATA_TYPE::Mode5STransponderLocation( (::KDIS::DATA_TYPE::Mode5STransponderLocation*)(*citrEnd) ) ) );
			break;
		case ::KDIS::DATA_TYPE::ENUMS::StandardVariableType::SquitterAirbornePositionReportRecord:
			lToReturn->Add( static_cast<TVariableType>( gcnew KDIS7_DLL::KDIS::DATA_TYPE::SquitterAirbornePositionReport( (::KDIS::DATA_TYPE::SquitterAirbornePositionReport*)(*citrEnd) ) ) );
			break;
		case ::KDIS::DATA_TYPE::ENUMS::StandardVariableType::SquitterAirborneVelocityReportRecord:
			lToReturn->Add( static_cast<TVariableType>( gcnew KDIS7_DLL::KDIS::DATA_TYPE::SquitterAirborneVelocityReport( (::KDIS::DATA_TYPE::SquitterAirborneVelocityReport*)(*citrEnd) ) ) );
			break;
		case ::KDIS::DATA_TYPE::ENUMS::StandardVariableType::InteractiveMode4ReplyRecord:
			lToReturn->Add( static_cast<TVariableType>( gcnew KDIS7_DLL::KDIS::DATA_TYPE::InteractiveMode4Reply( (::KDIS::DATA_TYPE::InteractiveMode4Reply*)(*citrEnd) ) ) );
			break;
		case ::KDIS::DATA_TYPE::ENUMS::StandardVariableType::InteractiveMode5ReplyRecord:
			lToReturn->Add( static_cast<TVariableType>( gcnew KDIS7_DLL::KDIS::DATA_TYPE::InteractiveMode5Reply( (::KDIS::DATA_TYPE::InteractiveMode5Reply*)(*citrEnd) ) ) );
			break;
		case ::KDIS::DATA_TYPE::ENUMS::StandardVariableType::InteractiveBasicModeSRecord:
			lToReturn->Add( static_cast<TVariableType>( gcnew KDIS7_DLL::KDIS::DATA_TYPE::InteractiveBasicModeS( (::KDIS::DATA_TYPE::InteractiveBasicModeS*)(*citrEnd) ) ) );
			break;
		case ::KDIS::DATA_TYPE::ENUMS::StandardVariableType::BasicInteractiveRecord:
		default:
			lToReturn->Add( static_cast<TVariableType>( gcnew KDIS7_DLL::KDIS::DATA_TYPE::BasicInteractive( (::KDIS::DATA_TYPE::BasicInteractive*)(*citrEnd) ) ) );
			break;
		}
	}

	return lToReturn;
}
