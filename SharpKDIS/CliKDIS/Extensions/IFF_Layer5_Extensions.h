#pragma once

#include "..\Generated\CppSharp.h"

namespace KDIS7_DLL
{
	namespace KDIS
	{
		namespace DATA_TYPE
		{
			ref class StandardVariable;
			ref class IFF_Layer5;

			[System::Runtime::CompilerServices::ExtensionAttribute]
			public ref class IFF_Layer5_Extensions abstract sealed
			{

			public:

				generic <typename TVariableType> where TVariableType : ref class, gcnew(), DATA_TYPE::StandardVariable
				[System::Runtime::CompilerServices::ExtensionAttribute]
				static TVariableType GetCastedDataRecords(IFF_Layer5 ^ext, int pIndex);

				generic <typename TVariableType> where TVariableType : ref class, gcnew(), DATA_TYPE::StandardVariable
				[System::Runtime::CompilerServices::ExtensionAttribute]
				static System::Collections::Generic::List<TVariableType>^ GetCastedDataRecords(IFF_Layer5 ^ext);
			};
		}
	}
}
