#pragma once

#include "..\Generated\CppSharp.h"

namespace KDIS7_DLL
{
	namespace KDIS
	{
		namespace DATA_TYPE 
		{
			ref class FixedDatum;
			ref class VariableDatum;
		}

		namespace PDU
		{
			ref class CommentPDU;

			[System::Runtime::CompilerServices::ExtensionAttribute]
			public ref class CommentPDU_Extensions abstract sealed
			{

			public:

				generic <typename TVariableType> where TVariableType : ref class, gcnew(), DATA_TYPE::FixedDatum
				[System::Runtime::CompilerServices::ExtensionAttribute]
				static TVariableType GetCastedFixedDatum(CommentPDU ^ext, int pIndex);

				generic <typename TVariableType> where TVariableType : ref class, gcnew(), DATA_TYPE::FixedDatum
				[System::Runtime::CompilerServices::ExtensionAttribute]
				static System::Collections::Generic::List<TVariableType>^ GetCastedFixedDatum(CommentPDU ^ext);

				generic <typename TVariableType> where TVariableType : ref class, gcnew(), DATA_TYPE::VariableDatum
				[System::Runtime::CompilerServices::ExtensionAttribute]
				static TVariableType GetCastedVariableDatum(CommentPDU ^ext, int pIndex);

				generic <typename TVariableType> where TVariableType : ref class, gcnew(), DATA_TYPE::VariableDatum
				[System::Runtime::CompilerServices::ExtensionAttribute]
				static System::Collections::Generic::List<TVariableType>^ GetCastedVariableDatum(CommentPDU ^ext);
			};
		}
	}
}
