// ----------------------------------------------------------------------------
// <auto-generated>
// This is autogenerated code by CppSharp.
// Do not edit this file or all your changes will be lost after re-generation.
// </auto-generated>
// ----------------------------------------------------------------------------
#include "Mode5InterrogatorBasicData.h"
#include "EntityIdentifier.h"
#include "KDataStream.h"
#include "Mode5InterrogatorStatus.h"

using namespace System;
using namespace System::Runtime::InteropServices;

KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorBasicData::Mode5InterrogatorBasicData(::KDIS::DATA_TYPE::Mode5InterrogatorBasicData* native)
    : KDIS7_DLL::KDIS::DATA_TYPE::DataTypeBase((::KDIS::DATA_TYPE::DataTypeBase*)native)
{
}

KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorBasicData^ KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorBasicData::__CreateInstance(::System::IntPtr native)
{
    return gcnew ::KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorBasicData((::KDIS::DATA_TYPE::Mode5InterrogatorBasicData*) native.ToPointer());
}

KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorBasicData::~Mode5InterrogatorBasicData()
{
    if (NativePtr)
    {
        auto __nativePtr = NativePtr;
        NativePtr = 0;
        delete (::KDIS::DATA_TYPE::Mode5InterrogatorBasicData*) __nativePtr;
    }
}

KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorBasicData::Mode5InterrogatorBasicData()
    : KDIS7_DLL::KDIS::DATA_TYPE::DataTypeBase((::KDIS::DATA_TYPE::DataTypeBase*)nullptr)
{
    __ownsNativeInstance = true;
    NativePtr = new ::KDIS::DATA_TYPE::Mode5InterrogatorBasicData();
}

KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorBasicData::Mode5InterrogatorBasicData(KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorStatus^ Status, unsigned int FormatsPresent, KDIS7_DLL::KDIS::DATA_TYPE::EntityIdentifier^ ID)
    : KDIS7_DLL::KDIS::DATA_TYPE::DataTypeBase((::KDIS::DATA_TYPE::DataTypeBase*)nullptr)
{
    __ownsNativeInstance = true;
    if (ReferenceEquals(Status, nullptr))
        throw gcnew ::System::ArgumentNullException("Status", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::DATA_TYPE::Mode5InterrogatorStatus*)Status->NativePtr;
    auto __arg1 = (::KDIS::KUINT32)FormatsPresent;
    if (ReferenceEquals(ID, nullptr))
        throw gcnew ::System::ArgumentNullException("ID", "Cannot be null because it is a C++ reference (&).");
    auto &__arg2 = *(::KDIS::DATA_TYPE::EntityIdentifier*)ID->NativePtr;
    NativePtr = new ::KDIS::DATA_TYPE::Mode5InterrogatorBasicData(__arg0, __arg1, __arg2);
}

KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorBasicData::Mode5InterrogatorBasicData(KDIS7_DLL::KDIS::KDataStream^ stream)
    : KDIS7_DLL::KDIS::DATA_TYPE::DataTypeBase((::KDIS::DATA_TYPE::DataTypeBase*)nullptr)
{
    __ownsNativeInstance = true;
    if (ReferenceEquals(stream, nullptr))
        throw gcnew ::System::ArgumentNullException("stream", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::KDataStream*)stream->NativePtr;
    NativePtr = new ::KDIS::DATA_TYPE::Mode5InterrogatorBasicData(__arg0);
}

void KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorBasicData::Decode(KDIS7_DLL::KDIS::KDataStream^ stream)
{
    if (ReferenceEquals(stream, nullptr))
        throw gcnew ::System::ArgumentNullException("stream", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::KDataStream*)stream->NativePtr;
    ((::KDIS::DATA_TYPE::Mode5InterrogatorBasicData*)NativePtr)->Decode(__arg0);
}

void KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorBasicData::EncodeToStream(KDIS7_DLL::KDIS::KDataStream^ stream)
{
    if (ReferenceEquals(stream, nullptr))
        throw gcnew ::System::ArgumentNullException("stream", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::KDataStream*)stream->NativePtr;
    ((::KDIS::DATA_TYPE::Mode5InterrogatorBasicData*)NativePtr)->EncodeToStream(__arg0);
}

bool KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorBasicData::operator==(KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorBasicData^ __op, KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorBasicData^ Value)
{
    bool __opNull = ReferenceEquals(__op, nullptr);
    bool ValueNull = ReferenceEquals(Value, nullptr);
    if (__opNull || ValueNull)
        return __opNull && ValueNull;
    auto &__arg0 = *(::KDIS::DATA_TYPE::Mode5InterrogatorBasicData*)__op->NativePtr;
    auto &__arg1 = *(::KDIS::DATA_TYPE::Mode5InterrogatorBasicData*)Value->NativePtr;
    auto __ret = __arg0 == __arg1;
    return __ret;
}

bool KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorBasicData::Equals(::System::Object^ obj)
{
    return this == safe_cast<KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorBasicData^>(obj);
}

bool KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorBasicData::operator!=(KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorBasicData^ __op, KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorBasicData^ Value)
{
    bool __opNull = ReferenceEquals(__op, nullptr);
    bool ValueNull = ReferenceEquals(Value, nullptr);
    if (__opNull || ValueNull)
        return !(__opNull && ValueNull);
    auto &__arg0 = *(::KDIS::DATA_TYPE::Mode5InterrogatorBasicData*)__op->NativePtr;
    auto &__arg1 = *(::KDIS::DATA_TYPE::Mode5InterrogatorBasicData*)Value->NativePtr;
    auto __ret = __arg0 != __arg1;
    return __ret;
}

KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorBasicData::Mode5InterrogatorBasicData(KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorBasicData^ _0)
    : KDIS7_DLL::KDIS::DATA_TYPE::DataTypeBase((::KDIS::DATA_TYPE::DataTypeBase*)nullptr)
{
    __ownsNativeInstance = true;
    if (ReferenceEquals(_0, nullptr))
        throw gcnew ::System::ArgumentNullException("_0", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::DATA_TYPE::Mode5InterrogatorBasicData*)_0->NativePtr;
    NativePtr = new ::KDIS::DATA_TYPE::Mode5InterrogatorBasicData(__arg0);
}

KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorBasicData::operator KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorBasicData^(KDIS7_DLL::KDIS::KDataStream^ stream)
{
    if (ReferenceEquals(stream, nullptr))
        throw gcnew ::System::ArgumentNullException("stream", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::KDataStream*)stream->NativePtr;
    auto __ret = (::KDIS::DATA_TYPE::Mode5InterrogatorBasicData) __arg0;
    auto ____ret = new ::KDIS::DATA_TYPE::Mode5InterrogatorBasicData(__ret);
    return (____ret == nullptr) ? nullptr : gcnew KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorBasicData((::KDIS::DATA_TYPE::Mode5InterrogatorBasicData*)____ret);
}

KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorStatus^ KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorBasicData::Status::get()
{
    auto &__ret = ((::KDIS::DATA_TYPE::Mode5InterrogatorBasicData*)NativePtr)->GetStatus();
    return (KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorStatus^)((&__ret == nullptr) ? nullptr : gcnew KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorStatus((::KDIS::DATA_TYPE::Mode5InterrogatorStatus*)&__ret));
}

void KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorBasicData::Status::set(KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorStatus^ S)
{
    if (ReferenceEquals(S, nullptr))
        throw gcnew ::System::ArgumentNullException("S", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::DATA_TYPE::Mode5InterrogatorStatus*)S->NativePtr;
    ((::KDIS::DATA_TYPE::Mode5InterrogatorBasicData*)NativePtr)->SetStatus(__arg0);
}

unsigned int KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorBasicData::MessageFormatsPresent::get()
{
    auto __ret = ((::KDIS::DATA_TYPE::Mode5InterrogatorBasicData*)NativePtr)->GetMessageFormatsPresent();
    return __ret;
}

void KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorBasicData::MessageFormatsPresent::set(unsigned int MFP)
{
    auto __arg0 = (::KDIS::KUINT32)MFP;
    ((::KDIS::DATA_TYPE::Mode5InterrogatorBasicData*)NativePtr)->SetMessageFormatsPresent(__arg0);
}

KDIS7_DLL::KDIS::DATA_TYPE::EntityIdentifier^ KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorBasicData::InterrogatedEntityID::get()
{
    auto &__ret = ((::KDIS::DATA_TYPE::Mode5InterrogatorBasicData*)NativePtr)->GetInterrogatedEntityID();
    return (KDIS7_DLL::KDIS::DATA_TYPE::EntityIdentifier^)((&__ret == nullptr) ? nullptr : gcnew KDIS7_DLL::KDIS::DATA_TYPE::EntityIdentifier((::KDIS::DATA_TYPE::EntityIdentifier*)&__ret));
}

void KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorBasicData::InterrogatedEntityID::set(KDIS7_DLL::KDIS::DATA_TYPE::EntityIdentifier^ ID)
{
    if (ReferenceEquals(ID, nullptr))
        throw gcnew ::System::ArgumentNullException("ID", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::DATA_TYPE::EntityIdentifier*)ID->NativePtr;
    ((::KDIS::DATA_TYPE::Mode5InterrogatorBasicData*)NativePtr)->SetInterrogatedEntityID(__arg0);
}

System::String^ KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorBasicData::AsString::get()
{
    auto __ret = ((::KDIS::DATA_TYPE::Mode5InterrogatorBasicData*)NativePtr)->GetAsString();
    if (__ret == nullptr) return nullptr;
    return (__ret == 0 ? nullptr : clix::marshalString<clix::E_UTF8>(__ret));
}

KDIS7_DLL::KDIS::KDataStream^ KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorBasicData::EncodedStream::get()
{
    auto __ret = ((::KDIS::DATA_TYPE::Mode5InterrogatorBasicData*)NativePtr)->GetEncodedStream();
    if (__ret == nullptr) return nullptr;
    return (__ret == nullptr) ? nullptr : gcnew KDIS7_DLL::KDIS::KDataStream((::KDIS::KDataStream*)__ret);
}

unsigned short KDIS7_DLL::KDIS::DATA_TYPE::Mode5InterrogatorBasicData::MODE_5INTERROGATOR_BASIC_DATA_SIZE::get()
{
    return ::KDIS::DATA_TYPE::Mode5InterrogatorBasicData::MODE_5_INTERROGATOR_BASIC_DATA_SIZE;
}

