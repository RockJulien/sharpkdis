#pragma once

#include <exception>

namespace Std
{
	public ref class Exception : System::Exception
	{
	protected:

		std::exception* NativePtr;

		bool __ownsNativeInstance;

	public:

		Exception(std::exception* pNative);
	};
}