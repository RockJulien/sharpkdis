// ----------------------------------------------------------------------------
// <auto-generated>
// This is autogenerated code by CppSharp.
// Do not edit this file or all your changes will be lost after re-generation.
// </auto-generated>
// ----------------------------------------------------------------------------
#pragma once

#include "CppSharp.h"
#include <C:/ND3/DXT/Dev/SharpKDIS/CppKDIS/include/KDIS/DataTypes/Enums/EnumSimulationManagement.h>

namespace KDIS7_DLL
{
    namespace KDIS
    {
        namespace DATA_TYPE
        {
            namespace ENUMS
            {
                enum struct AcknowledgeFlag;
                enum struct AcknowledgeResponseFlag;
                enum struct ActionID;
                enum struct EventType;
                enum struct FrozenBehavior;
                enum struct RequestStatus;
                enum struct RequiredReliabilityService;
                enum struct StopFreezeReason;
                ref class EnumDescriptor;
            }
        }
    }
}

namespace KDIS7_DLL
{
    namespace KDIS
    {
        namespace DATA_TYPE
        {
            namespace ENUMS
            {
                /// <summary>*********************************************************************</summary>
                public enum struct RequiredReliabilityService
                {
                    Acknowledged = 0,
                    Unacknowledged = 1
                };

                /// <summary>*********************************************************************</summary>
                public enum struct StopFreezeReason
                {
                    OtherStopFreezeReason = 0,
                    Recess = 1,
                    Termination = 2,
                    SystemFailure = 3,
                    SecurityViolation = 4,
                    EntityReconstitution = 5,
                    StopForReset = 6,
                    StopForRestart = 7,
                    AbortTrainingReturnToTacticalOperations = 8
                };

                /// <summary>*********************************************************************</summary>
                public enum struct FrozenBehavior
                {
                    SimClock = 0,
                    TransmitPDU = 1,
                    ReceivePDU = 2
                };

                /// <summary>*********************************************************************</summary>
                public enum struct AcknowledgeFlag
                {
                    CreateEntityPDU = 1,
                    RemoveEntityPDU = 2,
                    StartResumePDU = 3,
                    StopFreezePDU = 4,
                    TransferControlRequest = 5
                };

                /// <summary>*********************************************************************</summary>
                public enum struct AcknowledgeResponseFlag
                {
                    OtherAcknowledgeResponseFlag = 0,
                    AbleToComply = 1,
                    UnableToComply = 2
                };

                /// <summary>*********************************************************************</summary>
                public enum struct ActionID
                {
                    OtherActionID = 0,
                    LocalStorageOfTheRequestedInformation = 1,
                    InformSMofEventRanOutOfAmmunition = 2,
                    InformSMofEventKilledInaAction = 3,
                    InformSMofEventDamage = 4,
                    InformSMofEventMobilityDisabled = 5,
                    InformSMofEventFireDisabled = 6,
                    InformSMofEventRanOutOfFuel = 7,
                    RecallCheckPointData = 8,
                    RecallInitialParameters = 9,
                    InitiateTetherLead = 10,
                    InitiateTetherFollow = 11,
                    Unthether = 12,
                    InitiateServiceStationResupply = 13,
                    InitiatetailGateResupply = 14,
                    InitiaTehitchLead = 15,
                    InitiaTehitchFollow = 16,
                    Unhitch = 17,
                    Mount = 18,
                    Dismount = 19,
                    StartDRC_DailyReadinessCheck = 20,
                    StopDRC = 21,
                    DataQuery = 22,
                    StatusRequest = 23,
                    SendObjectStateData = 24,
                    Reconstitute = 25,
                    LockSiteConfiguration = 26,
                    UnlockSiteConfiguration = 27,
                    UpdateSiteConfiguration = 28,
                    QuerySiteConfiguration = 29,
                    TetheringInformation = 30,
                    MountIntent = 31,
                    AcceptSubscription = 33,
                    UnSubscribe = 34,
                    TeleportEntity = 35,
                    Changeaggregatestate = 36,
                    RequestStartPDU = 37,
                    WakeUpGetReadyForInitialization = 38,
                    InitializeInternalparameters = 39,
                    SendPlanData = 40,
                    SynchronizeInternalClocks = 41,
                    Run = 42,
                    SaveInternalParameters = 43,
                    SimulatemalFunction = 44,
                    JoinExercise = 45,
                    ResignExercise = 46,
                    TimeAdvance = 47,
                    TACCSFLOSRequestType1 = 100,
                    TACCSFLOSRequestType2 = 101
                };

                /// <summary>*********************************************************************</summary>
                public enum struct RequestStatus
                {
                    OtherRequestStatus = 0,
                    Pending = 1,
                    Executing = 2,
                    PartiallyComplete = 3,
                    Complete = 4,
                    RequestRejected = 5,
                    ReTransmitRequestNow = 6,
                    ReTransmitRequestLater = 7,
                    InvalidTimeParameters = 8,
                    SimulationTimeExceeded = 9,
                    RequestDone = 10,
                    TACCSFLOSReplyType1 = 100,
                    TACCSFLOSReplyType2 = 101,
                    JoinExerciseRequestRejected = 201
                };

                /// <summary>*********************************************************************</summary>
                public enum struct EventType
                {
                    OtherEvent = 0,
                    IndirectFireOrCASFire = 10,
                    MinefieldEntry = 11,
                    MinefieldDetonation = 12,
                    VehicleMasterPowerOn = 13,
                    VehicleMasterPowerOff = 14,
                    AggregateStateChangeRequested = 15,
                    PreventCollisionDetonation = 16,
                    OwnershipReport = 17,
                    RanOutOfAmmunition = 2,
                    KilledInAction = 3,
                    Damage = 4,
                    MobilityDisabled = 5,
                    FireDisabled = 6,
                    RanOutOfFuel = 7,
                    EntityInitialization = 8,
                    RequestForIndirectFireOrCASMission = 9
                };

                public ref class EnumSimulationManagement
                {
                public:
                    static unsigned int GetEnumSizeRequiredReliabilityService();
                    static KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::EnumDescriptor^ GetEnumDescriptorRequiredReliabilityService(unsigned int Index);
                    static System::String^ GetEnumAsStringRequiredReliabilityService(int Value);
                    static bool GetEnumFromStringRequiredReliabilityService(System::String^ Value, [System::Runtime::InteropServices::In, System::Runtime::InteropServices::Out] int% ValueOut);
                    static unsigned int GetEnumSizeStopFreezeReason();
                    static KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::EnumDescriptor^ GetEnumDescriptorStopFreezeReason(unsigned int Index);
                    static System::String^ GetEnumAsStringStopFreezeReason(int Value);
                    static bool GetEnumFromStringStopFreezeReason(System::String^ Value, [System::Runtime::InteropServices::In, System::Runtime::InteropServices::Out] int% ValueOut);
                    static unsigned int GetEnumSizeFrozenBehavior();
                    static KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::EnumDescriptor^ GetEnumDescriptorFrozenBehavior(unsigned int Index);
                    static System::String^ GetEnumAsStringFrozenBehavior(int Value);
                    static bool GetEnumFromStringFrozenBehavior(System::String^ Value, [System::Runtime::InteropServices::In, System::Runtime::InteropServices::Out] int% ValueOut);
                    static unsigned int GetEnumSizeAcknowledgeFlag();
                    static KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::EnumDescriptor^ GetEnumDescriptorAcknowledgeFlag(unsigned int Index);
                    static System::String^ GetEnumAsStringAcknowledgeFlag(int Value);
                    static bool GetEnumFromStringAcknowledgeFlag(System::String^ Value, [System::Runtime::InteropServices::In, System::Runtime::InteropServices::Out] int% ValueOut);
                    static unsigned int GetEnumSizeAcknowledgeResponseFlag();
                    static KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::EnumDescriptor^ GetEnumDescriptorAcknowledgeResponseFlag(unsigned int Index);
                    static System::String^ GetEnumAsStringAcknowledgeResponseFlag(int Value);
                    static bool GetEnumFromStringAcknowledgeResponseFlag(System::String^ Value, [System::Runtime::InteropServices::In, System::Runtime::InteropServices::Out] int% ValueOut);
                    static unsigned int GetEnumSizeActionID();
                    static KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::EnumDescriptor^ GetEnumDescriptorActionID(unsigned int Index);
                    static System::String^ GetEnumAsStringActionID(int Value);
                    static bool GetEnumFromStringActionID(System::String^ Value, [System::Runtime::InteropServices::In, System::Runtime::InteropServices::Out] int% ValueOut);
                    static unsigned int GetEnumSizeRequestStatus();
                    static KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::EnumDescriptor^ GetEnumDescriptorRequestStatus(unsigned int Index);
                    static System::String^ GetEnumAsStringRequestStatus(int Value);
                    static bool GetEnumFromStringRequestStatus(System::String^ Value, [System::Runtime::InteropServices::In, System::Runtime::InteropServices::Out] int% ValueOut);
                    static unsigned int GetEnumSizeEventType();
                    static KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::EnumDescriptor^ GetEnumDescriptorEventType(unsigned int Index);
                    static System::String^ GetEnumAsStringEventType(int Value);
                    static bool GetEnumFromStringEventType(System::String^ Value, [System::Runtime::InteropServices::In, System::Runtime::InteropServices::Out] int% ValueOut);
                };
            }
        }
    }
}
