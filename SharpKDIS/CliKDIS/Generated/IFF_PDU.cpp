// ----------------------------------------------------------------------------
// <auto-generated>
// This is autogenerated code by CppSharp.
// Do not edit this file or all your changes will be lost after re-generation.
// </auto-generated>
// ----------------------------------------------------------------------------
#include "IFF_PDU.h"
#include "EntityIdentifier.h"
#include "FundamentalOperationalData.h"
#include "KDataStream.h"
#include "LayerHeader.h"
#include "SystemIdentifier.h"
#include "Vector.h"

using namespace System;
using namespace System::Runtime::InteropServices;

KDIS7_DLL::KDIS::PDU::IFF_PDU::IFF_PDU(::KDIS::PDU::IFF_PDU* native)
    : KDIS7_DLL::KDIS::PDU::Header7((::KDIS::PDU::Header7*)native)
{
}

KDIS7_DLL::KDIS::PDU::IFF_PDU^ KDIS7_DLL::KDIS::PDU::IFF_PDU::__CreateInstance(::System::IntPtr native)
{
    return gcnew ::KDIS7_DLL::KDIS::PDU::IFF_PDU((::KDIS::PDU::IFF_PDU*) native.ToPointer());
}

KDIS7_DLL::KDIS::PDU::IFF_PDU::~IFF_PDU()
{
    if (NativePtr)
    {
        auto __nativePtr = NativePtr;
        NativePtr = 0;
        delete (::KDIS::PDU::IFF_PDU*) __nativePtr;
    }
}

KDIS7_DLL::KDIS::PDU::IFF_PDU::IFF_PDU()
    : KDIS7_DLL::KDIS::PDU::Header7((::KDIS::PDU::Header7*)nullptr)
{
    __ownsNativeInstance = true;
    NativePtr = new ::KDIS::PDU::IFF_PDU();
}

KDIS7_DLL::KDIS::PDU::IFF_PDU::IFF_PDU(KDIS7_DLL::KDIS::PDU::Header7^ H)
    : KDIS7_DLL::KDIS::PDU::Header7((::KDIS::PDU::Header7*)nullptr)
{
    __ownsNativeInstance = true;
    if (ReferenceEquals(H, nullptr))
        throw gcnew ::System::ArgumentNullException("H", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::PDU::Header7*)H->NativePtr;
    NativePtr = new ::KDIS::PDU::IFF_PDU(__arg0);
}

KDIS7_DLL::KDIS::PDU::IFF_PDU::IFF_PDU(KDIS7_DLL::KDIS::KDataStream^ stream)
    : KDIS7_DLL::KDIS::PDU::Header7((::KDIS::PDU::Header7*)nullptr)
{
    __ownsNativeInstance = true;
    if (ReferenceEquals(stream, nullptr))
        throw gcnew ::System::ArgumentNullException("stream", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::KDataStream*)stream->NativePtr;
    NativePtr = new ::KDIS::PDU::IFF_PDU(__arg0);
}

KDIS7_DLL::KDIS::PDU::IFF_PDU::IFF_PDU(KDIS7_DLL::KDIS::PDU::Header7^ H, KDIS7_DLL::KDIS::KDataStream^ stream)
    : KDIS7_DLL::KDIS::PDU::Header7((::KDIS::PDU::Header7*)nullptr)
{
    __ownsNativeInstance = true;
    if (ReferenceEquals(H, nullptr))
        throw gcnew ::System::ArgumentNullException("H", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::PDU::Header7*)H->NativePtr;
    if (ReferenceEquals(stream, nullptr))
        throw gcnew ::System::ArgumentNullException("stream", "Cannot be null because it is a C++ reference (&).");
    auto &__arg1 = *(::KDIS::KDataStream*)stream->NativePtr;
    NativePtr = new ::KDIS::PDU::IFF_PDU(__arg0, __arg1);
}

KDIS7_DLL::KDIS::PDU::IFF_PDU::IFF_PDU(KDIS7_DLL::KDIS::DATA_TYPE::EntityIdentifier^ EmittingID, KDIS7_DLL::KDIS::DATA_TYPE::EntityIdentifier^ EventID, KDIS7_DLL::KDIS::DATA_TYPE::Vector^ Location, KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier^ ID, KDIS7_DLL::KDIS::DATA_TYPE::FundamentalOperationalData^ FOD)
    : KDIS7_DLL::KDIS::PDU::Header7((::KDIS::PDU::Header7*)nullptr)
{
    __ownsNativeInstance = true;
    if (ReferenceEquals(EmittingID, nullptr))
        throw gcnew ::System::ArgumentNullException("EmittingID", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::DATA_TYPE::EntityIdentifier*)EmittingID->NativePtr;
    if (ReferenceEquals(EventID, nullptr))
        throw gcnew ::System::ArgumentNullException("EventID", "Cannot be null because it is a C++ reference (&).");
    auto &__arg1 = *(::KDIS::DATA_TYPE::EntityIdentifier*)EventID->NativePtr;
    if (ReferenceEquals(Location, nullptr))
        throw gcnew ::System::ArgumentNullException("Location", "Cannot be null because it is a C++ reference (&).");
    auto &__arg2 = *(::KDIS::DATA_TYPE::Vector*)Location->NativePtr;
    if (ReferenceEquals(ID, nullptr))
        throw gcnew ::System::ArgumentNullException("ID", "Cannot be null because it is a C++ reference (&).");
    auto &__arg3 = *(::KDIS::DATA_TYPE::SystemIdentifier*)ID->NativePtr;
    if (ReferenceEquals(FOD, nullptr))
        throw gcnew ::System::ArgumentNullException("FOD", "Cannot be null because it is a C++ reference (&).");
    auto &__arg4 = *(::KDIS::DATA_TYPE::FundamentalOperationalData*)FOD->NativePtr;
    NativePtr = new ::KDIS::PDU::IFF_PDU(__arg0, __arg1, __arg2, __arg3, __arg4);
}

void KDIS7_DLL::KDIS::PDU::IFF_PDU::AddLayer(KDIS7_DLL::KDIS::DATA_TYPE::LayerHeader^ L)
{
    if (ReferenceEquals(L, nullptr))
        throw gcnew ::System::ArgumentNullException("L", "Cannot be null because it is a C++ reference (&).");
    auto __arg0 = (::KDIS::DATA_TYPE::LayerHeader*)L->NativePtr;
    ((::KDIS::PDU::IFF_PDU*)NativePtr)->AddLayer(__arg0);
}

KDIS7_DLL::KDIS::DATA_TYPE::LayerHeader^ KDIS7_DLL::KDIS::PDU::IFF_PDU::GetLayer(int pIndex)
{
    auto __ret = ((::KDIS::PDU::IFF_PDU*)NativePtr)->GetLayer(pIndex);
    return (__ret == nullptr) ? nullptr : gcnew KDIS7_DLL::KDIS::DATA_TYPE::LayerHeader((::KDIS::DATA_TYPE::LayerHeader*)__ret);
}

void KDIS7_DLL::KDIS::PDU::IFF_PDU::ClearLayers()
{
    ((::KDIS::PDU::IFF_PDU*)NativePtr)->ClearLayers();
}

void KDIS7_DLL::KDIS::PDU::IFF_PDU::Decode(KDIS7_DLL::KDIS::KDataStream^ stream, bool ignoreHeader)
{
    if (ReferenceEquals(stream, nullptr))
        throw gcnew ::System::ArgumentNullException("stream", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::KDataStream*)stream->NativePtr;
    ((::KDIS::PDU::IFF_PDU*)NativePtr)->Decode(__arg0, ignoreHeader);
}

void KDIS7_DLL::KDIS::PDU::IFF_PDU::EncodeToStream(KDIS7_DLL::KDIS::KDataStream^ stream)
{
    if (ReferenceEquals(stream, nullptr))
        throw gcnew ::System::ArgumentNullException("stream", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::KDataStream*)stream->NativePtr;
    ((::KDIS::PDU::IFF_PDU*)NativePtr)->EncodeToStream(__arg0);
}

bool KDIS7_DLL::KDIS::PDU::IFF_PDU::operator==(KDIS7_DLL::KDIS::PDU::IFF_PDU^ __op, KDIS7_DLL::KDIS::PDU::IFF_PDU^ Value)
{
    bool __opNull = ReferenceEquals(__op, nullptr);
    bool ValueNull = ReferenceEquals(Value, nullptr);
    if (__opNull || ValueNull)
        return __opNull && ValueNull;
    auto &__arg0 = *(::KDIS::PDU::IFF_PDU*)__op->NativePtr;
    auto &__arg1 = *(::KDIS::PDU::IFF_PDU*)Value->NativePtr;
    auto __ret = __arg0 == __arg1;
    return __ret;
}

bool KDIS7_DLL::KDIS::PDU::IFF_PDU::Equals(::System::Object^ obj)
{
    return this == safe_cast<KDIS7_DLL::KDIS::PDU::IFF_PDU^>(obj);
}

bool KDIS7_DLL::KDIS::PDU::IFF_PDU::operator!=(KDIS7_DLL::KDIS::PDU::IFF_PDU^ __op, KDIS7_DLL::KDIS::PDU::IFF_PDU^ Value)
{
    bool __opNull = ReferenceEquals(__op, nullptr);
    bool ValueNull = ReferenceEquals(Value, nullptr);
    if (__opNull || ValueNull)
        return !(__opNull && ValueNull);
    auto &__arg0 = *(::KDIS::PDU::IFF_PDU*)__op->NativePtr;
    auto &__arg1 = *(::KDIS::PDU::IFF_PDU*)Value->NativePtr;
    auto __ret = __arg0 != __arg1;
    return __ret;
}

KDIS7_DLL::KDIS::PDU::IFF_PDU::IFF_PDU(KDIS7_DLL::KDIS::PDU::IFF_PDU^ _0)
    : KDIS7_DLL::KDIS::PDU::Header7((::KDIS::PDU::Header7*)nullptr)
{
    __ownsNativeInstance = true;
    if (ReferenceEquals(_0, nullptr))
        throw gcnew ::System::ArgumentNullException("_0", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::PDU::IFF_PDU*)_0->NativePtr;
    NativePtr = new ::KDIS::PDU::IFF_PDU(__arg0);
}

KDIS7_DLL::KDIS::PDU::IFF_PDU::operator KDIS7_DLL::KDIS::PDU::IFF_PDU^(KDIS7_DLL::KDIS::KDataStream^ stream)
{
    if (ReferenceEquals(stream, nullptr))
        throw gcnew ::System::ArgumentNullException("stream", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::KDataStream*)stream->NativePtr;
    auto __ret = (::KDIS::PDU::IFF_PDU) __arg0;
    auto ____ret = new ::KDIS::PDU::IFF_PDU(__ret);
    return (____ret == nullptr) ? nullptr : gcnew KDIS7_DLL::KDIS::PDU::IFF_PDU((::KDIS::PDU::IFF_PDU*)____ret);
}

KDIS7_DLL::KDIS::DATA_TYPE::EntityIdentifier^ KDIS7_DLL::KDIS::PDU::IFF_PDU::EmittingEntityID::get()
{
    auto &__ret = ((::KDIS::PDU::IFF_PDU*)NativePtr)->GetEmittingEntityID();
    return (KDIS7_DLL::KDIS::DATA_TYPE::EntityIdentifier^)((&__ret == nullptr) ? nullptr : gcnew KDIS7_DLL::KDIS::DATA_TYPE::EntityIdentifier((::KDIS::DATA_TYPE::EntityIdentifier*)&__ret));
}

void KDIS7_DLL::KDIS::PDU::IFF_PDU::EmittingEntityID::set(KDIS7_DLL::KDIS::DATA_TYPE::EntityIdentifier^ ID)
{
    if (ReferenceEquals(ID, nullptr))
        throw gcnew ::System::ArgumentNullException("ID", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::DATA_TYPE::EntityIdentifier*)ID->NativePtr;
    ((::KDIS::PDU::IFF_PDU*)NativePtr)->SetEmittingEntityID(__arg0);
}

KDIS7_DLL::KDIS::DATA_TYPE::EntityIdentifier^ KDIS7_DLL::KDIS::PDU::IFF_PDU::EventID::get()
{
    auto &__ret = ((::KDIS::PDU::IFF_PDU*)NativePtr)->GetEventID();
    return (KDIS7_DLL::KDIS::DATA_TYPE::EntityIdentifier^)((&__ret == nullptr) ? nullptr : gcnew KDIS7_DLL::KDIS::DATA_TYPE::EntityIdentifier((::KDIS::DATA_TYPE::EntityIdentifier*)&__ret));
}

void KDIS7_DLL::KDIS::PDU::IFF_PDU::EventID::set(KDIS7_DLL::KDIS::DATA_TYPE::EntityIdentifier^ ID)
{
    if (ReferenceEquals(ID, nullptr))
        throw gcnew ::System::ArgumentNullException("ID", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::DATA_TYPE::EntityIdentifier*)ID->NativePtr;
    ((::KDIS::PDU::IFF_PDU*)NativePtr)->SetEventID(__arg0);
}

KDIS7_DLL::KDIS::DATA_TYPE::Vector^ KDIS7_DLL::KDIS::PDU::IFF_PDU::Location::get()
{
    auto &__ret = ((::KDIS::PDU::IFF_PDU*)NativePtr)->GetLocation();
    return (KDIS7_DLL::KDIS::DATA_TYPE::Vector^)((&__ret == nullptr) ? nullptr : gcnew KDIS7_DLL::KDIS::DATA_TYPE::Vector((::KDIS::DATA_TYPE::Vector*)&__ret));
}

void KDIS7_DLL::KDIS::PDU::IFF_PDU::Location::set(KDIS7_DLL::KDIS::DATA_TYPE::Vector^ L)
{
    if (ReferenceEquals(L, nullptr))
        throw gcnew ::System::ArgumentNullException("L", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::DATA_TYPE::Vector*)L->NativePtr;
    ((::KDIS::PDU::IFF_PDU*)NativePtr)->SetLocation(__arg0);
}

KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier^ KDIS7_DLL::KDIS::PDU::IFF_PDU::SystemIdentifier::get()
{
    auto &__ret = ((::KDIS::PDU::IFF_PDU*)NativePtr)->GetSystemIdentifier();
    return (KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier^)((&__ret == nullptr) ? nullptr : gcnew KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier((::KDIS::DATA_TYPE::SystemIdentifier*)&__ret));
}

void KDIS7_DLL::KDIS::PDU::IFF_PDU::SystemIdentifier::set(KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier^ ID)
{
    if (ReferenceEquals(ID, nullptr))
        throw gcnew ::System::ArgumentNullException("ID", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::DATA_TYPE::SystemIdentifier*)ID->NativePtr;
    ((::KDIS::PDU::IFF_PDU*)NativePtr)->SetSystemIdentifier(__arg0);
}

KDIS7_DLL::KDIS::DATA_TYPE::FundamentalOperationalData^ KDIS7_DLL::KDIS::PDU::IFF_PDU::FundamentalOperationalData::get()
{
    auto &__ret = ((::KDIS::PDU::IFF_PDU*)NativePtr)->GetFundamentalOperationalData();
    return (KDIS7_DLL::KDIS::DATA_TYPE::FundamentalOperationalData^)((&__ret == nullptr) ? nullptr : gcnew KDIS7_DLL::KDIS::DATA_TYPE::FundamentalOperationalData((::KDIS::DATA_TYPE::FundamentalOperationalData*)&__ret));
}

void KDIS7_DLL::KDIS::PDU::IFF_PDU::FundamentalOperationalData::set(KDIS7_DLL::KDIS::DATA_TYPE::FundamentalOperationalData^ FOD)
{
    if (ReferenceEquals(FOD, nullptr))
        throw gcnew ::System::ArgumentNullException("FOD", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::DATA_TYPE::FundamentalOperationalData*)FOD->NativePtr;
    ((::KDIS::PDU::IFF_PDU*)NativePtr)->SetFundamentalOperationalData(__arg0);
}

unsigned char KDIS7_DLL::KDIS::PDU::IFF_PDU::SystemDesignator::get()
{
    auto __ret = ((::KDIS::PDU::IFF_PDU*)NativePtr)->GetSystemDesignator();
    return __ret;
}

void KDIS7_DLL::KDIS::PDU::IFF_PDU::SystemDesignator::set(unsigned char SD)
{
    auto __arg0 = (::KDIS::KUINT8)SD;
    ((::KDIS::PDU::IFF_PDU*)NativePtr)->SetSystemDesignator(__arg0);
}

unsigned char KDIS7_DLL::KDIS::PDU::IFF_PDU::SystemSpecificData::get()
{
    auto __ret = ((::KDIS::PDU::IFF_PDU*)NativePtr)->GetSystemSpecificData();
    return __ret;
}

void KDIS7_DLL::KDIS::PDU::IFF_PDU::SystemSpecificData::set(unsigned char SSD)
{
    auto __arg0 = (::KDIS::KUINT8)SSD;
    ((::KDIS::PDU::IFF_PDU*)NativePtr)->SetSystemSpecificData(__arg0);
}

System::Collections::Generic::List<KDIS7_DLL::KDIS::DATA_TYPE::LayerHeader^>^ KDIS7_DLL::KDIS::PDU::IFF_PDU::Layers::get()
{
    auto &__ret = ((::KDIS::PDU::IFF_PDU*)NativePtr)->GetLayers();
    auto _tmp__ret = gcnew System::Collections::Generic::List<KDIS7_DLL::KDIS::DATA_TYPE::LayerHeader^>();
    for(auto _element : __ret)
    {
        auto _marshalElement = (_element == nullptr) ? nullptr : gcnew KDIS7_DLL::KDIS::DATA_TYPE::LayerHeader((::KDIS::DATA_TYPE::LayerHeader*)_element);
        _tmp__ret->Add(_marshalElement);
    }
    return (System::Collections::Generic::List<KDIS7_DLL::KDIS::DATA_TYPE::LayerHeader^>^)(_tmp__ret);
}

void KDIS7_DLL::KDIS::PDU::IFF_PDU::Layers::set(System::Collections::Generic::List<KDIS7_DLL::KDIS::DATA_TYPE::LayerHeader^>^ L)
{
    auto _tmpL = std::vector<::KDIS::DATA_TYPE::LyrHdrPtr>();
    for each(KDIS7_DLL::KDIS::DATA_TYPE::LayerHeader^ _element in L)
    {
        auto _marshalElement = (::KDIS::DATA_TYPE::LayerHeader*)_element->NativePtr;
        _tmpL.push_back(_marshalElement);
    }
    auto __arg0 = _tmpL;
    ((::KDIS::PDU::IFF_PDU*)NativePtr)->SetLayers(__arg0);
}

System::String^ KDIS7_DLL::KDIS::PDU::IFF_PDU::AsString::get()
{
    auto __ret = ((::KDIS::PDU::IFF_PDU*)NativePtr)->GetAsString();
    if (__ret == nullptr) return nullptr;
    return (__ret == 0 ? nullptr : clix::marshalString<clix::E_UTF8>(__ret));
}

int KDIS7_DLL::KDIS::PDU::IFF_PDU::LayerCount::get()
{
    auto __ret = ((::KDIS::PDU::IFF_PDU*)NativePtr)->GetLayerCount();
    return __ret;
}

unsigned short KDIS7_DLL::KDIS::PDU::IFF_PDU::IFF_PDU_SIZE::get()
{
    return ::KDIS::PDU::IFF_PDU::IFF_PDU_SIZE;
}

