// ----------------------------------------------------------------------------
// <auto-generated>
// This is autogenerated code by CppSharp.
// Do not edit this file or all your changes will be lost after re-generation.
// </auto-generated>
// ----------------------------------------------------------------------------
#pragma once

#include "CppSharp.h"
#include <C:/ND3/DXT/Dev/SharpKDIS/CppKDIS/include/KDIS/DataTypes/ModeSLevelsPresent.h>
#include "DataTypeBase.h"

namespace KDIS7_DLL
{
    namespace KDIS
    {
        ref class KDataStream;
        namespace DATA_TYPE
        {
            ref class ModeSLevelsPresent;
        }
    }
}

namespace KDIS7_DLL
{
    namespace KDIS
    {
        namespace DATA_TYPE
        {
            public ref class ModeSLevelsPresent : KDIS7_DLL::KDIS::DATA_TYPE::DataTypeBase
            {
            public:

                ModeSLevelsPresent(::KDIS::DATA_TYPE::ModeSLevelsPresent* native);
                static ModeSLevelsPresent^ __CreateInstance(::System::IntPtr native);
                ModeSLevelsPresent();

                ModeSLevelsPresent(KDIS7_DLL::KDIS::KDataStream^ stream);

                ModeSLevelsPresent(bool Level1, bool Level2_ElementarySurveillanceSublevel, bool Level2_EnhancedSurveillanceSublevel, bool Level3, bool Level4);

                ModeSLevelsPresent(KDIS7_DLL::KDIS::DATA_TYPE::ModeSLevelsPresent^ _0);

                ~ModeSLevelsPresent();

                property bool Level1
                {
                    bool get();
                    void set(bool);
                }

                property bool Level2ElementarySurveillanceSublevel
                {
                    bool get();
                    void set(bool);
                }

                property bool Level2EnhancedSurveillanceSublevel
                {
                    bool get();
                    void set(bool);
                }

                property bool Level3
                {
                    bool get();
                    void set(bool);
                }

                property bool Level4
                {
                    bool get();
                    void set(bool);
                }

                property System::String^ AsString
                {
                    System::String^ get();
                }

                property KDIS7_DLL::KDIS::KDataStream^ EncodedStream
                {
                    KDIS7_DLL::KDIS::KDataStream^ get();
                }

                virtual void Decode(KDIS7_DLL::KDIS::KDataStream^ stream) override;

                virtual void EncodeToStream(KDIS7_DLL::KDIS::KDataStream^ stream) override;

                static bool operator==(KDIS7_DLL::KDIS::DATA_TYPE::ModeSLevelsPresent^ __op, KDIS7_DLL::KDIS::DATA_TYPE::ModeSLevelsPresent^ Value);

                virtual bool Equals(::System::Object^ obj) override;

                static bool operator!=(KDIS7_DLL::KDIS::DATA_TYPE::ModeSLevelsPresent^ __op, KDIS7_DLL::KDIS::DATA_TYPE::ModeSLevelsPresent^ Value);

                static operator KDIS7_DLL::KDIS::DATA_TYPE::ModeSLevelsPresent^(KDIS7_DLL::KDIS::KDataStream^ stream);

                static property unsigned short MODE_S_LEVELS_PRESENT_SIZE
                {
                    unsigned short get();
                }
            };
        }
    }
}
