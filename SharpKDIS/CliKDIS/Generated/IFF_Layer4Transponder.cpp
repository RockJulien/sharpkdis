// ----------------------------------------------------------------------------
// <auto-generated>
// This is autogenerated code by CppSharp.
// Do not edit this file or all your changes will be lost after re-generation.
// </auto-generated>
// ----------------------------------------------------------------------------
#include "IFF_Layer4Transponder.h"
#include "KDataStream.h"
#include "LayerHeader.h"
#include "ModeSTransponderBasicData.h"
#include "SimulationIdentifier.h"
#include "StandardVariable.h"

using namespace System;
using namespace System::Runtime::InteropServices;

KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4Transponder::IFF_Layer4Transponder(::KDIS::DATA_TYPE::IFF_Layer4Transponder* native)
    : KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4((::KDIS::DATA_TYPE::IFF_Layer4*)native)
{
}

KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4Transponder^ KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4Transponder::__CreateInstance(::System::IntPtr native)
{
    return gcnew ::KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4Transponder((::KDIS::DATA_TYPE::IFF_Layer4Transponder*) native.ToPointer());
}

KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4Transponder::~IFF_Layer4Transponder()
{
    if (NativePtr)
    {
        auto __nativePtr = NativePtr;
        NativePtr = 0;
        delete (::KDIS::DATA_TYPE::IFF_Layer4Transponder*) __nativePtr;
    }
}

KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4Transponder::IFF_Layer4Transponder()
    : KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4((::KDIS::DATA_TYPE::IFF_Layer4*)nullptr)
{
    __ownsNativeInstance = true;
    NativePtr = new ::KDIS::DATA_TYPE::IFF_Layer4Transponder();
}

KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4Transponder::IFF_Layer4Transponder(KDIS7_DLL::KDIS::KDataStream^ stream)
    : KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4((::KDIS::DATA_TYPE::IFF_Layer4*)nullptr)
{
    __ownsNativeInstance = true;
    if (ReferenceEquals(stream, nullptr))
        throw gcnew ::System::ArgumentNullException("stream", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::KDataStream*)stream->NativePtr;
    NativePtr = new ::KDIS::DATA_TYPE::IFF_Layer4Transponder(__arg0);
}

KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4Transponder::IFF_Layer4Transponder(KDIS7_DLL::KDIS::DATA_TYPE::SimulationIdentifier^ ReportingSimulation, KDIS7_DLL::KDIS::DATA_TYPE::ModeSTransponderBasicData^ Data, System::Collections::Generic::List<KDIS7_DLL::KDIS::DATA_TYPE::StandardVariable^>^ Records)
    : KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4((::KDIS::DATA_TYPE::IFF_Layer4*)nullptr)
{
    __ownsNativeInstance = true;
    if (ReferenceEquals(ReportingSimulation, nullptr))
        throw gcnew ::System::ArgumentNullException("ReportingSimulation", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::DATA_TYPE::SimulationIdentifier*)ReportingSimulation->NativePtr;
    if (ReferenceEquals(Data, nullptr))
        throw gcnew ::System::ArgumentNullException("Data", "Cannot be null because it is a C++ reference (&).");
    auto &__arg1 = *(::KDIS::DATA_TYPE::ModeSTransponderBasicData*)Data->NativePtr;
    auto _tmpRecords = std::vector<::KDIS::DATA_TYPE::StdVarPtr>();
    for each(KDIS7_DLL::KDIS::DATA_TYPE::StandardVariable^ _element in Records)
    {
        auto _marshalElement = (::KDIS::DATA_TYPE::StandardVariable*)_element->NativePtr;
        _tmpRecords.push_back(_marshalElement);
    }
    auto __arg2 = _tmpRecords;
    NativePtr = new ::KDIS::DATA_TYPE::IFF_Layer4Transponder(__arg0, __arg1, __arg2);
}

KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4Transponder::IFF_Layer4Transponder(KDIS7_DLL::KDIS::DATA_TYPE::LayerHeader^ H, KDIS7_DLL::KDIS::KDataStream^ stream)
    : KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4((::KDIS::DATA_TYPE::IFF_Layer4*)nullptr)
{
    __ownsNativeInstance = true;
    if (ReferenceEquals(H, nullptr))
        throw gcnew ::System::ArgumentNullException("H", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::DATA_TYPE::LayerHeader*)H->NativePtr;
    if (ReferenceEquals(stream, nullptr))
        throw gcnew ::System::ArgumentNullException("stream", "Cannot be null because it is a C++ reference (&).");
    auto &__arg1 = *(::KDIS::KDataStream*)stream->NativePtr;
    NativePtr = new ::KDIS::DATA_TYPE::IFF_Layer4Transponder(__arg0, __arg1);
}

void KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4Transponder::Decode(KDIS7_DLL::KDIS::KDataStream^ stream, bool ignoreHeader)
{
    if (ReferenceEquals(stream, nullptr))
        throw gcnew ::System::ArgumentNullException("stream", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::KDataStream*)stream->NativePtr;
    ((::KDIS::DATA_TYPE::IFF_Layer4Transponder*)NativePtr)->Decode(__arg0, ignoreHeader);
}

void KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4Transponder::EncodeToStream(KDIS7_DLL::KDIS::KDataStream^ stream)
{
    if (ReferenceEquals(stream, nullptr))
        throw gcnew ::System::ArgumentNullException("stream", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::KDataStream*)stream->NativePtr;
    ((::KDIS::DATA_TYPE::IFF_Layer4Transponder*)NativePtr)->EncodeToStream(__arg0);
}

bool KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4Transponder::operator==(KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4Transponder^ __op, KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4Transponder^ Value)
{
    bool __opNull = ReferenceEquals(__op, nullptr);
    bool ValueNull = ReferenceEquals(Value, nullptr);
    if (__opNull || ValueNull)
        return __opNull && ValueNull;
    auto &__arg0 = *(::KDIS::DATA_TYPE::IFF_Layer4Transponder*)__op->NativePtr;
    auto &__arg1 = *(::KDIS::DATA_TYPE::IFF_Layer4Transponder*)Value->NativePtr;
    auto __ret = __arg0 == __arg1;
    return __ret;
}

bool KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4Transponder::Equals(::System::Object^ obj)
{
    return this == safe_cast<KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4Transponder^>(obj);
}

bool KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4Transponder::operator!=(KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4Transponder^ __op, KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4Transponder^ Value)
{
    bool __opNull = ReferenceEquals(__op, nullptr);
    bool ValueNull = ReferenceEquals(Value, nullptr);
    if (__opNull || ValueNull)
        return !(__opNull && ValueNull);
    auto &__arg0 = *(::KDIS::DATA_TYPE::IFF_Layer4Transponder*)__op->NativePtr;
    auto &__arg1 = *(::KDIS::DATA_TYPE::IFF_Layer4Transponder*)Value->NativePtr;
    auto __ret = __arg0 != __arg1;
    return __ret;
}

KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4Transponder::IFF_Layer4Transponder(KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4Transponder^ _0)
    : KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4((::KDIS::DATA_TYPE::IFF_Layer4*)nullptr)
{
    __ownsNativeInstance = true;
    if (ReferenceEquals(_0, nullptr))
        throw gcnew ::System::ArgumentNullException("_0", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::DATA_TYPE::IFF_Layer4Transponder*)_0->NativePtr;
    NativePtr = new ::KDIS::DATA_TYPE::IFF_Layer4Transponder(__arg0);
}

KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4Transponder::operator KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4Transponder^(KDIS7_DLL::KDIS::KDataStream^ stream)
{
    if (ReferenceEquals(stream, nullptr))
        throw gcnew ::System::ArgumentNullException("stream", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::KDataStream*)stream->NativePtr;
    auto __ret = (::KDIS::DATA_TYPE::IFF_Layer4Transponder) __arg0;
    auto ____ret = new ::KDIS::DATA_TYPE::IFF_Layer4Transponder(__ret);
    return (____ret == nullptr) ? nullptr : gcnew KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4Transponder((::KDIS::DATA_TYPE::IFF_Layer4Transponder*)____ret);
}

KDIS7_DLL::KDIS::DATA_TYPE::ModeSTransponderBasicData^ KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4Transponder::BasicData::get()
{
    auto &__ret = ((::KDIS::DATA_TYPE::IFF_Layer4Transponder*)NativePtr)->GetBasicData();
    return (KDIS7_DLL::KDIS::DATA_TYPE::ModeSTransponderBasicData^)((&__ret == nullptr) ? nullptr : gcnew KDIS7_DLL::KDIS::DATA_TYPE::ModeSTransponderBasicData((::KDIS::DATA_TYPE::ModeSTransponderBasicData*)&__ret));
}

void KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4Transponder::BasicData::set(KDIS7_DLL::KDIS::DATA_TYPE::ModeSTransponderBasicData^ BD)
{
    if (ReferenceEquals(BD, nullptr))
        throw gcnew ::System::ArgumentNullException("BD", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::DATA_TYPE::ModeSTransponderBasicData*)BD->NativePtr;
    ((::KDIS::DATA_TYPE::IFF_Layer4Transponder*)NativePtr)->SetBasicData(__arg0);
}

System::String^ KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4Transponder::AsString::get()
{
    auto __ret = ((::KDIS::DATA_TYPE::IFF_Layer4Transponder*)NativePtr)->GetAsString();
    if (__ret == nullptr) return nullptr;
    return (__ret == 0 ? nullptr : clix::marshalString<clix::E_UTF8>(__ret));
}

KDIS7_DLL::KDIS::DATA_TYPE::ModeSTransponderBasicData^ KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4Transponder::BasicDatan::get()
{
    auto &__ret = ((::KDIS::DATA_TYPE::IFF_Layer4Transponder*)NativePtr)->GetBasicDatan();
    return (KDIS7_DLL::KDIS::DATA_TYPE::ModeSTransponderBasicData^)((&__ret == nullptr) ? nullptr : gcnew KDIS7_DLL::KDIS::DATA_TYPE::ModeSTransponderBasicData((::KDIS::DATA_TYPE::ModeSTransponderBasicData*)&__ret));
}

