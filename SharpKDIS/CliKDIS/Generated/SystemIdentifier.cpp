// ----------------------------------------------------------------------------
// <auto-generated>
// This is autogenerated code by CppSharp.
// Do not edit this file or all your changes will be lost after re-generation.
// </auto-generated>
// ----------------------------------------------------------------------------
#include "SystemIdentifier.h"
#include "EnumSystemIdentifier.h"
#include "KDataStream.h"

using namespace System;
using namespace System::Runtime::InteropServices;

KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::SystemIdentifier(::KDIS::DATA_TYPE::SystemIdentifier* native)
    : KDIS7_DLL::KDIS::DATA_TYPE::DataTypeBase((::KDIS::DATA_TYPE::DataTypeBase*)native)
{
}

KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier^ KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::__CreateInstance(::System::IntPtr native)
{
    return gcnew ::KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier((::KDIS::DATA_TYPE::SystemIdentifier*) native.ToPointer());
}

KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::~SystemIdentifier()
{
    if (NativePtr)
    {
        auto __nativePtr = NativePtr;
        NativePtr = 0;
        delete (::KDIS::DATA_TYPE::SystemIdentifier*) __nativePtr;
    }
}

KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::SystemIdentifier()
    : KDIS7_DLL::KDIS::DATA_TYPE::DataTypeBase((::KDIS::DATA_TYPE::DataTypeBase*)nullptr)
{
    __ownsNativeInstance = true;
    NativePtr = new ::KDIS::DATA_TYPE::SystemIdentifier();
}

KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::SystemIdentifier(KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::SystemType T, KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::SystemName N, KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::SystemMode M, bool ChangeIndicator, bool AltMode4, bool AltModeC)
    : KDIS7_DLL::KDIS::DATA_TYPE::DataTypeBase((::KDIS::DATA_TYPE::DataTypeBase*)nullptr)
{
    __ownsNativeInstance = true;
    auto __arg0 = (::KDIS::DATA_TYPE::ENUMS::SystemType)T;
    auto __arg1 = (::KDIS::DATA_TYPE::ENUMS::SystemName)N;
    auto __arg2 = (::KDIS::DATA_TYPE::ENUMS::SystemMode)M;
    auto __arg3 = (::KDIS::KBOOL)ChangeIndicator;
    auto __arg4 = (::KDIS::KBOOL)AltMode4;
    auto __arg5 = (::KDIS::KBOOL)AltModeC;
    NativePtr = new ::KDIS::DATA_TYPE::SystemIdentifier(__arg0, __arg1, __arg2, __arg3, __arg4, __arg5);
}

KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::SystemIdentifier(KDIS7_DLL::KDIS::KDataStream^ stream)
    : KDIS7_DLL::KDIS::DATA_TYPE::DataTypeBase((::KDIS::DATA_TYPE::DataTypeBase*)nullptr)
{
    __ownsNativeInstance = true;
    if (ReferenceEquals(stream, nullptr))
        throw gcnew ::System::ArgumentNullException("stream", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::KDataStream*)stream->NativePtr;
    NativePtr = new ::KDIS::DATA_TYPE::SystemIdentifier(__arg0);
}

void KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::SetChangeOptions(bool ChangeIndicator, bool AltMode4, bool AltModeC)
{
    auto __arg0 = (::KDIS::KBOOL)ChangeIndicator;
    auto __arg1 = (::KDIS::KBOOL)AltMode4;
    auto __arg2 = (::KDIS::KBOOL)AltModeC;
    ((::KDIS::DATA_TYPE::SystemIdentifier*)NativePtr)->SetChangeOptions(__arg0, __arg1, __arg2);
}

void KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::Decode(KDIS7_DLL::KDIS::KDataStream^ stream)
{
    if (ReferenceEquals(stream, nullptr))
        throw gcnew ::System::ArgumentNullException("stream", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::KDataStream*)stream->NativePtr;
    ((::KDIS::DATA_TYPE::SystemIdentifier*)NativePtr)->Decode(__arg0);
}

void KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::EncodeToStream(KDIS7_DLL::KDIS::KDataStream^ stream)
{
    if (ReferenceEquals(stream, nullptr))
        throw gcnew ::System::ArgumentNullException("stream", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::KDataStream*)stream->NativePtr;
    ((::KDIS::DATA_TYPE::SystemIdentifier*)NativePtr)->EncodeToStream(__arg0);
}

bool KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::operator==(KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier^ __op, KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier^ Value)
{
    bool __opNull = ReferenceEquals(__op, nullptr);
    bool ValueNull = ReferenceEquals(Value, nullptr);
    if (__opNull || ValueNull)
        return __opNull && ValueNull;
    auto &__arg0 = *(::KDIS::DATA_TYPE::SystemIdentifier*)__op->NativePtr;
    auto &__arg1 = *(::KDIS::DATA_TYPE::SystemIdentifier*)Value->NativePtr;
    auto __ret = __arg0 == __arg1;
    return __ret;
}

bool KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::Equals(::System::Object^ obj)
{
    return this == safe_cast<KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier^>(obj);
}

bool KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::operator!=(KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier^ __op, KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier^ Value)
{
    bool __opNull = ReferenceEquals(__op, nullptr);
    bool ValueNull = ReferenceEquals(Value, nullptr);
    if (__opNull || ValueNull)
        return !(__opNull && ValueNull);
    auto &__arg0 = *(::KDIS::DATA_TYPE::SystemIdentifier*)__op->NativePtr;
    auto &__arg1 = *(::KDIS::DATA_TYPE::SystemIdentifier*)Value->NativePtr;
    auto __ret = __arg0 != __arg1;
    return __ret;
}

KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::SystemIdentifier(KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier^ _0)
    : KDIS7_DLL::KDIS::DATA_TYPE::DataTypeBase((::KDIS::DATA_TYPE::DataTypeBase*)nullptr)
{
    __ownsNativeInstance = true;
    if (ReferenceEquals(_0, nullptr))
        throw gcnew ::System::ArgumentNullException("_0", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::DATA_TYPE::SystemIdentifier*)_0->NativePtr;
    NativePtr = new ::KDIS::DATA_TYPE::SystemIdentifier(__arg0);
}

KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::operator KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier^(KDIS7_DLL::KDIS::KDataStream^ stream)
{
    if (ReferenceEquals(stream, nullptr))
        throw gcnew ::System::ArgumentNullException("stream", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::KDataStream*)stream->NativePtr;
    auto __ret = (::KDIS::DATA_TYPE::SystemIdentifier) __arg0;
    auto ____ret = new ::KDIS::DATA_TYPE::SystemIdentifier(__ret);
    return (____ret == nullptr) ? nullptr : gcnew KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier((::KDIS::DATA_TYPE::SystemIdentifier*)____ret);
}

KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::SystemType KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::SystemType::get()
{
    auto __ret = ((::KDIS::DATA_TYPE::SystemIdentifier*)NativePtr)->GetSystemType();
    return (KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::SystemType)__ret;
}

void KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::SystemType::set(KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::SystemType T)
{
    auto __arg0 = (::KDIS::DATA_TYPE::ENUMS::SystemType)T;
    ((::KDIS::DATA_TYPE::SystemIdentifier*)NativePtr)->SetSystemType(__arg0);
}

KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::SystemName KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::SystemName::get()
{
    auto __ret = ((::KDIS::DATA_TYPE::SystemIdentifier*)NativePtr)->GetSystemName();
    return (KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::SystemName)__ret;
}

void KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::SystemName::set(KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::SystemName N)
{
    auto __arg0 = (::KDIS::DATA_TYPE::ENUMS::SystemName)N;
    ((::KDIS::DATA_TYPE::SystemIdentifier*)NativePtr)->SetSystemName(__arg0);
}

KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::SystemMode KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::SystemMode::get()
{
    auto __ret = ((::KDIS::DATA_TYPE::SystemIdentifier*)NativePtr)->GetSystemMode();
    return (KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::SystemMode)__ret;
}

void KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::SystemMode::set(KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::SystemMode M)
{
    auto __arg0 = (::KDIS::DATA_TYPE::ENUMS::SystemMode)M;
    ((::KDIS::DATA_TYPE::SystemIdentifier*)NativePtr)->SetSystemMode(__arg0);
}

bool KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::ChangeIndicator::get()
{
    auto __ret = ((::KDIS::DATA_TYPE::SystemIdentifier*)NativePtr)->GetChangeIndicator();
    return __ret;
}

void KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::ChangeIndicator::set(bool CI)
{
    auto __arg0 = (::KDIS::KBOOL)CI;
    ((::KDIS::DATA_TYPE::SystemIdentifier*)NativePtr)->SetChangeIndicator(__arg0);
}

bool KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::AltMode4::get()
{
    auto __ret = ((::KDIS::DATA_TYPE::SystemIdentifier*)NativePtr)->GetAltMode4();
    return __ret;
}

void KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::AltMode4::set(bool AM)
{
    auto __arg0 = (::KDIS::KBOOL)AM;
    ((::KDIS::DATA_TYPE::SystemIdentifier*)NativePtr)->SetAltMode4(__arg0);
}

bool KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::AltModeC::get()
{
    auto __ret = ((::KDIS::DATA_TYPE::SystemIdentifier*)NativePtr)->GetAltModeC();
    return __ret;
}

void KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::AltModeC::set(bool AM)
{
    auto __arg0 = (::KDIS::KBOOL)AM;
    ((::KDIS::DATA_TYPE::SystemIdentifier*)NativePtr)->SetAltModeC(__arg0);
}

bool KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::HeartbeatIndicator::get()
{
    auto __ret = ((::KDIS::DATA_TYPE::SystemIdentifier*)NativePtr)->GetHeartbeatIndicator();
    return __ret;
}

void KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::HeartbeatIndicator::set(bool HI)
{
    auto __arg0 = (::KDIS::KBOOL)HI;
    ((::KDIS::DATA_TYPE::SystemIdentifier*)NativePtr)->SetHeartbeatIndicator(__arg0);
}

bool KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::TransponderInterrogatorIndicator::get()
{
    auto __ret = ((::KDIS::DATA_TYPE::SystemIdentifier*)NativePtr)->GetTransponderInterrogatorIndicator();
    return __ret;
}

void KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::TransponderInterrogatorIndicator::set(bool TII)
{
    auto __arg0 = (::KDIS::KBOOL)TII;
    ((::KDIS::DATA_TYPE::SystemIdentifier*)NativePtr)->SetTransponderInterrogatorIndicator(__arg0);
}

bool KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::SimulationMode::get()
{
    auto __ret = ((::KDIS::DATA_TYPE::SystemIdentifier*)NativePtr)->GetSimulationMode();
    return __ret;
}

void KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::SimulationMode::set(bool SM)
{
    auto __arg0 = (::KDIS::KBOOL)SM;
    ((::KDIS::DATA_TYPE::SystemIdentifier*)NativePtr)->SetSimulationMode(__arg0);
}

bool KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::InteractiveCapable::get()
{
    auto __ret = ((::KDIS::DATA_TYPE::SystemIdentifier*)NativePtr)->GetInteractiveCapable();
    return __ret;
}

void KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::InteractiveCapable::set(bool IC)
{
    auto __arg0 = (::KDIS::KBOOL)IC;
    ((::KDIS::DATA_TYPE::SystemIdentifier*)NativePtr)->SetInteractiveCapable(__arg0);
}

bool KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::TestMode::get()
{
    auto __ret = ((::KDIS::DATA_TYPE::SystemIdentifier*)NativePtr)->GetTestMode();
    return __ret;
}

void KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::TestMode::set(bool TM)
{
    auto __arg0 = (::KDIS::KBOOL)TM;
    ((::KDIS::DATA_TYPE::SystemIdentifier*)NativePtr)->SetTestMode(__arg0);
}

System::String^ KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::AsString::get()
{
    auto __ret = ((::KDIS::DATA_TYPE::SystemIdentifier*)NativePtr)->GetAsString();
    if (__ret == nullptr) return nullptr;
    return (__ret == 0 ? nullptr : clix::marshalString<clix::E_UTF8>(__ret));
}

KDIS7_DLL::KDIS::KDataStream^ KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::EncodedStream::get()
{
    auto __ret = ((::KDIS::DATA_TYPE::SystemIdentifier*)NativePtr)->GetEncodedStream();
    if (__ret == nullptr) return nullptr;
    return (__ret == nullptr) ? nullptr : gcnew KDIS7_DLL::KDIS::KDataStream((::KDIS::KDataStream*)__ret);
}

unsigned short KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier::SYSTEM_IDENTIFER_SIZE::get()
{
    return ::KDIS::DATA_TYPE::SystemIdentifier::SYSTEM_IDENTIFER_SIZE;
}

