// ----------------------------------------------------------------------------
// <auto-generated>
// This is autogenerated code by CppSharp.
// Do not edit this file or all your changes will be lost after re-generation.
// </auto-generated>
// ----------------------------------------------------------------------------
#pragma once

#include "CppSharp.h"
#include <C:/ND3/DXT/Dev/SharpKDIS/CppKDIS/include/KDIS/DataTypes/FundamentalOperationalData_Soviet.h>

namespace KDIS7_DLL
{
    namespace KDIS
    {
        namespace DATA_TYPE
        {
            ref class FundamentalOperationalDataSoviet;
        }
    }
}

namespace KDIS7_DLL
{
    namespace KDIS
    {
        namespace DATA_TYPE
        {
            public ref class FundamentalOperationalDataSoviet : ICppInstance
            {
            public:

                property ::KDIS::DATA_TYPE::FundamentalOperationalData_Soviet* NativePtr;
                property System::IntPtr __Instance
                {
                    virtual System::IntPtr get();
                    virtual void set(System::IntPtr instance);
                }

                FundamentalOperationalDataSoviet(::KDIS::DATA_TYPE::FundamentalOperationalData_Soviet* native);
                static FundamentalOperationalDataSoviet^ __CreateInstance(::System::IntPtr native);
                FundamentalOperationalDataSoviet(KDIS7_DLL::KDIS::DATA_TYPE::FundamentalOperationalDataSoviet^ _0);

                FundamentalOperationalDataSoviet();

                ~FundamentalOperationalDataSoviet();

                property bool SystemStatusSystemOn
                {
                    bool get();
                    void set(bool);
                }

                property bool SystemStatusParam1Capable
                {
                    bool get();
                    void set(bool);
                }

                property bool SystemStatusParam2Capable
                {
                    bool get();
                    void set(bool);
                }

                property bool SystemStatusParam3Capable
                {
                    bool get();
                    void set(bool);
                }

                property bool SystemStatusParam4Capable
                {
                    bool get();
                    void set(bool);
                }

                property bool SystemStatusParam5Capable
                {
                    bool get();
                    void set(bool);
                }

                property bool SystemStatusParam6Capable
                {
                    bool get();
                    void set(bool);
                }

                property bool SystemStatusIsOperational
                {
                    bool get();
                    void set(bool);
                }

                property bool IsInfomationLayer1Present
                {
                    bool get();
                }

                property bool IsInfomationLayer2Present
                {
                    bool get();
                }

                property bool IsParameter1StatusOn
                {
                    bool get();
                }

                property bool IsParameter1Damaged
                {
                    bool get();
                }

                property bool IsParameter1Malfunctioning
                {
                    bool get();
                }

                property bool IsParameter2StatusOn
                {
                    bool get();
                }

                property bool IsParameter2Damaged
                {
                    bool get();
                }

                property bool IsParameter2Malfunctioning
                {
                    bool get();
                }

                property bool IsParameter3StatusOn
                {
                    bool get();
                }

                property bool IsParameter3Damaged
                {
                    bool get();
                }

                property bool IsParameter3Malfunctioning
                {
                    bool get();
                }

                property bool IsParameter4StatusOn
                {
                    bool get();
                }

                property bool IsParameter4Damaged
                {
                    bool get();
                }

                property bool IsParameter4Malfunctioning
                {
                    bool get();
                }

                property bool IsParameter5StatusOn
                {
                    bool get();
                }

                property bool IsParameter5Damaged
                {
                    bool get();
                }

                property bool IsParameter5Malfunctioning
                {
                    bool get();
                }

                property bool IsParameter6StatusOn
                {
                    bool get();
                }

                property bool IsParameter6Damaged
                {
                    bool get();
                }

                property bool IsParameter6Malfunctioning
                {
                    bool get();
                }

                void SetSystemStatus(bool IsSystemOn, bool IsParam1Capable, bool IsParam2Capable, bool IsParam3Capable, bool IsParam4Capable, bool IsParam5Capable, bool IsParam6Capable, bool IsOperational);

                void SetInfomationLayersPresence(bool IsLayer1Present, bool IsLayer2Present);

                void SetInfomationLayer1(bool IsPresent);

                void SetInfomationLayer2(bool IsPresent);

                void SetParameter1(bool IsStatusOn, bool IsDamaged, bool IsMalfunctioning);

                void SetParameter1Status(bool IsOn);

                void SetParameter1Damage(bool IsDamaged);

                void SetParameter1Malfunction(bool IsMalfunctioning);

                void SetParameter2(bool IsStatusOn, bool IsDamaged, bool IsMalfunctioning);

                void SetParameter2Status(bool IsOn);

                void SetParameter2Damage(bool IsDamaged);

                void SetParameter2Malfunction(bool IsMalfunctioning);

                void SetParameter3(bool IsStatusOn, bool IsDamaged, bool IsMalfunctioning);

                void SetParameter3Status(bool IsOn);

                void SetParameter3Damage(bool IsDamaged);

                void SetParameter3Malfunction(bool IsMalfunctioning);

                void SetParameter4(bool IsStatusOn, bool IsDamaged, bool IsMalfunctioning);

                void SetParameter4Status(bool IsOn);

                void SetParameter4Damage(bool IsDamaged);

                void SetParameter4Malfunction(bool IsMalfunctioning);

                void SetParameter5(bool IsStatusOn, bool IsDamaged, bool IsMalfunctioning);

                void SetParameter5Status(bool IsOn);

                void SetParameter5Damage(bool IsDamaged);

                void SetParameter5Malfunction(bool IsMalfunctioning);

                void SetParameter6(bool IsStatusOn, bool IsDamaged, bool IsMalfunctioning);

                void SetParameter6Status(bool IsOn);

                void SetParameter6Damage(bool IsDamaged);

                void SetParameter6Malfunction(bool IsMalfunctioning);

                static bool operator==(KDIS7_DLL::KDIS::DATA_TYPE::FundamentalOperationalDataSoviet^ __op, KDIS7_DLL::KDIS::DATA_TYPE::FundamentalOperationalDataSoviet^ Value);

                virtual bool Equals(::System::Object^ obj) override;

                static bool operator!=(KDIS7_DLL::KDIS::DATA_TYPE::FundamentalOperationalDataSoviet^ __op, KDIS7_DLL::KDIS::DATA_TYPE::FundamentalOperationalDataSoviet^ Value);

            protected:
                bool __ownsNativeInstance;
            };
        }
    }
}
