// ----------------------------------------------------------------------------
// <auto-generated>
// This is autogenerated code by CppSharp.
// Do not edit this file or all your changes will be lost after re-generation.
// </auto-generated>
// ----------------------------------------------------------------------------
#include "InterrogatedModes.h"
#include "KDataStream.h"

using namespace System;
using namespace System::Runtime::InteropServices;

KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::InterrogatedModes(::KDIS::DATA_TYPE::InterrogatedModes* native)
    : KDIS7_DLL::KDIS::DATA_TYPE::DataTypeBase((::KDIS::DATA_TYPE::DataTypeBase*)native)
{
}

KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes^ KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::__CreateInstance(::System::IntPtr native)
{
    return gcnew ::KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes((::KDIS::DATA_TYPE::InterrogatedModes*) native.ToPointer());
}

KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::~InterrogatedModes()
{
    if (NativePtr)
    {
        auto __nativePtr = NativePtr;
        NativePtr = 0;
        delete (::KDIS::DATA_TYPE::InterrogatedModes*) __nativePtr;
    }
}

KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::InterrogatedModes()
    : KDIS7_DLL::KDIS::DATA_TYPE::DataTypeBase((::KDIS::DATA_TYPE::DataTypeBase*)nullptr)
{
    __ownsNativeInstance = true;
    NativePtr = new ::KDIS::DATA_TYPE::InterrogatedModes();
}

KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::InterrogatedModes(KDIS7_DLL::KDIS::KDataStream^ stream)
    : KDIS7_DLL::KDIS::DATA_TYPE::DataTypeBase((::KDIS::DATA_TYPE::DataTypeBase*)nullptr)
{
    __ownsNativeInstance = true;
    if (ReferenceEquals(stream, nullptr))
        throw gcnew ::System::ArgumentNullException("stream", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::KDataStream*)stream->NativePtr;
    NativePtr = new ::KDIS::DATA_TYPE::InterrogatedModes(__arg0);
}

KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::InterrogatedModes(bool Mode1, bool Mode2, bool Mode3A, bool Mode4, bool Mode5, bool ModeS, bool ModeCAlt, bool TCAS_ACAS, bool SpecialIP, bool Squitter)
    : KDIS7_DLL::KDIS::DATA_TYPE::DataTypeBase((::KDIS::DATA_TYPE::DataTypeBase*)nullptr)
{
    __ownsNativeInstance = true;
    auto __arg0 = (::KDIS::KBOOL)Mode1;
    auto __arg1 = (::KDIS::KBOOL)Mode2;
    auto __arg2 = (::KDIS::KBOOL)Mode3A;
    auto __arg3 = (::KDIS::KBOOL)Mode4;
    auto __arg4 = (::KDIS::KBOOL)Mode5;
    auto __arg5 = (::KDIS::KBOOL)ModeS;
    auto __arg6 = (::KDIS::KBOOL)ModeCAlt;
    auto __arg7 = (::KDIS::KBOOL)TCAS_ACAS;
    auto __arg8 = (::KDIS::KBOOL)SpecialIP;
    auto __arg9 = (::KDIS::KBOOL)Squitter;
    NativePtr = new ::KDIS::DATA_TYPE::InterrogatedModes(__arg0, __arg1, __arg2, __arg3, __arg4, __arg5, __arg6, __arg7, __arg8, __arg9);
}

void KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::Decode(KDIS7_DLL::KDIS::KDataStream^ stream)
{
    if (ReferenceEquals(stream, nullptr))
        throw gcnew ::System::ArgumentNullException("stream", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::KDataStream*)stream->NativePtr;
    ((::KDIS::DATA_TYPE::InterrogatedModes*)NativePtr)->Decode(__arg0);
}

void KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::EncodeToStream(KDIS7_DLL::KDIS::KDataStream^ stream)
{
    if (ReferenceEquals(stream, nullptr))
        throw gcnew ::System::ArgumentNullException("stream", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::KDataStream*)stream->NativePtr;
    ((::KDIS::DATA_TYPE::InterrogatedModes*)NativePtr)->EncodeToStream(__arg0);
}

bool KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::operator==(KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes^ __op, KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes^ Value)
{
    bool __opNull = ReferenceEquals(__op, nullptr);
    bool ValueNull = ReferenceEquals(Value, nullptr);
    if (__opNull || ValueNull)
        return __opNull && ValueNull;
    auto &__arg0 = *(::KDIS::DATA_TYPE::InterrogatedModes*)__op->NativePtr;
    auto &__arg1 = *(::KDIS::DATA_TYPE::InterrogatedModes*)Value->NativePtr;
    auto __ret = __arg0 == __arg1;
    return __ret;
}

bool KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::Equals(::System::Object^ obj)
{
    return this == safe_cast<KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes^>(obj);
}

bool KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::operator!=(KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes^ __op, KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes^ Value)
{
    bool __opNull = ReferenceEquals(__op, nullptr);
    bool ValueNull = ReferenceEquals(Value, nullptr);
    if (__opNull || ValueNull)
        return !(__opNull && ValueNull);
    auto &__arg0 = *(::KDIS::DATA_TYPE::InterrogatedModes*)__op->NativePtr;
    auto &__arg1 = *(::KDIS::DATA_TYPE::InterrogatedModes*)Value->NativePtr;
    auto __ret = __arg0 != __arg1;
    return __ret;
}

KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::InterrogatedModes(KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes^ _0)
    : KDIS7_DLL::KDIS::DATA_TYPE::DataTypeBase((::KDIS::DATA_TYPE::DataTypeBase*)nullptr)
{
    __ownsNativeInstance = true;
    if (ReferenceEquals(_0, nullptr))
        throw gcnew ::System::ArgumentNullException("_0", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::DATA_TYPE::InterrogatedModes*)_0->NativePtr;
    NativePtr = new ::KDIS::DATA_TYPE::InterrogatedModes(__arg0);
}

KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::operator KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes^(KDIS7_DLL::KDIS::KDataStream^ stream)
{
    if (ReferenceEquals(stream, nullptr))
        throw gcnew ::System::ArgumentNullException("stream", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::KDataStream*)stream->NativePtr;
    auto __ret = (::KDIS::DATA_TYPE::InterrogatedModes) __arg0;
    auto ____ret = new ::KDIS::DATA_TYPE::InterrogatedModes(__ret);
    return (____ret == nullptr) ? nullptr : gcnew KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes((::KDIS::DATA_TYPE::InterrogatedModes*)____ret);
}

bool KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::Mode1::get()
{
    auto __ret = ((::KDIS::DATA_TYPE::InterrogatedModes*)NativePtr)->GetMode1();
    return __ret;
}

void KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::Mode1::set(bool Mode1)
{
    auto __arg0 = (::KDIS::KBOOL)Mode1;
    ((::KDIS::DATA_TYPE::InterrogatedModes*)NativePtr)->SetMode1(__arg0);
}

bool KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::Mode2::get()
{
    auto __ret = ((::KDIS::DATA_TYPE::InterrogatedModes*)NativePtr)->GetMode2();
    return __ret;
}

void KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::Mode2::set(bool Mode2)
{
    auto __arg0 = (::KDIS::KBOOL)Mode2;
    ((::KDIS::DATA_TYPE::InterrogatedModes*)NativePtr)->SetMode2(__arg0);
}

bool KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::Mode3A::get()
{
    auto __ret = ((::KDIS::DATA_TYPE::InterrogatedModes*)NativePtr)->GetMode3A();
    return __ret;
}

void KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::Mode3A::set(bool Mode3A)
{
    auto __arg0 = (::KDIS::KBOOL)Mode3A;
    ((::KDIS::DATA_TYPE::InterrogatedModes*)NativePtr)->SetMode3A(__arg0);
}

bool KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::Mode4::get()
{
    auto __ret = ((::KDIS::DATA_TYPE::InterrogatedModes*)NativePtr)->GetMode4();
    return __ret;
}

void KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::Mode4::set(bool Mode4)
{
    auto __arg0 = (::KDIS::KBOOL)Mode4;
    ((::KDIS::DATA_TYPE::InterrogatedModes*)NativePtr)->SetMode4(__arg0);
}

bool KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::Mode5::get()
{
    auto __ret = ((::KDIS::DATA_TYPE::InterrogatedModes*)NativePtr)->GetMode5();
    return __ret;
}

void KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::Mode5::set(bool Mode5)
{
    auto __arg0 = (::KDIS::KBOOL)Mode5;
    ((::KDIS::DATA_TYPE::InterrogatedModes*)NativePtr)->SetMode5(__arg0);
}

bool KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::ModeS::get()
{
    auto __ret = ((::KDIS::DATA_TYPE::InterrogatedModes*)NativePtr)->GetModeS();
    return __ret;
}

void KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::ModeS::set(bool ModeS)
{
    auto __arg0 = (::KDIS::KBOOL)ModeS;
    ((::KDIS::DATA_TYPE::InterrogatedModes*)NativePtr)->SetModeS(__arg0);
}

bool KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::ModeCAlt::get()
{
    auto __ret = ((::KDIS::DATA_TYPE::InterrogatedModes*)NativePtr)->GetModeCAlt();
    return __ret;
}

void KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::ModeCAlt::set(bool ModeCAlt)
{
    auto __arg0 = (::KDIS::KBOOL)ModeCAlt;
    ((::KDIS::DATA_TYPE::InterrogatedModes*)NativePtr)->SetModeCAlt(__arg0);
}

bool KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::TCAS_ACAS::get()
{
    auto __ret = ((::KDIS::DATA_TYPE::InterrogatedModes*)NativePtr)->GetTCAS_ACAS();
    return __ret;
}

void KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::TCAS_ACAS::set(bool TCAS_ACAS)
{
    auto __arg0 = (::KDIS::KBOOL)TCAS_ACAS;
    ((::KDIS::DATA_TYPE::InterrogatedModes*)NativePtr)->SetTCAS_ACAS(__arg0);
}

bool KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::SpecialIP::get()
{
    auto __ret = ((::KDIS::DATA_TYPE::InterrogatedModes*)NativePtr)->GetSpecialIP();
    return __ret;
}

void KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::SpecialIP::set(bool SpecialIP)
{
    auto __arg0 = (::KDIS::KBOOL)SpecialIP;
    ((::KDIS::DATA_TYPE::InterrogatedModes*)NativePtr)->SetSpecialIP(__arg0);
}

bool KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::Squitter::get()
{
    auto __ret = ((::KDIS::DATA_TYPE::InterrogatedModes*)NativePtr)->GetSquitter();
    return __ret;
}

void KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::Squitter::set(bool Squitter)
{
    auto __arg0 = (::KDIS::KBOOL)Squitter;
    ((::KDIS::DATA_TYPE::InterrogatedModes*)NativePtr)->SetSquitter(__arg0);
}

System::String^ KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::AsString::get()
{
    auto __ret = ((::KDIS::DATA_TYPE::InterrogatedModes*)NativePtr)->GetAsString();
    if (__ret == nullptr) return nullptr;
    return (__ret == 0 ? nullptr : clix::marshalString<clix::E_UTF8>(__ret));
}

KDIS7_DLL::KDIS::KDataStream^ KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::EncodedStream::get()
{
    auto __ret = ((::KDIS::DATA_TYPE::InterrogatedModes*)NativePtr)->GetEncodedStream();
    if (__ret == nullptr) return nullptr;
    return (__ret == nullptr) ? nullptr : gcnew KDIS7_DLL::KDIS::KDataStream((::KDIS::KDataStream*)__ret);
}

unsigned short KDIS7_DLL::KDIS::DATA_TYPE::InterrogatedModes::INTERROGATED_MODES_SIZE::get()
{
    return ::KDIS::DATA_TYPE::InterrogatedModes::INTERROGATED_MODES_SIZE;
}

