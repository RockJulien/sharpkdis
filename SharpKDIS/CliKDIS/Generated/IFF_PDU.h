// ----------------------------------------------------------------------------
// <auto-generated>
// This is autogenerated code by CppSharp.
// Do not edit this file or all your changes will be lost after re-generation.
// </auto-generated>
// ----------------------------------------------------------------------------
#pragma once

#include "CppSharp.h"
#include <C:/ND3/DXT/Dev/SharpKDIS/CppKDIS/include/KDIS/PDU/Distributed_Emission_Regeneration/IFF_PDU.h>
#include "Header7.h"

namespace KDIS7_DLL
{
    namespace KDIS
    {
        ref class KDataStream;
        namespace DATA_TYPE
        {
            ref class EntityIdentifier;
            ref class FundamentalOperationalData;
            ref class LayerHeader;
            ref class SystemIdentifier;
            ref class Vector;
        }

        namespace PDU
        {
            ref class IFF_PDU;
        }
    }
}

namespace KDIS7_DLL
{
    namespace KDIS
    {
        namespace PDU
        {
            public ref class IFF_PDU : KDIS7_DLL::KDIS::PDU::Header7
            {
            public:

                IFF_PDU(::KDIS::PDU::IFF_PDU* native);
                static IFF_PDU^ __CreateInstance(::System::IntPtr native);
                IFF_PDU();

                IFF_PDU(KDIS7_DLL::KDIS::PDU::Header7^ H);

                IFF_PDU(KDIS7_DLL::KDIS::KDataStream^ stream);

                IFF_PDU(KDIS7_DLL::KDIS::PDU::Header7^ H, KDIS7_DLL::KDIS::KDataStream^ stream);

                IFF_PDU(KDIS7_DLL::KDIS::DATA_TYPE::EntityIdentifier^ EmittingID, KDIS7_DLL::KDIS::DATA_TYPE::EntityIdentifier^ EventID, KDIS7_DLL::KDIS::DATA_TYPE::Vector^ Location, KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier^ ID, KDIS7_DLL::KDIS::DATA_TYPE::FundamentalOperationalData^ FOD);

                IFF_PDU(KDIS7_DLL::KDIS::PDU::IFF_PDU^ _0);

                ~IFF_PDU();

                property KDIS7_DLL::KDIS::DATA_TYPE::EntityIdentifier^ EmittingEntityID
                {
                    KDIS7_DLL::KDIS::DATA_TYPE::EntityIdentifier^ get();
                    void set(KDIS7_DLL::KDIS::DATA_TYPE::EntityIdentifier^);
                }

                property KDIS7_DLL::KDIS::DATA_TYPE::EntityIdentifier^ EventID
                {
                    KDIS7_DLL::KDIS::DATA_TYPE::EntityIdentifier^ get();
                    void set(KDIS7_DLL::KDIS::DATA_TYPE::EntityIdentifier^);
                }

                property KDIS7_DLL::KDIS::DATA_TYPE::Vector^ Location
                {
                    KDIS7_DLL::KDIS::DATA_TYPE::Vector^ get();
                    void set(KDIS7_DLL::KDIS::DATA_TYPE::Vector^);
                }

                property KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier^ SystemIdentifier
                {
                    KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier^ get();
                    void set(KDIS7_DLL::KDIS::DATA_TYPE::SystemIdentifier^);
                }

                property KDIS7_DLL::KDIS::DATA_TYPE::FundamentalOperationalData^ FundamentalOperationalData
                {
                    KDIS7_DLL::KDIS::DATA_TYPE::FundamentalOperationalData^ get();
                    void set(KDIS7_DLL::KDIS::DATA_TYPE::FundamentalOperationalData^);
                }

                property unsigned char SystemDesignator
                {
                    unsigned char get();
                    void set(unsigned char);
                }

                property unsigned char SystemSpecificData
                {
                    unsigned char get();
                    void set(unsigned char);
                }

                property System::Collections::Generic::List<KDIS7_DLL::KDIS::DATA_TYPE::LayerHeader^>^ Layers
                {
                    System::Collections::Generic::List<KDIS7_DLL::KDIS::DATA_TYPE::LayerHeader^>^ get();
                    void set(System::Collections::Generic::List<KDIS7_DLL::KDIS::DATA_TYPE::LayerHeader^>^);
                }

                property System::String^ AsString
                {
                    System::String^ get();
                }

                property int LayerCount
                {
                    int get();
                }

                void AddLayer(KDIS7_DLL::KDIS::DATA_TYPE::LayerHeader^ L);

                KDIS7_DLL::KDIS::DATA_TYPE::LayerHeader^ GetLayer(int pIndex);

                void ClearLayers();

                virtual void Decode(KDIS7_DLL::KDIS::KDataStream^ stream, bool ignoreHeader) override;

                virtual void EncodeToStream(KDIS7_DLL::KDIS::KDataStream^ stream) override;

                static bool operator==(KDIS7_DLL::KDIS::PDU::IFF_PDU^ __op, KDIS7_DLL::KDIS::PDU::IFF_PDU^ Value);

                virtual bool Equals(::System::Object^ obj) override;

                static bool operator!=(KDIS7_DLL::KDIS::PDU::IFF_PDU^ __op, KDIS7_DLL::KDIS::PDU::IFF_PDU^ Value);

                static operator KDIS7_DLL::KDIS::PDU::IFF_PDU^(KDIS7_DLL::KDIS::KDataStream^ stream);

                static property unsigned short IFF_PDU_SIZE
                {
                    unsigned short get();
                }
            };
        }
    }
}
