// ----------------------------------------------------------------------------
// <auto-generated>
// This is autogenerated code by CppSharp.
// Do not edit this file or all your changes will be lost after re-generation.
// </auto-generated>
// ----------------------------------------------------------------------------
#pragma once

#include "CppSharp.h"
#include <C:/ND3/DXT/Dev/SharpKDIS/CppKDIS/include/KDIS/DataTypes/IFF_Layer4.h>
#include "LayerHeader.h"

namespace KDIS7_DLL
{
    namespace KDIS
    {
        namespace DATA_TYPE
        {
            ref class IFF_Layer4;
            ref class SimulationIdentifier;
            ref class StandardVariable;
        }
    }
}

namespace KDIS7_DLL
{
    namespace KDIS
    {
        namespace DATA_TYPE
        {
            public ref class IFF_Layer4 : KDIS7_DLL::KDIS::DATA_TYPE::LayerHeader
            {
            public:

                IFF_Layer4(::KDIS::DATA_TYPE::IFF_Layer4* native);
                static IFF_Layer4^ __CreateInstance(::System::IntPtr native);
                IFF_Layer4();

                IFF_Layer4(KDIS7_DLL::KDIS::DATA_TYPE::LayerHeader^ H);

                IFF_Layer4(KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4^ _0);

                ~IFF_Layer4();

                property KDIS7_DLL::KDIS::DATA_TYPE::SimulationIdentifier^ ReportingSimulation
                {
                    KDIS7_DLL::KDIS::DATA_TYPE::SimulationIdentifier^ get();
                    void set(KDIS7_DLL::KDIS::DATA_TYPE::SimulationIdentifier^);
                }

                property System::Collections::Generic::List<KDIS7_DLL::KDIS::DATA_TYPE::StandardVariable^>^ DataRecords
                {
                    System::Collections::Generic::List<KDIS7_DLL::KDIS::DATA_TYPE::StandardVariable^>^ get();
                    void set(System::Collections::Generic::List<KDIS7_DLL::KDIS::DATA_TYPE::StandardVariable^>^);
                }

                property unsigned short NumberDataRecords
                {
                    unsigned short get();
                }

                void AddDataRecord(KDIS7_DLL::KDIS::DATA_TYPE::StandardVariable^ DR);

                void ClearDataRecords();

                static bool operator==(KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4^ __op, KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4^ Value);

                virtual bool Equals(::System::Object^ obj) override;

                static bool operator!=(KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4^ __op, KDIS7_DLL::KDIS::DATA_TYPE::IFF_Layer4^ Value);

                static property unsigned short IFF_LAYER4SIZE
                {
                    unsigned short get();
                }
            };
        }
    }
}
