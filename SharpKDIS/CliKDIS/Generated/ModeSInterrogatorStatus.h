// ----------------------------------------------------------------------------
// <auto-generated>
// This is autogenerated code by CppSharp.
// Do not edit this file or all your changes will be lost after re-generation.
// </auto-generated>
// ----------------------------------------------------------------------------
#pragma once

#include "CppSharp.h"
#include <C:/ND3/DXT/Dev/SharpKDIS/CppKDIS/include/KDIS/DataTypes/ModeSInterrogatorStatus.h>
#include "DataTypeBase.h"

namespace KDIS7_DLL
{
    namespace KDIS
    {
        ref class KDataStream;
        namespace DATA_TYPE
        {
            ref class ModeSInterrogatorStatus;
            namespace ENUMS
            {
                enum struct TransmitStateModeS;
            }
        }
    }
}

namespace KDIS7_DLL
{
    namespace KDIS
    {
        namespace DATA_TYPE
        {
            public ref class ModeSInterrogatorStatus : KDIS7_DLL::KDIS::DATA_TYPE::DataTypeBase
            {
            public:

                ModeSInterrogatorStatus(::KDIS::DATA_TYPE::ModeSInterrogatorStatus* native);
                static ModeSInterrogatorStatus^ __CreateInstance(::System::IntPtr native);
                ModeSInterrogatorStatus();

                ModeSInterrogatorStatus(KDIS7_DLL::KDIS::KDataStream^ stream);

                ModeSInterrogatorStatus(bool Status, KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::TransmitStateModeS TS, bool Dmg, bool Malfnc);

                ModeSInterrogatorStatus(KDIS7_DLL::KDIS::DATA_TYPE::ModeSInterrogatorStatus^ _0);

                ~ModeSInterrogatorStatus();

                property bool Status
                {
                    bool get();
                    void set(bool);
                }

                property KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::TransmitStateModeS TransmitState
                {
                    KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::TransmitStateModeS get();
                    void set(KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::TransmitStateModeS);
                }

                property System::String^ AsString
                {
                    System::String^ get();
                }

                property KDIS7_DLL::KDIS::KDataStream^ EncodedStream
                {
                    KDIS7_DLL::KDIS::KDataStream^ get();
                }

                property bool IsDamaged
                {
                    bool get();
                }

                property bool IsMalfunctioning
                {
                    bool get();
                }

                void SetDamaged(bool D);

                void SetMalfunctioning(bool M);

                virtual void Decode(KDIS7_DLL::KDIS::KDataStream^ stream) override;

                virtual void EncodeToStream(KDIS7_DLL::KDIS::KDataStream^ stream) override;

                static bool operator==(KDIS7_DLL::KDIS::DATA_TYPE::ModeSInterrogatorStatus^ __op, KDIS7_DLL::KDIS::DATA_TYPE::ModeSInterrogatorStatus^ Value);

                virtual bool Equals(::System::Object^ obj) override;

                static bool operator!=(KDIS7_DLL::KDIS::DATA_TYPE::ModeSInterrogatorStatus^ __op, KDIS7_DLL::KDIS::DATA_TYPE::ModeSInterrogatorStatus^ Value);

                static operator KDIS7_DLL::KDIS::DATA_TYPE::ModeSInterrogatorStatus^(KDIS7_DLL::KDIS::KDataStream^ stream);

                static property unsigned short MODE_S_INTERROGATOR_STATUS_SIZE
                {
                    unsigned short get();
                }
            };
        }
    }
}
