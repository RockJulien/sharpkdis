// ----------------------------------------------------------------------------
// <auto-generated>
// This is autogenerated code by CppSharp.
// Do not edit this file or all your changes will be lost after re-generation.
// </auto-generated>
// ----------------------------------------------------------------------------
#pragma once

#include "CppSharp.h"
#include <C:/ND3/DXT/Dev/SharpKDIS/CppKDIS/include/KDIS/DataTypes/Enums/EnumHeader.h>

namespace KDIS7_DLL
{
    namespace KDIS
    {
        namespace DATA_TYPE
        {
            namespace ENUMS
            {
                enum struct LVCIndicator;
                enum struct PDUType;
                enum struct ProtocolFamily;
                enum struct ProtocolVersion;
                enum struct TimeStampType;
                ref class EnumDescriptor;
            }
        }
    }
}

namespace KDIS7_DLL
{
    namespace KDIS
    {
        namespace DATA_TYPE
        {
            namespace ENUMS
            {
                /// <summary>*********************************************************************</summary>
                public enum struct ProtocolVersion
                {
                    OtherPV = 0,
                    DIS_PDU_Version1 = 1,
                    IEEE_1278_1993 = 2,
                    DIS_PDU_Version2ThirdDraft = 3,
                    DIS_PDU_Version2FourthDraft = 4,
                    IEEE_1278_1_1995 = 5,
                    IEEE_1278_1A_1998 = 6,
                    IEEE_1278_1_2012 = 7
                };

                /// <summary>*********************************************************************</summary>
                public enum struct PDUType
                {
                    OtherPDU_Type = 0,
                    EntityStatePDU_Type = 1,
                    FirePDU_Type = 2,
                    DetonationPDU_Type = 3,
                    CollisionPDU_Type = 4,
                    ServiceRequestPDU_Type = 5,
                    ResupplyOfferPDU_Type = 6,
                    ResupplyReceivedPDU_Type = 7,
                    ResupplyCancelPDU_Type = 8,
                    RepairCompletePDU_Type = 9,
                    RepairResponsePDU_Type = 10,
                    CreateEntityPDU_Type = 11,
                    RemoveEntityPDU_Type = 12,
                    StartResumePDU_Type = 13,
                    StopFreezePDU_Type = 14,
                    AcknowledgePDU_Type = 15,
                    ActionRequestPDU_Type = 16,
                    ActionResponsePDU_Type = 17,
                    DataQueryPDU_Type = 18,
                    SetDataPDU_Type = 19,
                    DataPDU_Type = 20,
                    EventReportPDU_Type = 21,
                    MessagePDU_Type = 22,
                    ElectromagneticEmissionPDU_Type = 23,
                    DesignatorPDU_Type = 24,
                    TransmitterPDU_Type = 25,
                    SignalPDU_Type = 26,
                    ReceiverPDU_Type = 27,
                    IFF_ATC_NAVAIDS_PDU_Type = 28,
                    UnderwaterAcousticPDU_Type = 29,
                    SupplementalEmissionEntityStatePDU_Type = 30,
                    IntercomSignalPDU_Type = 31,
                    IntercomControlPDU_Type = 32,
                    AggregateStatePDU_Type = 33,
                    IsGroupOfPDU_Type = 34,
                    TransferControlPDU_Type = 35,
                    IsPartOfPDU_Type = 36,
                    MinefieldStatePDU_Type = 37,
                    MinefieldQueryPDU_Type = 38,
                    MinefieldDataPDU_Type = 39,
                    MinefieldResponseNAK_PDU_Type = 40,
                    EnvironmentalProcessPDU_Type = 41,
                    GriddedDataPDU_Type = 42,
                    PointObjectStatePDU_Type = 43,
                    LinearObjectStatePDU_Type = 44,
                    ArealObjectStatePDU_Type = 45,
                    TSPI_PDU_Type = 46,
                    AppearancePDU_Type = 47,
                    ArticulatedPartsPDU_Type = 48,
                    LEFirePDU_Type = 49,
                    LEDetonationPDU_Type = 50,
                    CreateEntityR_PDU_Type = 51,
                    RemoveEntityR_PDU_Type = 52,
                    StartResumeR_PDU_Type = 53,
                    StopFreezeR_PDU_Type = 54,
                    AcknowledgeR_PDU_Type = 55,
                    ActionRequestR_PDU_Type = 56,
                    ActionResponseR_PDU_Type = 57,
                    DataQueryR_PDU_Type = 58,
                    SetDataR_PDU_Type = 59,
                    DataR_PDU_Type = 60,
                    EventReportR_PDU_Type = 61,
                    CommentR_PDU_Type = 62,
                    RecordR_PDU_Type = 63,
                    SetRecordR_PDU_Type = 64,
                    RecordQueryR_PDU_Type = 65,
                    CollisionElasticPDU_Type = 66,
                    EntityStateUpdatePDU_Type = 67,
                    DirectedEnergyFirePDU_Type = 68,
                    EntityDamageStatusPDU_Type = 69,
                    IO_ActionPDU_Type = 70,
                    IO_ReportPDU_Type = 71,
                    AttributePDU_Type = 72,
                    AnnounceObjectPDU_Type = 129,
                    DeleteObjectPDU_Type = 130,
                    DescribeEventPDU_Type = 132,
                    DescribeObjectPDU_Type = 133,
                    RequestEventPDU_Type = 134,
                    RequestObjectPDU_Type = 135
                };

                /// <summary>*********************************************************************</summary>
                public enum struct ProtocolFamily
                {
                    OtherPFF = 0,
                    EntityInformationInteraction = 1,
                    Warfare = 2,
                    Logistics = 3,
                    RadioCommunications = 4,
                    SimulationManagement = 5,
                    DistributedEmissionRegeneration = 6,
                    EntityManagement = 7,
                    Minefield = 8,
                    SyntheticEnvironment = 9,
                    SimulationManagementwithReliability = 10,
                    LiveEntity = 11,
                    NonRealTime = 12,
                    InformationOperations = 13,
                    ExperimentalCGF = 129,
                    ExperimentalEntityInfomationFieldInstrumentation = 130,
                    ExperimentalWarfareFieldInstrumentation = 131,
                    ExperimentalEnvironmentObjectInfomationInteraction = 132,
                    ExperimentalEntityManagement = 133
                };

                /// <summary>*********************************************************************</summary>
                public enum struct TimeStampType
                {
                    RelativeTime = 0,
                    AbsoluteTime = 1
                };

                /// <summary>*********************************************************************</summary>
                public enum struct LVCIndicator
                {
                    NoStatementLVC = 0,
                    LiveLVC = 1,
                    VirtualLVC = 2,
                    ConstructiveLVC = 3
                };

                public ref class EnumHeader
                {
                public:
                    static unsigned int GetEnumSizeProtocolVersion();
                    static KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::EnumDescriptor^ GetEnumDescriptorProtocolVersion(unsigned int Index);
                    static System::String^ GetEnumAsStringProtocolVersion(int Value);
                    static bool GetEnumFromStringProtocolVersion(System::String^ Value, [System::Runtime::InteropServices::In, System::Runtime::InteropServices::Out] int% ValueOut);
                    static unsigned int GetEnumSizePDUType();
                    static KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::EnumDescriptor^ GetEnumDescriptorPDUType(unsigned int Index);
                    static System::String^ GetEnumAsStringPDUType(int Value);
                    static bool GetEnumFromStringPDUType(System::String^ Value, [System::Runtime::InteropServices::In, System::Runtime::InteropServices::Out] int% ValueOut);
                    static unsigned int GetEnumSizeProtocolFamily();
                    static KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::EnumDescriptor^ GetEnumDescriptorProtocolFamily(unsigned int Index);
                    static System::String^ GetEnumAsStringProtocolFamily(int Value);
                    static bool GetEnumFromStringProtocolFamily(System::String^ Value, [System::Runtime::InteropServices::In, System::Runtime::InteropServices::Out] int% ValueOut);
                    static unsigned int GetEnumSizeTimeStampType();
                    static KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::EnumDescriptor^ GetEnumDescriptorTimeStampType(unsigned int Index);
                    static System::String^ GetEnumAsStringTimeStampType(int Value);
                    static bool GetEnumFromStringTimeStampType(System::String^ Value, [System::Runtime::InteropServices::In, System::Runtime::InteropServices::Out] int% ValueOut);
                    static unsigned int GetEnumSizeLVCIndicator();
                    static KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::EnumDescriptor^ GetEnumDescriptorLVCIndicator(unsigned int Index);
                    static System::String^ GetEnumAsStringLVCIndicator(int Value);
                    static bool GetEnumFromStringLVCIndicator(System::String^ Value, [System::Runtime::InteropServices::In, System::Runtime::InteropServices::Out] int% ValueOut);
                };
            }
        }
    }
}
