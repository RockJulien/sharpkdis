// ----------------------------------------------------------------------------
// <auto-generated>
// This is autogenerated code by CppSharp.
// Do not edit this file or all your changes will be lost after re-generation.
// </auto-generated>
// ----------------------------------------------------------------------------
#pragma once

#include "CppSharp.h"
#include <C:/ND3/DXT/Dev/SharpKDIS/CppKDIS/include/KDIS/Extras/PDU_Factory_Filters.h>

namespace KDIS7_DLL
{
    namespace KDIS
    {
        namespace UTILS
        {
            ref class FactoryFilterExerciseID;
            ref class PDU_FactoryFilter;
        }

        namespace PDU
        {
            ref class Header7;
        }
    }
}

namespace KDIS7_DLL
{
    namespace KDIS
    {
        namespace PDU
        {
        }

        namespace UTILS
        {
            /// <summary>///////////////////////////////////////////////////////////////////////</summary>
            public ref class PDU_FactoryFilter : ICppInstance
            {
            public:

                [System::Runtime::InteropServices::UnmanagedFunctionPointer(System::Runtime::InteropServices::CallingConvention::ThisCall)] 
                delegate bool Func_bool_IntPtr_IntPtr(::System::IntPtr _0, KDIS7_DLL::KDIS::PDU::Header7^ _1);

                property ::KDIS::UTILS::PDU_Factory_Filter* NativePtr;
                property System::IntPtr __Instance
                {
                    virtual System::IntPtr get();
                    virtual void set(System::IntPtr instance);
                }

                PDU_FactoryFilter(::KDIS::UTILS::PDU_Factory_Filter* native);
                static PDU_FactoryFilter^ __CreateInstance(::System::IntPtr native);
                PDU_FactoryFilter();

                PDU_FactoryFilter(KDIS7_DLL::KDIS::UTILS::PDU_FactoryFilter^ _0);

                ~PDU_FactoryFilter();

                virtual bool ApplyFilterBeforeDecodingPDUBody(KDIS7_DLL::KDIS::PDU::Header7^ H);

                virtual bool ApplyFilter(KDIS7_DLL::KDIS::PDU::Header7^ H);

            protected:
                bool __ownsNativeInstance;
            };

            /// <summary>///////////////////////////////////////////////////////////////////////</summary>
            public ref class FactoryFilterExerciseID : KDIS7_DLL::KDIS::UTILS::PDU_FactoryFilter
            {
            public:

                FactoryFilterExerciseID(::KDIS::UTILS::FactoryFilterExerciseID* native);
                static FactoryFilterExerciseID^ __CreateInstance(::System::IntPtr native);
                FactoryFilterExerciseID(unsigned char ID);

                FactoryFilterExerciseID(KDIS7_DLL::KDIS::UTILS::FactoryFilterExerciseID^ _0);

                ~FactoryFilterExerciseID();

                virtual bool ApplyFilter(KDIS7_DLL::KDIS::PDU::Header7^ H) override;

                static operator KDIS7_DLL::KDIS::UTILS::FactoryFilterExerciseID^(unsigned char ID);
            };
        }
    }
}
