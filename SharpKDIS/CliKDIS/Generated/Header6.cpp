// ----------------------------------------------------------------------------
// <auto-generated>
// This is autogenerated code by CppSharp.
// Do not edit this file or all your changes will be lost after re-generation.
// </auto-generated>
// ----------------------------------------------------------------------------
#include "Header6.h"
#include "EnumHeader.h"
#include "KDataStream.h"
#include "TimeStamp.h"

using namespace System;
using namespace System::Runtime::InteropServices;

KDIS7_DLL::KDIS::PDU::Header6::Header6(::KDIS::PDU::Header6* native)
    : __ownsNativeInstance(false)
{
    NativePtr = native;
}

KDIS7_DLL::KDIS::PDU::Header6^ KDIS7_DLL::KDIS::PDU::Header6::__CreateInstance(::System::IntPtr native)
{
    return gcnew ::KDIS7_DLL::KDIS::PDU::Header6((::KDIS::PDU::Header6*) native.ToPointer());
}

KDIS7_DLL::KDIS::PDU::Header6::~Header6()
{
    delete NativePtr;
}

KDIS7_DLL::KDIS::PDU::Header6::Header6()
{
    __ownsNativeInstance = true;
    NativePtr = new ::KDIS::PDU::Header6();
}

KDIS7_DLL::KDIS::PDU::Header6::Header6(KDIS7_DLL::KDIS::KDataStream^ stream)
{
    __ownsNativeInstance = true;
    if (ReferenceEquals(stream, nullptr))
        throw gcnew ::System::ArgumentNullException("stream", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::KDataStream*)stream->NativePtr;
    NativePtr = new ::KDIS::PDU::Header6(__arg0);
}

KDIS7_DLL::KDIS::PDU::Header6::Header6(KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::ProtocolVersion PV, unsigned char ExerciseID, KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::PDUType PT, KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::ProtocolFamily PF, KDIS7_DLL::KDIS::DATA_TYPE::TimeStamp^ TS, unsigned short PDULength)
{
    __ownsNativeInstance = true;
    auto __arg0 = (::KDIS::DATA_TYPE::ENUMS::ProtocolVersion)PV;
    auto __arg1 = (::KDIS::KUINT8)ExerciseID;
    auto __arg2 = (::KDIS::DATA_TYPE::ENUMS::PDUType)PT;
    auto __arg3 = (::KDIS::DATA_TYPE::ENUMS::ProtocolFamily)PF;
    if (ReferenceEquals(TS, nullptr))
        throw gcnew ::System::ArgumentNullException("TS", "Cannot be null because it is a C++ reference (&).");
    auto &__arg4 = *(::KDIS::DATA_TYPE::TimeStamp*)TS->NativePtr;
    auto __arg5 = (::KDIS::KUINT16)PDULength;
    NativePtr = new ::KDIS::PDU::Header6(__arg0, __arg1, __arg2, __arg3, __arg4, __arg5);
}

void KDIS7_DLL::KDIS::PDU::Header6::Decode(KDIS7_DLL::KDIS::KDataStream^ stream, bool ignoreHeader)
{
    if (ReferenceEquals(stream, nullptr))
        throw gcnew ::System::ArgumentNullException("stream", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::KDataStream*)stream->NativePtr;
    ((::KDIS::PDU::Header6*)NativePtr)->Decode(__arg0, ignoreHeader);
}

void KDIS7_DLL::KDIS::PDU::Header6::EncodeToStream(KDIS7_DLL::KDIS::KDataStream^ stream)
{
    if (ReferenceEquals(stream, nullptr))
        throw gcnew ::System::ArgumentNullException("stream", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::KDataStream*)stream->NativePtr;
    ((::KDIS::PDU::Header6*)NativePtr)->EncodeToStream(__arg0);
}

bool KDIS7_DLL::KDIS::PDU::Header6::operator==(KDIS7_DLL::KDIS::PDU::Header6^ __op, KDIS7_DLL::KDIS::PDU::Header6^ Value)
{
    bool __opNull = ReferenceEquals(__op, nullptr);
    bool ValueNull = ReferenceEquals(Value, nullptr);
    if (__opNull || ValueNull)
        return __opNull && ValueNull;
    auto &__arg0 = *(::KDIS::PDU::Header6*)__op->NativePtr;
    auto &__arg1 = *(::KDIS::PDU::Header6*)Value->NativePtr;
    auto __ret = __arg0 == __arg1;
    return __ret;
}

bool KDIS7_DLL::KDIS::PDU::Header6::Equals(::System::Object^ obj)
{
    return this == safe_cast<KDIS7_DLL::KDIS::PDU::Header6^>(obj);
}

bool KDIS7_DLL::KDIS::PDU::Header6::operator!=(KDIS7_DLL::KDIS::PDU::Header6^ __op, KDIS7_DLL::KDIS::PDU::Header6^ Value)
{
    bool __opNull = ReferenceEquals(__op, nullptr);
    bool ValueNull = ReferenceEquals(Value, nullptr);
    if (__opNull || ValueNull)
        return !(__opNull && ValueNull);
    auto &__arg0 = *(::KDIS::PDU::Header6*)__op->NativePtr;
    auto &__arg1 = *(::KDIS::PDU::Header6*)Value->NativePtr;
    auto __ret = __arg0 != __arg1;
    return __ret;
}

KDIS7_DLL::KDIS::PDU::Header6::Header6(KDIS7_DLL::KDIS::PDU::Header6^ _0)
{
    __ownsNativeInstance = true;
    if (ReferenceEquals(_0, nullptr))
        throw gcnew ::System::ArgumentNullException("_0", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::PDU::Header6*)_0->NativePtr;
    NativePtr = new ::KDIS::PDU::Header6(__arg0);
}

KDIS7_DLL::KDIS::PDU::Header6::operator KDIS7_DLL::KDIS::PDU::Header6^(KDIS7_DLL::KDIS::KDataStream^ stream)
{
    if (ReferenceEquals(stream, nullptr))
        throw gcnew ::System::ArgumentNullException("stream", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::KDataStream*)stream->NativePtr;
    auto __ret = (::KDIS::PDU::Header6) __arg0;
    auto ____ret = new ::KDIS::PDU::Header6(__ret);
    return (____ret == nullptr) ? nullptr : gcnew KDIS7_DLL::KDIS::PDU::Header6((::KDIS::PDU::Header6*)____ret);
}

System::IntPtr KDIS7_DLL::KDIS::PDU::Header6::__Instance::get()
{
    return System::IntPtr(NativePtr);
}

void KDIS7_DLL::KDIS::PDU::Header6::__Instance::set(System::IntPtr object)
{
    NativePtr = (::KDIS::PDU::Header6*)object.ToPointer();
}

KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::ProtocolVersion KDIS7_DLL::KDIS::PDU::Header6::ProtocolVersion::get()
{
    auto __ret = ((::KDIS::PDU::Header6*)NativePtr)->GetProtocolVersion();
    return (KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::ProtocolVersion)__ret;
}

void KDIS7_DLL::KDIS::PDU::Header6::ProtocolVersion::set(KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::ProtocolVersion PV)
{
    auto __arg0 = (::KDIS::DATA_TYPE::ENUMS::ProtocolVersion)PV;
    ((::KDIS::PDU::Header6*)NativePtr)->SetProtocolVersion(__arg0);
}

unsigned char KDIS7_DLL::KDIS::PDU::Header6::ExerciseID::get()
{
    auto __ret = ((::KDIS::PDU::Header6*)NativePtr)->GetExerciseID();
    return __ret;
}

void KDIS7_DLL::KDIS::PDU::Header6::ExerciseID::set(unsigned char EID)
{
    auto __arg0 = (::KDIS::KUINT8)EID;
    ((::KDIS::PDU::Header6*)NativePtr)->SetExerciseID(__arg0);
}

KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::PDUType KDIS7_DLL::KDIS::PDU::Header6::PDUType::get()
{
    auto __ret = ((::KDIS::PDU::Header6*)NativePtr)->GetPDUType();
    return (KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::PDUType)__ret;
}

void KDIS7_DLL::KDIS::PDU::Header6::PDUType::set(KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::PDUType Type)
{
    auto __arg0 = (::KDIS::DATA_TYPE::ENUMS::PDUType)Type;
    ((::KDIS::PDU::Header6*)NativePtr)->SetPDUType(__arg0);
}

KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::ProtocolFamily KDIS7_DLL::KDIS::PDU::Header6::ProtocolFamily::get()
{
    auto __ret = ((::KDIS::PDU::Header6*)NativePtr)->GetProtocolFamily();
    return (KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::ProtocolFamily)__ret;
}

void KDIS7_DLL::KDIS::PDU::Header6::ProtocolFamily::set(KDIS7_DLL::KDIS::DATA_TYPE::ENUMS::ProtocolFamily PF)
{
    auto __arg0 = (::KDIS::DATA_TYPE::ENUMS::ProtocolFamily)PF;
    ((::KDIS::PDU::Header6*)NativePtr)->SetProtocolFamily(__arg0);
}

KDIS7_DLL::KDIS::DATA_TYPE::TimeStamp^ KDIS7_DLL::KDIS::PDU::Header6::TimeStamp::get()
{
    auto &__ret = ((::KDIS::PDU::Header6*)NativePtr)->GetTimeStamp();
    return (KDIS7_DLL::KDIS::DATA_TYPE::TimeStamp^)((&__ret == nullptr) ? nullptr : gcnew KDIS7_DLL::KDIS::DATA_TYPE::TimeStamp((::KDIS::DATA_TYPE::TimeStamp*)&__ret));
}

void KDIS7_DLL::KDIS::PDU::Header6::TimeStamp::set(KDIS7_DLL::KDIS::DATA_TYPE::TimeStamp^ TS)
{
    if (ReferenceEquals(TS, nullptr))
        throw gcnew ::System::ArgumentNullException("TS", "Cannot be null because it is a C++ reference (&).");
    auto &__arg0 = *(::KDIS::DATA_TYPE::TimeStamp*)TS->NativePtr;
    ((::KDIS::PDU::Header6*)NativePtr)->SetTimeStamp(__arg0);
}

unsigned short KDIS7_DLL::KDIS::PDU::Header6::PDULength::get()
{
    auto __ret = ((::KDIS::PDU::Header6*)NativePtr)->GetPDULength();
    return __ret;
}

void KDIS7_DLL::KDIS::PDU::Header6::PDULength::set(unsigned short PDUL)
{
    auto __arg0 = (::KDIS::KUINT16)PDUL;
    ((::KDIS::PDU::Header6*)NativePtr)->SetPDULength(__arg0);
}

System::String^ KDIS7_DLL::KDIS::PDU::Header6::AsString::get()
{
    auto __ret = ((::KDIS::PDU::Header6*)NativePtr)->GetAsString();
    if (__ret == nullptr) return nullptr;
    return (__ret == 0 ? nullptr : clix::marshalString<clix::E_UTF8>(__ret));
}

KDIS7_DLL::KDIS::KDataStream^ KDIS7_DLL::KDIS::PDU::Header6::EncodedStream::get()
{
    auto __ret = ((::KDIS::PDU::Header6*)NativePtr)->GetEncodedStream();
    if (__ret == nullptr) return nullptr;
    return (__ret == nullptr) ? nullptr : gcnew KDIS7_DLL::KDIS::KDataStream((::KDIS::KDataStream*)__ret);
}

unsigned short KDIS7_DLL::KDIS::PDU::Header6::HEADER6PDU_SIZE::get()
{
    return ::KDIS::PDU::Header6::HEADER6_PDU_SIZE;
}

