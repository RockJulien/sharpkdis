// ----------------------------------------------------------------------------
// <auto-generated>
// This is autogenerated code by CppSharp.
// Do not edit this file or all your changes will be lost after re-generation.
// </auto-generated>
// ----------------------------------------------------------------------------
#pragma once

#include "CppSharp.h"
#include <C:/ND3/DXT/Dev/SharpKDIS/CppKDIS/include/KDIS/PDU/Simulation_Management/Data_PDU.h>
#include "Comment_PDU.h"

namespace KDIS7_DLL
{
    namespace KDIS
    {
        ref class KDataStream;
        namespace PDU
        {
            ref class DataPDU;
            ref class Header7;
        }

        namespace DATA_TYPE
        {
            ref class EntityIdentifier;
            ref class VariableDatum;
        }
    }
}

namespace KDIS7_DLL
{
    namespace KDIS
    {
        namespace PDU
        {
            public ref class DataPDU : KDIS7_DLL::KDIS::PDU::CommentPDU
            {
            public:

                DataPDU(::KDIS::PDU::Data_PDU* native);
                static DataPDU^ __CreateInstance(::System::IntPtr native);
                DataPDU();

                DataPDU(KDIS7_DLL::KDIS::PDU::Header7^ H);

                DataPDU(KDIS7_DLL::KDIS::KDataStream^ stream);

                DataPDU(KDIS7_DLL::KDIS::PDU::Header7^ H, KDIS7_DLL::KDIS::KDataStream^ stream);

                DataPDU(KDIS7_DLL::KDIS::DATA_TYPE::EntityIdentifier^ OriginatingEntityID, KDIS7_DLL::KDIS::DATA_TYPE::EntityIdentifier^ ReceivingEntityID, unsigned int RequestID);

                DataPDU(KDIS7_DLL::KDIS::PDU::DataPDU^ _0);

                ~DataPDU();

                property unsigned int RequestID
                {
                    unsigned int get();
                    void set(unsigned int);
                }

                property System::Collections::Generic::List<KDIS7_DLL::KDIS::DATA_TYPE::VariableDatum^>^ VariableDatum
                {
                    System::Collections::Generic::List<KDIS7_DLL::KDIS::DATA_TYPE::VariableDatum^>^ get();
                    void set(System::Collections::Generic::List<KDIS7_DLL::KDIS::DATA_TYPE::VariableDatum^>^);
                }

                property System::String^ AsString
                {
                    System::String^ get();
                }

                virtual void Decode(KDIS7_DLL::KDIS::KDataStream^ stream, bool ignoreHeader) override;

                virtual void EncodeToStream(KDIS7_DLL::KDIS::KDataStream^ stream) override;

                static bool operator==(KDIS7_DLL::KDIS::PDU::DataPDU^ __op, KDIS7_DLL::KDIS::PDU::DataPDU^ Value);

                virtual bool Equals(::System::Object^ obj) override;

                static bool operator!=(KDIS7_DLL::KDIS::PDU::DataPDU^ __op, KDIS7_DLL::KDIS::PDU::DataPDU^ Value);

                static operator KDIS7_DLL::KDIS::PDU::DataPDU^(KDIS7_DLL::KDIS::KDataStream^ stream);

                static property unsigned short DATA_PDU_SIZE
                {
                    unsigned short get();
                }
            };
        }
    }
}
