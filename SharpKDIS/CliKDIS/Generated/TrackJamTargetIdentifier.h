// ----------------------------------------------------------------------------
// <auto-generated>
// This is autogenerated code by CppSharp.
// Do not edit this file or all your changes will be lost after re-generation.
// </auto-generated>
// ----------------------------------------------------------------------------
#pragma once

#include "CppSharp.h"
#include <C:/ND3/DXT/Dev/SharpKDIS/CppKDIS/include/KDIS/DataTypes/TrackJamTargetIdentifier.h>
#include "EntityIdentifier.h"

namespace KDIS7_DLL
{
    namespace KDIS
    {
        ref class KDataStream;
        namespace DATA_TYPE
        {
            ref class TrackJamTargetIdentifier;
        }
    }
}

namespace KDIS7_DLL
{
    namespace KDIS
    {
        namespace DATA_TYPE
        {
            public ref class TrackJamTargetIdentifier : KDIS7_DLL::KDIS::DATA_TYPE::EntityIdentifier
            {
            public:

                TrackJamTargetIdentifier(::KDIS::DATA_TYPE::TrackJamTargetIdentifier* native);
                static TrackJamTargetIdentifier^ __CreateInstance(::System::IntPtr native);
                TrackJamTargetIdentifier();

                TrackJamTargetIdentifier(KDIS7_DLL::KDIS::KDataStream^ stream);

                TrackJamTargetIdentifier(KDIS7_DLL::KDIS::DATA_TYPE::EntityIdentifier^ EntityID);

                TrackJamTargetIdentifier(unsigned short SiteID, unsigned short ApplicatonID, unsigned short EntityID, unsigned char EmitterID, unsigned char BeamID);

                TrackJamTargetIdentifier(KDIS7_DLL::KDIS::DATA_TYPE::TrackJamTargetIdentifier^ _0);

                ~TrackJamTargetIdentifier();

                property unsigned char EmitterID
                {
                    unsigned char get();
                    void set(unsigned char);
                }

                property unsigned char BeamID
                {
                    unsigned char get();
                    void set(unsigned char);
                }

                property System::String^ AsString
                {
                    System::String^ get();
                }

                virtual void Decode(KDIS7_DLL::KDIS::KDataStream^ stream) override;

                virtual void EncodeToStream(KDIS7_DLL::KDIS::KDataStream^ stream) override;

                static bool operator==(KDIS7_DLL::KDIS::DATA_TYPE::TrackJamTargetIdentifier^ __op, KDIS7_DLL::KDIS::DATA_TYPE::TrackJamTargetIdentifier^ Value);

                virtual bool Equals(::System::Object^ obj) override;

                static bool operator!=(KDIS7_DLL::KDIS::DATA_TYPE::TrackJamTargetIdentifier^ __op, KDIS7_DLL::KDIS::DATA_TYPE::TrackJamTargetIdentifier^ Value);

                static operator KDIS7_DLL::KDIS::DATA_TYPE::TrackJamTargetIdentifier^(KDIS7_DLL::KDIS::KDataStream^ stream);

                static property unsigned short TRACK_JAM_TARGET_SIZE
                {
                    unsigned short get();
                }
            };
        }
    }
}
