/********************************************************************
    class:      IFF_Layer4
    DIS:        (7) 1278.1 - 2012
    created:    21/03/2019
    author:     Nicolas Duboys

    purpose:    Base class for Layer 4 Mode S interrogator and transponder functional data.
	            Note: The Layer 1 System Type field isused to determine whether Layer 4
				represents a Mode S capable interrogator or transponder.

    Size:       288 bits / 36 octets - min size
*********************************************************************/

#pragma once

#include "./LayerHeader.h"
#include "./SimulationIdentifier.h"
#include "./StandardVariable.h"

namespace KDIS {
namespace DATA_TYPE {

class KDIS_EXPORT IFF_Layer4 : public LayerHeader
{
protected:

	SimulationIdentifier m_RptSim;

	// Transponder/interrogator in the inherited classes.

	KUINT16 m_ui16Padding;
		
	KUINT16 m_ui16NumIffRecs;

	std::vector<KDIS::DATA_TYPE::StdVarPtr> m_vStdVarRecs;

public:

    static const KUINT16 IFF_LAYER4_SIZE = 36; // Min size

    IFF_Layer4();

	IFF_Layer4( const LayerHeader & H );

    virtual ~IFF_Layer4();

    //************************************
    // FullName:    KDIS::DATA_TYPE::IFF_Layer4::SetReportingSimulation
    //              KDIS::DATA_TYPE::IFF_Layer4::GetReportingSimulation
    // Description: The simulation reporting this IFF PDU.
    // Parameter:   const SimulationIdentifier & RS
    //************************************
    void SetReportingSimulation( const SimulationIdentifier & RS );
    const SimulationIdentifier & GetReportingSimulation() const;
    SimulationIdentifier & GetReportingSimulation();
			
    //************************************
    // FullName:    KDIS::DATA_TYPE::IFF_Layer4::GetNumberDataRecords
    // Description: The number of IFF Data records in this PDU.
    //************************************
	KUINT16 GetNumberDataRecords() const;

	//************************************
    // FullName:    KDIS::PDU::IFF_Layer4::AddDataRecord
    //              KDIS::PDU::IFF_Layer4::SetDataRecords
    //              KDIS::PDU::IFF_Layer4::GetDataRecords
    //              KDIS::PDU::IFF_Layer4::ClearDataRecords
    // Description: IFF Data records are used when variable records are required to be included in the layer.
	//              They are identical to Standard Variable Specification record format except that alignment is to
	//              a 32-bit boundary for each IFF Data record instead of to a 64-bit boundary. 
	//              This means the records can not contain 64 bit floats or 64 bit integers.
	//              See B.2.1.1 of IEEE 1278.1-2012 for a full list of supported records (Note: I have not implmented them all yet but do plan to).
    // Parameter:   StdVarPtr DR, const vector<StdVarPtr> & DRS
    //************************************
    void AddDataRecord( StdVarPtr DR );
	void SetDataRecords( const std::vector<StdVarPtr> & DRS );
	const std::vector<StdVarPtr> & GetDataRecords() const;
    void ClearDataRecords();

    KBOOL operator == ( const IFF_Layer4 & Value ) const;
    KBOOL operator != ( const IFF_Layer4 & Value ) const;
};

} // END namespace DATA_TYPE
} // END namespace KDIS
