/********************************************************************
    class:      IFF_Layer4Transponder
    DIS:        (7) 1278.1 - 2012
    created:    21/03/2019
    author:     Nicolas Duboys

    purpose:    Layer 4 Mode S transponder functional data.

    Size:       288 bits / 36 octets - min size
*********************************************************************/

#pragma once

#include "./IFF_Layer4.h"
#include "./SimulationIdentifier.h"
#include "./ModeSTransponderBasicData.h"

namespace KDIS {
namespace DATA_TYPE {

class KDIS_EXPORT IFF_Layer4Transponder : public IFF_Layer4
{
protected:

    ModeSTransponderBasicData m_BasicData;

public:

    IFF_Layer4Transponder();

    IFF_Layer4Transponder( KDataStream & stream ) ;

    IFF_Layer4Transponder( const SimulationIdentifier & ReportingSimulation, const ModeSTransponderBasicData & Data,
                           std::vector<StdVarPtr> & Records );

    IFF_Layer4Transponder( const LayerHeader & H, KDataStream & stream ) ;

    virtual ~IFF_Layer4Transponder();

    //************************************
    // FullName:    KDIS::DATA_TYPE::IFF_Layer4Transponder::SetBasicData
    //              KDIS::DATA_TYPE::IFF_Layer4Transponder::GetBasicData
    // Description: Basic Mode 5 transponder data that is always required to be transmitted.
    // Parameter:   const ModeSTransponderBasicData & BD
    //************************************
    void SetBasicData( const ModeSTransponderBasicData & BD );
    const ModeSTransponderBasicData & GetBasicData() const;
    ModeSTransponderBasicData & GetBasicDatan();

    //************************************
    // FullName:    KDIS::DATA_TYPE::IFF_Layer4Transponder::GetAsString
    // Description: Returns a string representation
    //************************************
    virtual const KOCTET* GetAsString() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::IFF_Layer4Transponder::Decode
    // Description: Convert From Network Data.
    // Parameter:   KDataStream & stream
    // Parameter:   bool ignoreHeader = false - Decode the layer header from the stream?
    //************************************
	virtual void Decode( KDataStream & stream ) ;
    virtual void Decode( KDataStream & stream, bool ignoreHeader = false ) ;

    //************************************
    // FullName:    KDIS::DATA_TYPE::IFF_Layer4Transponder::Encode
    // Description: Convert To Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual void EncodeToStream( KDataStream & stream ) const;

    KBOOL operator == ( const IFF_Layer4Transponder & Value ) const;
    KBOOL operator != ( const IFF_Layer4Transponder & Value ) const;
};

} // END namespace DATA_TYPE
} // END namespace KDIS
