/********************************************************************
    class:      ModeSAltitude
    DIS:        (7) 1278.1 - 2012
    created:    21/03/2019
    author:     Nicolas Duboys

    purpose:    

    Size:       8 bits / 1 octet
*********************************************************************/

#pragma once

#include "./DataTypeBase.h"

namespace KDIS {
namespace DATA_TYPE {

class KDIS_EXPORT ModeSAltitude : public DataTypeBase
{
protected:

	union
	{
		struct
		{
			KUINT16 m_ui16Altitude                   : 11;
			KUINT16 m_ui16AltitudeResolution          : 1;
			KUINT16 m_ui16Padding                     : 4;
		};
		KUINT16 m_ui16ModeSAltitude;

	} m_ModeSAltitudeUnion;

	mutable KDataStream m_stream;
		
public:

    static const KUINT16 MODE_S_ALTITUDE_SIZE = 1; 

    ModeSAltitude();

    ModeSAltitude( KDataStream & stream ) ;

	ModeSAltitude( KUINT16 Altitude, KUINT16 AltitudeResolution );
	
    virtual ~ModeSAltitude();

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSAltitude::SetAltitude
    //              KDIS::DATA_TYPE::ModeSAltitude::GetAltitude
    // Description: Indicate the Mode S altitude. It shall be either set to a value that
    //              indicates to use entity truth altitude (2047 decimal), to compute 
    //              the received Mode S altitude, or may contain the actual Mode S altitude 
    //              as determined by the Mode S transponder model. 
    //              When it is reporting actual Mode S altitude, it shall be represented as 
    //              a binary coded altitude referenced to �1000 feet. 
    //              All altitudes greater than 50 175 feet shall be reported with 100-foot resolution. 
    //              A receiving simulation is not required to process an actual Mode S altitude value 
    //              by this standard and may always compute it from the entity truth altitude.
    // Parameter:   KUINT16 Altitude 
    //************************************
	void SetAltitude( KUINT16 Altitude );
	KUINT16 GetAltitude() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSAltitude::SetAltitudeResolution
    //              KDIS::DATA_TYPE::ModeSAltitude::GetAltitudeResolution
    // Description: Indicate the Mode S altitude resolution as being reported in either 100-foot (0) or 25-foot (1) increments.
    // Parameter:   KDIS::DATA_TYPE::ENUMS::AltitudeResolution AltitudeResolution 
    //************************************
	void SetAltitudeResolution( KDIS::DATA_TYPE::ENUMS::AltitudeResolution AltitudeResolution );
	KDIS::DATA_TYPE::ENUMS::AltitudeResolution GetAltitudeResolution() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSAltitude::GetAsString
    // Description: Returns a string representation 
    //************************************
    virtual const KOCTET* GetAsString() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSAltitude::Decode
    // Description: Convert From Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual void Decode( KDataStream & stream ) ;

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSAltitude::Encode
    // Description: Convert To Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual const KDataStream* GetEncodedStream() const;
    virtual void EncodeToStream( KDataStream & stream ) const;

    KBOOL operator == ( const ModeSAltitude & Value ) const;
    KBOOL operator != ( const ModeSAltitude & Value ) const;
};

} // END namespace DATA_TYPE
} // END namespace KDIS
