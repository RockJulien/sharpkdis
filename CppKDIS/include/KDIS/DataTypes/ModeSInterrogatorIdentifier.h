/********************************************************************
    class:      ModeSInterrogatorIdentifier
    created:    21/03/2019
    author:     Nicolas Duboys

    purpose:    The Mode S Interrogator Identifier record shall provide 
                Mode S Interrogator Code (IC) information. When
                used, this record shall be included as Parameter 4 of 
                the Fundamental Operational Data record. This is a 16-bit
                recordMode S interrogator identifier 
                Entry for IFF data record (Layer 5 transponder).
    size:       16 bits / 2 octets
*********************************************************************/

#pragma once

#include "./DataTypeBase.h"

namespace KDIS {
namespace DATA_TYPE {

class KDIS_EXPORT ModeSInterrogatorIdentifier : public DataTypeBase
{
protected:

    union
	{
        struct 
        {
            union
            {
		        struct
		        {
			        KUINT8 m_ui8ICType                               : 1;
			        KUINT8 m_ui8ICCode                               : 7;
		        };
		        KUINT8 m_ui8PrimaryICCode;
            } m_ui8PrimaryICCodeUnion;
            union
            {
		        struct
		        {
			        KUINT8 m_ui8ICType                               : 1;
			        KUINT8 m_ui8ICCode                               : 7;
		        };
		        KUINT8 m_ui8SecondaryICCode;
            } m_ui8SecondaryICCodeUnion;
        };
	    KUINT16 m_ui16ModeSInterrogatorIdentifier;
	} m_ModeSInterrogatorIdentifierUnion;

	mutable KDataStream m_stream;

public:

    static const KUINT16 MODE_S_INTERROGATOR_IDENTIFIER_TYPE_SIZE = 2;

    ModeSInterrogatorIdentifier( KDIS::DATA_TYPE::ENUMS::ModeSInterrogatorIdentifierICType MSIIICT_P, KUINT8 Code_P,
        KDIS::DATA_TYPE::ENUMS::ModeSInterrogatorIdentifierICType MSIIICT_S, KUINT8 Code_S) ;

    ModeSInterrogatorIdentifier();

    ModeSInterrogatorIdentifier( KDataStream & stream ) ;

    virtual ~ModeSInterrogatorIdentifier();

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSInterrogatorIdentifier::SetPrimaryICType
    //              KDIS::DATA_TYPE::ModeSInterrogatorIdentifier::GetPrimaryICType
    // Description: Set primary IC Type.
    // Parameter:   Primary IC Type
    //************************************
    void SetPrimaryICType( KDIS::DATA_TYPE::ENUMS::ModeSInterrogatorIdentifierICType MSIIICT_P );
    KDIS::DATA_TYPE::ENUMS::ModeSInterrogatorIdentifierICType GetPrimaryICType() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSInterrogatorIdentifier::SetPrimaryICCode
    //              KDIS::DATA_TYPE::ModeSInterrogatorIdentifier::GetPrimaryICCode
    // Description: Set primary IC code.
    // Parameter:   Primary IC code
    //************************************
    void SetPrimaryICCode( KUINT8 code );
    KUINT8 GetPrimaryICCode() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSInterrogatorIdentifier::SetSecondaryICType
    //              KDIS::DATA_TYPE::ModeSInterrogatorIdentifier::GetSecondaryICType
    // Description: Set Secondary IC Type.
    // Parameter:   Secondary IC Type
    //************************************
    void SetSecondaryICType( KDIS::DATA_TYPE::ENUMS::ModeSInterrogatorIdentifierICType MSIIICT_S );
    KDIS::DATA_TYPE::ENUMS::ModeSInterrogatorIdentifierICType GetSecondaryICType() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSInterrogatorIdentifier::SetSecondaryICCode
    //              KDIS::DATA_TYPE::ModeSInterrogatorIdentifier::GetSecondaryICCode
    // Description: Set Secondary IC code.
    // Parameter:   Secondary IC code
    //************************************
    void SetSecondaryICCode( KUINT8 code );
    KUINT8 GetSecondaryICCode() const;


    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSInterrogatorIdentifier::GetAsString
    // Description: Returns a string representation.
    //************************************
    virtual const KOCTET* GetAsString() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSInterrogatorIdentifier::Decode
    // Description: Convert From Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual void Decode( KDataStream & stream ) ;

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSInterrogatorIdentifier::Encode
    // Description: Convert To Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual const KDataStream* GetEncodedStream() const;
    virtual void EncodeToStream( KDataStream & stream ) const;

    KBOOL operator == ( const ModeSInterrogatorIdentifier & Value ) const;
    KBOOL operator != ( const ModeSInterrogatorIdentifier & Value ) const;
};

} // END namespace DATA_TYPES
} // END namespace KDIS
