/********************************************************************
    class:      ModeSTransponderBasicData
    DIS:        (7) 1278.1 - 2012
    created:    21/03/2019
    author:     Nicolas Duboys

    purpose:    Included in the Mode S Transponder format for Layer 4 of the IFF PDU.

    Size:       192 bits / 24 octets
*********************************************************************/

#pragma once

#include "./DataTypeBase.h"
#include "./ModeSTransponderStatus.h"
#include "./ModeSLevelsPresent.h"
#include "./DAPSource.h"
#include "./ModeSAltitude.h"

namespace KDIS {
namespace DATA_TYPE {

class KDIS_EXPORT ModeSTransponderBasicData : public DataTypeBase
{
protected:
    static const KUINT8 AIRCRAFT_IDENTIFICATION_SIZE = 8;

    ModeSTransponderStatus m_Status;

    ModeSLevelsPresent m_ModeSLevelsPresent;

    KUINT8 m_ui8AircraftPresentDomain;

	KUINT8 m_ui64AircraftIdentification[AIRCRAFT_IDENTIFICATION_SIZE];

    KUINT32 m_ui32AircraftAddress;

    KUINT8 m_ui8AircraftIDType;

    DAPSource m_ui8DAPSource;

    ModeSAltitude m_ui16ModeSAltitude;

    KUINT8 m_ui8CapabilityReport;

    KUINT8 m_ui8Padding;

    KUINT16 m_ui16Padding;

	mutable KDataStream m_stream;

public:

    static const KUINT16 MODE_S_TRANSPONDER_BASIC_DATA_SIZE = 24;

    ModeSTransponderBasicData();

    ModeSTransponderBasicData( const ModeSTransponderStatus & S, const ModeSLevelsPresent & MSLS, KDIS::DATA_TYPE::ENUMS::AircraftPresentDomain APD,
                               const KOCTET* AircraftIdentification, KUINT8 AircraftIdentificationSize, KUINT32 AircraftAddress, KDIS::DATA_TYPE::ENUMS::AircraftIDType AIDT,
                               const DAPSource & DAPS, const ModeSAltitude & MSA, KDIS::DATA_TYPE::ENUMS::CapabilityReport CR );

    ModeSTransponderBasicData( KDataStream & stream ) ;

    virtual ~ModeSTransponderBasicData();

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSTransponderBasicData::SetStatus
    //              KDIS::DATA_TYPE::ModeSTransponderBasicData::GetStatus
    // Description: The Mode S Message Formats supported by this Mode S transponder.
    // Parameter:   const TransponderStatus & S
    //************************************
    void SetStatus( const ModeSTransponderStatus & S );
    const ModeSTransponderStatus & GetStatusConst() const;
    ModeSTransponderStatus & GetStatus();

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSTransponderBasicData::SetModeSLevelsPresent
    //              KDIS::DATA_TYPE::ModeSTransponderBasicData::GetModeSLevelsPresent
    // Description: Indicate the Mode S levels present. All levels that would be
    //              able to be responded to for a Mode S interrogation shall be set.
    // Parameter:   const TransponderStatus & MSLS
    //************************************
    void SetModeSLevelsPresent( const ModeSLevelsPresent & MSLS );
    const ModeSLevelsPresent & GetModeSLevelsPresentConst() const;
    ModeSLevelsPresent & GetModeSLevelsPresent();

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSTransponderBasicData::SetAircraftPresentDomain
    //              KDIS::DATA_TYPE::ModeSTransponderBasicData::GetAircraftPresentDomain
    // Description: Indicate the present domain of the aircraft as either No
    //              Statement (0), Airborne (1), or On Ground/Surface (2). 
    //              Surface means on a water platform surface such as a ship deck or 
    //              on the water surface directly (e.g., pontoon-equipped aircraft). 
    //              This field is referred to as Flight Status in ICAO Publications.
    // Parameter:   KDIS::DATA_TYPE::ENUMS::AircraftPresentDomain APD
    //************************************
    void SetAircraftPresentDomain( KDIS::DATA_TYPE::ENUMS::AircraftPresentDomain APD );
    KDIS::DATA_TYPE::ENUMS::AircraftPresentDomain GetAircraftPresentDomain() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSTransponderBasicData::SetAircraftIdentification
    //              KDIS::DATA_TYPE::ModeSTransponderBasicData::GetAircraftIdentification
    // Description: Aircraft identification (Call sign).
    // Parameter:   KUINT8 * AircraftIdentification
    //************************************
    void SetAircraftIdentification(const KOCTET* AircraftIdentification, KUINT8 AircraftIdentificationSize);
    const KOCTET* GetAircraftIdentificationConst() const;
	const KOCTET* GetAircraftIdentification();
    
    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSTransponderBasicData::SetAircraftAddress
    //              KDIS::DATA_TYPE::ModeSTransponderBasicData::GetAircraftAddress
    // Description: Aircraft identification (Call sign).
    // Parameter:   KUINT8 * AircraftAddress
    //************************************
    void SetAircraftAddress( KUINT32 AircraftAddress );
    KUINT32 GetAircraftAddress() const;
    
    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSTransponderBasicData::SetAircraftIDType
    //              KDIS::DATA_TYPE::ModeSTransponderBasicData::GetAircraftIDType
    // Description: Aircraft identitication.
    // Parameter:   KDIS::DATA_TYPE::ENUMS::AircraftIDType AIDT
    //************************************
    void SetAircraftIDType( KDIS::DATA_TYPE::ENUMS::AircraftIDType AIDT );
    KDIS::DATA_TYPE::ENUMS::AircraftIDType GetAircraftIDType() const;
    
    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSTransponderBasicData::SetDAPSource
    //              KDIS::DATA_TYPE::ModeSTransponderBasicData::GetDAPSource
    // Description: Indicate the Downlink of Aircraft Parameters (DAP) source.
    // Parameter:   const DAPSource & DAPS
    //************************************
    void SetDAPSource( const DAPSource & DAPS );
    const DAPSource & GetDAPSourceConst() const;
    DAPSource & GetDAPSource();
    
    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSTransponderBasicData::SetModeSAltitude
    //              KDIS::DATA_TYPE::ModeSTransponderBasicData::GetModeSAltitude
    // Description: Mode S Altitude.
    // Parameter:   const ModeSAltitude & MSA
    //************************************
    void SetModeSAltitude( const ModeSAltitude & MSA );
    const ModeSAltitude & GetModeSAltitudeConst() const;
    ModeSAltitude & GetModeSAltitude();
    
    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSTransponderBasicData::SetCapabilityReport
    //              KDIS::DATA_TYPE::ModeSTransponderBasicData::GetCapabilityReport
    // Description: Capability report.
    //              The numeric values are from Annex 10 to ICAO Vol. IV, 
    //              para 3.1.2.5.2.2.1, except for No Statement (see Clause 2).
    //              If no specific capability is indicated, this field shall be 
    //              set to No Statement (255)
    // Parameter:   KDIS::DATA_TYPE::ENUMS::CapabilityReport CR
    //************************************
    void SetCapabilityReport( KDIS::DATA_TYPE::ENUMS::CapabilityReport CR );
    KDIS::DATA_TYPE::ENUMS::CapabilityReport GetCapabilityReport() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSTransponderBasicData::GetAsString
    // Description: Returns a string representation
    //************************************
    virtual const KOCTET* GetAsString() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSTransponderBasicData::Decode
    // Description: Convert From Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual void Decode( KDataStream & stream ) ;

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSTransponderBasicData::Encode
    // Description: Convert To Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual const KDataStream* GetEncodedStream() const;
    virtual void EncodeToStream( KDataStream & stream ) const;

    KBOOL operator == ( const ModeSTransponderBasicData & Value ) const;
    KBOOL operator != ( const ModeSTransponderBasicData & Value ) const;
};

} // END namespace DATA_TYPE
} // END namespace KDIS
