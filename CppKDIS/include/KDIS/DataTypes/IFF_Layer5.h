/********************************************************************
    class:      IFF_Layer5
    DIS:        (7) 1278.1 - 2012
    created:    21/03/2019
    author:     Nicolas Duboys

    purpose:    Base class for Layer 5 interrogator and transponder functional data.

    Size:       128 bits / 16 octets - min size
*********************************************************************/

#pragma once

#include "./LayerHeader.h"
#include "./SimulationIdentifier.h"
#include "./InformationLayers.h"
#include "./StandardVariable.h"

namespace KDIS {
namespace DATA_TYPE {

class KDIS_EXPORT IFF_Layer5 : public LayerHeader
{
protected:

	SimulationIdentifier m_RptSim;

	KUINT16 m_ui16Padding_1;

    InformationLayers m_ui8ApplicableLayers;

    KUINT8 m_ui8DataCategory;

	KUINT16 m_ui16Padding_2;
		
	KUINT16 m_ui16NumIffRecs;

	std::vector<KDIS::DATA_TYPE::StdVarPtr> m_vStdVarRecs;

public:

    static const KUINT16 IFF_LAYER5_SIZE = 16; // Min size

    IFF_Layer5();

    IFF_Layer5( KDataStream & stream ) ;

	IFF_Layer5( const LayerHeader & H, KDataStream & stream );

    virtual ~IFF_Layer5();

    //************************************
    // FullName:    KDIS::DATA_TYPE::IFF_Layer5::SetReportingSimulation
    //              KDIS::DATA_TYPE::IFF_Layer5::GetReportingSimulation
    // Description: The simulation reporting this IFF PDU.
    // Parameter:   const SimulationIdentifier & RS
    //************************************
    void SetReportingSimulation( const SimulationIdentifier & RS );
    const SimulationIdentifier & GetReportingSimulation() const;
    SimulationIdentifier & GetReportingSimulation();

    //************************************
    // FullName:    KDIS::DATA_TYPE::IFF_Layer5::SetApplicableLayers
    //              KDIS::DATA_TYPE::IFF_Layer5::GetApplicableLayers
    // Description: Indicates to which layer(s) the IFF data records present in Layer 5 are
    //              applicable. 
    //              In this case, each Layer 1 through 7 field shall be set 
    //              to either Not Applicable (false) or Applicable (true)
    // Parameter:   const InformationLayers & IL
    //************************************
    void SetApplicableLayers( const InformationLayers & IL );
    const InformationLayers & GetApplicableLayers() const;
    InformationLayers & GetApplicableLayers();
			
    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSTransponderBasicData::SetDataCategory
    //              KDIS::DATA_TYPE::ModeSTransponderBasicData::GetDataCategory
    // Description: Indicates the category of data represented by the included IFF data
    //              records. This is to support data filtering at the receive simulation.
    // Parameter:   KDIS::DATA_TYPE::ENUMS::DataCategory DC
    //************************************
    void SetDataCategory( KDIS::DATA_TYPE::ENUMS::DataCategory DC );
    KDIS::DATA_TYPE::ENUMS::DataCategory GetDataCategory() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::IFF_Layer5::GetNumberDataRecords
    // Description: The number of IFF Data records in this PDU.
    //************************************
	KUINT16 GetNumberDataRecords() const;

	//************************************
    // FullName:    KDIS::PDU::IFF_Layer5::AddDataRecord
    //              KDIS::PDU::IFF_Layer5::SetDataRecords
    //              KDIS::PDU::IFF_Layer5::GetDataRecords
    //              KDIS::PDU::IFF_Layer5::ClearDataRecords
    // Description: IFF Data records are used when variable records are required to be included in the layer.
	//              They are identical to Standard Variable Specification record format except that alignment is to
	//              a 32-bit boundary for each IFF Data record instead of to a 64-bit boundary. 
	//              This means the records can not contain 64 bit floats or 64 bit integers.
	//              See B.2.1.1 of IEEE 1278.1-2012 for a full list of supported records (Note: I have not implmented them all yet but do plan to).
    // Parameter:   StdVarPtr DR, const vector<StdVarPtr> & DRS
    //************************************
    void AddDataRecord( StdVarPtr DR );
	void SetDataRecords( const std::vector<StdVarPtr> & DRS );
	const std::vector<StdVarPtr> & GetDataRecords() const;
    void ClearDataRecords();
    
    //************************************
    // FullName:    KDIS::DATA_TYPE::IFF_Layer5::GetAsString
    // Description: Returns a string representation
    //************************************
    virtual const KOCTET* GetAsString() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::IFF_Layer5::Decode
    // Description: Convert From Network Data.
    // Parameter:   KDataStream & stream
    // Parameter:   bool ignoreHeader = false - Decode the layer header from the stream? 
    //************************************
	virtual void Decode( KDataStream & stream ) ;
    virtual void Decode( KDataStream & stream, bool ignoreHeader = false ) ;

    //************************************
    // FullName:    KDIS::DATA_TYPE::IFF_Layer5::Encode
    // Description: Convert To Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual void EncodeToStream( KDataStream & stream ) const;

    KBOOL operator == ( const IFF_Layer5 & Value ) const;
    KBOOL operator != ( const IFF_Layer5 & Value ) const;
};

} // END namespace DATA_TYPE
} // END namespace KDIS
