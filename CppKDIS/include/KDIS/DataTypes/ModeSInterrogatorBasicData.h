/********************************************************************
    class:      ModeSInterrogatorBasicData
    DIS:        (7) 1278.1 - 2012
    created:    21/03/2019
    author:     Nicolas Duboys

    purpose:    Included in the Mode S Transponder format for Layer 4 of the IFF PDU.

    Size:       192 bits / 24 octets
*********************************************************************/

#pragma once

#include "./DataTypeBase.h"
#include "./ModeSInterrogatorStatus.h"
#include "./ModeSLevelsPresent.h"

namespace KDIS {
namespace DATA_TYPE {

class KDIS_EXPORT ModeSInterrogatorBasicData : public DataTypeBase
{
protected:
    static const KUINT8 AIRCRAFT_IDENTIFICATION_SIZE = 8;

    ModeSInterrogatorStatus m_Status;

    KUINT8 m_ui8Padding_1;

    ModeSLevelsPresent m_ModeSLevelsPresent;

    KUINT8 m_ui8Padding_2;

    KUINT32 m_ui32Padding_1;

    KUINT32 m_ui32Padding_2;

    KUINT32 m_ui32Padding_3;

    KUINT32 m_ui32Padding_4;

    KUINT32 m_ui32Padding_5;

	mutable KDataStream m_stream;

public:

    static const KUINT16 MODE_S_INTERROGATOR_BASIC_DATA_SIZE = 24;

    ModeSInterrogatorBasicData();

    ModeSInterrogatorBasicData( const ModeSInterrogatorStatus & S, const ModeSLevelsPresent & MSLS );

    ModeSInterrogatorBasicData( KDataStream & stream ) ;

    virtual ~ModeSInterrogatorBasicData();

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSInterrogatorBasicData::SetStatus
    //              KDIS::DATA_TYPE::ModeSInterrogatorBasicData::GetStatus
    // Description: The Mode S Message Formats supported by this Mode S interrogator.
    // Parameter:   const InterrogatorStatus & S
    //************************************
    void SetStatus( const ModeSInterrogatorStatus & S );
    const ModeSInterrogatorStatus & GetStatus() const;
    ModeSInterrogatorStatus & GetStatus();

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSInterrogatorBasicData::SetModeSLevelsPresent
    //              KDIS::DATA_TYPE::ModeSInterrogatorBasicData::GetModeSLevelsPresent
    // Description: Indicate the Mode S levels present. All levels that would be
    //              able to be responded to for a Mode S interrogation shall be set.
    // Parameter:   const InterrogatorStatus & MSLS
    //************************************
    void SetModeSLevelsPresent( const ModeSLevelsPresent & MSLS );
    const ModeSLevelsPresent & GetModeSLevelsPresent() const;
    ModeSLevelsPresent & GetModeSLevelsPresent();

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSInterrogatorBasicData::GetAsString
    // Description: Returns a string representation
    //************************************
    virtual const KOCTET* GetAsString() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSInterrogatorBasicData::Decode
    // Description: Convert From Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual void Decode( KDataStream & stream ) ;

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSInterrogatorBasicData::Encode
    // Description: Convert To Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual const KDataStream* GetEncodedStream() const;
    virtual void EncodeToStream( KDataStream & stream ) const;

    KBOOL operator == ( const ModeSInterrogatorBasicData & Value ) const;
    KBOOL operator != ( const ModeSInterrogatorBasicData & Value ) const;
};

} // END namespace DATA_TYPE
} // END namespace KDIS
