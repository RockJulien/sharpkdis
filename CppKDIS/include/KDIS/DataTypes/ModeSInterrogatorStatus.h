/********************************************************************
    class:      ModeSInterrogatorStatus
    DIS:        (7) 1278.1 - 2012
    created:    21/03/2019
    author:     Nicolas Duboys

    purpose:    

    Size:       8 bits / 1 octet
*********************************************************************/

#pragma once

#include "./DataTypeBase.h"

namespace KDIS {
namespace DATA_TYPE {

class KDIS_EXPORT ModeSInterrogatorStatus : public DataTypeBase
{
protected:

	union
	{
		struct
		{
			KUINT8 m_ui8OnOff                           : 1;
			KUINT8 m_ui8TransmitState                   : 3;
			KUINT8 m_ui8Dmg                             : 1;
			KUINT8 m_ui8MalFnc                          : 1;
			KUINT8 m_ui8Padding                         : 2;
		};
		KUINT8 m_ui8Status;

	} m_StatusUnion;

	mutable KDataStream m_stream;
		
public:

    static const KUINT16 MODE_S_INTERROGATOR_STATUS_SIZE = 1; 

    ModeSInterrogatorStatus();

    ModeSInterrogatorStatus( KDataStream & stream ) ;

	ModeSInterrogatorStatus( KBOOL Status, KDIS::DATA_TYPE::ENUMS::TransmitStateModeS TS, KBOOL Dmg, KBOOL Malfnc );
	
    virtual ~ModeSInterrogatorStatus();
    
	//************************************
    // FullName:    KDIS::DATA_TYPE::ModeSInterrogatorStatus::SetStatus
    //              KDIS::DATA_TYPE::ModeSInterrogatorStatus::GetStatus
    // Description: Indicates whether the Mode 5 Interrogator is On (true or Off (false).
    // Parameter:   KBOOL S
    //************************************
	void SetStatus( KBOOL S );
	KBOOL GetStatus() const;

	//************************************
    // FullName:    KDIS::DATA_TYPE::ModeSInterrogatorStatus::SetTransmitState
    //              KDIS::DATA_TYPE::ModeSInterrogatorStatus::GetTransmitState
    // Description: Indicates the state of the Mode S interrogator.
    // Parameter:   KDIS::DATA_TYPE::ENUMS::TransmitStateModeS TS
    //************************************
	void SetTransmitState( KDIS::DATA_TYPE::ENUMS::TransmitStateModeS TS );
	KDIS::DATA_TYPE::ENUMS::TransmitStateModeS GetTransmitState() const;

	//************************************
    // FullName:    KDIS::DATA_TYPE::ModeSInterrogatorStatus::SetDamaged
    //              KDIS::DATA_TYPE::ModeSInterrogatorStatus::IsDamaged
    // Description: Indicates whether there is damage to the Mode 5 Interrogator.
    // Parameter:   KBOOL S
    //************************************
	void SetDamaged( KBOOL D );
	KBOOL IsDamaged() const;
		
	//************************************
    // FullName:    KDIS::DATA_TYPE::ModeSInterrogatorStatus::SetMalfunctioning
    //              KDIS::DATA_TYPE::ModeSInterrogatorStatus::IsMalfunctioning
    // Description: Indicates whether there is damage to the Mode 5 Interrogator.
    // Parameter:   KBOOL M
    //************************************
	void SetMalfunctioning( KBOOL M );
	KBOOL IsMalfunctioning() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSInterrogatorStatus::GetAsString
    // Description: Returns a string representation 
    //************************************
    virtual const KOCTET* GetAsString() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSInterrogatorStatus::Decode
    // Description: Convert From Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual void Decode( KDataStream & stream ) ;

    //************************************
    // FullName:    KDIS::DATA_TYPE::ModeSInterrogatorStatus::Encode
    // Description: Convert To Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual const KDataStream* GetEncodedStream() const;
    virtual void EncodeToStream( KDataStream & stream ) const;

    KBOOL operator == ( const ModeSInterrogatorStatus & Value ) const;
    KBOOL operator != ( const ModeSInterrogatorStatus & Value ) const;
};

} // END namespace DATA_TYPE
} // END namespace KDIS
