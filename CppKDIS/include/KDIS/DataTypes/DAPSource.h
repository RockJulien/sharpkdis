/********************************************************************
    class:      DAPSource
    DIS:        (7) 1278.1 - 2012
    created:    21/03/2019
    author:     Nicolas Duboys

    purpose:    

    Size:       8 bits / 1 octet
*********************************************************************/

#pragma once

#include "./DataTypeBase.h"

namespace KDIS {
namespace DATA_TYPE {

class KDIS_EXPORT DAPSource : public DataTypeBase
{
protected:

	union
	{
		struct
		{
			KUINT8 m_ui8IAS                        : 1;
			KUINT8 m_ui8MachNumber                 : 1;
			KUINT8 m_ui8GroundSpeed                : 1;
			KUINT8 m_ui8MagneticHeading            : 1;
			KUINT8 m_ui8TrackAngleRate             : 1;
			KUINT8 m_ui8TrueTrackAngle             : 1;
			KUINT8 m_ui8TrueAirSpeed               : 1;
			KUINT8 m_ui8VerticalRate               : 1;
		};
		KUINT8 m_ui8DAPSource;

	} m_DAPSourceUnion;

	mutable KDataStream m_stream;
		
public:

    static const KUINT16 DAP_SOURCE_SIZE = 1; 

    DAPSource();

    DAPSource( KDataStream & stream ) ;

	DAPSource( KBOOL IAS, KBOOL MachNumber, KBOOL GroundSpeed, KBOOL MagneticHeading, KBOOL TrackAngleRate, KBOOL TrueTrackAngle, KBOOL TrueAirSpeed, KBOOL VerticalRate );
	
    virtual ~DAPSource();

    //************************************
    // FullName:    KDIS::DATA_TYPE::DAPSource::SetIAS
    //              KDIS::DATA_TYPE::DAPSource::GetIAS
    // Description: Compute Locally (false) or IFF Data Record Available (true).
    // Parameter:   KBOOL IAS 
    //************************************
	void SetIAS( KBOOL IAS );
	KBOOL GetIAS() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::DAPSource::SetMachNumber
    //              KDIS::DATA_TYPE::DAPSource::GetMachNumber
    // Description: Compute Locally (false) or IFF Data Record Available (true).
    // Parameter:   KBOOL MachNumber 
    //************************************
	void SetMachNumber( KBOOL MachNumber );
	KBOOL GetMachNumber() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::DAPSource::SetGroundSpeed
    //              KDIS::DATA_TYPE::DAPSource::GetGroundSpeed
    // Description: Compute Locally (false) or IFF Data Record Available (true).
    // Parameter:   KBOOL GroundSpeed 
    //************************************
	void SetGroundSpeed( KBOOL GroundSpeed );
	KBOOL GetGroundSpeed() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::DAPSource::SetMagneticHeading
    //              KDIS::DATA_TYPE::DAPSource::GetMagneticHeading
    // Description: Compute Locally (false) or IFF Data Record Available (true).
    // Parameter:   KBOOL MagneticHeading 
    //************************************
	void SetMagneticHeading( KBOOL MagneticHeading );
	KBOOL GetMagneticHeading() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::DAPSource::SetTrackAngleRate
    //              KDIS::DATA_TYPE::DAPSource::GetTrackAngleRate
    // Description: Compute Locally (false) or IFF Data Record Available (true).
    // Parameter:   KBOOL TrackAngleRate 
    //************************************
	void SetTrackAngleRate( KBOOL TrackAngleRate );
	KBOOL GetTrackAngleRate() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::DAPSource::SetTrueTrackAngle
    //              KDIS::DATA_TYPE::DAPSource::GetTrueTrackAngle
    // Description: Compute Locally (false) or IFF Data Record Available (true).
    // Parameter:   KBOOL TrueTrackAngle 
    //************************************
	void SetTrueTrackAngle( KBOOL TrueTrackAngle );
	KBOOL GetTrueTrackAngle() const;
  //************************************
    // FullName:    KDIS::DATA_TYPE::DAPSource::SetTrueAirSpeed
    //              KDIS::DATA_TYPE::DAPSource::GetTrueAirSpeed
    // Description: Compute Locally (false) or IFF Data Record Available (true).
    // Parameter:   KBOOL TrueAirSpeed 
    //************************************
	void SetTrueAirSpeed( KBOOL TrueAirSpeed );
	KBOOL GetTrueAirSpeed() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::DAPSource::SetVerticalRate
    //              KDIS::DATA_TYPE::DAPSource::GetVerticalRate
    // Description: Compute Locally (false) or IFF Data Record Available (true).
    // Parameter:   KBOOL VerticalRate 
    //************************************
	void SetVerticalRate( KBOOL VerticalRate );
	KBOOL GetVerticalRate() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::DAPSource::GetAsString
    // Description: Returns a string representation 
    //************************************
    virtual const KOCTET* GetAsString() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::DAPSource::Decode
    // Description: Convert From Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual void Decode( KDataStream & stream ) ;

    //************************************
    // FullName:    KDIS::DATA_TYPE::DAPSource::Encode
    // Description: Convert To Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual const KDataStream* GetEncodedStream() const;
    virtual void EncodeToStream( KDataStream & stream ) const;

    KBOOL operator == ( const DAPSource & Value ) const;
    KBOOL operator != ( const DAPSource & Value ) const;
};

} // END namespace DATA_TYPE
} // END namespace KDIS
