/********************************************************************
    class:      InteractiveBasicModeS
    created:    21/03/2019
    author:     Nicolas Duboys

    purpose:    Interactive modeS reply entry for IFF data record (Layer 5 transponder).
    size:       48 bits / 6 octets
*********************************************************************/

#pragma once

#include "./StandardVariable.h"
#include "./ModeSLevelsPresent.h"
#include "./ModeSInterrogatorIdentifier.h"

namespace KDIS {
namespace DATA_TYPE {

class KDIS_EXPORT InteractiveBasicModeS : public StandardVariable
{
protected:

    ModeSLevelsPresent          m_ModeSLevelsPresent;
    KUINT8                      m_ui16TransmitState;
    ModeSInterrogatorIdentifier m_ModeSInterrogatorIdentifier;
    KUINT16                     m_ui16Padding;

public:

    static const KUINT16 INTERACTIVE_BASIC_MODES_REPLY_TYPE_SIZE = 12;

    InteractiveBasicModeS( const ModeSLevelsPresent & MSLP, KDIS::DATA_TYPE::ENUMS::TransmitStateModeS TS, const ModeSInterrogatorIdentifier & MSII ) ;

    InteractiveBasicModeS();

    InteractiveBasicModeS( KDataStream & stream ) ;

    virtual ~InteractiveBasicModeS();

    //************************************
    // FullName:    KDIS::DATA_TYPE::InteractiveBasicModeS::SetModeSLevelsPresent
    //              KDIS::DATA_TYPE::InteractiveBasicModeS::GetModeSLevelsPresent
    // Description: Indicate the Mode S levels present. All levels that would be
    //              able to be responded to for a Mode S interrogation shall be set.
    // Parameter:   const InterrogatorStatus & MSLS
    //************************************
    void SetModeSLevelsPresent( const ModeSLevelsPresent & MSLS );
    const ModeSLevelsPresent & GetModeSLevelsPresent() const;
    ModeSLevelsPresent & GetModeSLevelsPresent();

	//************************************
    // FullName:    KDIS::DATA_TYPE::InteractiveBasicModeS::SetTransmitState
    //              KDIS::DATA_TYPE::InteractiveBasicModeS::GetTransmitState
    // Description: Indicates the state of the Mode S interrogator.
    // Parameter:   KDIS::DATA_TYPE::ENUMS::TransmitStateModeS TS
    //************************************
	void SetTransmitState( KDIS::DATA_TYPE::ENUMS::TransmitStateModeS TS );
	KDIS::DATA_TYPE::ENUMS::TransmitStateModeS GetTransmitState() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::InteractiveBasicModeS::SetModeSInterrogatorIdentifier
    //              KDIS::DATA_TYPE::InteractiveBasicModeS::GetModeSInterrogatorIdentifier
    // Description: This field shall specify the Mode S Interrogator Identifier that would
    //              be contained in this specific Mode S interactive interrogation. 
    //              It shall use the Mode S Interrogator Identifier record. 
    //              The values of this record may or may not be the same as the present
    //              values for this record as contained in Parameter 4 of the Fundamental 
    //              Operational Data record of Layer 4 for a Mode S interrogator. 
    //              The Mode S Interrogator Identifier record as used for this field
    //              shall contain, as a minimum, a valid Primary IC Code (IC Type and 
    //              IC Code fields contain valid data). 
    //              The Secondary IC Code field is optional:
    //                  1) Mode S Interrogator. Indicates the interrogator identifier 
    //                     for this Mode S interrogator.
    //                  2) Mode S Transponder. Not applicable and set to zero.
    // Parameter:   const ModeSInterrogatorIdentifier & MSII
    //************************************
    void SetModeSInterrogatorIdentifier( const ModeSInterrogatorIdentifier & MSII );
    const ModeSInterrogatorIdentifier & GetModeSInterrogatorIdentifier() const;
    ModeSInterrogatorIdentifier & GetModeSInterrogatorIdentifier();

    //************************************
    // FullName:    KDIS::DATA_TYPE::InteractiveBasicModeS::GetAsString
    // Description: Returns a string representation.
    //************************************
    virtual const KOCTET* GetAsString() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::InteractiveBasicModeS::Decode
    // Description: Convert From Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual void Decode( KDataStream & stream ) ;

    //************************************
    // FullName:    KDIS::DATA_TYPE::InteractiveBasicModeS::Encode
    // Description: Convert To Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual void EncodeToStream( KDataStream & stream ) const;

    KBOOL operator == ( const InteractiveBasicModeS & Value ) const;
    KBOOL operator != ( const InteractiveBasicModeS & Value ) const;
};

} // END namespace DATA_TYPES
} // END namespace KDIS
