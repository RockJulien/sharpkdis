/********************************************************************
    class:      InteractiveMode5Reply
    created:    21/03/2019
    author:     Nicolas Duboys

    purpose:    Interactive mode5 reply entry for IFF data record (Layer 5 transponder).
    size:       48 bits / 6 octets
*********************************************************************/

#pragma once

#include "./StandardVariable.h"

namespace KDIS {
namespace DATA_TYPE {

class KDIS_EXPORT InteractiveMode5Reply : public StandardVariable
{
protected:

    KUINT8  m_ui8InteractiveMode5Reply;

    KUINT8  m_ui8Padding;

    KUINT32 m_ui32Padding;

public:

    static const KUINT16 INTERACTIVE_MODE5_REPLY_TYPE_SIZE = 12;

    InteractiveMode5Reply( KDIS::DATA_TYPE::ENUMS::Mode5Reply M5R ) ;

    InteractiveMode5Reply();

    InteractiveMode5Reply( KDataStream & stream ) ;

    virtual ~InteractiveMode5Reply();

    //************************************
    // FullName:    KDIS::DATA_TYPE::InteractiveMode5Reply::SetMode5Reply
    //              KDIS::DATA_TYPE::InteractiveMode5Reply::GetMode5Reply
    // Description: Set the reply.
    // Parameter:   Alternate Mode5 Reply
    //************************************
    void SetMode5Replyv( KDIS::DATA_TYPE::ENUMS::Mode5Reply M5R );
    KDIS::DATA_TYPE::ENUMS::Mode5Reply GetMode5Reply() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::InteractiveMode5Reply::GetAsString
    // Description: Returns a string representation.
    //************************************
    virtual const KOCTET* GetAsString() const;

    //************************************
    // FullName:    KDIS::DATA_TYPE::InteractiveMode5Reply::Decode
    // Description: Convert From Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual void Decode( KDataStream & stream ) ;

    //************************************
    // FullName:    KDIS::DATA_TYPE::InteractiveMode5Reply::Encode
    // Description: Convert To Network Data.
    // Parameter:   KDataStream & stream
    //************************************
    virtual void EncodeToStream( KDataStream & stream ) const;

    KBOOL operator == ( const InteractiveMode5Reply & Value ) const;
    KBOOL operator != ( const InteractiveMode5Reply & Value ) const;
};

} // END namespace DATA_TYPES
} // END namespace KDIS
